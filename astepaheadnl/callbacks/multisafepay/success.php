<?php
if(!is_dir(__DIR__ . '/logs')) {
    mkdir(__DIR__ . '/logs', 0744);
}
file_put_contents('logs/success.txt',var_export($_GET,true).PHP_EOL,FILE_APPEND);
/**
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the MultiSafepay plugin
 * to newer versions in the future. If you wish to customize the plugin for your
 * needs please document your changes and make backups before you update.
 *
 * @category    MultiSafepay
 * @package     Connect
 * @author      TechSupport <techsupport@multisafepay.com>
 * @copyright   Copyright (c) 2017 MultiSafepay, Inc. (http://www.multisafepay.com)
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
chdir("../../../");
$rootPath = getcwd();
require("includes/application_top.php");
define('DIR_WS_LANGUAGES', 'includes/languages/');
define('FILENAME_CHECKOUT_PROCESS', 'checkout.php');
define('FILENAME_CHECKOUT_SUCCESS', 'checkout_success.php');
include(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_PROCESS);

if ($multisafepay_order_id && $_GET['customer_id'] && $_GET['hash']) {
    if (md5($multisafepay_order_id . $_GET['customer_id']) == $_GET['hash']) {
        $customer_id = $_GET['customer_id'];
        $check_customer_query = tep_db_query("select customers_id, customers_firstname, customers_password, customers_email_address, customers_default_address_id from " . TABLE_CUSTOMERS . " where customers_id = '" . (int) $customer_id . "'");
        $check_customer = tep_db_fetch_array($check_customer_query);
        $check_country_query = tep_db_query("select entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int) $check_customer['customers_id'] . "' and address_book_id = '" . (int) $check_customer['customers_default_address_id'] . "'");
        $check_country = tep_db_fetch_array($check_country_query);
        $customer_id = $check_customer['customers_id'];
        $customer_default_address_id = $check_customer['customers_default_address_id'];
        $customer_first_name = $check_customer['customers_firstname'];
        $customer_country_id = $check_country['entry_country_id'];
        $customer_zone_id = $check_country['entry_zone_id'];
        tep_session_register('customer_default_address_id');
        tep_session_register('customer_first_name');
        tep_session_register('customer_country_id');
        tep_session_register('customer_zone_id');
    }
}

if(isset($_GET['transactionid']) && !empty($_GET['send']) && $_GET['send'] === 'true') {

    require(DIR_WS_CLASSES . "payment.php");
    $payment_modules = new payment("multisafepay");

    if(!class_exists('multisafepay')) {
        require_once DIR_WS_MODULES . '/payment/multisafepay.php';
    }

//    $payment_module = $GLOBALS[$payment_modules->selected_module];
    $payment_module = new multisafepay();

    require(DIR_WS_CLASSES . "order.php");
    $order = new order($_GET['transactionid']);

    $order_status_query = tep_db_query("SELECT orders_status_id FROM " . TABLE_ORDERS_STATUS . " WHERE orders_status_name = '" . $order->info['orders_status'] . "' AND language_id = '" . $languages_id . "'");
    $order_status = tep_db_fetch_array($order_status_query);
    $order->info['order_status'] = $order_status['orders_status_id'];

    require(DIR_WS_CLASSES . "order_total.php");
    $order_total_modules = new order_total();

    $customer_id = $order->customer['id'];
    $order_totals = $order->totals;

    $payment_module->order_id = $_GET['transactionid'];
    $transdata = $payment_module->check_transaction();

    // ASDOJoijd3i43hr
    if ($transdata->fastcheckout == 'NO') {
        $status = $payment_module->checkout_notify();
    } else {
        $payment_modules = new payment("multisafepay_fastcheckout");
        $payment_module = $GLOBALS[$payment_modules->selected_module];
        $status = $payment_module->checkout_notify();
    }

    $customer_id = $order->customer['id'];
    $order_totals = $order->totals;

    include_once DIR_WS_CLASSES . 'onepage_checkout.php';
    $onePageCheckOut = new osC_onePageCheckout();
    $onePageCheckOut->loadSessionVars();
    $onePageCheckOut->createEmails((int)$_GET['transactionid']);

    $_SESSION['allowCheckoutSuccessPageId'] = (int)$_GET['transactionid'];
}

$cart->reset(true);

tep_session_unregister('sendto');
tep_session_unregister('billto');
tep_session_unregister('shipping');
tep_session_unregister('payment');
tep_session_unregister('comments');

//$redirect = 'includes/callbacks/multisafepay/notify_checkout.php?transactionid=' . $_GET['transactionid'] . '&send_email=true';
$redirect = FILENAME_CHECKOUT_SUCCESS;

if ($customer_id) {
    tep_session_register('customer_id');
    tep_redirect(addHostnameToLink(tep_href_link($redirect, 'order_id=' . (int)$_GET['transactionid'])));
} else {
    tep_redirect(addHostnameToLink(tep_href_link($redirect, 'order_id=' . (int)$_GET['transactionid'])));
}
?>