<?php
/*
  $Id: checkout_process.php,v 1.2 2003/09/24 15:34:25 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
  if( isset($_GET['include']) && $_GET['include'] == 'at' ) {
    
    // -------------------------------------------------------------------------------------------//
    // !!!!!!!----redirects from paypal, liqpay works on application_top.php now!!!!!!!!!--------//
                  search for: if (tep_session_is_registered('cart_payment_id') and isset($_SESSION['complete_status'])) {
    // -------------------------------------------------------------------------------------------//
    
    require('includes/application_top.php');
  
    // load all enabled payment modules
    require(DIR_WS_CLASSES . 'payment.php');
    $payment_modules = new payment;
    
    require(DIR_WS_CLASSES . 'order_total.php');
    $order_total_modules = new order_total;    
  
    $selected_module = $_SESSION['onepage']['info']['payment_method'];
    $payment_modules->selected_module = $selected_module;
    require('includes/classes/onepage_checkout.php');
    
  } else {   */
  
  
//    require('includes/configure.php'); // include server parameters
//    require(DIR_WS_INCLUDES . 'spider_configure.php'); // Spiderkiller 
    
  //  debug($_SESSION);
    if (!is_object($lng)) {
      include(DIR_WS_CLASSES . 'language.php');
      $lng = new language;
    }
    
    
    if (isset($_GET['language']) && tep_not_null($_GET['language'])) {
      $lng->set_language($_GET['language']);
    } else {
      $lng->get_browser_language();
    }
    
    $language = $lng->language['directory'];
//  }


  include(DIR_WS_LANGUAGES . $language . '/checkout_process.php');
  

  $onePageCheckout = new osC_onePageCheckout();
  $onePageCheckout->loadSessionVars(); 

//  $payment_modules->before_process();

  // Order creating in DB:
  //if(!isset($order_totals)) $order_totals = $order_total_modules->process();
  $insert_id = $onePageCheckout->createOrder($order->info['order_status']);
  
  // sending emails, session deleting, redirect to checkout success:
  $onePageCheckout->createEmails($insert_id);
  $_SESSION['allowCheckoutSuccessPageId'] = $insert_id;
  
  // clear sessions:
  clear_order_sessions($insert_id);

  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>