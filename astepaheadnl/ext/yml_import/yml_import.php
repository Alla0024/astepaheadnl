<?php

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$exportLog = '';
$allowed_image_types = ['image/jpeg','image/gif','image/png','image/webp'];
$allowed_table_types = ['text/xml','application/xml'];

if (tep_not_null($_FILES) && in_array($_FILES['excelfile']['type'],$allowed_table_types)){
    $file_path = $_FILES['excelfile']['tmp_name'];
//    $current_currency = $_POST['currency']?:'USD';
    $upload_images = isset($_POST['uploadImages']);
    $truncate_table = isset($_POST['truncateTable']);
    include_once ('../ext/yml_import/yml.php');
}

include_once('html-open.php');
include_once('header.php');
?>
<!--    <div class="container">-->
<!--        <div class="col-md-12">-->
<!--            --><?php //include __DIR__ . "/../../admin/includes/material/blocks/tabs/import_export.php" ?>
<!--        </div>-->
<!--    </div>-->
    <div class="container">
        <h1 class="h1">Импорт из YML</h1>

        <?php if (tep_not_null($exportLog)){ ?>
        <?= $exportLog ?>
        <?php } else { ?>
        <form enctype="multipart/form-data" method="post">

            <div class="form-group"><label for="excelfile">Выберите .xml файл:</label>
                <input type="file" required class="form-control" name="excelfile" id="excelfile" accept=".xml">
            </div>
            <div class="form-group">
                <input type="checkbox" name="uploadImages" id="uploadImages">
                <label for="uploadImages">Импортировать изображения? (займёт много времени)</label>
            </div>
            <div class="form-group">
                <input type="checkbox" name="truncateTable" id="truncateTable">
                <label for="truncateTable">Очистить текущую базу данных от товаров/категорий/производителей/аттрибутов</label>
            </div>
            <button type="submit" class="btn btn-info btn-rounded">Загрузить</button>
        </form>
    <?php } ?>
    </div>
<?
require('footer.php');
require('html-close.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');

