	$(function() {
    
    var color_id = $('input[name=color_id]').val(); 
		var color_attributes = $('.color_attributes input');
		color_attributes.on('change', function(event) {
			event.preventDefault();
      
      // change select and refresh it and recalculate summ after changing image of color 
      $('#select_id_'+color_id).val($(this).val()); 
      $('#select_id_'+color_id).change(); 
 //     $('#select_id_'+color_id).multiselect('refresh');
      
			$.get('./includes/modules/additional_images2.php', {
				method: 'ajax',
				pid: jQuery("input[name=products_id]").val(),
				colid: color_id,
				col: $(this).val(),
				color_images: jQuery("#color_images").val()
			}, function(obj) {
				jQuery('.additional_images2').html(obj).promise().done(function() {
          var sync1 = $("#sync1");
					var isSyncedCarousels = $('#sync1_1').length ? true : false;
          var isNewSyncedCarousels = $('#product_big_slider').length ? true : false;
          
          $("#sync1 .lazyload, .additional_images2 .lazyload, #sync1_1 .lazyload, #product_big_slider .lazyload").lazyload();
          
          if (isSyncedCarousels && typeof syncedCarousel === 'function') {
              carouselClass = new syncedCarousel('#sync1_1','#sync2');
          } else if (isNewSyncedCarousels && typeof syncedCarousel === 'function'){
              carouselClass = new syncedCarousel('#product_big_slider','#product_small_slider');
          } else{
            $("#sync1").owlCarousel({
                items: 1,
                loop: true,
                dots: true,
                slideSpeed: 200,
                nav: true,
                navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
                dotsContainer: '#sync2',
                responsiveRefreshRate: 200
            });
          }
				});
			});
		});

	});
