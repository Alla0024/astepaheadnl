<div class="subscribe_news">
    <p><?php echo MAIN_NEWS_SUBSCRIBE; ?></p>

    <form class="form_subscribe_news" action="subscripbe.php" method="POST">
        <input type="hidden" name="podpiska" value="yes">
        <input type="email" class="form-control form_subscribe_input" required autocomplete="off"
               placeholder="<?php echo MAIN_NEWS_EMAIL; ?>" name="email_address">
        <button type="submit" class="btn btn-default"><?php echo MAIN_NEWS_SUBSCRIBE_BUT; ?></button>
    </form>
</div>