<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:05:03 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class stripe {
    var $code, $title, $description, $enabled;

// class constructor
    function __construct() {
      global $order;

      $this->code = 'stripe';
      $this->title = MODULE_PAYMENT_STRIPE_TEXT_TITLE;
      $this->description = MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION;
      $this->sort_order = MODULE_PAYMENT_STRIPE_SORT_ORDER;
      $this->enabled = ((MODULE_PAYMENT_STRIPE_STATUS == 'True') ? true : false);

/*      if ((int)MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID > 0) {
        $this->order_status = MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID;
      }*/

      if (is_object($order)) $this->update_status();
    }

// class methods
    function update_status() {
      global $order;

      if ( ($this->enabled == true) && ((int)MODULE_PAYMENT_STRIPE_ZONE > 0) ) {
        $check_flag = false;
        $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_PAYMENT_STRIPE_ZONE . "' and (zone_country_id = '" . $order->billing['country']['id'] . "' or zone_country_id=0) order by zone_id");
        while ($check = tep_db_fetch_array($check_query)) {
          if ($check['zone_id'] < 1) {
            $check_flag = true;
            break;
          } elseif ($check['zone_id'] == $order->billing['zone_id']) {
            $check_flag = true;
            break;
          }
        }

        if ($check_flag == false) {
          $this->enabled = false;
        }
      }
    }

    function selection() {
      global $order, $stripe_customer_id;

        if(!payment::isModuleAvailable()) {
            return false;
        }

        require(DIR_WS_CLASSES.'Stripe/init.php');

        // Устанавливаем секретный ключ
        \Stripe\Stripe::setApiKey(MODULE_PAYMENT_SECRET_KEY);

      for ($i=1; $i<13; $i++) {
        $expires_month[] = array('id' => sprintf('%02d', $i), 'text' => strftime('%B',mktime(0,0,0,$i,1,2000)));
      }

      $today = getdate(); 
      for ($i=$today['year']; $i < $today['year']+10; $i++) {
        $expires_year[] = array('id' => strftime('%y',mktime(0,0,0,1,1,$i)), 'text' => strftime('%Y',mktime(0,0,0,1,1,$i)));
      }

        $stripe_customer_id = '';
        if(!empty($_SESSION['customer_id'])){
            $query = tep_db_query('select stripe_customer_id from customers where customers_id = '.$_SESSION['customer_id'].'');
            $stripe_customer_id = tep_db_fetch_array($query)['stripe_customer_id'];
            $_SESSION['stripe_customer_id'] = $stripe_customer_id;
        }
        if(!$stripe_customer_id){
      $strip_field = '
            <script></script>
            <input type="hidden" name="current_code" value="'.$this->code.'" />
            <script>
                $(document).ready(function(){
                    Stripe.setPublishableKey("'.MODULE_PAYMENT_PUBLIC_KEY.'");
                        var number;
                        var exp_month;
                        var exp_year;
                        var cvc;
                            if($("#checkoutButtonContainer #additional-button-container").length == 0) {
                              $("#checkoutButtonContainer").append(\'<div id="additional-button-container"></div>\');
                              $("#additional-button-container").html(\'\');
                              $("#additional-button-container").append(\'<span id="sub_butt" class="btn btn-default" value="Submit">Checkout</span>\');
                            } 
                            if($("input[name=payment][value="+$("input[name=current_code]").val()+"]").prop("checked")==true) {
                              $("#checkoutButton").hide(); 
                              $("#additional-button-container").show();                            
                            }
                    $(function() {
                        var $form = $(\'#payment-form\');

                        
                        $(\'body\').on(\'click\', \'#sub_butt\', function(event) {    
                            number = $form.find(\'[data-stripe="number"]\').val();
                            exp_month = $form.find(\'[data-stripe="exp_month"]\').val();
                            exp_year = $form.find(\'[data-stripe="exp_year"]\').val();
                            cvc = $form.find(\'[data-stripe="cvc"]\').val();
                        
                            $(\'#paymentHiddenFields\').append($(\'<input type="hidden" name="number">\').val(number));
                            $(\'#paymentHiddenFields\').append($(\'<input type="hidden" name="exp_month">\').val(exp_month));
                            $(\'#paymentHiddenFields\').append($(\'<input type="hidden" name="exp_year">\').val(exp_year));
                            $(\'#paymentHiddenFields\').append($(\'<input type="hidden" name="cvc">\').val(cvc));
                                
                            // Отключим кнопку, чтобы предотвратить повторные клики
                            $(\'#sub_butt\').prop(\'disabled\', true);
            
                            // Запрашиваем token у Stripe
                            if(number){
                                Stripe.card.createToken($form, stripeResponseHandler);
                            }                           
            
                            // Запретим форме submit
                            return false;
                        });
                    });
                });
            
                function stripeResponseHandler(status, response) {
                    // Получим форму:
                    var $form = $(\'#payment-form\');
            
                    if (response.error) { // Problem!
            
                        // Показываем ошибки в форме:
                        $form.find(\'.payment-errors\').text(response.error.message);
                        $(\'#sub_butt\').prop(\'disabled\', false); // Разрешим submit
            
                    } else { // Token был создан
            
                        // Получаем token id:
                        var token = response.id;
           
                        // Вставим token в форму, чтобы при submit он пришел на сервер:

                        $(\'#paymentHiddenFields\').append($(\'<input type="hidden" name="stripeToken">\').val(token));
            
                      $("#additional-button-container").hide();
                      $("#checkoutButton").show(); 
                      $("#checkoutButton").click();
                        // Сабмитим форму:
                       // $form.get(0).submit();
                    }
                };
            </script>
             <style type="text/css">
             #payment-form{
              position: relative; 
              font-size: 
              12px; font-family: Arial, \'Helvetica CY\', sans-serif;  
              line-height: 1;
             }
             </style>
            
            <form action="" method="POST" id="payment-form">
                <span class="payment-errors"></span>
                <input type="text" size="20" data-stripe="number" placeholder="'.MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER.'" class="form-control full-width-field">
                <input type="text" size="3" data-stripe="exp_month" placeholder="'.MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM.'" class="form-control  full-width-field">
                <input type="text" size="3" data-stripe="exp_year" placeholder="'.MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY.'" class="form-control full-width-field">
                <input type="text" size="6" data-stripe="cvc" placeholder="CVC" class="form-control full-width-field">
            </form>

      ';
        } else {
            $stripe_customer = \Stripe\Customer::retrieve($stripe_customer_id);
            $strip_field = '<div>Card number: ...'.$stripe_customer->sources->data[0]->last4.'</div>';
        }

      $selection = array('id' => $this->code,
          'module' => $this->title,
          'fields' => array(
              array('title' => '',
                  'field' => $strip_field)
          ));

      return $selection;
    }

    function pre_confirmation_check() {
      global $_POST, $order, $stripe_customer_id;

      include(DIR_WS_CLASSES . 'cc_validation.php');

       if (!$_SESSION['stripe_customer_id']){
           $cc_validation = new cc_validation();
           $result = $cc_validation->validate($_POST['number'], $_POST['exp_month'], $_POST['exp_year']);

           $error = '';
           switch ($result) {
               case -1:
                   $error = sprintf(TEXT_CCVAL_ERROR_UNKNOWN_CARD, substr($cc_validation->cc_number, 0, 4));
                   break;
               case -2:
               case -3:
               case -4:
                   $error = TEXT_CCVAL_ERROR_INVALID_DATE;
                   break;
               case false:
                   $error = TEXT_CCVAL_ERROR_INVALID_NUMBER;
                   break;
           }



           if ( ($result == false) || ($result < 1) ) {
               $payment_error_return = 'payment_error=' . $this->code . '&error=' . urlencode($error) . '&cc_owner=' . urlencode($_POST['cc_owner']) . '&cc_expires_month=' . $_POST['exp_month'] . '&cc_expires_year=' . $_POST['exp_year'];

               tep_redirect(tep_href_link(FILENAME_CHECKOUT, $payment_error_return, 'SSL', true, false));
           }

           $this->cc_card_type = $cc_validation->cc_type;
           $this->cc_card_number = $cc_validation->cc_number;

           $order->info['cc_number'] = $_POST['number'];
           $order->info['cc_type'] = $_POST['cc_type'];
           $order->info['cc_owner'] = $_POST['cc_owner'];
           $order->info['cc_expires'] = $_POST['exp_month'].$_POST['exp_year'];
           $order->info['cvvnumber'] = $_POST['cvc'];
       }
    }

    function confirmation() {
      global $_POST, $order, $stripe_customer_id;

      require(DIR_WS_CLASSES.'Stripe/init.php');

      // Устанавливаем секретный ключ
      \Stripe\Stripe::setApiKey(MODULE_PAYMENT_SECRET_KEY);

if(!$_SESSION['stripe_customer_id']){
    // Забираем token из формы
    $token = $_POST['stripeToken'];
// Create a Customer:
    $customer = \Stripe\Customer::create([
        'source' => $token,
        'email' => $order->customer['email_address'],
    ]);
    $stripe_customer_id = $customer->id;
    tep_db_query('update customers c set c.stripe_customer_id = "'.$stripe_customer_id.'" where c.customers_id = '.$_SESSION['customer_id']);
} else {
    $stripe_customer_id = $_SESSION['stripe_customer_id'];
}


// Создаём оплату
      try {
        $charge = \Stripe\Charge::create(array(
            "amount" => round($order->info['total'])*100, // сумма в центах
            "currency" => strtolower($order->info['currency']),
            //"source" => $token,
            "customer" => $stripe_customer_id,
        ));
      } catch(\Stripe\Error\Card $e) {
          // Since it's a decline, \Stripe\Error\Card will be caught
          $body = $e->getJsonBody();
          $err  = $body['error'];
          print('Status is:' . $e->getHttpStatus() . " ".$err['message']);
      } catch (\Stripe\Error\RateLimit $e) {
          // Too many requests made to the API too quickly
          $body = $e->getJsonBody();
          $err  = $body['error'];
          print('Status is:' . $e->getHttpStatus() . " ".$err['message']);
      } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
          $body = $e->getJsonBody();
          $err  = $body['error'];
          print('Status is:' . $e->getHttpStatus() . " ".$err['message']);
      } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
          $body = $e->getJsonBody();
          $err  = $body['error'];
          print('Status is:' . $e->getHttpStatus() . " ".$err['message']);
      } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
          $body = $e->getJsonBody();
          $err  = $body['error'];
          print('Status is:' . $e->getHttpStatus() . " ".$err['message']);
      } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
          $body = $e->getJsonBody();
          $err  = $body['error'];
          print('Status is:' . $e->getHttpStatus() . " ".$err['message']);
      } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
      }
        $_SESSION['succeeded'] = false;
      if($charge->status = 'succeeded'){
            $_SESSION['succeeded'] = true;
      }


      $confirmation = array('title' => $this->title . ': ' . $this->cc_card_type,
                            'fields' => array(array('title' => MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER,
                                                    'field' => $_POST['cc_owner']),
                                              array('title' => MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER,
                                                    'field' => substr($this->cc_card_number, 0, 4) . str_repeat('X', (strlen($this->cc_card_number) - 8)) . substr($this->cc_card_number, -4)),
                                              array('title' => MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES,
                                                    'field' => strftime('%B, %Y', mktime(0,0,0,$_POST['cc_expires_month'], 1, '20' . $_POST['cc_expires_year'])))));

      return $confirmation;
    }

    function process_button() {
      global $_POST;

      $process_button_string = tep_draw_hidden_field('cc_owner', $_POST['cc_owner']) .
                               tep_draw_hidden_field('cc_expires', $_POST['cc_expires_month'] . $_POST['cc_expires_year']) .
                               tep_draw_hidden_field('cc_type', $this->cc_card_type) .
                               tep_draw_hidden_field('cc_number', $this->cc_card_number);

      return $process_button_string;
    }

    function before_process() {
      global $_POST, $order;

      if ( (defined('MODULE_PAYMENT_STRIPE_EMAIL')) && (tep_validate_email(MODULE_PAYMENT_STRIPE_EMAIL)) ) {
        $len = strlen($_POST['cc_number']);

        $this->cc_middle = substr($_POST['cc_number'], 4, ($len-8));
        $order->info['cc_number'] = substr($_POST['cc_number'], 0, 4) . str_repeat('X', (strlen($_POST['cc_number']) - 8)) . substr($_POST['cc_number'], -4);
      }
    }

    function after_process() {
      global $insert_id, $order;

        if($_SESSION['succeeded']){
            $order->info['order_status'] = MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID;

            tep_db_perform('orders', array('orders_status' => MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID), 'update', "orders_id='".$insert_id."'");
            tep_db_perform('orders_status_history', array('orders_id' => $insert_id,
                'orders_status_id' => MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID,
                'date_added' => 'now()',
                'customer_notified' => '0',
                'comments' => 'Stripe - success!'));

            // send email to customer:
            $email_order = 'Hello, '.$order->customer['firstname'].'.<br />Your order was successfully paid through Stripe.';
            tep_mail($order->customer['firstname'], $order->customer['email_address'], 'Status for order #' . $insert_id . ' - ' . strftime(DATE_FORMAT_LONG), nl2br($email_order), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, '');
        }

/*      if ( (defined('MODULE_PAYMENT_STRIPE_EMAIL')) && (tep_validate_email(MODULE_PAYMENT_STRIPE_EMAIL)) ) {
        $message = 'Заказ номер ' . $insert_id . "\n\n" . 'Средние цифры: ' . $this->cc_middle . "\n\n";
        
        tep_mail('', MODULE_PAYMENT_STRIPE_EMAIL, 'Информация по заказу номер: ' . $insert_id, $message, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
      }*/
    }

    function get_error() {
      global $_GET;

      $error = array('title' => MODULE_PAYMENT_STRIPE_TEXT_ERROR,
                     'error' => stripslashes(urldecode($_GET['error'])));

      return $error;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_STRIPE_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    static function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Allow Stripe module', 'MODULE_PAYMENT_STRIPE_STATUS', 'True', 'Allow Stripe module?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Public key', 'MODULE_PAYMENT_PUBLIC_KEY', '', 'Public key', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Secret key', 'MODULE_PAYMENT_SECRET_KEY', '', 'Secret key', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order.', 'MODULE_PAYMENT_STRIPE_SORT_ORDER', '0', 'Sort order.', '6', '0' , now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone', 'MODULE_PAYMENT_STRIPE_ZONE', '0', 'Zone', '6', '2', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Success status', 'MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID', '0', 'Success status', '6', '0', 'tep_cfg_pull_down_order_statuses(', 'tep_get_order_status_name', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", static::keys()) . "')");
    }

    static function keys() {
      return array('MODULE_PAYMENT_STRIPE_STATUS', 'MODULE_PAYMENT_STRIPE_ZONE', 'MODULE_PAYMENT_STRIPE_ORDER_STATUS_ID', 'MODULE_PAYMENT_STRIPE_SORT_ORDER', 'MODULE_PAYMENT_PUBLIC_KEY', 'MODULE_PAYMENT_SECRET_KEY');
    }
  }
?>
