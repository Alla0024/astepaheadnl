<?php

    if(!defined('GOOGLE_FEED_MODULE_ENABLED'))
    {


        /**
         * check.php -- файл для автоматического добавления констант модуля в БД
         */
        tep_db_query("INSERT INTO ".TABLE_CONFIGURATION."
        (configuration_title,
         configuration_key,
         configuration_value,
         configuration_description,
         configuration_group_id,
         sort_order,
         date_added,
         last_modified,
         use_function,
         set_function)
         VALUES
        ('Модуль GOOGLE_FEED',
        'GOOGLE_FEED_MODULE_ENABLED',
        'google_feed:false',
        'Модуль Google Feed',
        '277',
        '1',
        '".date('Y-m-d H:i:s')."',
        '".date('Y-m-d H:i:s')."',
        'tep_check_modules_folder',
        'tep_cfg_select_option(array(\"google_feed:true\", \"google_feed:false\"),')
        ");

        
        $module_installed = true;
    }