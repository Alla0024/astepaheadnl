<?php
    function getCompare($id) {
      if(isset($_SESSION['compares'][$id])) {
  			$compa_checked = 'checked';
  			$compa_text = GO_COMPARE;
  		} else {
  		  $compa_checked = '';
  		  $compa_text = COMPARE;
  		} 
      return array('text'=>$compa_text,'checked'=>$compa_checked);
    }
?>