<?php
/**
 * Created by PhpStorm.
 * User: 'Serhii.M'
 * Date: 01.03.2019
 * Time: 14:17
 */
//INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Google Tag ID', 'GOOGLE_TAGS_ID', '', 'Google tags id', 1, 1234, '2018-03-02 07:08:08', '2014-02-10 10:10:10', null, null);
//INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Google Tag ID Status', 'GOOGLE_TAGS_ID_STATUS', 'False', 'Google tags id status', 1, 1233, '2018-03-02 07:08:08', '2014-02-10 10:10:10', null, "tep_cfg_select_option(array('True', 'False'), ");
    function makeGTagScript($prodid = '', $pagetype = '', $totalvalue = '') {
        $script = "
              gtag('event', 'page_view', {'send_to': '" . GOOGLE_TAGS_ID . "',";
        if ($prodid) {
            $script .= PHP_EOL."'ecomm_prodid': $prodid,";
        }
        if ($pagetype) {
            $script .= PHP_EOL."'ecomm_pagetype': $pagetype,";
        }
        if ($totalvalue) {
            $script .= PHP_EOL."'ecomm_totalvalue': $totalvalue";
        }
        $script .= "});
            ";
        return $script;
    }

    function makeProductsForGTags($products) {
        $product_list = [];
        foreach ($products as $product) {
            $product_list[] = "\"{$product['id']}\"";
        }
        if (count($product_list)>1) {
            $product_list = '[' . implode(',', $product_list) . ']';
        } else {
            $product_list = array_pop($product_list);
        }
        return $product_list;
    }

    function makePriceForGTags($products) {
        $product_list = [];
        foreach ($products as $product) {
            $qty = $product['quantity']?:$product['qty'];
            $price = $product['final_price'] * $qty;
            $product_list[] = "$price";
        }
        if (count($product_list)>1) {
            $product_list = '[' . implode(',', $product_list) . ']';
        } else {
            $product_list = array_pop($product_list);
        }
        return $product_list;
    }

    ?>
    <?php if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', '<?=GOOGLE_TAGS_ID?>');
    <?php endif; ?>
    <?
    if (basename($PHP_SELF) === FILENAME_DEFAULT && isset($_GET['keywords'])){
        $query = tep_db_query($listing_split->sql_query);
        $id_array = [];
        $price_array = [];
        while ($row = tep_db_fetch_array($query)){
            $id_array[] = "\"{$row['products_id']}\"";
            $price_array[] = (float)number_format($row['products_price'],2);
        }
        $id_array = count($id_array) == 1 ? reset($id_array) : "[".implode(',',$id_array)."]";
        $price_array = count($price_array) == 1 ? reset($price_array) : "[".implode(',',$price_array)."]";
        echo makeGTagScript($id_array,'"searchresults"',$price_array);
    }elseif (basename($PHP_SELF) === FILENAME_DEFAULT && !$cPath){
        echo makeGTagScript('""','"home"');
    }elseif (basename($PHP_SELF) === FILENAME_PRODUCT_INFO){
        $price = $new_price?:$product_info['products_price'];
        $price = $price*$currencies->currencies[$currency]['value'];
        echo makeGTagScript("\"$products_id\"",'"product"',$price);
    }elseif (basename($PHP_SELF) === FILENAME_CHECKOUT_SUCCESS){
        require(DIR_WS_CLASSES . 'order.php');
        $order = new order($_GET['order_id']);
        $product_list = makeProductsForGTags($order->products);
        $price = makePriceForGTags($order->products);
        //        $price = (float)str_replace(',','.',preg_replace( '/[^,\.\d]+/', '', $order->info['total']));
        //        $price = makePriceForGTags($order->products);
        echo makeGTagScript($product_list,'"purchase"',$price);

    }elseif (basename($PHP_SELF) === FILENAME_CHECKOUT || basename($PHP_SELF) === 'popup_cart.php'){
        $products = $cart->get_products();
        $product_list = makeProductsForGTags($products);
        $price = makePriceForGTags($products);

        echo makeGTagScript($product_list,'"cart"',$price);
    }

?>