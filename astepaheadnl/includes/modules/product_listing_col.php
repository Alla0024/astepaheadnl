<?php

//$time_start = microtime(true);

if(!is_array($tpl_settings)) {
    $tpl_settings = array(
        'request' => $listing_query,
        'id' => 'r_spisok',
        //'classes' => array(),
        //'title' => '',   
        //'cols' => array('xs'=>$cols[0],'sm'=>$cols[1],'md'=>$cols[2],'lg'=>$cols[0]),//'cols' => array('xs'=>'2','sm'=>'2','md'=>'3','lg'=>'4'),
        'wishlist'=>true,
        'compare' => true
    );
}
$default_cols = explode(';',$template->getMainconf('MC_PRODUCT_QNT_IN_ROW')); // XS, SM, MD, LG                            
//$default_cols = unserialize(RTPL_COLS);

if($tpl_settings['cols']!='') {
    $tpl_cols = explode(';', $tpl_settings['cols']); // XS, SM, MD, LG
    if (count($tpl_cols)>1) {
        $blocks_num_xs = 12 / ($tpl_cols[0]?:12);
        $blocks_num_sm = 12 / ($tpl_cols[1]?:12);
        $blocks_num_md = 12 / ($tpl_cols[2]?:12);
        $blocks_num_lg = 12 / ($tpl_cols[3]?:12);
        $blocks_num_xl = 12 / ($tpl_cols[4]?:6);
    } else {
        $blocks_num_xs = $blocks_num_sm = $blocks_num_md = $blocks_num_lg = $blocks_num_xl = $default_cols[2] = 12 / $tpl_settings['cols'];
    }
}else {

    $blocks_num_xs = 12/($default_cols[0]?:2);//default value if empty
    $blocks_num_sm = 12/($default_cols[1]?:3);//default value if empty
//  $default_cols[2] = $tpl_settings['cols']?:$default_cols[2];
    $blocks_num_md = 12/($default_cols[2]?:4);//default value if empty
    $blocks_num_lg = 12/($default_cols[3]?:4);//default value if empty
    $blocks_num_xl = 12/($default_cols[4]?:6);//default value if empty
}
$blocks_num_xs = is_float($blocks_num_xs) ? str_replace('.','-',$blocks_num_xs) : $blocks_num_xs;
$blocks_num_sm = is_float($blocks_num_sm) ? str_replace('.','-',$blocks_num_sm) : $blocks_num_sm;
$blocks_num_md = is_float($blocks_num_md) ? str_replace('.','-',$blocks_num_md) : $blocks_num_md;
$blocks_num_lg = is_float($blocks_num_lg) ? str_replace('.','-',$blocks_num_lg) : $blocks_num_lg;
$blocks_num_xl = is_float($blocks_num_xl) ? str_replace('.','-',$blocks_num_xl) : $blocks_num_xl;

if($listing_split->number_of_rows > 0 or $tpl_settings['id']!='r_spisok') {
    $vue_array = array();
    if($tpl_settings['id']!='r_spisok') $display = 'block'; // $display = PRODUCT_LISTING_DISPLAY_STYLE;

    if ($display == 'list') { // display LIST ------------------------------------------------------
        $pmodel_class = 'l_list';
        $stock_pull = 'pull-left';
//        $compare_class = 'compare_list';
        $listing_layout = file_get_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/listing_list.html');
        $attr_listing = RTPL_PRODUCTS_ATTR_LISTING_LIST;
        if(is_array($attrs_array) and !isMobile()) $attr_body = RTPL_PRODUCTS_ATTR_BODY_LIST;
    } else {  // display COLUMNS ------------------------------------------------------
        $pmodel_class = 'p_list_model';
        $stock_pull = 'pull-right';
//        $compare_class = 'compare';
        $listing_layout = file_get_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/listing_columns.html');
        $attr_listing = RTPL_PRODUCTS_ATTR_LISTING_COL;
        if(is_array($attrs_array) and !isMobile()) $attr_body = RTPL_PRODUCTS_ATTR_BODY_COL;
    }

    $categories_id = $current_category_id;
    $catalog_path = tep_href_link(FILENAME_DEFAULT);

    if (is_array($tpl_settings['classes'])) $render_class = implode(' ', $tpl_settings['classes']);
    else $render_class = '';

    // listing_header:
    $listing_header = '';

    if ($tpl_settings['title'] != '') {
        $listing_header .= '<div class="like_h2">' . $tpl_settings['title'];
        if ($tpl_settings['description'] != '') $listing_header .= '<p class="like_p">'.$tpl_settings['description'].'</p>';
        if ($tpl_settings['additional_title_block'] != '') $listing_header .= $tpl_settings['additional_title_block'];
        $listing_header .= '</div>';
    }
    // if its slider:
    if (is_array($tpl_settings['classes']) and in_array('product_slider', $tpl_settings['classes'])) {
        $listing_header .= sprintf(RTPL_LISTING_HEADER_SLIDER, $tpl_settings['id'], $render_class);
    } else {
        $listing_header .= sprintf(RTPL_LISTING_HEADER_NORMAL, $render_class, $tpl_settings['id']);
    }
    // END listing_header


    // listing_footer:
    $listing_footer = '</div>';

    if (is_array($tpl_settings['classes']) and in_array('product_slider', $tpl_settings['classes'])) {

        $listing_footer .= '</div>';

        $sliders_output .= '                                 
    	           jQuery(document).ready(function() {    
                                        
                    $("#'.$tpl_settings['id'].'").owlCarousel({
                    items:' . ($tpl_cols[3]?:$default_cols[3]) . ',  
                    responsive:{
                        0:{items:' . ($tpl_cols[0]?:$default_cols[0]) . ',nav:true},
                        600:{items:' . ($tpl_cols[1]?:$default_cols[1]) . ',nav:true},
                        992:{items:' . ($tpl_cols[2]?:$default_cols[2]). ',nav:true,loop:true},
                        1200:{items:' . ($tpl_cols[3]?:$default_cols[3]). ',nav:true,loop:true},
                        1600:{items:' . ($tpl_cols[4]?:$default_cols[4]). ',nav:true,loop:true}
                    },  
                    nav: true,  
                    loop:true,
                    dots: false, 
                    onInitialized:function () {$(".product_slider ").css(\'overflow\',\'visible\');},  
                    navText:[\''.RTPL_ARROW_LEFT.'\',\''.RTPL_ARROW_RIGHT.'\'],  
                    slideSpeed: 200,
    	            	});
    	            	$("#'.$tpl_settings['id'].'").on(\'changed.owl.carousel\', function(e) {
                        $("#'.$tpl_settings['id'].' img").unveil(500);
                    });
    	           });';
        if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && isset($_GET['get-module'])) echo '<script>'.$sliders_output.'</script>';
        // afterInit : $(".product_slider img").unveil(500),
    }

    if($tpl_settings['id']=='r_spisok') {
        if ($listing_split->number_of_rows > $row_by_page) {
            $listing_footer .= sprintf(RTPL_NUMBER_OF_ROWS, $listing_split->number_of_rows);
            if($template->show('LIST_LOAD_MORE')) {
                // $listing_footer .=  RTPL_LOAD_MORE;
                if ($listing_split->current_page_number<$listing_split->number_of_pages) $listing_footer .=  RTPL_LOAD_MORE;
            }
            if($template->show('LIST_NUMBER_OF_ROWS')) {
                $listing_footer .= sprintf(RTPL_PAGES_HTML, $listing_split->display_links(10, tep_get_all_get_params(array('page', 'info', 'x', 'y', 'ajaxloading', 'language'))));
            }
        }
        if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true' && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && empty($_GET['manufacturers_id'])) {
            $isFilter = true;

            if (!isset($redirectOptionsIdsArrayForCheck)){
                $redirectOptionsIdsArrayForCheck = array_filter(array_keys($_GET), 'is_numeric');
                $redirectOptionsIdsArrayForCheck = array_intersect_key($_GET, array_flip($redirectOptionsIdsArrayForCheck));
            }
        }
    }
    // END listing_footer
    if (isset($tpl_settings['wishlist']) && defined('WISHLIST_MODULE_ENABLED') && WISHLIST_MODULE_ENABLED == 'true'){
        $wish_enabled = $tpl_settings['wishlist'];
    }else{
        $wish_enabled = WISHLIST_MODULE_ENABLED == 'true';
    }
    if (isset($tpl_settings['compare']) && defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true'){
        $compa_enabled = $tpl_settings['compare'];
    }else{
        $compa_enabled = COMPARE_MODULE_ENABLED == 'true';
    }
    if (isset($content) && $content === '404'){
        $pdf_link = '';
    }else{
        $pdf_link = tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('language')).'pdf=true');
    }
    // one global array:
    $vue_array['globals'] = array('categories_id' => $categories_id,
                                  'cart_incart_text' => IMAGE_BUTTON_IN_CART,
                                  'cart_addto_cart' => IMAGE_BUTTON_ADDTO_CART,
                                  'compa_enabled' => $compa_enabled,
                                  'promUrls' => $promUrls,
                                  'compa_text_in' => GO_COMPARE,
                                  'compa_text' => COMPARE,
                                  'wish_enabled' => $wish_enabled,
                                  'wish_text_in' => IN_WHISHLIST,
                                  'wish_text' => WHISH,
                                  'product_stock_text_in' => LIST_TEMP_INSTOCK,
                                  'product_stock_text' => LIST_TEMP_OUTSTOCK,
                                  'display' => $display,
                                  'pdf_link' => $pdf_link,
                                  'pmodel_class' => $pmodel_class,
                                  'compare_class' => $compare_class,
                                  'attr_listing' => $attr_listing,
                                  'attr_body' => $attr_body,
                                  'stock_pull' => $stock_pull,
                                  'catalog_path' => $catalog_path,
                                  'blocks_num' =>  array('xs'=>$blocks_num_xs,'sm'=>$blocks_num_sm,'md'=>$blocks_num_md,'lg'=>$blocks_num_lg, 'xl'=>$blocks_num_xl),
                                  'listing_header' => $listing_header,
                                  'listing_footer' => $listing_footer,
                                  'listing_layout' => $listing_layout,
                                  'img_end' => SMALL_IMAGE_WIDTH . 'x' . SMALL_IMAGE_HEIGHT,
                                  'img_default' => 'default.png',
                                  'all_attribs' => $attr_names_array);

    if($template->getMainconf('MC_THUMB_FIT')!=0) $vue_array['globals']['add_classes'] .= ' object_fit';
    if (defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true') {
        $compare_page_link = tep_href_link('compare.php');
    }
    $i=1;
    while ($listing = tep_db_fetch_array($tpl_settings['request'])) {
        $id = $listing['products_id'];
        $product_name = strip_tags($listing['products_name']);

        if($listing['products_url']!='') {
            $product_href = $listing['products_url']; // if "products_url" is set, show link without DB query to get names 
        } else {
            $products_url = $seo_urls->strip($product_name);
            tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_url = '" . $products_url . "' where language_id = '" . $languages_id . "' and products_id = '" . tep_db_input($id) . "'");
            $product_href = $products_url?:'-'; // if "products_url" empty, then get name from DB
        }

        $product_href = getCPathUrlPart($id) . preg_replace('|'.$catalog_path.'|i','',$product_href);

        if($listing['products_image']!='') $product_image = explode(';', $listing['products_image'])[0];
        else $product_image = '';

        if($template->getMainconf('MC_SHOW_THUMB2')==1) $product_image2 = explode(';', $listing['products_image'])[1];

        $product_model = $listing['products_model'];
        $product_stock = $listing['products_quantity'];
        $cart_incart = $cart->in_cart($id);

        // PRICES
        $currencies->taxWrapper = 'span';
        $currencies->enableCurrencies = true;

        $old_price = $currencies->display_price($listing['products_price'], tep_get_tax_rate($listing['products_tax_class_id']));
        // PRICES with discounts
        if ($listing['specials_new_products_price']) $spec_price = $listing['specials_new_products_price'];
        elseif($spec_array[$id]) $spec_price = $spec_array[$id];
        elseif($salemakers_array[$id]) $spec_price = $salemakers_array[$id];
        else $spec_price = '';

        if($spec_price!='') $new_price = $currencies->display_price($spec_price,tep_get_tax_rate($listing['products_tax_class_id']));
        else $new_price = '';
        // END -- PRICES

        // LABELS
        if (PRODUCT_LABELS_MODULE_ENABLED == 'true') {
            $label_arr = getLabel($listing);
            $label = $label_arr['name'];
            $label_class = $label_arr['class'];
        } else {
            $label = '';
            $label_class = '';
        }
        // END -- LABELS

        // COMPARE & WISHLIST
        if (defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true') {

            $compa_arr = getCompare($id);
            $compa_checked = $compa_arr['checked'];
            $compa_text = $compa_arr['text'];

        }

        if(defined('WISHLIST_MODULE_ENABLED') && WISHLIST_MODULE_ENABLED == 'true')
        {
            $wish_arr = getWishList($id);
            $wish_checked = $wish_arr['checked'];
            $wish_text = $wish_arr['text'];
        }

        // END COMPARE & WISHLIST

        // ATTRIBUTES
        $listing_product_attributes = array();
        $attr_limit = 4; // attributes in product hover
        if (is_array($r_pid_attr_array[$id])) {
            foreach ($attrs_array as $at_id) {
                if (!empty($r_pid_attr_array[$id][$at_id]) and $attr_limit!=0 and is_array($show_in_product_listing) and in_array($at_id, $show_in_product_listing)) {
                    ksort($r_pid_attr_array[$id][$at_id]);
                    $listing_product_attributes[$at_id] = implode(', ', $r_pid_attr_array[$id][$at_id]);
                    $attr_limit--;
                }
            }
        }
        // END -- ATTRIBUTES

        // array for every product:
        $vue_array[$i] = array('p_id' => $id,
                               'p_name' => htmlspecialchars($product_name),
                               'p_href' => $product_href,
                               'p_img' => $product_image,
                               'p_qty' => $product_stock,
                               'show_button' => STOCK_SHOW_BUY_BUTTON,
                               'cat_name' => $cat_names[$prodToCat[$id]],
                               'p_price' => $old_price);

        if($cart_incart) $vue_array[$i]['cart_incart'] = $cart_incart;
        if($label!='') $vue_array[$i]['lbl'] = $label;
        if($label_class!='') $vue_array[$i]['l_class'] = $label_class;
        if($compa_checked!='') {
            $vue_array[$i]['compare'] = $compa_checked;
            $vue_array[$i]['compare_link_before'] = '<a href="'.$compare_page_link.'">';
            $vue_array[$i]['compare_link_after'] = '</a>';
        }else{
            $vue_array[$i]['compare_link_before'] = '';
            $vue_array[$i]['compare_link_after'] = '';
        }
        if($product_image2!='') $vue_array[$i]['p_img2'] = $product_image2;
        if($wish_checked!='') $vue_array[$i]['wish'] = $wish_checked;
        if($new_price!='') $vue_array[$i]['p_specprice'] = $new_price;
        if($listing['products_info']!='') $vue_array[$i]['p_info'] = $listing['products_info'];
        if(is_array($listing_product_attributes) and isset($listing_product_attributes)) $vue_array[$i]['p_attr'] = $listing_product_attributes;
        $vue_array[$i]['p_model'] = $template->show('LIST_MODEL')?$product_model:'';

        $i++;
    }

    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && !isset($_GET['get-module'])) {
        if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {
            ob_start();

            $filesToRequire = [
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/left/filter_seo.php',
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/left/filter.php',
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/listing/filter_seo.php',
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/listing/filter.php',
                DIR_WS_TEMPLATES . 'default/boxes/left/filter_seo.php',
            ];

            foreach ($filesToRequire as $fileToRequire) {
                if(file_exists($fileToRequire)) {
                    require $fileToRequire;
                    break;
                }
            }

            $vue_array['globals']['filtersBlock'] = ob_get_contents();
            $addPage=true;
            unset($tempSeoFilterInfo);
            if($isFilter && empty($_GET['manufacturers_id']) && empty($_GET['keywords'])){
                if (empty($_GET['filter_id']) && empty($redirectOptionsIdsArrayForCheck)){
                    $vue_array['globals']['currentHref'] = HTTP_SERVER . '/' . tep_href_link(FILENAME_DEFAULT, 'cPath='.($_GET['cPath'] ?: 0));
                }else {
                    $vue_array['globals']['currentHref'] = HTTP_SERVER . getFilterUrl(
                            $_GET['cPath'] ?: 0,
                            (isset($_GET['filter_id']) ? $_GET['filter_id'] : ''),
                            $redirectOptionsIdsArrayForCheck
                        );
                }
            } elseif (isset($_GET['cPath'])){
                $vue_array['globals']['currentHref'] = $isFilter ?  : tep_href_link(FILENAME_DEFAULT,'cPath='.$_GET['cPath']);
            }
            $addPage=false;

            ob_end_clean();
        }
        echo json_encode($vue_array);
        die;
    } // if its ajax-request
    else { // if its usual request

//        if(RTPL_PDF_ENABLED and $tpl_settings['id']=='r_spisok' && $current_category_id!=0 ) $output = sprintf(RTPL_LISTING_HEADER, $vue_array['globals']['pdf_link']);
//        else $output = '';
        $output = '';
        $output .= $vue_array['globals']['listing_header'];

        // show each product
        foreach($vue_array as $key => $listing) {

            if($key!='globals') {
                $id = $listing['p_id'];
                $product_name = $listing['p_name'];
                $product_info_text = $listing['p_info'];
                $cat_name = $listing['cat_name'];
                if ($promUrls) {
                    $product_href = $vue_array['globals']['catalog_path'] . 'p' . $id . '-' . $listing['p_href'] . '.html';
                } else {
                    $product_href = $vue_array['globals']['catalog_path'] . $listing['p_href'] . '/p-' . $id . '.html';
                }
                $product_image = sprintf(RTPL_PRODUCTS_IMAGE, $vue_array['globals']['img_end'], $listing['p_img']?:$vue_array['globals']['img_default'], $product_name, $product_name, ($listing['p_img2']?'data-hover="getimage/'.$vue_array['globals']['img_end'].'/products/'.$listing['p_img2'].'"':''));
                $pmodel = ($listing['p_model']!=''?sprintf(RTPL_PRODUCTS_MODEL, $vue_array['globals']['pmodel_class'], $listing['p_model']):'');
                $stock = ($listing['p_qty']>0?sprintf(RTPL_PRODUCTS_STOCK, $vue_array['globals']['stock_pull']):sprintf(RTPL_PRODUCTS_OUTSTOCK, $vue_array['globals']['stock_pull']));
                $label = $listing['lbl']!=''?sprintf(RTPL_LABEL, $listing['l_class'], $listing['lbl']):'';
                $add_classes = $vue_array['globals']['add_classes']?:'';

                $spec_price = $listing['p_specprice'];
                $old_price = $listing['p_price'];
                $final_price = ($spec_price!=''?sprintf(RTPL_PRODUCTS_SPEC_PRICE, $spec_price, $old_price):sprintf(RTPL_PRODUCTS_PRICE, $old_price));

                $new_price = sprintf(RTPL_PRODUCTS_PRICE, ($spec_price?:$old_price)); // separated new price
                $old_price = $spec_price?sprintf(RTPL_PRODUCTS_OLD_PRICE, $old_price):''; // separated old price

                //  $cart_button = tep_draw_hidden_field('cart_quantity', 1) . tep_draw_hidden_field('products_id', $id);

                if( $listing['p_qty'] <= 0) {
                    if (STOCK_SHOW_BUY_BUTTON == "false") {
                        $cart_button = "";
                    } else {
                        $cart_button = $listing['cart_incart']?sprintf(RTPL_CART_BUTTON, $id):sprintf(RTPL_ADD_TO_CART_BUTTON, $id, 1);
                    }
                } else {
                    $cart_button = $listing['cart_incart']?sprintf(RTPL_CART_BUTTON, $id):sprintf(RTPL_ADD_TO_CART_BUTTON, $id, 1);
                }


                // Compare & Wishlist
                $compare_output = $wishlist_output = '';
                if ($vue_array['globals']['compa_enabled'] == 'true' or $vue_array['globals']['wish_enabled'] == 'true') {
                    if ($vue_array['globals']['compa_enabled'] == 'true' and !isMobile()) {
                        if (TEMPLATE_NAME === 'default') {
                            $compare_output .= sprintf(
                                RTPL_PRODUCTS_COMPARE,
                                $id,
                                $id,
                                $id,
                                $listing['compare'],
                                $listing['compare_link_before'],
                                $id,
                                ($listing['compare'] ? $vue_array['globals']['compa_text_in'] : $vue_array['globals']['compa_text']),
                                $listing['compare_link_after']

                            );
                        }else{
                            $compare_output .= sprintf(
                                RTPL_PRODUCTS_COMPARE,
                                $id,
                                $id,
                                $id,
                                $listing['compare'],
                                $id,
                                ($listing['compare'] ? $vue_array['globals']['compa_text_in'] : $vue_array['globals']['compa_text'])
                            );
                        }
                    }
                    if ($vue_array['globals']['wish_enabled'] == 'true' and !isMobile())  $wishlist_output .= sprintf(RTPL_PRODUCTS_WISHLIST, $id, $id, $id, $listing['wish'], $id, $listing['wish']?$vue_array['globals']['wish_text_in']:$vue_array['globals']['wish_text']);
                }
                //   $compare_output = ($compare_output!=''?sprintf(RTPL_COMPARE_OUTPUT, $vue_array['globals']['compare_class'], $compare_output):'');
                $listing_product_attributes = '';

                if(is_array($listing['p_attr']) && !empty($listing['p_attr'])) {

                    foreach ($listing['p_attr'] as $ana_name => $ana_vals) $listing_product_attributes .= sprintf($vue_array['globals']['attr_listing'], $vue_array['globals']['all_attribs'][$ana_name], $ana_vals);
                    $listing_product_attributes = sprintf($vue_array['globals']['attr_body'], $listing_product_attributes);
                }

                // array to replace variables from html template:
                $array_from_to = array (
                    '{{blocks_num_xl}}' =>       $vue_array['globals']['blocks_num']['xl'],
                    '{{blocks_num_lg}}' =>       $vue_array['globals']['blocks_num']['lg'],
                    '{{blocks_num_md}}' =>       $vue_array['globals']['blocks_num']['md'],
                    '{{blocks_num_sm}}' =>       $vue_array['globals']['blocks_num']['sm'],
                    '{{blocks_num_xs}}' =>       $vue_array['globals']['blocks_num']['xs'],
                    '{{product_href}}' =>        $product_href,
                    '{{label}}' =>               $label,
                    '{{product_image}}' =>       $product_image,
                    '{{products_model}}' =>      $pmodel,
                    '{{stock}}' =>               $stock,
                    '{{final_price}}' =>         $final_price,
                    '{{new_price}}' =>           $new_price,
                    '{{old_price}}' =>           $old_price,
                    '{{category_name}}' =>       $cat_name,
                    '{{id}}' =>                  $id,
                    '{{product_name}}' =>        $product_name,
                    '{{product_info}}' =>        $product_info_text,
                    '{{product_attributes}}' =>  $listing_product_attributes,
                    '{{compare_output}}' =>      $compare_output,
                    '{{wishlist_output}}' =>     $wishlist_output,
                    '{{cart_button}}' =>         $cart_button,
                    '{{not_available}}' =>       ($listing['p_qty']==0?' not_available':''),
                    '{{add_classes}}' =>         $add_classes);

                $output .= strtr($vue_array['globals']['listing_layout'], $array_from_to);
                //  $output .= strtr(constant($vue_array['globals']['listing_layout']), $array_from_to);
            }
        }

        $output .= $vue_array['globals']['listing_footer'];
        echo $output;

    }

} else {
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') echo json_encode(array());
    else echo '';
}

?>     