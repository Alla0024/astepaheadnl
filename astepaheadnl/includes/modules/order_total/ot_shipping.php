<?php
/*
  $Id: ot_shipping.php,v 1.1.1.1 2003/09/18 19:04:58 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class ot_shipping {
    var $title, $output;

    function __construct() {
      $this->code = 'ot_shipping';
      $this->title = MODULE_ORDER_TOTAL_SHIPPING_TITLE;
      $this->description = MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION;
      $this->enabled = ((MODULE_ORDER_TOTAL_SHIPPING_STATUS == 'true') ? true : false);
      $this->sort_order = MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER;

      $this->output = array();
    }

    function process() {
      global $order, $currencies,$currency, $free_shipping;

      if (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') {
//        switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
//          case 'national':
//            if ($order->delivery['country_id'] == STORE_COUNTRY) $pass = true; break;
//          case 'international':
//            if ($order->delivery['country_id'] != STORE_COUNTRY) $pass = true; break;
//          case 'both':
//            $pass = true; break;
//          default:
//            $pass = false; break;
//        }



/* One Page Checkout - BEGIN */
   //     if (ONEPAGE_CHECKOUT_ENABLED != 'True'){
   //        print_r($pass);
//				  if ( ($pass == true) && ( ($order->info['total'] - ($order->info['shipping_cost']!==NULL?:0)) >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
//            $r_pass = true;
//            $free_shipping = true;
//					  $order->info['shipping_method'] = $order->info['shipping_method'].'.<br /> '.FREE_SHIPPING_TITLE.'';
//            $order->info['total'] -= $order->info['shipping_cost'];
//            $order->info['shipping_cost'] = 0;
//            $order->info['free_ship'] = '1';
//          }
    //    }
/* One Page Checkout - END */

      }

//      $module = substr($GLOBALS['shipping']['id'], 0, strpos($GLOBALS['shipping']['id'], '_'));

      if (tep_not_null($order->info['shipping_method'])) {
//        if ($GLOBALS[$module]->tax_class > 0) {
//          $shipping_tax = tep_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
//          $shipping_tax_description = tep_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
//
//          $order->info['tax'] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
//          $order->info['tax_groups']["$shipping_tax_description"] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
//          $order->info['total'] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
//
//          if (DISPLAY_PRICE_WITH_TAX == 'true') $order->info['shipping_cost'] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
//        }

//				if($r_pass == true) {
//				  $r_text = '&nbsp;';
//				  $r_title = $order->info['shipping_method'] . ' '.MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER.' '.($currencies->currencies[$currency]['symbol_left']?$currencies->currencies[$currency]['symbol_left']:$currencies->currencies[$currency]['symbol_right']);
//				} else {
//				  $r_text = $currencies->format($order->info['shipping_cost'] + $order->info['vac'], true, $order->info['currency'], $order->info['currency_value']);
//				  $r_title = $order->info['shipping_method'] . ': ';
//				}

//        $order->info['tax'] = 21;
//        $order->info['vac'] = $order->info['subtotal'] * $order->info['tax'] / 100;

//        (float) $order->info['total'] = (float) $order->info['subtotal'] + (float) $order->info['vac'];

          $order->info['vac'] = $order->info['subtotal'] * $order->info['tax'] / 100;

            $this->output[] = array(
              'title' => MODULE_ORDER_TOTAL_SHIPPING_TITLE.":",
              'text' => $currencies->format($order->info['shipping_cost'], true, $order->info['currency'], $order->info['currency_value']),
              'value' => $order->info['shipping_cost'],
      );

//        $this->output[] = array('title' => "VAT 21%",
//                                'text' => $currencies->format($order->info['vac'], true, $order->info['currency'], $order->info['currency_value']),
//                                'value' => $order->info['vac']);


      }
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_ORDER_TOTAL_SHIPPING_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }

      return $this->_check;
    }

    static function keys() {
      return array('MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION');
    }

    static function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Показывать доставку', 'MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'true', 'Вы хотите показывать стоимость доставки?', '6', '1','tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Порядок сортировки', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', '2', 'Порядок сортировки модуля.', '6', '2', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Разрешить бесплатную доставку', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'false', 'Вы хотите разрешить беслатную доставку?', '6', '3', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, date_added) values ('Бесплатная доставка для заказов свыше', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', '50', 'Для заказов, свыше указанной величины, доставка будет бесплатной..', '6', '4', 'currencies->format', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Бесплатная доставка для заказов', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION', 'national', 'Укажите, для каких именно заказов будет действительна бесплатная доставка.', '6', '5', 'tep_cfg_select_option(array(\'national\', \'international\', \'both\'), ', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", static::keys()) . "')");
    }
  }
?>
