<?php
/*
  $Id: flat.php,v 1.1.1.1 2003/09/18 19:04:54 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class nwposhtanew {
    var $code, $title, $description, $icon, $enabled, $sort_order, $tax_class;
      public $countries = array("220");

// class constructor
    function __construct() {
      global $order;

      $this->code = 'nwposhtanew';
      $isObject = is_object($order);
      $custom_name = constant('MODULE_SHIPPING_NWPOSHTANEW_CUSTOM_NAME');
      $checkTitle = empty($custom_name);
        if (!$checkTitle && $isObject){
            $this->title = MODULE_SHIPPING_NWPOSHTANEW_CUSTOM_NAME;
        }else{
            $this->title = MODULE_SHIPPING_NWPOSHTANEW_TEXT_TITLE;
        }
        $this->description = MODULE_SHIPPING_NWPOSHTANEW_TEXT_DESCRIPTION;
      $this->sort_order = MODULE_SHIPPING_NWPOSHTANEW_SORT_ORDER;
      $this->icon = '';
      $this->tax_class = MODULE_SHIPPING_NWPOSHTANEW_TAX_CLASS;
      $this->enabled = ((MODULE_SHIPPING_NWPOSHTANEW_STATUS == 'True') ? true : false);

//      if ( ($this->enabled == true) && ((int)MODULE_SHIPPING_NWPOSHTANEW_ZONE > 0) ) {
//        $check_flag = false;
//        $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_NWPOSHTANEW_ZONE . "' and (zone_country_id = '" . $order->delivery['country']['id'] . "' or zone_country_id=0) order by zone_id");
//        while ($check = tep_db_fetch_array($check_query)) {
//          if ($check['zone_id'] < 1) {
//            $check_flag = true;
//            break;
//          } elseif ($check['zone_id'] == $order->delivery['zone_id']) {
//            $check_flag = true;
//            break;
//          }
//        }
//
//        if ($check_flag == false) {
//          $this->enabled = false;
//        }
//      }
    }
    function getWarehouses(){
      global $lng,$languages_id,$languages;
      $existLngArray = [];
      foreach ($lng->catalog_languages as $oneLng){
          $existLngArray[] = $oneLng['id'];
      }

      $query = tep_db_query("SELECT language_id FROM novaposhta_warehouses group by language_id");
      $dbLanguages = [];
      while ($dbLanguagesLine = tep_db_fetch_array($query)){
          $dbLanguages[] = $dbLanguagesLine['language_id'];
      }

      $lang_id = in_array($languages_id,$dbLanguages)?$languages_id: $dbLanguages[0];
      $query = tep_db_query("SELECT city,name FROM novaposhta_warehouses WHERE language_id = '{$lang_id}' and city != '' ORDER BY number,city ASC");
      $warehouses = [];
      while ($warehouse = tep_db_fetch_array($query)){
          $warehouses[]=[
              'city'=>trim($warehouse['city']),
              'name'=>$warehouse['name'],
          ];
      }
      return $warehouses ? $this->drawSelects($warehouses) : '';
    }
    function drawSelects($warehouses){
      global $order;
      $selectCity = '<select id="np_cities">';
      $selectWarehouse = '<select id="np_warehouses">';
        $selectCityArray = $selectWarehouseArray = [];
      foreach ($warehouses as $warehouse){
          $selectedCity = $warehouse['city'] == $order->delivery['city'] ? ' selected' : '';
          $selectedWarehouse = ($selectedCity != "" && $warehouse['name'] == $order->delivery['street_address']) ? ' selected' : '';

          $selectCityArray[$warehouse['city']] =      '<option value="'.$warehouse['city'].'"'.$selectedCity.'>'.$warehouse['city'].'</option>';
          $selectWarehouseArray[$warehouse['city']][]= ['id'=>$warehouse['name'],'name'=>$warehouse['name']];
      }
      $selectWarehouseJSON = json_encode($selectWarehouseArray);
      $selectCity.=implode($selectCityArray);
        $selectCity .= '</select>';
        $selectWarehouse .= '</select>';
        $placeholder = MODULE_SHIPPING_NWPOSHTANEW_PLACEHOLDER;
        $scripts = <<<SCRIPT
    <script>
        var warehouses = $selectWarehouseJSON;
        var placeholder = '$placeholder';
        var sel1 = document.querySelector('#np_cities');
        var sel2 = document.querySelector('#np_warehouses');
        var selected = JSON.stringify($(sel2).find('option:selected').val());
        var options2 = sel2.querySelectorAll('option');
        var select_state;
        var select_city,  select_city_i;
         select_city = $('#np_warehouses').selectize({
            valueField: 'name',
            labelField: 'name',
            placeholder: placeholder,
            searchField: ['name'],
            onChange: function(value){
               $('[name="shipping_street_address"]').val(value).trigger('blur')
              }
        });
        
        select_city_i  = select_city[0].selectize;
        select_state = $('#np_cities').selectize({
          maxItems:1,
          hideSelected: true,
          sortField: [
              { field: "\$order", direction: "asc" }
          ],
          // openOnFocus: false,
          onDropdownClose:
              function(dropdown){
              $(dropdown).find('.selected').not('.active').removeClass('selected');
          },
          onChange: function(value){
                  if (!value.length) return;            
                  $('[name="shipping_city"]').val(value).trigger('blur');
  
                  select_city_i.disable();                
                  select_city_i.clear();
                  select_city_i.clearOptions()
                  select_city_i.addOption(warehouses[value]);
                  select_city_i.enable();
                      // console.log(warehouses[value]);
                      // callback(warehouses[value]);
          },
          onFocus: function(){
              this.clear();
          },
          onInitialize: function(){
              $('[name="shipping_city"]').val(sel1.value).trigger('blur');
              this.refreshItems();
            }
        });
        
        
        
        

       

</script>
SCRIPT;
        return $selectCity.PHP_EOL.$selectWarehouse.PHP_EOL.$scripts;
    }
// class methods
    function quote($method = '') {
      global $order;

      $this->quotes = array('id' => $this->code,
                            'module' => $this->title,
                            'methods' => array(array('id' => $this->code,
                                                     'title' => $this->title,
//                                                     'html' => $this->getWarehouses(),
                                                     'cost' => MODULE_SHIPPING_NWPOSHTANEW_COST)));
     if ($order->info['shipping_method'] == $this->title){
         $this->quotes['methods'][0]['html'] = $this->getWarehouses();
     }
      if ($this->tax_class > 0) {
        $this->quotes['tax'] = tep_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
      }

      if (tep_not_null($this->icon)) $this->quotes['icon'] = tep_image($this->icon, $this->title);

      return $this->quotes;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_NWPOSHTANEW_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    static function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable method', 'MODULE_SHIPPING_NWPOSHTANEW_STATUS', 'True', 'Do you want to enable this method?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Custom module name', 'MODULE_SHIPPING_NWPOSHTANEW_CUSTOM_NAME', '', 'Leave empty if you want to use default module name', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Cost', 'MODULE_SHIPPING_NWPOSHTANEW_COST', '5.00', 'Cost for this shipping method.', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax', 'MODULE_SHIPPING_NWPOSHTANEW_TAX_CLASS', '0', 'Use tax.', '6', '0', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone', 'MODULE_SHIPPING_NWPOSHTANEW_ZONE', '0', 'If zone is set, this module will available only for customers from selected zone.', '6', '0', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order', 'MODULE_SHIPPING_NWPOSHTANEW_SORT_ORDER', '0', 'Enter sort order for this module.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", static::keys()) . "')");
    }

    static function keys() {
      return array('MODULE_SHIPPING_NWPOSHTANEW_STATUS', 'MODULE_SHIPPING_NWPOSHTANEW_CUSTOM_NAME', 'MODULE_SHIPPING_NWPOSHTANEW_COST', 'MODULE_SHIPPING_NWPOSHTANEW_TAX_CLASS', 'MODULE_SHIPPING_NWPOSHTANEW_ZONE', 'MODULE_SHIPPING_NWPOSHTANEW_SORT_ORDER');
    }
  }
?>
