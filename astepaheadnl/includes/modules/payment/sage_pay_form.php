<?php

$rootDirectory = __DIR__ . "/../../../";
$pathToPaymentModule   = 'ext/acquiring/sage_pay_form/sage_pay_form.php';

if (CARDS_ENABLED == 'true' && file_exists($rootDirectory . $pathToPaymentModule)) {
    require_once $rootDirectory . $pathToPaymentModule;
} else {
    class sage_pay_form {
        var $code, $title, $description, $enabled;

        function __construct() {
        }

        function check() {
            return false;
        }
    }
}