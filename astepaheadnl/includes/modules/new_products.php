<?php  

   $listing_sql = "SELECT p.products_id
                     FROM " . TABLE_PRODUCTS . " p      
                LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.products_id = p2c.products_id                                   
                          " . ($new_products_from?:'') . " 
                    WHERE p2c.categories_id in(".$all_active_cats.")                             
                      AND p.products_status = '1' 
                 ORDER BY  ".($tpl_settings['orderby']?:'p.products_date_added DESC')." 
                           ".($tpl_settings['limit']?'LIMIT '.$tpl_settings['limit']:''); 

   $listing_sql = tep_get_query_products_info($listing_sql); // split query to 2 small queries: 1) find all products ids, 2) get info for each product

   $module_products = tep_db_query($listing_sql);  
   $salemakers_array = get_salemakers($module_products);
   mysqli_data_seek($module_products, 0);

   if($module_products->num_rows and $tpl_settings['disable_listing'] !=true) {
     $tpl_settings['request'] = $module_products;
     include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
   }  
  
?>