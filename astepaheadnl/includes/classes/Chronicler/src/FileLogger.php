<?php

namespace SoloMono\Chronicler;

/**
 * Class FileLogger
 *
 * @package SoloMono\Chronicler
 */
class FileLogger implements LoggerInterface
{
    /**
     * Log data separator
     */
    const SEPARATOR = " | ";

    /**
     * @var string
     */
    private $loggerDirectory;

    /**
     * @var string
     */
    private $loggerName;

    /**
     * FileLogger constructor.
     *
     * @param $loggerDirectory
     * @param $loggerName
     */
    public function __construct($loggerDirectory, $loggerName)
    {
        $this->loggerDirectory = $loggerDirectory;
        $this->loggerName      = $loggerName;
    }

    /**
     * @inheritDoc
     */
    public function critical($message, $context = [])
    {
        $this->log(LogLevels::CRITICAL, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function log($lvl, $message, $context = [])
    {
        $dateTime = date('c');
        foreach ($context as $key => $value) {
            if (is_string($value) || $this->isConvertibleToSting($value)) {
                $value = (string) $value;
                $message = str_replace("{" . $key . "}", $value, $message);
            }
        }
        $message = implode(static::SEPARATOR, [
            $dateTime,
            $lvl,
            $message,
        ]);
        file_put_contents(
            $this->loggerDirectory . DIRECTORY_SEPARATOR . $this->loggerName . ".log",
            $message . PHP_EOL,
            FILE_APPEND
        );
    }

    /**
     * @inheritDoc
     */
    public function emergency($message, $context = [])
    {
        $this->log(LogLevels::EMERGENCY, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function alert($message, $context = [])
    {
        $this->log(LogLevels::ALERT, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function error($message, $context = [])
    {
        $this->log(LogLevels::ERROR, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function warning($message, $context = [])
    {
        $this->log(LogLevels::WARNING, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function debug($message, $context = [])
    {
        $this->log(LogLevels::DEBUG, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function info($message, $context = [])
    {
        $this->log(LogLevels::INFO, $message, $context);
    }

    /**
     * @param mixed $var
     * @return bool
     */
    private function isConvertibleToSting($var) {
        return $var === null
            || is_scalar($var)
            || (is_object($var) && method_exists($var, '__toString'));
    }

}