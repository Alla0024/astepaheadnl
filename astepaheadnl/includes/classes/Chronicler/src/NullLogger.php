<?php

namespace SoloMono\Chronicler;

/**
 * Class NullLogger
 *
 * @package SoloMono\Chronicler
 */
class NullLogger extends FileLogger
{
    /**
     * @param string $lvl
     * @param string $message
     * @param array $context
     */
    public function log($lvl, $message, $context = [])
    {
    }
}