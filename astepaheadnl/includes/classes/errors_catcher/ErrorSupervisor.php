<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 26.10.2020
 * Time: 14:33
 */

class ErrorSupervisor
{
    /**
     * fatal errors constant
     */
    const E_FATAL =  E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_RECOVERABLE_ERROR;

    /**
     * Select or all errors
     * E_ALL | E_STRICT;
     * or FATAL and WARNING
     * self::E_FATAL | E_WARNING;
     */
    const ERROR_REPORTING = self::E_FATAL | E_WARNING;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * ErrorSupervisor constructor.
     */
    public function __construct()
    {
        // регистрация ошибок
        set_error_handler(array($this, 'otherErrorCatcher'));
        // перехват критических ошибок
        register_shutdown_function(array($this, 'fatalErrorCatcher'));
    }

    /**
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param string $errline
     * @return void
     */
    public function otherErrorCatcher($errno, $errstr, $errfile, $errline)
    {
        if(!($errno & self::ERROR_REPORTING)) {
            return;
        } else {
            $message = $this->makeMessage($errno, $errstr, $errfile, $errline);
            $this->setError($message);
        }
    }

    /**
     * fatalErrorCatcher
     */
    public function fatalErrorCatcher()
    {
        $error = error_get_last();

        if($error && ($error['type'] == E_ERROR
                || $error['type'] == E_PARSE
                || $error['type'] == E_COMPILE_ERROR
                || $error['type'] == E_CORE_ERROR)){

            $message = $this->makeMessage($error['type'], $error['message'], $error['file'], $error['line']);
            $this->setError($message);
            $this->sendMail();
            http_response_code(500);
        }
    }

    /**
     * sendMail
     */
    public function sendMail()
    {
        $str = '';
        if (count($this->errors)) {
            foreach ($this->errors as $error) {
                $str .= implode(PHP_EOL. '<br>', $error);
            }
//            file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'some.txt', $str, FILE_APPEND);
        }

        if ($str) {

            $from_email_name = $_SERVER["SERVER_NAME"];
            $from_email_address = 'site_error@'.$_SERVER["SERVER_NAME"];

            $boundary = md5( uniqid() . microtime() );

            $headers='MIME-Version: 1.0' . "\r\n";
            $headers.="From: " . mb_encode_mimeheader($from_email_name) . " <" . $from_email_address . ">\r\n";
            $headers.= "Content-Type: multipart/alternative; boundary=\"$boundary\"\r\n\r\n";

            // Plain text version of message
            $body = "--$boundary\r\n" .
                "Content-Type: text/plain; charset=utf-8\r\n" .//charset=ISO-8859-1
                "Content-Transfer-Encoding: base64\r\n\r\n";
            $body .= chunk_split( base64_encode( trim(preg_replace('/^[ \t]*[\r\n]+/m', '', strip_tags($str))) ) );
            // HTML version of message
            $body .= "--$boundary\r\n" .
                "Content-Type: text/html; charset=utf-8\r\n" . //charset=ISO-8859-1
                "Content-Transfer-Encoding: base64\r\n\r\n";
            $body .= chunk_split( base64_encode('<html>'.$str.'</html>') );
            $body .= "--$boundary--";
            @mail('admin@solomono.net, maksym.sapozhnikov@gmail.com', '=?utf-8?B?' . base64_encode('solomono error report') . '?=', $body, $headers,'-f '.$from_email_address);
        }
    }

    /**
     * @param string $message
     */
    private function setError($message)
    {
//        file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'some.txt', $message . PHP_EOL, FILE_APPEND);
        $error =  [
            'url' => $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
            'message' => strip_tags($message)
        ];

        $this->errors[] = $error;
    }

    /**
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param string $errline
     * @return string
     */
    private function makeMessage($errno, $errstr, $errfile, $errline)
    {
        switch ($errno){

            case E_ERROR: // 1 //
                $typestr = 'E_ERROR'; break;
            case E_WARNING: // 2 //
                $typestr = 'E_WARNING'; break;
            case E_PARSE: // 4 //
                $typestr = 'E_PARSE'; break;
            case E_NOTICE: // 8 //
                $typestr = 'E_NOTICE'; break;
            case E_CORE_ERROR: // 16 //
                $typestr = 'E_CORE_ERROR'; break;
            case E_CORE_WARNING: // 32 //
                $typestr = 'E_CORE_WARNING'; break;
            case E_COMPILE_ERROR: // 64 //
                $typestr = 'E_COMPILE_ERROR'; break;
            case E_CORE_WARNING: // 128 //
                $typestr = 'E_COMPILE_WARNING'; break;
            case E_USER_ERROR: // 256 //
                $typestr = 'E_USER_ERROR'; break;
            case E_USER_WARNING: // 512 //
                $typestr = 'E_USER_WARNING'; break;
            case E_USER_NOTICE: // 1024 //
                $typestr = 'E_USER_NOTICE'; break;
            case E_STRICT: // 2048 //
                $typestr = 'E_STRICT'; break;
            case E_RECOVERABLE_ERROR: // 4096 //
                $typestr = 'E_RECOVERABLE_ERROR'; break;
            case E_DEPRECATED: // 8192 //
                $typestr = 'E_DEPRECATED'; break;
            case E_USER_DEPRECATED: // 16384 //
                $typestr = 'E_USER_DEPRECATED'; break;
        }

        $message =
            '<b>' . PHP_EOL . $typestr .
            ': </b>'. PHP_EOL . $errstr .
            ' in <b>'. PHP_EOL . $errfile .
            '</b>' . PHP_EOL . 'on line <b>' . $errline .
            '</b>' . PHP_EOL . '<br/>';

        return $message;
    }
}