function googleStartAuth() {
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: googleClientID
            // Scopes to request in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        auth2.grantOfflineAccess().then(googleSignInCallback);
    });
}

function googleSignInCallback(authResult) {
    if(authResult['code']) {
        $.ajax({
            type: 'POST',
            url: googleRedirectUri,
            dataType: 'json',
            data: {
                code: authResult['code']
            },
            success: function (result) {
                if(result.success) {
                    checkLoginvk(result.id, result.firstname, result.lastname, result.picture, result.email, '', '');
                }else {
                    console.log(result.message);
                }
            }
        });
    }else {
        console.log('Error google authResult');
    }
}

$(document).on('click', '.googleSigninButton', function () {
    var script = '&lt;script src="https://apis.google.com/js/client:platform.js?onload=googleStartAuth" async defer>&lt;\/script>';
    $('body').append(script.replace(/&lt;/g, '<'));
});