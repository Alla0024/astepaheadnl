<!-- scripts for
  - HTML5,
  - media-queries,
  - box-sizing,
  - background-size -->
<!--[if lt IE 9]>

<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<script src ="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://raw.githubusercontent.com/srobbin/jquery-backstretch/master/jquery.backstretch.min.js"></script>
<![endif]-->

<?php
          // MINIFY_CSSJS: 0 - not minify, 1 - minify always, 2 - use before minified version, not use minify

          $js_array = [
//              'includes/javascript/lib/jquery-3.3.1.min.js', // <!-- Jquery Library-->
              'includes/javascript/lib/jquery-3.5.1_fix_eventlistenter.min.js', // <!-- Jquery Library-->
              'includes/javascript/jqueryui/js/jquery-ui-1.12.1.custom.min.js', // <!-- Include all compiled plugins (below), or include individual files as needed -->
              'includes/javascript/bootstrap-tabcollapse.js',
              'includes/javascript/bootstrap-number-input.js',
              'includes/javascript/functions.js', // <!-- Functions -->
              'includes/javascript/init.js', // <!-- Initialization -->
              'includes/javascript/superfish/superfish.js', // <!-- PLUGINS -->
              'includes/javascript/superfish/jquery.hoverintent.js',
              'includes/javascript/lib/jquery.form.js',
              'includes/javascript/lib/jquery.autocomplete.js',
              'includes/javascript/owl-carousel/owl.carousel.min.js',
              'includes/javascript/lib/jquery.unveil.js',
              'includes/javascript/accordion/js/jquery.dcjqaccordion.js',
              'includes/javascript/lazyload.js',
              'includes/javascript/selectize/selectize.min.js'
          ];
          if(defined('PRODUCT_MODAL_ON') && constant('PRODUCT_MODAL_ON') == 'true'){
            $js_array[]='includes/javascript/product_modal.js';
          }
          if($to_date) {
            $js_array[] = 'includes/javascript/jquery.countdown/jquery.countdown.min.js';
            $js_array[] = 'includes/javascript/jquery.countdown/initialization.js';
          }

          if(TEMPLATE_NAME=='default2') {
            $js_array[] = 'includes/javascript/Popper.js';
            $js_array[] = 'includes/javascript/bootstrap4.min.js';
            $js_array[] = 'includes/javascript/OverlayScrollbars/jquery.overlayScrollbars.min.js';
          }else {
            $js_array[] = 'includes/javascript/lib/bootstrap.min.js';
          }

          //template js:
          if(file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME .'/js/custom.js'))
            $js_array[] = DIR_WS_TEMPLATES . TEMPLATE_NAME.'/js/custom.js';
          if(file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME .'/js/simplebar.js'))
            $js_array[] = DIR_WS_TEMPLATES . TEMPLATE_NAME.'/js/simplebar.js';

          if(defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true')
            $js_array[] = 'ext/compare/compare.js';
          if(defined('WISHLIST_MODULE_ENABLED') && WISHLIST_MODULE_ENABLED == 'true')
              $js_array[] = 'ext/wishlist/wishlist.js';
          if(defined('MULTICOLOR_ENABLED') && MULTICOLOR_ENABLED == 'true')
            $js_array[] = 'ext/multicolor/multicolor.js';
          if (getenv('APP_ENV')=='production' or getenv('APP_ENV')=='demo') {
            $js_array[] = 'includes/javascript/Spectrum/spectrum.min.js';
            $js_array[] = 'admin/includes/javascript/colorpicker/js/colorpicker.js';

          }

          // No minified scripts (because they depends on specific pages):
          if ($content == 'checkout') {
            if(defined('MODULE_PAYMENT_STRIPE_STATUS') &&  MODULE_PAYMENT_STRIPE_STATUS == 'True') {
              $js_nominify_array[] = 'https://js.stripe.com/v2/';
            }
            if(defined('MODULE_PAYMENT_PAYLIKE_STATUS') && MODULE_PAYMENT_PAYLIKE_STATUS == 'True') {
              $js_nominify_array[] = 'https://sdk.paylike.io/3.js';
              $js_nominify_array[] = DIR_WS_JAVASCRIPT.'paylike.js';
            }

          }

            if (GOOGLE_TAGS_ID_STATUS === 'True' && tep_not_null(GOOGLE_TAGS_ID)) {
                $js_array[] = 'https://www.googletagmanager.com/gtag/js?id='.GOOGLE_TAGS_ID;
            }else{
                echo "<script>function gtag() {return false;}</script>";
            }

          if(($_GET['cPath'] != '' or isset($_GET['manufacturers_id']) or isset($_GET['keywords'])) and ATTRIBUTES_PRODUCTS_MODULE_ENABLED == 'true') {
              if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {
                  $js_nominify_array[] = 'ext/filter/filter_seo.js'; // <!--  Scripts for refreshing products-listing -->

              } else {
                  $js_nominify_array[] = 'ext/filter/filter.js'; // <!--  Scripts for refreshing products-listing -->
              }
          }
          if (isset($_GET['products_id'])) {
            $js_nominify_array[] = 'includes/javascript/lightbox/lightbox.min.js';
            $js_nominify_array[] = 'includes/modules/rating/rating.js';
          }
        if(defined('GOOGLE_OAUTH_STATUS') && GOOGLE_OAUTH_STATUS == 'true'){
            echo "<script>var googleClientID = '".$googleClientID."';var googleRedirectUri = '".$googleRedirectUri."';</script>\n\t";
            $js_array[] = 'includes/javascript/google_oauth.js';
        }

          if(MINIFY_CSSJS==0 || !is_file(DIR_WS_EXT . 'minifier/minifier.php')) { // show all scripts SEPARATELY
            if(!empty($js_nominify_array)) $js_array = array_merge($js_array, $js_nominify_array);
            foreach($js_array as $js) echo "<script type=\"text/javascript\" src=\"".$js."\"></script>\n\t";
          } elseif(MINIFY_CSSJS==1) {

              foreach ($js_array as $k => $js) {
                  $js_array[$k] = file_get_contents($js);
                  if (!$js_array[$k]) $js_array[$k] = curl_get_contents($js); // if not allow_url_fopen

              }

              if (file_exists(DIR_WS_EXT . 'minifier/minifier.php')) {
                  require_once(DIR_WS_EXT . 'minifier/minifier.php');
                  $minifierJS->add(implode(' ', $js_array));
                  file_put_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/js/all.min.js', $minifierJS->minify());
                  tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '2' where configuration_key = 'MINIFY_CSSJS'");
              }
          }
          if(MINIFY_CSSJS!=0 && is_file(DIR_WS_EXT . 'minifier/minifier.php')) {
            echo "<script src=\"".DIR_WS_TEMPLATES . TEMPLATE_NAME."/js/all.min.js?v=".$versionTimestamp."\" ></script>\n\t";
            if(is_array($js_nominify_array)) {
              foreach($js_nominify_array as $k => $js) {
                if( (int) ini_get("allow_url_fopen") !== 0) {
                  $js_nominify_array[$k] = file_get_contents($js);
                } else {
                  $js_nominify_array[$k] = curl_get_contents($js); // if not allow_url_fopen
                }
              }
              echo "<script>".implode("\n\t",$js_nominify_array)."</script>\n\t";
            }
          }

          // push constants to JS:
          if(!empty($array_for_js)) {
          	foreach($array_for_js as $js_k => $js_val) $for_js .= 'const '.$js_k.' = "'.$js_val.'"; ';
            echo '<script>'.$for_js.'</script>';
          }

          // sliders scripts from products_listing:
          if($sliders_output!='') echo '<script>'.$sliders_output.'</script>';

          // comments js:
          if($comment_aj_js!='') echo '<script>'.$comment_aj_js.'</script>';


        	// scripts from admin
        	echo renderArticle('scripts');
?>

<?php if(class_exists("\JsonLd\Container"))
    \JsonLd\Container::generate(); ?>

<script type="text/javascript">
  var LongScriptsLoaded = (document.cookie.match(/^(?:.*;)?\s*LongScriptsLoaded\s*=\s*([^;]+)(?:.*)?$/)||[,null])[1];

  $(document).ready(function() {

     if (!LongScriptsLoaded) {
        var cookie_date = new Date ( );
        cookie_date.setTime ( cookie_date.getTime()+60*60*28*1000); //24 hours
        document.cookie = "LongScriptsLoaded=1;path=/;expires=" + cookie_date.toGMTString();
     }

     setTimeout(function() {
         $.ajaxSetup({cache: true});
        <?php if(SOCIAL_WIDGETS_ENABLED == 'true' and (!isMobile() or $content != 'index_default')) { // dont start on mobile main page ?>
        // facebook:
        $.getScript("//connect.facebook.net/<?php echo $lng->language['code']; ?>_<?php echo strtoupper($lng->language['code']); ?>/sdk.js", function(){
          FB.init({
            appId: '<?php echo $fb_app_id;?>',
            xfbml: true,
            version: 'v3.2'
          });
          //   FB.Event.subscribe('edge.create', function(response) {alert('You will get discount 5%');});
          //   FB.Event.subscribe('edge.remove',function(response) {alert('Your discount 5% was canceled!');});
        });
        <?php   
              } ?>


         $('.youtube_iframe').each(function () {
             $(this).attr('src',$(this).attr('data-src'));
         });

       <?php if(defined('FACEBOOK_PIXEL_MODULE_ENABLED') && FACEBOOK_PIXEL_MODULE_ENABLED === 'true') include "ext/facebook_pixel/facebook_pixel.php"; ?>
       <?php if(defined('GOOGLE_TAGS_ID_STATUS') && GOOGLE_TAGS_ID_STATUS === 'True') require(DIR_WS_INCLUDES.'google_tags.php'); ?>

       <?php if(file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/css/fonts.css')) { ?>
       if (!LongScriptsLoaded) {
         $("head").append("<link href='<?php echo DIR_WS_TEMPLATES . TEMPLATE_NAME;?>/css/fonts.css' rel='stylesheet' type='text/css'>"); // fonts
       }
       <?php } ?>
       //JIVOSITE
         <?php
         
         if(is_file($rootPath . "ext/jivosite/jivosite.php")) {
             require_once $rootPath . "ext/jivosite/jivosite.php";
         } ?>
         //END JIVOSITE
         renderCustomizationPanel();
      },3000);

      var mainPageModules = [];
      $('.ajax-module-box').each(function () {
          mainPageModules.push($(this).attr('data-module-id'));
      });
      blockUnveil(mainPageModules, 100);
      $(window).scroll(function() {
          blockUnveil(mainPageModules, 100);
      });

      <?php if(GOOGLE_GOALS_ADD_TO_CART === 'true') {?>
          $('body').on('click', '.add2cart', function (event) {
              gtag('event', 'add_to_cart');
          });
          $('body').on('submit', 'form[name="cart_quantity"]', function(event) {
              gtag('event', 'add_to_cart');
          });
      <?php }
      if($content == 'checkout' && GOOGLE_GOALS_ON_CHECKOUT === 'true'){?>
          $(document).ready(function() {
              gtag('event', 'checkout');
          });
      <?php }
      if(GOOGLE_GOALS_CLICK_ON_CHAT === 'true') {?>
          $('body').on('click', '#jvlabelWrap, .online', function (event) {
              gtag('event', 'click_chat');
          });
      <?php }?>
  });
  var GOOGLE_GOALS_CHECKOUT_PROCESS = <?= json_encode(GOOGLE_GOALS_CHECKOUT_PROCESS === 'true' ? true : false )?>;
  var GOOGLE_GOALS_CLICK_ON_PHONE_DESKTOP = <?= json_encode(GOOGLE_GOALS_CLICK_ON_PHONE_DESKTOP === 'true' ? true : false )?>;
  var GOOGLE_GOALS_CLICK_ON_PHONE_MOBILE = <?= json_encode(GOOGLE_GOALS_CLICK_ON_PHONE_MOBILE === 'true' ? true : false )?>;
</script>