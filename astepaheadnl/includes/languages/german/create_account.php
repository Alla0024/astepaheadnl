<?php
/*
  $Id: create_account.tpl.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// guest_account start
if ($guest_account == false) { // Not a Guest Account
define('NAVBAR_TITLE', 'Registrierung');
} else {
  define('NAVBAR_TITLE', 'Bestellung');
}
// guest_account endet
define('HEADING_TITLE', 'Meine Daten');
define('CR_ENTER', 'Anmelden');
define('CR_THX', 'Danke, du bist erfolgreich registriert!');
define('CR_IF', 'wenn Sie bereits registriert sind.');
// guest_account end
define('TEXT_ORIGIN_LOGIN', '<span><span><small><b>ACHTUNG:</b></small></span>&nbsp;Sie müssen das Anmeldeformular ausfüllen. Dies gibt Ihnen die Möglichkeit, eine Bestellung in unserem Online-Shop, br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Wenn Sie bereits auf unserer Website registriert sind, geben Sie bitte Ihren Benutzernamen und Ihr Passwort ein&nbsp;</span><a href="%s"><u>hier</u></a>. ');
define('EMAIL_SUBJECT', 'Willkommen in' . STORE_NAME);
define('EMAIL_GREET_MR', 'Sehr geehrte %s! <br><br>');
define('EMAIL_GREET_MS', 'Sehr geehrte %s! <br><br>');
define('EMAIL_GREET_NONE', 'Sehr geehrte %s! <br><br>');
define('EMAIL_WELCOME', 'Wir freuen uns, Sie zum Online-Shop einladen <b>' . STORE_NAME . '</b>. <br><br>');
define('EMAIL_CONTACT', 'Wenn Sie Fragen haben, schreiben Sie:' . STORE_OWNER_EMAIL_ADDRESS . '.<br><br>');
define('EMAIL_TEXT', 'Jetzt können Sie Einkäufe tätigen.' . "\n");
define('EMAIL_WARNING', '');

/* ICW Credit class gift voucher begin */
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Wir teilen Ihnen auch mit, dass Sie ein Zertifikat für den Betrag von %s erhalten');
define('EMAIL_GV_REDEEM', 'Der Code Ihres Zertifikats %s, Sie können Ihr Zertifikat verwenden, wenn Sie die Bestellung bezahlen, der Nennwert des Zertifikats wird als Zahlung für die gesamte Bestellung oder als Teil der Kosten Ihrer Bestellung enthalten sein.');
define('EMAIL_GV_LINK', 'Folgen Sie dem Link, um das Zertifikat zu aktivieren:');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Herzlichen Glückwunsch zur Registrierung in unserem Geschäft, wir freuen uns, Ihnen mitteilen zu können, dass wir Ihnen einen Gutschein für einen Rabatt in unserem Geschäft geben.' . "\n" .
                                        ' Dieser Gutschein ist nur für Sie gültig. ' . "\n");
define('EMAIL_COUPON_REDEEM', 'Um den Coupon zu verwenden, müssen Sie den Coupon-Code angeben, mit dem Sie Ihre Bestellung aufgeben, um einen Rabatt zu erhalten.' . "\n" . 'Ihr Gutschein-Code: %s');
define('CR_LOGIN', 'Dein Login (E-Mail)');
define('CR_PASS', 'Dein Passwort');
define('CR_ADD_EMAIL', 'Um die Registrierung abzuschließen<br /> geben Sie bitte Ihre <b>e-mail:</b>');
define('CR_SUBMIT', 'Confirm');
/* ICW Credit class gift voucher end */