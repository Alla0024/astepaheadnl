<?php
/*
  $Id: article_info.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_ARTICLE_NOT_FOUND', 'Artikel nicht gefunden');
define('TEXT_ARTICLE_NOT_FOUND', 'Sorry, aber der gewünschte Artikel ist nicht verfügbar!');
define('TEXT_DATE_ADDED', 'Dieser Artikel wurde von %s veröffentlicht.');
define('TEXT_DATE_AVAILABLE', 'Dieser Artikel wird für verfügbar sein  %s.');
define('TEXT_BY', 'by');