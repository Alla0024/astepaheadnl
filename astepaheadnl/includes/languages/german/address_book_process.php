<?php
/*
  $Id: address_book_process.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Meine Daten');
define('NAVBAR_TITLE_2', 'Adressbuch');
define('NAVBAR_TITLE_ADD_ENTRY', 'Neuer Eintrag');
define('NAVBAR_TITLE_MODIFY_ENTRY', 'Eintrag bearbeiten');
define('NAVBAR_TITLE_DELETE_ENTRY', 'Eintrag löschen');
define('HEADING_TITLE_ADD_ENTRY', 'Neuer Eintrag');
define('HEADING_TITLE_MODIFY_ENTRY', 'Eintrag bearbeiten');
define('HEADING_TITLE_DELETE_ENTRY', 'Eintrag löschen');
define('DELETE_ADDRESS_TITLE', 'Adresse löschen');
define('DELETE_ADDRESS_DESCRIPTION', 'Möchten Sie die ausgewählten Adressen wirklich aus dem Adressbuch löschen?');
define('NEW_ADDRESS_TITLE', 'Neuer Eintrag');
define('SELECTED_ADDRESS', 'Ausgewählte Adresse');
define('SET_AS_PRIMARY', 'Als Standard festlegen.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'Die ausgewählte Adresse wurde aus dem Adressbuch gelöscht.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Ihr Adressbuch wurde aktualisiert.');
define('WARNING_PRIMARY_ADDRESS_DELETION', 'Die Standardadresse kann nicht gelöscht werden. Setzen Sie den Standardstatus auf eine andere Adresse und versuchen Sie es erneut.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'Adressbuch nicht gefunden.');
define('ERROR_ADDRESS_BOOK_FULL', 'Ihr Adressbuch ist vollständig ausgefüllt. Löschen Sie die Adresse, die Sie nicht benötigen und nur dann können Sie eine neue Adresse hinzufügen.');