<?php
/*
  $Id: password_forgotten.php,v 1.8 2003/06/09 22:46:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Login');
define('NAVBAR_TITLE_2', 'Passwortwiederherstellung');
define('HEADING_TITLE', 'Ich habe mein Passwort vergessen!');
define('TEXT_MAIN', 'Wenn Sie Ihr Passwort vergessen haben, geben Sie Ihre E-Mail-Adresse und wir werden Ihre parol auf E-Mail senden, die Sie angegeben haben.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', '<span><b> Fehler:. </b></span> E-Mail-Adresse nicht von Ihnen paßt, versuchen Sie es erneut Konto');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - ist dein Passwort');
define('EMAIL_PASSWORD_REMINDER_BODY', 'Antrag auf ein neues Passwort aus dem empfangenen worden ' . $REMOTE_ADDR . '.' . "\n\n" .  'Ihr neues Passwort in dem \'' . STORE_NAME . '\' :' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'Abgeschlossen: Ihr neues Passwort per E-Mail zugeschickt.');