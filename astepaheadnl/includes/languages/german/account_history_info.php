<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Meine Daten');
define('NAVBAR_TITLE_2', 'Bestellhistorie');
define('NAVBAR_TITLE_3', 'Bestellinformation');
define('HEADING_TITLE', 'Bestellinformation');
define('HEADING_ORDER_NUMBER', 'Bestellnummer% s');
define('HEADING_ORDER_DATE', 'Datum');
define('HEADING_ORDER_TOTAL', 'Bestellwert:');
define('HEADING_DELIVERY_ADDRESS', 'Lieferadresse');
define('HEADING_SHIPPING_METHOD', 'Zustellmethode');
define('HEADING_PRODUCTS', 'Produkt');
define('HEADING_TAX', 'Tax');
define('HEADING_TOTAL', 'Total');
define('HEADING_PAYMENT_METHOD', 'Zahlungsart');
define('HEADING_ORDER_HISTORY', 'Bestellhistorie');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Der Link ist gültig bis:');
define('TABLE_HEADING_DOWNLOAD_COUNT', 'mal kannst du die Datei hochladen.');
define('TABLE_HEADING_DATE_ADDED', 'Datum');
define('TABLE_HEADING_STATUS', 'Bestellstatus');
define('TABLE_HEADING_COMMENTS', 'Kommentare');