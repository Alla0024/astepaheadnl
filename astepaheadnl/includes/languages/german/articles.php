<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');

if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
  define('HEADING_TITLE', $topics['topics_name']);
  define('TEXT_NO_ARTICLES', 'Es gibt momentan keine Artikel in diesem Abschnitt.');
  define('TEXT_ARTICLES', 'Artikelliste:');
  define('TEXT_DATE_ADDED', 'Veröffentlicht:');
  define('TEXT_TOPIC', 'Abschnitt');
  define('TEXT_BY', 'Autor:');
} elseif ($topic_depth == 'top') {
  define('HEADING_TITLE', 'Alle Artikel');
  define('TEXT_CURRENT_ARTICLES', '');
  define('TEXT_UPCOMING_ARTICLES', 'Artikel, die demnächst veröffentlicht werden.');
  define('TEXT_NO_ARTICLES', 'Keine Artikel verfügbar.');
  define('TEXT_DATE_ADDED', 'Veröffentlicht:');
  define('TEXT_DATE_EXPECTED', 'Erwartet:');  
  define('TEXT_TOPIC', 'Abschnitt:');
  define('TEXT_BY', 'by');
} elseif ($topic_depth == 'nested') {
  define('HEADING_TITLE', 'Artikel');
}
  define('TOTAL_ARTICLES', 'Total articles');
  define('HEADING_TITLE', 'Artikel');