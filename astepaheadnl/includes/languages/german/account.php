<?php
/*
  $Id: account.php
*/

define('NAVBAR_TITLE', 'Meine Daten');
define('HEADING_TITLE', 'Meine Daten');
define('MY_ACCOUNT_TITLE', 'Meine Daten');

define('MY_ACCOUNT_PASSWORD', 'Passwort ändern.');
define('MY_ORDERS_VIEW', 'Meine Bestellungen anzeigen.');
define('MY_NAVI', 'Menü');
define('MY_INFO', 'Meine Informationen');
define('EDIT_ADDRESS_BOOK', 'Adresse bearbeiten');