<?php
/*
  $Id: zones.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SHIPPING_ZONES_TEXT_TITLE', 'Tarife für die Zone');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION', 'Zonenbasisrate');
define('MODULE_SHIPPING_ZONES_TEXT_WAY', 'Lieferung an');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS', 'Kg.');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE', 'Es gibt keine Lieferoption für das ausgewählte Land');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE', 'Weiterleitungskosten können jetzt nicht ermittelt werden');