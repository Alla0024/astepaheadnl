<?php
//
// +----------------------------------------------------------------------+
// | Better Together discount strings                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 That Software Guy                                 |
// +----------------------------------------------------------------------+
// | Released under the GNU General Public License.                       |
// +----------------------------------------------------------------------+
//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE', 'Companion Discount');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION', 'Companion Discount');
define('TWOFER_PROMO_STRING', 'Kaufe dieses Produkt und erhalte ein weiteres gratis!');
define('TWOFER_QUALIFY_STRING', "Du kannst ein weiteres %s kostenlos bekommen");
define('OFF_STRING_PCT', '- %s'); // z.B. um 50% reduziert
define('OFF_STRING_CURR', '- %s'); // z.B. $ 20 Rabatt
define('SECOND', 'second'); // wenn beide gleich sind
define('FREE_STRING', 'kostenlos'); // d. abbezahlen
// Reverse defs
define('REV_GET_ANY', 'Kaufe ein Produkt von');
define('REV_GET_THIS', 'Buy');
define('REV_GET_DISC', ', hole dieses Produkt');
// Kein Kontext (von der Produktinformationsseite)
define('FREE', 'for free');
define('GET_YOUR_PROD', "get");
define('GET_YOUR_CAT', "Wähle deine Auswahl");