<?php
/*
  $Id: ot_gv.php,v 1.0 2002/04/03 23:09:49 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_QTY_DISCOUNT_TITLE', 'Rabatt');
define('MODULE_QTY_DISCOUNT_DESCRIPTION', 'Rabatt abhängig von der Menge des gekauften Artikels.');