<?php
/*
  $Id: webmoney.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_WEBMONEY_TEXT_TITLE', 'WebMoney');
if(!defined('MODULE_PAYMENT_WEBMONEY_1')) define('MODULE_PAYMENT_WEBMONEY_1', '---');
if(!defined('MODULE_PAYMENT_WEBMONEY_2')) define('MODULE_PAYMENT_WEBMONEY_2', '---');
if(!defined('MODULE_PAYMENT_WEBMONEY_3')) define('MODULE_PAYMENT_WEBMONEY_3', '---');

define('MODULE_PAYMENT_WEBMONEY_TEXT_DESCRIPTION', 'Unsere Daten im WebMoney-System: <br><br> Unsere WM-ID: &nbsp;&nbsp;&nbsp;<b>' . MODULE_PAYMENT_WEBMONEY_1 . '</b><br> Unsere R-Nummer: &nbsp;&nbsp;&nbsp;<b>' . MODULE_PAYMENT_WEBMONEY_2 . '</b><br>Nummer unserer Z-Geldbörse: &nbsp;&nbsp;&nbsp;<b>' . MODULE_PAYMENT_WEBMONEY_3 . '</b><br><br> Für Zahlung der Bestellung, die Sie benötigen, um Geld in unsere Geldbörse R oder Z im WebMoney-System zu überweisen, und in der Notiz angeben <b>Ihre Bestellnummer</b><br><br><span>Ihre Bestellnummer wird an Ihre E-Mail gesendet Adresse</span><br><br><span>Die Übersetzung muss OHNE Transaktionsschutz sein.</span><br><br>So tätigen Sie eine Übertragung:<br>Um eine Übertragung durchzuführen, öffnen Sie WM Keeper und klicken Sie mit der rechten Maustaste auf die Geldbörse, von der Sie übertragen möchten, und wählen Sie dann im Menü Geld übertragen - Um die Geldbörse WebMoney. Geben Sie im angezeigten Dialogfenster den Überweisungsbetrag und die Nummer unserer Geldbörse ein, und geben Sie in der Notiz <b>Ihre Bestellnummer an</b>');
define('MODULE_PAYMENT_WEBMONEY_TEXT_EMAIL_FOOTER', "Unsere Daten im System WebMoney: \n\nUnser WM ID: " . MODULE_PAYMENT_WEBMONEY_1 . "\nZimmer unsere R Geld: " . MODULE_PAYMENT_WEBMONEY_2 . "\nZimmer unser Z Geld: " . MODULE_PAYMENT_WEBMONEY_3 . "\n\nAus Zahlungsauftrag müssen Sie einen Transfer von Geldern an unserer R oder Z-Geldbörse in dem WebMoney-System machen und in einer Note geben Sie Ihre Auftragsnummer\n\nZimmer Ihre Bestellung an Ihrer E-Mail-Adresse\n\nGeldtransfer ohne den Schutz der Transaktion\n\nBenutzen sein gesendet werden macht einen Transfer:\nFür die Begehung Transfer offen WM Keeper und die rechte Maustaste auf dem Geldbeutel aus dem Sie produ wollen Führen Übersetzung, und wählen Sie dann aus dem Menü Transfer, Geld - Als WebMoney Geldbeutel im Dialogfeld geben Sie den Betrag der Übertragung und die Zahl unserer Geldbeutel, und geben Sie in der Notiz Ihrer Bestellnummer.");