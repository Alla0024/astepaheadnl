<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_STRIPE_TEXT_TITLE', 'Stripe');
define('MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION', 'Stripe - Online-Zahlungen');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER', 'Kreditkarteninhaber:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER', 'Kreditkartennummer:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM', 'Ablauf MM:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY', 'Ablauf YY:');
define('MODULE_PAYMENT_STRIPE_TEXT_ERROR', 'Kreditkartenfehler!');