<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Beschreibung');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Modul aktivieren / deaktivieren');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Standardstatus');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Händler-Konto');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Händlergeheimnis');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'Die Sortierreihenfolge');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Zahlungsstatus');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'True - aktivieren, False - deaktivieren');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Der Status, der beim Erstellen der Bestellung festgelegt wird');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Händler-Konto');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Händlergeheimnis');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Sortierreihenfolge des Moduls im Block mit Zahlungsmodulen');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'Der Status, der bei erfolgreicher Zahlung auf die Bestellung gesetzt wird');