<?php
/*
  $Id: address_book.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Meine Adressen');
define('NAVBAR_TITLE_2', 'Adressbuch');
define('HEADING_TITLE', 'Mein Adressbuch');
define('PRIMARY_ADDRESS_TITLE', 'Deine Adresse');
define('PRIMARY_ADDRESS_DESCRIPTION', 'Diese Adresse ist standardmäßig festgelegt. <br>Sie wird verwendet, wenn Sie Einkäufe oder andere Aktionen in unserem Geschäft tätigen.');
define('ADDRESS_BOOK_TITLE', 'Adressbucheinträge');
define('PRIMARY_ADDRESS', '(*)');
define('TEXT_MAXIMUM_ENTRIES', '<b>HINWEIS:</b> Die maximale Größe des Adressbuchs ist <b>%s</b> Einträge');