<?php
/*
  $Id: account_history.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Meine Daten');
define('NAVBAR_TITLE_2', 'Bestellhistorie');
define('HEADING_TITLE', 'Bestellhistorie');
define('TEXT_ORDER_STATUS', 'Auftragsstatus');
define('TEXT_ORDER_DATE', 'Bestelldatum');
define('TEXT_ORDER_SHIPPED_TO', 'Lieferadresse:');
define('TEXT_ORDER_BILLED_TO', 'Käufer:');
define('TEXT_ORDER_COST', 'Summe');
define('TEXT_ORDER_PC', 'pcs.');
define('TEXT_NO_PURCHASES', 'Du hast leider noch nichts bei uns gekauft.');