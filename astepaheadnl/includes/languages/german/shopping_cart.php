<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Warenkorb');
define('HEADING_TITLE', 'Mein Warenkorb');
define('TABLE_HEADING_REMOVE', 'Delete');
define('TABLE_HEADING_REMOVE_FROM', 'aus dem Papierkorb');
define('TABLE_HEADING_QUANTITY', 'Menge');
define('TABLE_HEADING_IMAGE', 'Bild');
define('TABLE_HEADING_NAME', 'Name');
define('TABLE_HEADING_PRICE', 'Preis');
define('TABLE_HEADING_PRICE_TOTAL', 'Preis');
define('TABLE_HEADING_MODEL', 'Artikelcode');
define('TABLE_HEADING_PRODUCTS', 'Produkte');
define('TABLE_HEADING_TOTAL', 'Cost');
define('TEXT_CART_EMPTY', 'Dein Warenkorb ist leer!');
define('SUB_TITLE_COUPON', 'Coupon');
define('SUB_TITLE_COUPON_SUBMIT', 'Apply');
define('SUB_TITLE_COUPON_VALID', 'Congratulations! <br />Sie den richtigen Code eingegeben!');
define('SUB_TITLE_COUPON_INVALID', 'Sie haben den falschen Code eingegeben!');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'Waren markiert ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' in unserem Lager nicht in ausreichenden Mengen für Ihre Bestellung.<br>Bitte ändern Sie die Anzahl der Produkte ausgewählt (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), Danke');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Waren der Aufschrift‘. STOCK_MARK_PRODUCT_OUT_OF_STOCK. ‚für Ihre Bestellung in unserem Lager in unzureichender Menge vorhanden. <br> Allerdings können Sie sie kaufen und überprüfen Sie die Menge von zur schrittweisen Lieferung in der Umsetzung Ihrer Bestellung.');