<?php
/*
  Created by: Linda McGrath osCOMMERCE@WebMakers.com
  
  Update by: fram 05-05-2003
  Updated by: Donald Harriman - 08-08-2003 - MS2

  down_for_maintenance.php v1.1
  
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Das Geschäft ist wegen Wartungsarbeiten geschlossen.');
define('HEADING_TITLE', 'Der Laden ist vorübergehend geschlossen ...');
define('DOWN_FOR_MAINTENANCE_TEXT_INFORMATION', 'Technische Arbeit wird auf dem Server ausgeführt, komm später wieder.');
define('TEXT_MAINTENANCE_ON_AT_TIME', 'Abschlussdatum:');
define('TEXT_MAINTENANCE_PERIOD', 'Der Store setzt die Arbeit fort:');
define('DOWN_FOR_MAINTENANCE_STATUS_TEXT', 'Klicken Sie auf die Schaltfläche "Weiter", vielleicht funktioniert der Laden bereits:');