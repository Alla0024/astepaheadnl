<?php
/*
  $ Id: wishlist.php, v 3.0 2005/04/20 Dennis Blake
  osCommerce, Open Source E-Commerce Lösungen
  http://www.oscommerce.com

  Veröffentlicht unter der GNU General Public License
*/

define('HEADING_TITLE', 'aufgeschobene Waren');
define('BOX_TEXT_PRICE', 'Kosten');
define('BOX_TEXT_PRODUCT', 'Name');
define('BOX_TEXT_IMAGE', 'Bild');
define('BOX_TEXT_NO_ITEMS', 'Keine offenen Posten.');
define('TEXT_EMAIL', 'Email:');
define('TEXT_MESSAGE', 'Nachricht:');
define('TEXT_ITEM_IN_CART', 'Artikel im Warenkorb');
define('TEXT_ITEM_NOT_AVAILABLE', 'Produkt ist nicht mehr verfügbar');
define('WISHLIST_EMAIL_SUBJECT', 'dir eine Nachricht gesendet'); //Customers name will be automatically added to the beginning of this.
define('WISHLIST_SENT', 'Zurückgestellte Waren werden erfolgreich gesendet.');
define('WISHLIST_EMAIL_LINK', '

Die Liste der zurückgestellten Waren des Besuchers $from_name:
$link

Vielen Dank, ' . STORE_NAME); // $ from_name = Kundenname $ link = öffentlicher Wunschzettel-Link

define('WISHLIST_EMAIL_GUEST', 'Danke,' . STORE_NAME);


define('ERROR_YOUR_NAME', 'Gib deinen Namen ein.');
define('ERROR_YOUR_EMAIL', 'Gib deine E-Mail-Adresse an.');
define('ERROR_VALID_EMAIL', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
define('ERROR_ONE_EMAIL', 'Sie müssen mindestens einen Namen und eine E-Mail-Adresse angeben.');
define('ERROR_ENTER_EMAIL', 'E-Mail-Adresse angeben.');
define('ERROR_ENTER_NAME', 'Empfängernamen angeben.');
define('ERROR_MESSAGE', 'Eine Nachricht hinzufügen.');