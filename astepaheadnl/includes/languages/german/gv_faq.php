<?php
/*
  $Id: gv_faq.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Information über die Verwendung von Zertifikaten');
define('HEADING_TITLE', 'Information über die Verwendung von Zertifikaten');

define('TEXT_INFORMATION', '<a name="Top"></a>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=1','NONSSL').'"> Wie man ein Zertifikat kauft? </a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=2','NONSSL').'"> Wie sende ich ein Zertifikat an eine andere Person? </a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=3','NONSSL').'"> Warum brauche ich ein Zertifikat? </a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=4','NONSSL').'"> Wie benutze ich das Zertifikat bei der Bestellung? </a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=5','NONSSL').'"> Was kann ich tun, wenn ich Probleme mit der Verwendung von Zertifikaten habe? </a><br>');
switch ($_GET['faq_item']) {
  case '1':
define('SUB_HEADING_TITLE', 'Wie kaufe ich ein Zertifikat?');
define('SUB_HEADING_TEXT‘ werden ‚Zertifikate nur wie eine Ware gekauft, das heißt, müssen Sie das Zertifikat in den Warenkorb legen und bestellen, alles! Nachdem wir die Zahlung, Ihr Zertifikat wird aktiviert erhalten und Sie werden in der Lage Einkäufe zu machen Verwenden Sie Ihr Zertifikat oder geben Sie Ihr Zertifikat an Ihre Familie oder Freunde weiter. Nachdem Sie das Zertifikat aktiviert haben, erhalten Sie eine Benachrichtigung an Ihre E-Mail-Adresse. ');
  break;
  case '2':
define('SUB_HEADING_TITLE', 'Wie sende ich ein Zertifikat an jemand anderen?');
define('SUB_HEADING_TEXT', 'Sie werden aufgefordert, den Restbetrag auf dem Zertifikat nach der Bestellung zu senden, Sie müssen das Formular ausfüllen und auf "Weiter" klicken.');
  break;
  case '3':
  define('SUB_HEADING_TITLE', 'Warum brauche ich ein Zertifikat?');
  define('SUB_HEADING_TEXT', 'Das Zertifikat kann für die vollständige oder teilweise Zahlung der Bestellung (abhängig von der Höhe des Zertifikats) in unserem Online-Shop verwendet werden, und das Guthaben des Zertifikats brennt nicht, das verbleibende Geld kann in Zukunft für Einkäufe verwendet werden Unser Online-Shop, zusätzlich können Sie Ihr Zertifikat an Ihre Familie und Freunde spenden. ');
  break;
  case '4':
  define('SUB_HEADING_TITLE', 'Wie benutze ich das Zertifikat bei der Bestellung?');
  define('SUB_HEADING_TEXT', 'Bei der Bestellung in unserem Online-Shop werden Sie aufgefordert, das Zertifikat zu verwenden.');
  break;
  case '5':
  define('SUB_HEADING_TITLE', 'Was ist, wenn ich Probleme habe, Fragen bei der Verwendung von Zertifikaten?');
  define('SUB_HEADING_TEXT', 'Wenn Sie Probleme oder Fragen zur Verwendung von Zertifikaten haben, stellen Sie uns Ihre Fragen unter' STORE_OWNER_EMAIL_ADDRESS '. Wir beantworten alle Ihre Fragen in kurzer Zeit.');
  break;
  default:
  define('SUB_HEADING_TITLE','');
  define('SUB_HEADING_TEXT', 'Wähle die Frage, die dich interessiert.');
  
  }
?>