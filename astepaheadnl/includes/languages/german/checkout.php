<?php
/*
  One Page Checkout, Version: 1.08

  I.T. Web Experts
  http://www.itwebexperts.com

  Copyright (c) 2009 I.T. Web Experts

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Bestellung');
define('NAVBAR_TITLE_1', 'Bestellung');
define('HEADING_TITLE', 'Bestellung');
define('TABLE_HEADING_SHIPPING_ADDRESS', 'Lieferadresse');
define('TABLE_HEADING_BILLING_ADDRESS', 'Deine Daten');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Name');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Preis pro Stück');
define('TABLE_HEADING_PRODUCTS', 'Papierkorb');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Total');
define('ENTRY_TELEPHONE', 'Telefon');
define('ENTRY_COMMENT', 'Kommentar zur Bestellung');
define('TABLE_HEADING_SHIPPING_METHOD', 'Liefermethode');
define('TABLE_HEADING_PAYMENT_METHOD', 'Zahlungsart');
define('TEXT_CHOOSE_SHIPPING_METHOD', '');
define('TEXT_SELECT_PAYMENT_METHOD', '');
define('TEXT_ERROR_SHIPPING_METHOD', 'Bitte geben Sie Ihre Versandadresse ein, um die <b>%s</b> Angebote anzuzeigen');
define('TEXT_ENTER_SHIPPING_INFORMATION', ''); //This is currently the only shipping method available to use on this order.
define('TEXT_ENTER_PAYMENT_INFORMATION', '');
define('TABLE_HEADING_COMMENTS', 'Kommentare:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Einen Auftrag fortsetzen');
define('EMAIL_SUBJECT', 'Willkommen im Online-Shop' . STORE_NAME);
define('EMAIL_GREET_MR', 'Sehr geehrter %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Sehr geehrter %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Sehr geehrte (r) %s <br /> <br/>');
define('EMAIL_WELCOME', 'Willkommen im Online-Shop <b>' . STORE_NAME . '</b>. <br /><br/>');
define('EMAIL_TEXT', 'Jetzt haben Sie Zugriff auf einige zusätzliche Funktionen, die registrierten Benutzern zur Verfügung stehen:' . "\n\n" . '<li><b>Einkaufswagen</b> - alle Produkte, die zum Einkaufswagen hinzugefügt werden, bleib so lange dort, bis du sie löschst oder eine Bestellung aufgibst. ' . "\n" . '<li><b>Adressbuch</b> - jetzt können wir deine Produkte an die Adresse schicken, die du in der "Adresse" angegeben hast Lieferung.' . "\n" . '<li><b>Bestellhistorie</b> - Sie haben die Möglichkeit, die Bestellhistorie in unserem Shop anzuzeigen.<br><br>');
define('EMAIL_CONTACT', "Wenn Sie Fragen haben, schreiben Sie:" . STORE_OWNER_EMAIL_ADDRESS . '.'  . "\n\n");
define('EMAIL_WARNING', '');
// Start - CREDIT CLASS Gift Voucher Contribution
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'As part of our welcome to new customers, we have sent you an e-Gift Voucher worth %s');
define('EMAIL_GV_REDEEM', 'The redeem code for the e-Gift Voucher is %s, you can enter the redeem code when checking out while making a purchase');
define('EMAIL_GV_LINK', 'or by following this link ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Congratulations, to make your first visit to our online shop a more rewarding experience we are sending you an e-Discount Coupon.' . "\n" .
                    ' Below are details of the Discount Coupon created just for you' . "\n");
define('EMAIL_COUPON_REDEEM', 'To use the coupon enter the redeem code which is %s during checkout while making a purchase');
// End - CREDIT CLASS Gift Voucher Contribution
define('TEXT_PLEASE_SELECT', 'Auswählen');
define('TEXT_PASSWORD_FORGOTEN', 'Passwort vergessen?');
define('IMAGE_LOGIN', 'Login');
define('TEXT_HAVE_COUPON_KGT', 'Gibt es einen Coupon?');
define('TEXT_DIFFERENT_SHIPPING', 'Die Lieferadresse ist anders als die Zahlungsadresse?');
define('TEXT_DIFFERENT_BILLING', 'Andere Lieferadresse?');
// Points/Rewards Module V2.1rc2a BOF

define('TEXT_MIN_SUM', 'MINIMUM ORDER AMOUNT');
define('TEXT_NEW_CUSTOMER', 'neuer Kunde');
define('TEXT_LOGIN_SOCIAL', 'Login über das soziale Netzwerk');
define('TEXT_REGISTRATION_OFF', 'Geben Sie eine Bestellung ohne Registrierung auf');


define('TEXT_EMAIL_EXISTS', 'Diese E-Mail existiert bereits, bitte');
define('TEXT_EMAIL_EXISTS2', 'enter');
define('TEXT_EMAIL_EXISTS3', 'unter der angegebenen E-Mail');
define('TEXT_EMAIL_WRONG', 'Sie haben eine ungültige E-Mail-Adresse eingegeben.');
define('TEXT_ORDER_PROCESSING', 'Auftrag bearbeitet, warte ...');
define('TEXT_EMAIL_LOGIN', 'Login');
define('TEXT_EMAIL_PASS', 'Passwort');

define('TEXT_CHANGE_ADDRESS', 'Adresse ändern');
define('ADDRESS_BOOK', 'Adressbuch');
define('BILLING_ADDRESS_THE_SAME', 'Das Gleiche');

define('CH_JS_REFRESH', 'Update');
define('CH_JS_REFRESH_METHOD', 'Methodenaktualisierung');
define('CH_JS_SETTING_METHOD', 'Methode einstellen');
define('CH_JS_SETTING_ADDRESS', 'Address Setting');
define('CH_JS_SETTING_ADDRESS_BIL', 'Zahlung');
define('CH_JS_SETTING_ADDRESS_SHIP', 'Lieferung');

define('CH_JS_ERROR_SCART', 'Beim Aktualisieren des Papierkorbs ist ein Fehler aufgetreten, bitte informieren');
define('CH_JS_ERROR_SOME1', 'Während des Upgrades');
define('CH_JS_ERROR_SOME2', 'ein Fehler ist aufgetreten, bitte informieren');

define('CH_JS_ERROR_SET_SOME1', 'Fehler beim Installieren der Methode');
define('CH_JS_ERROR_SET_SOME2', 'Bitte informieren');
define('CH_JS_ERROR_SET_SOME3', 'über diesen Fehler.');

define('CH_JS_ERROR_REQ_BIL', 'Bitte füllen Sie die erforderlichen Felder im Abschnitt "Zahlungsadresse" aus');
define('CH_JS_ERROR_ERR_BIL', 'Bitte überprüfen Sie die Richtigkeit der Dateneingabe im Bereich "Zahlungsadresse"');
define('CH_JS_ERROR_REQ_SHIP', 'Bitte füllen Sie alle Pflichtfelder in der "Lieferadresse" aus');
define('CH_JS_ERROR_ERR_SHIP', 'Bitte überprüfen Sie die Richtigkeit der Dateneingabe im Bereich "Lieferadresse"');
define('CH_JS_ERROR_ADDRESS', 'Adressfehler');
define('CH_JS_ERROR_PMETHOD', 'Fehler bei der Auswahl der Zahlungsart');
define('CH_JS_ERROR_SELECT_PMETHOD', 'Sie müssen eine Zahlungsmethode auswählen.');
define('CH_JS_CHECK_EMAIL', 'Überprüfung der E-Mail-Adresse');
define('CH_JS_ERROR_EMAIL', 'Beim Verifizieren der E-Mail-Adresse ist ein Fehler aufgetreten, bitte informieren');