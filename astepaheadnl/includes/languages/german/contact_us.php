<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('HEADING_TITLE', 'KONTAKTE');
define('HEADING_SUBTITLE', 'Kontaktieren Sie uns jetzt und erhalten Sie Ihren Online-Shop und einfach zu bedienende CMS für den Online-Shop!');
define('NAVBAR_TITLE', 'Schreiben');
define('TEXT_SUCCESS', 'Deine Nachricht wurde erfolgreich versendet!');
define('EMAIL_SUBJECT', 'Nachricht von:' . STORE_NAME);
define('ENTRY_NAME', 'Dein Name:');
define('ENTRY_EMAIL', 'E-Mail-Adresse:');
define('ENTRY_PHONE', 'Dein Telefon:');
define('ENTRY_ENQUIRY', 'Nachricht:');
define('SEND_TO_TEXT', 'Brief an:');
define('POP_CONTACT_US', 'Hinterlasse eine Anfrage und wir rufen dich zurück');