<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Passwort');
define('HEADING_TITLE', 'Lass mich eintreten!');

define('TEXT_NEW_CUSTOMER', 'Anmelden!');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'ein Konto in unserem Shop erstellen, können Sie Einkäufe viel <b>schneller und </b>, zusätzlich zu machen, werden Sie in der Lage, die Umsetzung der Aufträge zu überwachen, finden Sie in der Geschichte Ihrer Aufträge.');
define('TEXT_RETURNING_CUSTOMER', 'Ich bin bereits registriert!');
define('TEXT_LOGIN_ERROR', '<span><b>Fehler:</b></span> ist ungültig \'E-Mail-Adresse\' und/oder \'Passwort\'.');
// guest_account starten
define('NO_CUSTOMER', 'Es gibt keinen solchen Benutzer in der Datenbank');
define('NO_PASS', 'Falsches Passwort');
define('SOC_VK', 'Vkontakte');
define('SOC_SITE', 'Site');
define('SOC_ENTER_FROM_OTHER', 'Abmelden von einem anderen sozialen Netzwerk oder unter deinem Login / Passwort.');
define('SOC_NOW_FROM', 'Jetzt versuchst du, von zu kommen');
define('SOC_NEED_FROM', 'Sie haben sich registriert von');
define('SOC_LOGIN_THX', 'Danke, Sie haben sich erfolgreich angemeldet!');