<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_CC_TEXT_TITLE', 'Кредитна картка');
define('MODULE_PAYMENT_CC_TEXT_DESCRIPTION', 'Інформація про кредитну картку для тесту:<br><br>Номер картки: 4111111111111111<br>Дійсна до: Будь-яка дата');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_OWNER', 'Власник кредитної картки:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_NUMBER', 'Номер кредитної картки:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_EXPIRES', 'Дійсна до:');
define('MODULE_PAYMENT_CC_TEXT_ERROR', 'Дані введені невірно!');