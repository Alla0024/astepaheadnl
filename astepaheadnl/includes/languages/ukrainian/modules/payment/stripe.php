<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_STRIPE_TEXT_TITLE', 'Stripe');
define('MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION', 'Stripe - онлайн платежи');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER', 'Власник кредитної картки:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER', 'Номер кредитної картки:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM', 'Термін дії MM:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY', 'Термін дії YY:');
define('MODULE_PAYMENT_STRIPE_TEXT_ERROR', 'Помилка кредитної картки!');