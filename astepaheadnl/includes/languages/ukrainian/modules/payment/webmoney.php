<?php
/*
  $Id: webmoney.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_WEBMONEY_TEXT_TITLE', 'WebMoney');
if(!defined('MODULE_PAYMENT_WEBMONEY_1')) define('MODULE_PAYMENT_WEBMONEY_1', '---');
if(!defined('MODULE_PAYMENT_WEBMONEY_2')) define('MODULE_PAYMENT_WEBMONEY_2', '---');
if(!defined('MODULE_PAYMENT_WEBMONEY_3')) define('MODULE_PAYMENT_WEBMONEY_3', '---');

define('MODULE_PAYMENT_WEBMONEY_TEXT_DESCRIPTION', 'Наші дані в системі WebMoney:<br><br>Наш WM Ідентифікатор: &nbsp;&nbsp;&nbsp;<b>' . MODULE_PAYMENT_WEBMONEY_1 . '</b><br>Номер нашого R гаманця: &nbsp;&nbsp;&nbsp;<b>' . MODULE_PAYMENT_WEBMONEY_2 . '</b><br>Номер нашого Z гаманця: &nbsp;&nbsp;&nbsp;<b>' . MODULE_PAYMENT_WEBMONEY_3 . '</b><br><br>Для оплати замовлення Вам необхідно здійснити переказ коштів на наш R або Z гаманець в системі WebMoney, а в примітці вкажіть <b>номер Вашого замовлення</b><br><br><span>Номер Вашего заказа будет выслан на Ваш Email адрес</span><br><br><span>Переказ повинен бути БЕЗ протекції операції</span><br><br>Як зробити переказ:<br>Для совершения перевода откройте WM Keeper и щелкните правой кнопкой на кошельке, с которого вы хотите произвести перевод, а затем выберите из меню Передать деньги - В кошелек WebMoney. В появившемся диалоговом окне введите сумму перевода и номер нашего кошелека, а в примечании укажите <b>номер Вашего заказа</b>');
define('MODULE_PAYMENT_WEBMONEY_TEXT_EMAIL_FOOTER', "Наші дані в системі WebMoney:\n\nНаш WM Ідентифікатор: " . MODULE_PAYMENT_WEBMONEY_1 . "\nНомер нашого R гаманця: " . MODULE_PAYMENT_WEBMONEY_2 . "\nНомер нашого Z гаманця: " . MODULE_PAYMENT_WEBMONEY_3 . "\n\nДля оплати замовлення Вам необхідно здійснити переказ коштів на наш R або Z гаманець в системі WebMoney, а в примітці вкажіть номер Вашого замовлення\n\nНомер Вашего заказа будет выслан на Ваш Email адрес\n\nПереказ повинен бути БЕЗ протекції операції\n\nКак сделать перевод:\nДля совершения перевода откройте WM Keeper и щелкните правой кнопкой на кошельке, с которого вы хотите произвести перевод, а затем выберите из меню Передать деньги - В кошелек WebMoney. В появившемся диалоговом окне введите сумму перевода и номер нашего кошелека, а в примечании укажите номер Вашего заказа");