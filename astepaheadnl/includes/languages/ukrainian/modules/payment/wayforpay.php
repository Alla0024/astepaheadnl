<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Description');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Включити / Виключити модуль');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Статус за замовчуванням');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Merchant account');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Merchant secret');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'статус оплати');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'True - включити, False - вимкнути');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Статус який встановлюється замовлення при його створенні');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Merchant account');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Merchant secret');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Порядок сортування модуля в блоці з модулями оплати');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'Статус який встановлюється замовлення при успішну оплату');
