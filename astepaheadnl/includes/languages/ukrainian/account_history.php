<?php
/*
  $Id: account_history.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Мої дані');
define('NAVBAR_TITLE_2', 'Історія замовлень');
define('HEADING_TITLE', 'Історія замовлень');
define('TEXT_ORDER_STATUS', 'Статус замовлення');
define('TEXT_ORDER_DATE', 'Дата замовлення');
define('TEXT_ORDER_SHIPPED_TO', 'Адреса доставки:');
define('TEXT_ORDER_BILLED_TO', 'Покупець:');
define('TEXT_ORDER_COST', 'Сума');
define('TEXT_ORDER_PC', 'шт.');
define('TEXT_NO_PURCHASES', 'Нажаль ви ще нічого не замовили в нашому магазині.');

?>