<?php
/*
  $Id: create_account.tpl.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// guest_account start
if ($guest_account == false) { // Not a Guest Account
  define('NAVBAR_TITLE', 'Реєстрація');
} else {
  define('NAVBAR_TITLE', 'Оформлення замовлення');
}
// guest_account end
define('HEADING_TITLE', 'Мої дані');
define('CR_ENTER', 'Увійдіть');
define('CR_THX', 'Дякуємо, ви успішно зареєстровані!');
define('CR_IF', 'якщо ви вже зареєстровані.');
// guest_account end
define('TEXT_ORIGIN_LOGIN', '<span><span><small><b>УВАГА:</b></small></span>&nbsp;Вам потрібно заповнити реєстраційну форму. Це дасть Вам можливість оформляти замовлення в нашому інтернет магазині,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;якщо Ви вже зареєстровані на нашому сайті, введіть, будь ласка, Ваш логін і пароль&nbsp;</span><a href="%s"><u>тут</u></a>.');
define('EMAIL_SUBJECT', 'Ласкаво просимо в ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Поважний %s! <br><br>');
define('EMAIL_GREET_MS', 'Поважна %s! <br><br>');
define('EMAIL_GREET_NONE', 'Поважний %s! <br><br>');
define('EMAIL_WELCOME', 'Ми раді запросити Вас в інтернет-магазин <b>' . STORE_NAME . '</b>. <br><br>');
define('EMAIL_CONTACT', 'Якщо у Вас виникли будь-які питання, пишіть: ' . STORE_OWNER_EMAIL_ADDRESS . '.<br><br>');
define('EMAIL_TEXT', 'Тепер Ви можете здійснювати покупки.' . "\n");
define('EMAIL_WARNING', '');

/* ICW Credit class gift voucher begin */
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Також повідомляємо Вас, що Ви отримуєте сертифікат на суму %s');
define('EMAIL_GV_REDEEM', 'Код Вашого сертифіката %s, Ви можете використовувати Ваш сертифікат при оплаті замовлення, при цьому номінальна вартість сертифіката буде зарахована як оплата за все замовлення, або в якості оплати частини вартості Вашого замовлення.');
define('EMAIL_GV_LINK', 'Перейдіть за посиланням для активації сертифікату: ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Вітаємо з реєстрацією в нашому магазині, раді повідомити, що ми даруємо Вам купон на отримання знижки в нашому магазині.' . "\n" .
                                        ' Даний купон дійсний тільки для Вас.' . "\n");
define('EMAIL_COUPON_REDEEM', 'Щоб скористатися купоном, Ви повинні вказати код купона в процесі оформлення замовлення, щоб отримати знижку.' . "\n" . 'Код Вашого купона: %s');
define('CR_LOGIN', 'Ваш логін(e-mail)');
define('CR_PASS', 'Ваш пароль');
define('CR_ADD_EMAIL', 'Для завершення реєстрації<br /> введіть, будь ласка, ваш <b>e-mail:</b>');
define('CR_SUBMIT', 'Підтвердити');
/* ICW Credit class gift voucher end */