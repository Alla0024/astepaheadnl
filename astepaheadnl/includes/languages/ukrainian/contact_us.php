<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('HEADING_TITLE', 'Зв\'яжіться з нами');
define('HEADING_SUBTITLE', 'Зв\'яжіться з нами зараз і наші менеджери напишуть вам найближчим часом');
define('NAVBAR_TITLE', 'Написати');
define('TEXT_SUCCESS', 'Ваше повідомлення успішно відправлено!');
define('EMAIL_SUBJECT', 'Повідомлення від ' . STORE_NAME);
define('ENTRY_NAME', 'Ваше ім\'я:');
define('ENTRY_EMAIL', 'E-Mail адреса:');
define('ENTRY_PHONE', 'Ваш телефон:');
define('ENTRY_ENQUIRY', 'Повідомлення:');
define('SEND_TO_TEXT', 'Лист в:');
define('POP_CONTACT_US','Залиште заявку, і ми Вам зателефонуємо');