<?php
/*
  $Id: print_my_invoice.php,v 6.1 2005/06/05 18:17:59 PopTheTop Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

//// START Edit the following defines to your liking ////

// Footing
define('INVOICE_TEXT_THANK_YOU', 'Дякуэмо що скористались магазином'); // Printed at the bottom of your invoices
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Статус замовлення:');

// Product Table Info Headings
define('TABLE_HEADING_PRODUCTS_MODEL', 'Код #'); // Change this to "Model #" or leave it as "SKU #"

//// END Editing the above defines to your liking ////

define('INVOICE_TEXT_INVOICE_NR', 'Замовлення #');
define('INVOICE_TEXT_INVOICE_DATE', 'Дата Замовлення:');
// Misc Invoice Info
define('INVOICE_TEXT_NUMBER_SIGN', '#');
define('INVOICE_TEXT_DASH', '-');
define('INVOICE_TEXT_COLON', ':');

define('INVOICE_TEXT_INVOICE', 'Рахунок');
define('INVOICE_TEXT_ORDER', 'Замовлення');
define('INVOICE_TEXT_DATE_OF_ORDER', 'Дата');
define('INVOICE_TEXT_DATE_DUE_DATE', 'Дата оплати');

// Customer Info
define('ENTRY_SOLD_TO', 'Адреса платника:');
define('ENTRY_SHIP_TO', 'Адреса доставки:');
define('ENTRY_PAYMENT_METHOD', 'Оплата:');

// Product Table Info Headings
define('TABLE_HEADING_PRODUCTS', 'Товари');
define('TABLE_HEADING_UNIT_PRICE', 'шт.');
define('TABLE_HEADING_TOTAL', 'Всього');

// Order Total Details Info
define('ENTRY_SUB_TOTAL', 'Сума:');
define('ENTRY_SHIPPING', 'Доставка:');
define('ENTRY_TAX', 'Податок:');
define('ENTRY_TOTAL', 'Всього:');

define('PRINT_HEADER_COMPANY_NAME', 'Назва компанії:');
define('PRINT_HEADER_ADDRESS', 'Адреса:');
define('PRINT_HEADER_PHONE', 'Телефон:');
define('PRINT_HEADER_EMAIL_ADDRESS', 'Електронна пошта:');
define('PRINT_HEADER_WEBSITE', 'Веб-сайт');
define('PDF_TITLE_TEXT', 'Рахунок-фактура');
define('INVOICE_CUSTOMER_NUMBER', 'Клієнт:');
define('TABLE_HEADING_PRODUCTS_PC', 'Кількість');
define('PDF_FOOTER_TEXT', 'Ваш текст у нижньому колонтитулі');
