<?php
/*
  $Id: account.php
*/

define('NAVBAR_TITLE', 'Мої дані');
define('HEADING_TITLE', 'Мої дані');
define('MY_ACCOUNT_TITLE', 'Мої дані');
define('MY_ACCOUNT_PASSWORD', 'Змінити пароль.');
define('MY_ORDERS_VIEW', 'Перегляд моїх замовлень.');
define('MY_NAVI', 'Меню');
define('MY_INFO', 'Моя інформація');
define('EDIT_ADDRESS_BOOK', 'Редагувати адресу');
