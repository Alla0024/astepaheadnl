<?php
/*
  $Id: specials.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Знижки');
define('HEADING_TITLE', 'Знижки');
define('TEXT_INFO_CATEGORY', 'Усі категорії');
define('TEXT_INFO_MANUFACTURERS', 'Всі бренди');
define('TEXT_INFO_ONLY_DISCOUNT', 'Тільки з знижкою');
?>
