<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Оформлення замовлення');
define('NAVBAR_TITLE_2', 'Успішно');
define('HEADING_TITLE', 'Ваше замовлення оформлено!');
define('TEXT_SUCCESS', 'Ваше замовлення успішно оформлено!');
define('TEXT_THANKS_FOR_SHOPPING', 'Ваше замовлення успішно оформлено!');
define('TABLE_HEADING_COMMENTS', 'Найближчим часом з вами зв\'яжуться.');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Посилання дійсне до: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' раз можна завантажити файл.');
define('SMS_NEW_ORDER', 'У Вас нове замовлення');
define('TEXT_SUCCESS_INFO', 'Всі подальші інструкції та реквізити для оплати відправлені вам на пошту');