<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Мої дані');
define('NAVBAR_TITLE_2', 'Історія замовлень');
define('NAVBAR_TITLE_3', 'Інформація про замовлення');
define('HEADING_TITLE', 'Інформація про замовлення');
define('HEADING_ORDER_NUMBER', 'Замовлення #%s');
define('HEADING_ORDER_DATE', 'Дата');
define('HEADING_ORDER_TOTAL', 'Вартість замовлення:');
define('HEADING_DELIVERY_ADDRESS', 'Адреса доставки');
define('HEADING_SHIPPING_METHOD', 'Спосіб доставки');
define('HEADING_PRODUCTS', 'Товар');
define('HEADING_TAX', 'Податок');
define('HEADING_TOTAL', 'Всього');
define('HEADING_PAYMENT_METHOD', 'Спосіб оплати');
define('HEADING_ORDER_HISTORY', 'Історія замовлення');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Посилання дійсне до: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' раз можна завантажити файл.');
define('TABLE_HEADING_DATE_ADDED', 'Дата');
define('TABLE_HEADING_STATUS', 'Статус замовлення');
define('TABLE_HEADING_COMMENTS', 'Коментарі');
