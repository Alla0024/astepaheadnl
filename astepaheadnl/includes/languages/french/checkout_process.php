<?php 
define('EMAIL_TEXT_SUBJECT','Votre commande');
define('EMAIL_TEXT_ORDER_NUMBER','Numéro de commande:');
define('EMAIL_TEXT_INVOICE_URL','Informations sur la commande:');
define('EMAIL_TEXT_DATE_ORDERED','Date de commande:');
define('EMAIL_TEXT_PRODUCTS','Vous avez commandé:');
define('EMAIL_TEXT_DELIVERY_ADDRESS','L\'adresse de livraison');
define('EMAIL_TEXT_BILLING_ADDRESS','L\'adresse de l\'acheteur');
define('EMAIL_TEXT_PAYMENT_METHOD','Mode de paiement');
define('EMAIL_SEPARATOR','------------------------------------------------------');
define('EMAIL_TEXT_CUSTOMER_NAME','L\'acheteur:');
define('EMAIL_TEXT_CUSTOMER_EMAIL_ADDRESS','Email:');
define('EMAIL_TEXT_CUSTOMER_TELEPHONE','Téléphone:');
