<?php 
define('TEXT_MAIN','');
if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
    define('HEADING_TITLE', $topics['topics_name']);
    define('TEXT_NO_ARTICLES', 'Il n`y a actuellement aucun article sur ce sujet.');
    define('TEXT_ARTICLES', 'Vous trouverez ci-dessous une liste d`articles avec les plus récents en premier.');
    define('TEXT_DATE_ADDED', 'Publié:');
    define('TEXT_TOPIC', 'Sujet:');
    define('TEXT_BY', 'par');
} elseif ($topic_depth == 'top') {
    define('HEADING_TITLE', 'Tous les articles');
    define('TEXT_CURRENT_ARTICLES', 'Articles actuels');
    define('TEXT_UPCOMING_ARTICLES', 'Articles à venir');
    define('TEXT_NO_ARTICLES', 'Il n\'y a actuellement aucun article répertorié.');
    define('TEXT_DATE_ADDED', 'Publié:');
    define('TEXT_DATE_EXPECTED', 'Attendu:');
    define('TEXT_TOPIC', 'Sujet:');
    define('TEXT_BY', 'par');
} elseif ($topic_depth == 'nested') {
    define('HEADING_TITLE', 'Des articles');
}
define('TOTAL_ARTICLES','Total des articles');
define('HEADING_TITLE','Article');
