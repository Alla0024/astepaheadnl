<?php 
define('INVOICE_TEXT_THANK_YOU','Merci d\'avoir utilisé le magasin');
define('TEXT_INFO_ORDERS_STATUS_NAME','Le statut de la commande:');
define('TABLE_HEADING_PRODUCTS_MODEL','Code #');
define('INVOICE_TEXT_INVOICE_NR','La commande #');
define('INVOICE_TEXT_INVOICE_DATE','Date De Commande:');
define('INVOICE_TEXT_NUMBER_SIGN','#');
define('INVOICE_TEXT_DASH','-');
define('INVOICE_TEXT_COLON',':');
define('INVOICE_TEXT_INVOICE','Compte');
define('INVOICE_TEXT_ORDER','Commande');
define('INVOICE_TEXT_DATE_OF_ORDER','Date');
define('INVOICE_TEXT_DATE_DUE_DATE','Date de paiement');
define('ENTRY_SOLD_TO','Adresse de facturation:');
define('ENTRY_SHIP_TO','Adresse de livraison:');
define('ENTRY_PAYMENT_METHOD','Paiement:');
define('TABLE_HEADING_PRODUCTS','Marchandises');
define('TABLE_HEADING_UNIT_PRICE','pièces');
define('TABLE_HEADING_TOTAL','Tout');
define('ENTRY_SUB_TOTAL','La somme de:');
define('ENTRY_SHIPPING','Livraison:');
define('ENTRY_TAX','Taxe de séjour:');
define('ENTRY_TOTAL','Total:');


define('PRINT_HEADER_COMPANY_NAME', 'Nom de la compagnie:');
define('PRINT_HEADER_ADDRESS', 'Adresse:');
define('PRINT_HEADER_PHONE', 'Téléphone:');
define('PRINT_HEADER_EMAIL_ADDRESS', 'Email:');
define('PRINT_HEADER_WEBSITE', 'Site Internet');
define('PDF_TITLE_TEXT', 'Facture d\'achat');
define('INVOICE_CUSTOMER_NUMBER', 'Client:');
define('TABLE_HEADING_PRODUCTS_PC', 'Quantité');
define('PDF_FOOTER_TEXT', 'Votre texte en pied de page');
