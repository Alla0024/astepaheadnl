<?php 
define('NAVBAR_TITLE','Mes données');
define('HEADING_TITLE','Mes données');
define('MY_ACCOUNT_TITLE','Mes données');
define('MY_ACCOUNT_PASSWORD','Modifier le mot de passe.');
define('MY_ORDERS_VIEW','Afficher mes ordres.');
define('MY_NAVI','Menu');
define('MY_INFO','Mes informations');
define('EDIT_ADDRESS_BOOK','Pour modifier l\'adresse');
