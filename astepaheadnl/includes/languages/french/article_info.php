<?php 
define('HEADING_ARTICLE_NOT_FOUND','L\'article n\'est pas trouvé');
define('TEXT_ARTICLE_NOT_FOUND','Désolé, mais l\'article que Vous avez demandée n\'est pas disponible!');
define('TEXT_DATE_ADDED','Cet article a été publié le %s.');
define('TEXT_DATE_AVAILABLE','Cet article sera disponible le %s.');
define('TEXT_BY','by ');
