<?php 
define('NAVBAR_TITLE','Informations sur l\'utilisation de certificats');
define('HEADING_TITLE','Informations sur l\'utilisation de certificats');
define('TEXT_INFORMATION','<a name="Top"></a>  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=1','NONSSL').'">Comment acheter un certificat?</a><br>  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=2','NONSSL').'">Comment envoyer un certificat de quelqu\'un d\'autre?</a><br>  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=3','NONSSL').'">Pourquoi un certificat?</a><br>  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=4','NONSSL').'">Comment utiliser le certificat lors de la commande?</a><br>  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=5','NONSSL').'">Que faire si vous rencontrez des problèmes, des questions lors de l\'utilisation de certificats?</a><br> ');
define('SUB_HEADING_TITLE','');
define('SUB_HEADING_TEXT','Sélectionnez une question.');
