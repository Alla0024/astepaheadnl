<?php 
define('NAVBAR_TITLE','La prsentation de la commande');
define('NAVBAR_TITLE_1','La prsentation de la commande');
define('HEADING_TITLE','La prsentation de la commande');
define('TABLE_HEADING_SHIPPING_ADDRESS','L\'adresse de livraison');
define('TABLE_HEADING_BILLING_ADDRESS','Adresse de paiement');
define('TABLE_HEADING_PRODUCTS_MODEL','Marque');
define('TABLE_HEADING_PRODUCTS_PRICE','Prix par pièce');
define('TABLE_HEADING_PRODUCTS','Panier');
define('TABLE_HEADING_TAX','Tax');
define('TABLE_HEADING_TOTAL','Tout');
define('ENTRY_TELEPHONE','Téléphone');
define('ENTRY_COMMENT','Le commentaire de la commande');
define('TABLE_HEADING_SHIPPING_METHOD','Mode de livraison');
define('TABLE_HEADING_PAYMENT_METHOD','Mode de paiement');
define('TEXT_CHOOSE_SHIPPING_METHOD','');
define('TEXT_SELECT_PAYMENT_METHOD','');
define('TEXT_ERROR_SHIPPING_METHOD','S\'il vous plaît, entrez votre adresse de livraison, pour voir <b>%s</b>');
define('TEXT_ENTER_SHIPPING_INFORMATION','');
define('TEXT_ENTER_PAYMENT_INFORMATION','');
define('TABLE_HEADING_COMMENTS','Commentaires:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE','Procéder à la commande');
define('EMAIL_SUBJECT','Heureux de Vous accueillir dans la boutique ou elle peut');
define('EMAIL_GREET_MR','Respecté de %s');
define('EMAIL_GREET_MS','Cher de %s');
define('EMAIL_GREET_NONE','Cher(e) %s <br /><br/>');
define('EMAIL_WELCOME','Heureux de Vous accueillir dans le magasin en ligne <b>ou elle peut</b>. <br /><br/>');
define('EMAIL_TEXT','Maintenant, Vous avez accès à certaines fonctionnalités avancées qui sont disponibles pour les utilisateurs enregistrés de:<li><b>Panier</b> - tous les produits добавленые dans le panier, restent là jusqu\'à ce que Vous les supprimiez ou de ne pas effectuer votre commande.<li><b>Carnet d\'adresses</b> - maintenant, nous pouvons envoyer leurs produits à l\'adresse que Vous avez indiqué dans la section "Adresse de livraison".<li><b>historique des commandes</b> - Vous avez la possibilité de consulter l\'historique des commandes dans notre boutique.<br><br>');
define('EMAIL_CONTACT','Si Vous avez des questions, écrivez à: '.STORE_OWNER_EMAIL_ADDRESS);
define('EMAIL_WARNING','');
define('EMAIL_GV_INCENTIVE_HEADER','As part of our welcome to new customers, we have sent you an e-Gift Voucher peine de %s');
define('EMAIL_GV_REDEEM','The redeem code for the e-Gift Voucher is %s, you can enter the redeem code when checking out while making a purchase');
define('EMAIL_GV_LINK','or by following this link ');
define('EMAIL_COUPON_INCENTIVE_HEADER','Congratulations to make your first visit to our online shop a plus enrichissante experience we are sending you an e-Discount Coupon. Below are details of the Discount Coupon created just for you');
define('EMAIL_COUPON_REDEEM','To use the coupon enter the redeem code which is %s during checkout while making a purchase');
define('TEXT_PLEASE_SELECT','Sélectionnez');
define('TEXT_PASSWORD_FORGOTTEN','Mot de passe oublié?');
define('IMAGE_LOGIN','Entrer');
define('TEXT_HAVE_COUPON_KGT','Il ya un coupon?');
define('TEXT_DIFFERENT_SHIPPING','L\'adresse de paiement est différente de l\'adresse de livraison?');
define('TEXT_DIFFERENT_BILLING','Adresse de livraison différente?');
define('TEXT_MIN_SUM','LE MONTANT MINIMUM DE COMMANDE');
define('TEXT_NEW_CUSTOMER','Un nouvel acheteur');
define('TEXT_LOGIN_SOCIAL','Entrer par le soc. réseau');
define('TEXT_REGISTRATION_OFF','Commander sans inscription');
define('TEXT_EMAIL_EXISTS','Un tel e-mail existe déjà, s\'il vous plaît');
define('TEXT_EMAIL_EXISTS2','connectez-vous');
define('TEXT_EMAIL_EXISTS3','sous le e-mail');
define('TEXT_EMAIL_WRONG','vous avez saisi un e-mail.');
define('TEXT_ORDER_PROCESSING','La commande est traitée, attendez...');
define('TEXT_EMAIL_LOGIN','Connexion');
define('TEXT_EMAIL_PASS','Le mot de passe');
define('TEXT_CHANGE_ADDRESS','Modifier l\'adresse');
define('ADDRESS_BOOK','Carnet d\'adresses');
define('BILLING_ADDRESS_THE_SAME','Même');
define('CH_JS_REFRESH','Mise à jour');
define('CH_JS_REFRESH_METHOD','Mise à jour de mode');
define('CH_JS_SETTING_METHOD','Installation de la méthode');
define('CH_JS_SETTING_ADDRESS','Le réglage de l\'adresse');
define('CH_JS_SETTING_ADDRESS_BIL','Paiement');
define('CH_JS_SETTING_ADDRESS_SHIP','Livraison');
define('CH_JS_ERROR_SCART','Dans le processus de mise à jour de la corbeille d\'erreur, veuillez en informer');
define('CH_JS_ERROR_SOME1','Dans le processus de mise à jour');
define('CH_JS_ERROR_SOME2','erreur, veuillez en informer');
define('CH_JS_ERROR_SET_SOME1','Une erreur s\'est produite lors de l\'installation de mode');
define('CH_JS_ERROR_SET_SOME2',' veuillez informer');
define('CH_JS_ERROR_SET_SOME3','à propos de cette erreur.');
define('CH_JS_ERROR_REQ_BIL','S\'il vous plaît remplir les champs nécessaires dans la rubrique "Adresse de paiement"');
define('CH_JS_ERROR_ERR_BIL','Assurez-vous de vérifier l\'exactitude de la saisie des données dans la section "Adresse de paiement"');
define('CH_JS_ERROR_REQ_SHIP','S\'il vous plaît remplir les champs "Adresse de livraison"');
define('CH_JS_ERROR_ERR_SHIP','Assurez-vous de vérifier l\'exactitude de la saisie des données dans la section "Adresse de livraison"');
define('CH_JS_ERROR_ADDRESS','Erreur d\'adresse');
define('CH_JS_ERROR_PMETHOD','Erreur de choix du mode de paiement');
define('CH_JS_ERROR_SELECT_PMETHOD','Vous devez choisir une méthode de paiement.');
define('CH_JS_CHECK_EMAIL','Vérification de l\'adresse E-mail');
define('CH_JS_ERROR_EMAIL','Dans le processus de vérification de l\'adresse email d\'erreur, veuillez en informer');
