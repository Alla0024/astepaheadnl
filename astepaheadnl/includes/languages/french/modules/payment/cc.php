<?php 
define('MODULE_PAYMENT_CC_TEXT_TITLE','Carte de crédit');
define('MODULE_PAYMENT_CC_TEXT_DESCRIPTION','Les informations concernant la carte de crédit pour le test:<br><br>Numéro de la carte: 4111111111111111<br>Valable jusqu\'au: date de');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_OWNER','Le titulaire de la carte:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_NUMBER','Numéro de carte de crédit:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_EXPIRES','Valable jusqu\'au:');
define('MODULE_PAYMENT_CC_TEXT_ERROR','Les données sont saisies correctement!');
