<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'La description');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Activer / désactiver le module');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Statut par défaut');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Compte marchand');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Secret marchand');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'L\'ordre de tri');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'État de paiement');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'Vrai - activer, Faux - désactiver');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Statut défini sur la commande lors de sa création');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Compte marchand');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Secret marchand');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Ordre de tri du module dans le bloc avec les modules de paiement');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'Le statut défini sur la commande lors du paiement réussi');