<?php 
define('MODULE_PAYMENT_STRIPE_TEXT_TITLE','Stripe');
define('MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION','Stripe - paiement en ligne');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER','Le titulaire de la carte:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER','Numéro de carte de crédit:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM','l\'expiration de MM:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY','l\'expiration YY:');
define('MODULE_PAYMENT_STRIPE_TEXT_ERROR','Erreur de carte de crédit!');
