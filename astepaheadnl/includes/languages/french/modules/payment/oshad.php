<?php
/*
  $Id: webmoney_merchant.php 1778 2008-01-09 23:37:44Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_OSHAD_TEXT_TITLE', 'Oshad');
define('MODULE_PAYMENT_OSHAD_TEXT_PUBLIC_TITLE', 'Oshadbank Visa/Mastercard');
define('MODULE_PAYMENT_OSHAD_TEXT_DESCRIPTION', 'Après avoir cliqué sur le bouton Confirmer la commande, vous irez sur le site du système de paiement pour payer la commande. Une fois le paiement effectué, votre commande sera exécutée.');
define('MODULE_PAYMENT_OSHAD_TEXT_ADMIN_DESCRIPTION', 'Oshad..');