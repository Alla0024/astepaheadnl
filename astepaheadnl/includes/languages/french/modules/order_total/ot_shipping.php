<?php 
define('MODULE_ORDER_TOTAL_SHIPPING_TITLE','Livraison');
define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION','Le coût de la livraison');
define('FREE_SHIPPING_TITLE','Livraison gratuite si le montant >');
define('FREE_SHIPPING_DESCRIPTION','Livraison gratuite pour les commandes de plus de %s');
