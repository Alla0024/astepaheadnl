<?php
/*
  $Id: print_my_invoice.php,v 6.1 2005/06/05 18:17:59 PopTheTop Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

//// START Edit the following defines to your liking ////

// Footing
define('INVOICE_TEXT_THANK_YOU', 'Thank you for shopping at'); // Printed at the bottom of your invoices
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Order Status:');

// Product Table Info Headings
define('TABLE_HEADING_PRODUCTS_MODEL', 'Model #'); // Change this to "Model #" or leave it as "SKU #"

//// END Editing the above defines to your liking ////

define('INVOICE_TEXT_INVOICE_NR', 'Invoice #: ');
define('INVOICE_TEXT_INVOICE_DATE', 'Date of invoice: ');
// Misc Invoice Info
define('INVOICE_TEXT_NUMBER_SIGN', '#');
define('INVOICE_TEXT_DASH', '-');
define('INVOICE_TEXT_COLON', ':');

define('INVOICE_TEXT_INVOICE', 'Invoice');
define('INVOICE_TEXT_ORDER', 'Order');
define('INVOICE_TEXT_DATE_OF_ORDER', 'Date');
define('INVOICE_TEXT_DATE_DUE_DATE', 'Payment Date');
define('ENTRY_PAYMENT_CC_NUMBER', 'Credit Card:');

// Customer Info
define('ENTRY_SOLD_TO', 'Invoice address:');
define('ENTRY_SHIP_TO', 'Shipment address');
define('ENTRY_PAYMENT_METHOD', 'Payment:');

// Product Table Info Headings
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Price (ex)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Price (inc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ex)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (inc)');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_UNIT_PRICE', 'Unit Price');
define('TABLE_HEADING_TOTAL', 'Total');

// Order Total Details Info
define('ENTRY_SUB_TOTAL', 'Sub-Total:');
define('ENTRY_SHIPPING', 'Shipping:');
define('ENTRY_TAX', 'Tax:');
define('ENTRY_TOTAL', 'Total:');

//Order Comments
define('TABLE_HEADING_COMMENTS', 'ORDER COMMENTS:');
define('TABLE_HEADING_DATE_ADDED', 'Date Added');
define('TABLE_HEADING_COMMENT_LEFT', 'Comment Left');
define('INVOICE_TEXT_NO_COMMENT', 'No comments have been left for this order');

define('PRINT_HEADER_COMPANY_NAME', 'Nombre de empresa:');
define('PRINT_HEADER_ADDRESS', 'Habla a:');
define('PRINT_HEADER_PHONE', 'Teléfono:');
define('PRINT_HEADER_EMAIL_ADDRESS', 'Email:');
define('PRINT_HEADER_WEBSITE', 'Sitio web');
define('PDF_TITLE_TEXT', 'Factura');
define('INVOICE_CUSTOMER_NUMBER', 'Cliente:');
define('TABLE_HEADING_PRODUCTS_PC', 'Cantidad');
define('PDF_FOOTER_TEXT', 'Tu texto en pie de página');
