<?php
/*
  $Id: account_history.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Mi cuenta');
define('NAVBAR_TITLE_2', 'Historia');
define('HEADING_TITLE', 'Mi historial de pedidos');
define('TEXT_ORDER_STATUS', 'Estado del pedido');
define('TEXT_ORDER_DATE', 'Fecha de orden');
define('TEXT_ORDER_SHIPPED_TO', 'Enviado a:');
define('TEXT_ORDER_BILLED_TO', 'Facturado a:');
define('TEXT_ORDER_COST', 'Costo de pedido');
define('TEXT_ORDER_PC', 'pc');
define('TEXT_NO_PURCHASES', 'Aun no has hecho ninguna compra.');
