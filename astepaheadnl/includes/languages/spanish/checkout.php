<?php
/*
  One Page Checkout, Version: 1.08

  I.T. Web Experts
  http://www.itwebexperts.com

  Copyright (c) 2009 I.T. Web Experts

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Pedido de pedido');
define('NAVBAR_TITLE_1', 'Pedido de pedido');
define('HEADING_TITLE', 'Pedido de pedido');
define('TABLE_HEADING_SHIPPING_ADDRESS', 'Dirección de envío');
define('TABLE_HEADING_BILLING_ADDRESS', 'Su información:');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Nombre');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Precio');
define('TABLE_HEADING_PRODUCTS', 'Carro');
define('TABLE_HEADING_TAX', 'Impuesto');
define('TABLE_HEADING_TOTAL', 'Total');
define('ENTRY_TELEPHONE', 'Número de teléfono');
define('ENTRY_COMMENT', 'Comentario');
define('TABLE_HEADING_SHIPPING_METHOD', 'Método de envío');
define('TABLE_HEADING_PAYMENT_METHOD', 'Método de pago');
define('TEXT_CHOOSE_SHIPPING_METHOD', '');
define('TEXT_SELECT_PAYMENT_METHOD', '');
define('TEXT_ERROR_SHIPPING_METHOD', 'Ingrese su DIRECCIÓN DE ENVÍO para ver las cotizaciones de <b>%s</b>');
define('TEXT_ENTER_SHIPPING_INFORMATION', 'Este es actualmente el único método de envío disponible para usar en este pedido.');
define('TEXT_ENTER_PAYMENT_INFORMATION', 'Este es actualmente el único método de pago disponible para usar en este pedido.');
define('TABLE_HEADING_COMMENTS', 'Comentarios:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continuar con el registro');
define('EMAIL_SUBJECT', 'Welcome to ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Dear %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Dear %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Dear %s' . "\n\n");
define('EMAIL_WELCOME', 'Welcome to <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'Ahora tiene acceso a algunas funciones adicionales que están disponibles sólo para usuarios registrados <br /><Li> <b> Cart </b> - Cualquier producto agregado al carro, permanece allí hasta que usted o suprimido o los ordena<br /><Li> <b> Directorio </b> - Ahora podemos enviar nuestros productos a la dirección que especificó en "Dirección de entrega".<br /><Li> <b> Historial de pedidos </b> - Usted tiene la oportunidad de mirar a través de la historia de los pedidos en nuestra tienda.');
define('EMAIL_CONTACT', 'Si tiene preguntas, envíenos un correo electrónico: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '');

// Start - CREDIT CLASS Gift Voucher Contribution
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Como parte de nuestra bienvenida a nuevos clientes, le hemos enviado un vale de regalo electrónico de %s');
define('EMAIL_GV_REDEEM', 'El código de canje del vale de regalo electrónico es% s, puedes ingresar el código de canjear al salir mientras realizas una compra');
define('EMAIL_GV_LINK', 'O siguiendo este enlace ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Enhorabuena, para que su primera visita a nuestra tienda en línea sea una experiencia más gratificante, le estamos enviando un cupón de descuento electrónico.' . "\n" .
										' A continuación se detallan los detalles del cupón de descuento creado para usted' . "\n");
define('EMAIL_COUPON_REDEEM', 'Para usar el cupón, ingrese el código de canje que es% s durante la compra mientras realiza una compra');
// End - CREDIT CLASS Gift Voucher Contribution
define('TEXT_PLEASE_SELECT', 'Seleccionar');
define('TEXT_PASSWORD_FORGOTTEN', 'Se te olvidó tu contraseña?');
define('IMAGE_LOGIN', 'Iniciar sesión');
define('TEXT_HAVE_COUPON_KGT', '¿Tiene un cupón?');
define('TEXT_DIFFERENT_SHIPPING', '¿Diferente de la dirección de facturación?');
define('TEXT_DIFFERENT_BILLING', '¿Dirección de envío diferente?');
// Points/Rewards Module V2.1rc2a EOF
define('TEXT_MIN_SUM', 'PRECIO DE LA ORDEN MÍNIMA');
define('TEXT_NEW_CUSTOMER', 'Nuevo cliente');
define('TEXT_LOGIN_SOCIAL', 'Facebook login');
define('TEXT_REGISTRATION_OFF', 'Hacer un pedido sin registro');

define('TEXT_EMAIL_EXISTS', 'Este correo electrónico ya está registrado, por favor');
define('TEXT_EMAIL_EXISTS2', 'login');
define('TEXT_EMAIL_EXISTS3', 'A través de su correo electrónico');
define('TEXT_EMAIL_WRONG', 'Ingresaste un correo electrónico incorrecto');
define('TEXT_ORDER_PROCESSING', 'Procesamiento de pedidos, por favor espere...');
define('TEXT_EMAIL_LOGIN', 'Login');
define('TEXT_EMAIL_PASS', 'Contraseña');

define('TEXT_CHANGE_ADDRESS', 'Cambiar dirección');
define('ADDRESS_BOOK', 'Directorio');
define('BILLING_ADDRESS_THE_SAME', 'la misma');

define('CH_JS_REFRESH', 'Refrescante');
define('CH_JS_REFRESH_METHOD', 'Método refrescante:');
define('CH_JS_SETTING_METHOD', 'Método de ajuste:');
define('CH_JS_SETTING_ADDRESS', 'Dirección de configuración:');
define('CH_JS_SETTING_ADDRESS_BIL', 'Facturación');
define('CH_JS_SETTING_ADDRESS_SHIP', 'Envío');

define('CH_JS_ERROR_SCART', 'Se ha producido un error al actualizar el carrito de la compra.');
define('CH_JS_ERROR_SOME1', 'refrescante');
define('CH_JS_ERROR_SOME2', 'Se produjo un error, por favor informe');

define('CH_JS_ERROR_SET_SOME1', 'Se ha producido un error al configurar');
define('CH_JS_ERROR_SET_SOME2', 'Método, por favor informe');
define('CH_JS_ERROR_SET_SOME3', 'Acerca de este error.');

define('CH_JS_ERROR_REQ_BIL', 'Rellene los campos obligatorios en la sección "Dirección de pago"');
define('CH_JS_ERROR_ERR_BIL', 'Por favor, marque los campos en la sección "Dirección de pago"');
define('CH_JS_ERROR_REQ_SHIP', 'Por favor, rellene los campos obligatorios en la sección "Dirección de envío"');
define('CH_JS_ERROR_ERR_SHIP', 'Por favor, marque los campos en la sección "Dirección de Envío"');
define('CH_JS_ERROR_ADDRESS', 'Error de dirección');
define('CH_JS_ERROR_PMETHOD', 'Error al seleccionar el método de pago');
define('CH_JS_ERROR_SELECT_PMETHOD', 'Seleccione el método de pago');
define('CH_JS_CHECK_EMAIL', 'Comprobación de su dirección de correo electrónico');
define('CH_JS_ERROR_EMAIL', 'Se ha producido un error al verificar las direcciones de correo electrónico.');
?>