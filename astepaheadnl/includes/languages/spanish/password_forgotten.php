<?php
/*
  $Id: password_forgotten.php,v 1.8 2003/06/09 22:46:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Iniciar sesion');
define('NAVBAR_TITLE_2', 'Contraseña olvidada');
define('HEADING_TITLE', 'He olvidado mi contrasena!');
define('TEXT_MAIN', 'Ingrese su direccion de correo electronico le enviaremos un mensaje con su nueva contraseña.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Error: La direccion de correo electronico no se encontro en nuestros registros. Intentelo de nuevo.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Nueva contrasena');
define('EMAIL_PASSWORD_REMINDER_BODY', 'Se solicito una nueva contrasena ' . $REMOTE_ADDR . '.' . "\n\n" . 'Su nueva contrasena para \'' . STORE_NAME . '\' is:' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'Exito: Se ha enviado una nueva contrasena a su direccion de correo electronico.');