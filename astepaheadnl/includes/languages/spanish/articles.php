<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');

if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
  define('HEADING_TITLE', $topics['Nombre de los temas']);
  define('TEXT_NO_ARTICLES', 'Actualmente no hay articulos en este tema.');
  define('TEXT_ALL_AUTHORS', 'Todos los autores');
  define('TEXT_ARTICLES', 'A continuacion se muestra una lista de articulos con los mas recientes listados primero.');
  define('TEXT_DATE_ADDED', 'Publicado:');
  define('TEXT_TOPIC', 'Tema:');
  define('TEXT_BY', 'por');
} elseif ($topic_depth == 'top') {
  define('HEADING_TITLE', 'Todos los articulos');
  define('TEXT_CURRENT_ARTICLES', 'Articulos actuales');
  define('TEXT_UPCOMING_ARTICLES', 'Proximos articulos');
  define('TEXT_NO_ARTICLES', 'No hay articulos en este momento.');
  define('TEXT_DATE_ADDED', 'Publicado:');
  define('TEXT_DATE_EXPECTED', 'Esperado:');
  define('TEXT_TOPIC', 'Tema:');
  define('TEXT_BY', 'por');
} elseif ($topic_depth == 'nested') {
  define('HEADING_TITLE', 'Articulos');
}
  define('TOTAL_ARTICLES', 'Total de articulos');
  define('HEADING_TITLE', 'Articulos');
