<?php
/*
  $Id $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Desconectarse');
define('NAVBAR_TITLE', 'Desconectarse');
define('TEXT_MAIN', 'Se ha desconectado su cuenta. ');
?>
