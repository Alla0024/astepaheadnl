<?php
/*
  $Id: ot_coupon.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Discount Coupons');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Gift Vouchers/Discount Coupons');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Discount Coupon');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Invalid Coupon Code');
define('ERROR_MINIMUM_CART_AMOUNT', 'El monto mínimo del carrito para este cupón es: %s');
define('ERROR_INVALID_STARTDATE_COUPON', 'This coupon is not available yet');
define('ERROR_INVALID_FINISDATE_COUPON', 'This coupon has expired');
define('ERROR_INVALID_USES_COUPON', 'This coupon could only be used ');
define('TIMES', ' times.');
define('ERROR_INVALID_USES_USER_COUPON', 'You have used the coupon the maximum number of times allowed per customer.');
define('TEXT_ENTER_COUPON_CODE', 'Enter Redeem Code&nbsp;&nbsp;');