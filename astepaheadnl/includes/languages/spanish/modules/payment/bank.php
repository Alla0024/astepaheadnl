<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Transferencia de banco');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', 'Nuestros datos bancarios:<br><br>Nombre del banco: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . '<br>Cuenta bancaria: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . '<br>IMF: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . '<br>Caja / factura: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . '<br>INN: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . '<br>Destinatario: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . '<br>PPC: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . '<br>Propósito del pago: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . '<br><br>Después de pagar el pedido, asegúrese de informarnos por correo electrónico <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS
  . '</a> sobre el hecho del pago. Su pedido será enviado inmediatamente después de la confirmación del pago.<br><br><a href=kvitan.php target=_blank><b><span>Recibo de pago</a></b>');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Nuestros datos bancarios:\n\nNombre del banco: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . "<br>Cuenta bancaria: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . "<br>IMF: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . "<br>Caja / factura: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . "<br>INN: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . "<br>Destinatario: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . "\nPPC: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . "\nPropósito del pago: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . "\n\nDespués de pagar el pedido, asegúrese de informarnos por correo electrónico " . STORE_OWNER_EMAIL_ADDRESS . " sobre el hecho del pago. Su pedido será enviado inmediatamente después de la confirmación del pago.");
