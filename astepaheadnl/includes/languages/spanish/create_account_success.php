<?php
/*
  $Id: create_account_success.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Crea una cuenta');
define('NAVBAR_TITLE_2', 'Exito');
define('HEADING_TITLE', 'Tu cuenta ha sido creada!');
define('TEXT_ACCOUNT_CREATED', 'Gracias por unirte a nuestra tienda en linea! En breves momentos recibiras un correo confirmando la creacion de tu cuenta.');
?>