<?php
/*
  $Id: wishlist.php,v 3.0  2005/04/20 Dennis Blake
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Mi lista de deseos contiene:');
define('BOX_TEXT_PRICE', 'Precio');
define('BOX_TEXT_PRODUCT', 'Nombre del producto');
define('BOX_TEXT_IMAGE', 'Imagen');
define('BOX_TEXT_NO_ITEMS', 'No hay productos en su lista de deseos. <br><br>');
define('TEXT_EMAIL', 'Email: ');
define('TEXT_MESSAGE', 'Mensaje: ');
define('TEXT_ITEM_IN_CART', 'Articulo en el carrito');
define('TEXT_ITEM_NOT_AVAILABLE', 'El articulo ya no esta disponible');
define('WISHLIST_EMAIL_SUBJECT', 'Te ha enviado su lista de deseos de ' . STORE_NAME);  //Customers name will be automatically added to the beginning of this.
define('WISHLIST_SENT', 'Se ha enviado su lista de deseos.');
define('WISHLIST_EMAIL_LINK', '

$from_name\'s public wishlist is located here:
$link

Thank you,
' . STORE_NAME); //$from_name = Customers name  $link = public wishlist link

define('WISHLIST_EMAIL_GUEST', 'Thank you,
' . STORE_NAME);

define('ERROR_YOUR_NAME' , 'Por favor, escriba su nombre..');
define('ERROR_YOUR_EMAIL' , 'Por favor introduzca su correo electronico.');
define('ERROR_VALID_EMAIL' , 'Por favor, introduce una direccion de correo electronico valida.');
define('ERROR_ONE_EMAIL' , 'Debe incluir al menos un nombre y un correo electronico.');
define('ERROR_ENTER_EMAIL' , 'Por favor introduzca una direccion de correo electronico.');
define('ERROR_ENTER_NAME' , 'Introduzca el nombre de los destinatarios de correo electronico.');
define('ERROR_MESSAGE', 'Por favor incluya un breve mensaje.');
?>
