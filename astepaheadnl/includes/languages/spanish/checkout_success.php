<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Revisa');
define('NAVBAR_TITLE_2', 'Exito');
define('HEADING_TITLE', '?Su orden ha sido procesada!');
define('TEXT_SUCCESS', '?Su pedido ha sido procesado correctamente! Sus productos llegaran a su destino dentro de 2-5 dias laborables.');
define('TEXT_THANKS_FOR_SHOPPING', 'Gracias por comprar con nosotros en linea!');
define('TABLE_HEADING_COMMENTS', 'Introduzca un comentario para el pedido procesado');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Fecha de caducidad: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' Descargas restantes');
define('SMS_NEW_ORDER', 'Tienes un nuevo pedido');
define('TEXT_SUCCESS_INFO', 'Todas las instrucciones y detalles adicionales para el pago le han sido enviados por correo');