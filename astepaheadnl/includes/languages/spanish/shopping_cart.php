<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Contenido del carro');
define('HEADING_TITLE', 'Carrito de compras');
define('TABLE_HEADING_REMOVE', 'retirar');
define('TABLE_HEADING_REMOVE_FROM', 'De carro');
define('TABLE_HEADING_QUANTITY', 'Cantidad.');
define('TABLE_HEADING_IMAGE', 'Imagen');
define('TABLE_HEADING_NAME', 'Nombre');
define('TABLE_HEADING_PRICE', 'Precio');
define('TABLE_HEADING_MODEL', 'Modelo');
define('TABLE_HEADING_PRODUCTS', 'Producto(s)');
define('TABLE_HEADING_PRICE_TOTAL', 'Total');

define('TABLE_HEADING_TOTAL', 'Total');
define('TEXT_CART_EMPTY', 'Inicie sesión para realizar su compra');
define('SUB_TITLE_COUPON', 'Cupon');
define('SUB_TITLE_COUPON_SUBMIT', 'Enviar');
define('SUB_TITLE_COUPON_VALID', 'Su cupon es valido. Compruebe el descuento:');
define('SUB_TITLE_COUPON_INVALID', 'Su cupon no es valido.');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Productos marcados con ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' No existe en la cantidad deseada en nuestro stock. <br> Por favor, modifique la cantidad de productos marcados con (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), Gracias');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Productos marcados con ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' No existen en la cantidad deseada en nuestro stock. <br> Usted puede comprar de todos modos y comprobar la cantidad que tenemos en stock para entrega inmediata en el proceso de pago.');
?>