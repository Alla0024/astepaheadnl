<?php
/*
  $Id: address_book_process.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Mi cuenta');
define('NAVBAR_TITLE_2', 'Directorio');
define('NAVBAR_TITLE_ADD_ENTRY', 'Nueva entrada');
define('NAVBAR_TITLE_MODIFY_ENTRY', 'Actualizar entrada');
define('NAVBAR_TITLE_DELETE_ENTRY', 'Eliminar la entrada');
define('HEADING_TITLE_ADD_ENTRY', 'Nueva entrada de la libreta de direcciones');
define('HEADING_TITLE_MODIFY_ENTRY', 'Actualizar entrada de la libreta de direcciones');
define('HEADING_TITLE_DELETE_ENTRY', 'Eliminar entrada de la libreta de direcciones');
define('DELETE_ADDRESS_TITLE', 'Eliminar direccion');
define('DELETE_ADDRESS_DESCRIPTION', '?Esta seguro de que desea eliminar la direccion seleccionada de su libreta de direcciones?');
define('NEW_ADDRESS_TITLE', 'Nueva entrada de la libreta de direcciones');
define('SELECTED_ADDRESS', 'Direccion seleccionada');
define('SET_AS_PRIMARY', 'Establecer como direccion principal.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'La direccion seleccionada se ha eliminado correctamente de su libreta de direcciones.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Su libreta de direcciones se ha actualizado correctamente.');
define('WARNING_PRIMARY_ADDRESS_DELETION', 'No se puede eliminar la direccion principal. Establezca otra direccion como direccion principal y vuelva a intentarlo.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'La entrada de la libreta de direcciones no existe.');
define('ERROR_ADDRESS_BOOK_FULL', 'Su libreta de direcciones esta llena. Elimine una direccion innecesaria para guardar una nueva.');
