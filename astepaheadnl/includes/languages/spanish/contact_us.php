<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Contactenos');
define('HEADING_SUBTITLE', 'Contáctenos ahora y nuestros especialistas nos responderán pronto!');
define('NAVBAR_TITLE', 'Contactenos');

define('TEXT_SUCCESS', 'Tu solicitud ha sido enviada correctamente al propietario de la tienda.');
define('EMAIL_SUBJECT', 'Consulta desde ' . STORE_NAME);
define('ENTRY_NAME', 'Nombre completo:');
define('ENTRY_EMAIL', 'Direccion de correo electronico:');
define('ENTRY_PHONE', 'Numero de telefono:');
define('ENTRY_ENQUIRY', 'Investigacion:');
define('SEND_TO_TEXT', 'Enviar a:');
define('SEND_TO_TYPE', ''); //Change to this for a dropdown menu.
define('POP_CONTACT_US','Envienos un correo electronico y le llamaremos!');