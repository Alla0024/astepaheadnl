<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', '?Producto no encontrado!');
define('TEXT_DATE_ADDED', 'Este producto fue anadido a nuestro catalogo el %s.');
define('TEXT_DATE_AVAILABLE', '<span>Este producto estara en stock el %s.</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Producto anadido a tu lista de deseos');
define('TEXT_COLOR', 'Color');
define('TEXT_SHARE', 'Compartir con amigos');
define('TEXT_REVIEWSES', 'critica');  
define('TEXT_REVIEWSES2', 'Critica');  
define('TEXT_DESCRIPTION', 'Descripcion');
define('TEXT_ATTRIBS', 'Caracteristicas');
define('TEXT_PAYM_SHIP', 'Detalles de envio');
define('SHORT_DESCRIPTION', 'Breve descripción');

//home
define('HOME_TEXT_PAYM_SHIP', 'Garantía');

?>