<?php
/*
  $Id: account_edit.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Mi cuenta');
define('NAVBAR_TITLE_2', 'Editar cuenta');

define('HEADING_TITLE', 'Informacion de mi cuenta');

define('MY_ACCOUNT_TITLE', 'Mi cuenta');

define('SUCCESS_ACCOUNT_UPDATED', 'Tu cuenta ha sido actualizada exitosamente.');
?>
