<?php 
define('NAVBAR_TITLE','Informazioni sull\'utilizzo di certificati');
define('HEADING_TITLE','Informazioni sull\'utilizzo di certificati');
define('TEXT_INFORMATION','<a name=\"Top\"></a>  <a href=\"'.tep_href_link(FILENAME_GV_FAQ,'faq_item=1',NONSSL).'\">Come acquistare il certificato?</a><br>  <a href=\"'.tep_href_link(FILENAME_GV_FAQ,'faq_item=2',NONSSL).'\">Come inviare il certificato di chiunque altro?</a><br>  <a href=\"'.tep_href_link(FILENAME_GV_FAQ,'faq_item=3',NONSSL).'\">a cosa serve il certificato?</a><br>  <a href=\"'.tep_href_link(FILENAME_GV_FAQ,'faq_item=4',NONSSL).'\">Come utilizzare il certificato al momento dell\'ordine?</a><br>  <a href=\"'.tep_href_link(FILENAME_GV_FAQ,'faq_item=5',NONSSL).'\">Cosa fare in caso di problemi, domande durante l\'utilizzo di certificati?</a><br> ');
define('SUB_HEADING_TITLE','');
define('SUB_HEADING_TEXT','Scegli la tua domanda.');
