<?php 
define('MODULE_PAYMENT_STRIPE_TEXT_TITLE','Stripe');
define('MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION','Stripe - pagamenti on-line');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER','Il proprietario della carta di credito:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER','Numero di carta di credito:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM','scadenza MM:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY','scadenza YY:');
define('MODULE_PAYMENT_STRIPE_TEXT_ERROR','Errore carta di credito!');
