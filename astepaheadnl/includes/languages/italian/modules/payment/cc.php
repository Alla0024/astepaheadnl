<?php 
define('MODULE_PAYMENT_CC_TEXT_TITLE','Carta di credito');
define('MODULE_PAYMENT_CC_TEXT_DESCRIPTION','Informazioni sulla carta di credito per la pasta:<br><br>il Numero di carta: 4111111111111111<br>Valida fino al: Qualsiasi data');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_OWNER','Il proprietario della carta di credito:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_NUMBER','Numero di carta di credito:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_EXPIRES','Valida fino al:');
define('MODULE_PAYMENT_CC_TEXT_ERROR','I dati inseriti in modo errato!');
