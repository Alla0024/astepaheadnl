<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Bonifico bancario');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', 'Le nostre coordinate bancarie:<br><br>Nome della banca: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . '<br>Conto bancario: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . '<br>MFI: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . '<br>Casella / fattura: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . '<br>INN: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . '<br>Destinatario: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . '<br>Trasmissione: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . '<br>Scopo del pagamento: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . '<br><br>Dopo aver pagato l\'ordine, informaci via e-mail <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS
  . '</a> sul fatto del pagamento. Il tuo ordine verrà inviato immediatamente dopo la conferma del pagamento.<br><br><a href=kvitan.php target=_blank><b><span>Ricevuta per pagamento</a></b>');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Le nostre coordinate bancarie:\n\nNome della banca: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . "<br>Conto bancario: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . "<br>MFI: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . "<br>Casella / fattura: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . "<br>INN: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . "<br>Destinatario: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . "\nTrasmissione: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . "\nScopo del pagamento: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . "\n\nDopo aver pagato l'ordine, informaci via e-mail " . STORE_OWNER_EMAIL_ADDRESS . " sul fatto del pagamento. Il tuo ordine verrà inviato immediatamente dopo la conferma del pagamento.");
