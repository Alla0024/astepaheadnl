<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Descrizione');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Abilita / disabilita il modulo');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Stato predefinito');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Conto commerciante');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Segreto mercantile');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'L\'ordinamento');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Stato di pagamento');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'True - abilita, False - disabilita');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Lo stato impostato sull\'ordine al momento della creazione');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Conto commerciante');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Segreto mercantile');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Ordinamento del modulo nel blocco con moduli di pagamento');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'Lo stato impostato sull\'ordine in caso di pagamento andato a buon fine');
