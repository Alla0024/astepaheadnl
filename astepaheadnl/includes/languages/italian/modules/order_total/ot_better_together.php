<?php 
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE','Accompagnamento di sconto');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION','Accompagnamento di sconto');
define('TWOFER_PROMO_STRING','Acquista questo prodotto e riceverai un altro gratis!');
define('TWOFER_QUALIFY_STRING','È possibile ottenere un %s gratis');
define('OFF_STRING_PCT','-%s');
define('OFF_STRING_CURR','-%s');
define('SECOND',' secondo ');
define('FREE_STRING',' gratis');
define('REV_GET_ANY','Acquistare l\'oggetto dal ');
define('REV_GET_THIS','Acquistare ');
define('REV_GET_DISC',' otterrete questo articolo ');
define('FREE',' gratis');
define('GET_YOUR_PROD',' otterrete ');
define('GET_YOUR_CAT',' ottenere la vostra scelta di ');
