<?php 
define('HEADING_ARTICLE_NOT_FOUND','Articolo non trovato');
define('TEXT_ARTICLE_NOT_FOUND','Scusate, ma l\'articolo che Hai richiesto non è disponibile!');
define('TEXT_DATE_ADDED','Questo articolo è stato pubblicato il %s.');
define('TEXT_DATE_AVAILABLE','Questo articolo sarà disponibile %s.');
define('TEXT_BY','by ');
