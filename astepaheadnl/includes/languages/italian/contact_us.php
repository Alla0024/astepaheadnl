<?php 
define('HEADING_TITLE','CONTATTI');
define('HEADING_SUBTITLE','Contattaci subito e i nostri esperti risponderanno a noi al più presto!');
define('NAVBAR_TITLE','Scrivere');
define('TEXT_SUCCESS','Il tuo messaggio è stato inviato con successo!');
define('EMAIL_SUBJECT','Messaggio da '.STORE_NAME);
define('ENTRY_NAME','Il tuo nome:');
define('ENTRY_EMAIL','Indirizzo E-Mail:');
define('ENTRY_PHONE','Il telefono:');
define('ENTRY_ENQUIRY','Messaggio:');
define('SEND_TO_TEXT','Lettera a:');
define('POP_CONTACT_US','Esprimi la domanda, e Vi richiameremo');
