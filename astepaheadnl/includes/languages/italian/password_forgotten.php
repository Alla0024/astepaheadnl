<?php 
define('NAVBAR_TITLE_1','Ingresso');
define('NAVBAR_TITLE_2','Il recupero della password');
define('HEADING_TITLE','Ho dimenticato la password!');
define('TEXT_MAIN','Se Hai dimenticato la tua password, inserisci il tuo indirizzo e-mail e vi invieremo la password per e-mail che Hai indicato.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND','<span><b>Errore:</b></span> indirizzo E-Mail non corrisponde al Tuo account, si prega di riprovare.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT','STORE_NAME - la Tua password');
define('EMAIL_PASSWORD_REMINDER_BODY','Richiesta di una nuova password è stata ricevuta da .La tua nuova password nel '.STORE_NAME.' :%s');
define('SUCCESS_PASSWORD_SENT','Fatto: la Tua nuova password inviata per e-mail.');
