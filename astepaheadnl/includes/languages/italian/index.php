<?php 
define('TEXT_MAIN','&nbsp;');
define('TABLE_HEADING_DATE_EXPECTED','La data di ricevimento');
define('TABLE_HEADING_DEFAULT_SPECIALS','Sconto %s');
define('HEADING_TITLE','Elenco dei prodotti');
define('TABLE_HEADING_IMAGE','<span class=\"sort\">Ordina per:</span>');
define('TABLE_HEADING_MODEL','Codice articolo');
define('TABLE_HEADING_PRODUCTS','<span class=\"cena\">Nome del prodotto</span>');
define('TABLE_HEADING_MANUFACTURER','Il costruttore');
define('TABLE_HEADING_QUANTITY','Il numero di');
define('TABLE_HEADING_PRICE','<span class=\"cena\">Prezzo</span>');
define('TABLE_HEADING_WEIGHT','Peso');
define('TABLE_HEADING_PRODUCT_SORT','L\'ordine');
define('TEXT_ALL_MANUFACTURERS','Tutti i produttori');
