<?php 
define('NAVBAR_TITLE_1','I miei indirizzi');
define('NAVBAR_TITLE_2','Rubrica');
define('HEADING_TITLE','La mia rubrica');
define('PRIMARY_ADDRESS_TITLE','Il vostro indirizzo');
define('PRIMARY_ADDRESS_DESCRIPTION','Questo indirizzo è selezionata per impostazione predefinita.<br>sarà utilizzato durante gli acquisti e qualsiasi altra azione nel nostro negozio.');
define('ADDRESS_BOOK_TITLE','Voci della rubrica');
define('PRIMARY_ADDRESS','(*)');
define('TEXT_MAXIMUM_ENTRIES','<b>NOTA:</b> il volume Massimo rubrica - <b>%s</b> record');
