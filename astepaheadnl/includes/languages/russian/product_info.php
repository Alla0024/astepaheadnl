<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Товар не найден!');
define('TEXT_DATE_ADDED', 'Товар был добавлен в наш каталог %s');
define('TEXT_DATE_AVAILABLE', '<span>Товар будет в наличии %s</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Товар успешно отложен!');
define('TEXT_COLOR', 'Цвет');
define('TEXT_SHARE', 'Поделитесь с друзьями');
define('TEXT_REVIEWSES', 'отзывов');  
define('TEXT_REVIEWSES2', 'Отзывы');   
define('TEXT_DESCRIPTION', 'Описание');
define('TEXT_ATTRIBS', 'Характеристики');
define('TEXT_PAYM_SHIP', 'Оплата и доставка');
define('SHORT_DESCRIPTION', 'Краткое описание');

//home
define('HOME_TEXT_PAYM_SHIP', 'Гарантия');