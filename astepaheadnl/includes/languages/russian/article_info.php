<?php
/*
  $Id: article_info.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_ARTICLE_NOT_FOUND', 'Статья не найдена');
define('TEXT_ARTICLE_NOT_FOUND', 'Извините, но статья, которую Вы запросили, недоступна!');
define('TEXT_DATE_ADDED', 'Эта статья была опубликована %s.');
define('TEXT_DATE_AVAILABLE', 'Данная статья будет доступна %s.');
define('TEXT_BY', 'by ');
