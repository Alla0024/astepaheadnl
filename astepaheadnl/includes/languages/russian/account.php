<?php
/*
  $Id: account.php
*/

define('NAVBAR_TITLE', 'Мои данные');
define('HEADING_TITLE', 'Мои данные');
define('MY_ACCOUNT_TITLE', 'Мои данные');

define('MY_ACCOUNT_PASSWORD', 'Изменить пароль.');
define('MY_ORDERS_VIEW', 'Просмотр моих заказов.');
define('MY_NAVI', 'Меню');
define('MY_INFO', 'Моя информация');
define('EDIT_ADDRESS_BOOK', 'Редактировать адрес');
