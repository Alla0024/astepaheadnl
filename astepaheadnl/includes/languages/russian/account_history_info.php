<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Мои данные');
define('NAVBAR_TITLE_2', 'История заказов');
define('NAVBAR_TITLE_3', 'Информация о заказах');
define('HEADING_TITLE', 'Информация о заказах');
define('HEADING_ORDER_NUMBER', 'Заказ #%s');
define('HEADING_ORDER_DATE', 'Дата');
define('HEADING_ORDER_TOTAL', 'Стоимость заказа:');
define('HEADING_DELIVERY_ADDRESS', 'Адрес доставки');
define('HEADING_SHIPPING_METHOD', 'Способ доставки');
define('HEADING_PRODUCTS', 'Товар');
define('HEADING_TAX', 'Налог');
define('HEADING_TOTAL', 'Всего');
define('HEADING_PAYMENT_METHOD', 'Способ оплаты');
define('HEADING_ORDER_HISTORY', 'История заказа');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Ссылка действительна до: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' раз можно загрузить файл.');
define('TABLE_HEADING_DATE_ADDED', 'Дата');
define('TABLE_HEADING_STATUS', 'Статус заказа');
define('TABLE_HEADING_COMMENTS', 'Комментарии');
