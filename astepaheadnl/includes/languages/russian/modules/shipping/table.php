<?php
/*
  $Id: table.php,v 1.5 2002/11/19 01:48:08 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SHIPPING_TABLE_TEXT_TITLE', 'Расчет стоимости');
define('MODULE_SHIPPING_TABLE_TEXT_DESCRIPTION', 'Расчет стоимости в зависимости от веса или цены товара');
define('MODULE_SHIPPING_TABLE_TEXT_WAY', 'Расчет стоимости');

?>