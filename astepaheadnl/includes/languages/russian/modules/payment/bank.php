<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Безналичный расчет');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', 'Наши банковские реквизиты:<br><br>Название банка: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . '<br>Расчетный счет: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . '<br>МФО: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . '<br>Кор./счет: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . '<br>ИНН: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . '<br>Получатель: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . '<br>КПП: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . '<br>Назначение платежа: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . '<br><br>После оплаты заказа обязательно сообщите нам по электронной почте <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS
  . '</a> о факте оплаты. Ваш заказ будет отправлен сразу после подтверждения факта оплаты.<br><br><a href=kvitan.php target=_blank><b><span>Квитанция для оплаты</a></b>');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Наши банковские реквизиты:\n\nНазвание банка: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . "<br>Расчетный счет: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . "<br>МФО: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . "<br>Кор./счет: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . "<br>ИНН: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . "<br>Получатель: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . "\nКПП: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . "\nНазначение платежа: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . "\n\nПосле оплаты заказа обязательно сообщите нам по электронной почте " . STORE_OWNER_EMAIL_ADDRESS . " о факте оплаты. Ваш заказ будет отправлен сразу после подтверждения факта оплаты.");
