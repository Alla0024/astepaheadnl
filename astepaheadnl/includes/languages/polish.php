<?php
/*
  $Id: russian.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'en_US.ISO_8859-1');  
@setlocale(LC_TIME, 'pl_PL.UTF-8');
define('OG_LOCALE', 'pl_PL');
define('GO_COMPARE', 'W porównaniu');
define('IN_WHISHLIST', 'W liście życzeń');
define('COMPARE', 'Porównać');
define('WHISH', 'Lista życzeń');

// HMCS: Begin Autologon   ******************************************************************
// HMCS: End Autologon     ******************************************************************
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // DATE_FORMAT_SHORTthis is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y r.'); // this is used for strftime()
//define('DATE_FORMAT_SHORT', '%d/%m/%Y');
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT', 'd.m.Y h:i:s'); // this is used for date()
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


define('TEXT_DAY_1','Poniedziałek');
define('TEXT_DAY_2','Wtorek');
define('TEXT_DAY_3','Środa');
define('TEXT_DAY_4','Czwartek');
define('TEXT_DAY_5','Piątek');
define('TEXT_DAY_6','Sobota');
define('TEXT_DAY_7','Niedziela');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1','Styczeń');
define('TEXT_MONTH_BASE_2','Stycznia');
define('TEXT_MONTH_BASE_3','Marzec');
define('TEXT_MONTH_BASE_4','Kwiecień');
define('TEXT_MONTH_BASE_5','Maj');
define('TEXT_MONTH_BASE_6','Czerwiec');
define('TEXT_MONTH_BASE_7','Lipiec');
define('TEXT_MONTH_BASE_8','Sierpień');
define('TEXT_MONTH_BASE_9','Wrzesień');
define('TEXT_MONTH_BASE_10','Październik');
define('TEXT_MONTH_BASE_11','Listopad');
define('TEXT_MONTH_BASE_12','Grudzień');
define('TEXT_MONTH_1','Stycznia');
define('TEXT_MONTH_2','Lutego');
define('TEXT_MONTH_3','Marca');
define('TEXT_MONTH_4','Kwietnia');
define('TEXT_MONTH_5','Maja');
define('TEXT_MONTH_6','Czerwca');
define('TEXT_MONTH_7','Lipca');
define('TEXT_MONTH_8','Sierpnia');
define('TEXT_MONTH_9','Września');
define('TEXT_MONTH_10','Października');
define('TEXT_MONTH_11','Listopada');
define('TEXT_MONTH_12','Grudnia');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'EUR');

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="LTR" lang="pl"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Sklep internetowy');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Rejestracja');
define('HEADER_TITLE_CHECKOUT', 'Złożyć zamówienie');
define('HEADER_TITLE_TOP', 'Strona główna');
define('HEADER_TITLE_LOGOFF', 'Wyjście');

define('HEAD_TITLE_LOGIN', 'Zaloguj się');
define('HEAD_TITLE_COMPARE', 'Porównanie');
define('HEAD_TITLE_WISHLIST', 'Ulubione produkty');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Zapomniane hasło');

define('HEAD_TITLE_CHECKOUT', 'Złożenie zamówienia');
define('HEAD_TITLE_CHECKOUT_SUCCESS', 'Twoje zamówienie zostało pomyślnie złożone!');
define('HEAD_TITLE_ACCOUNT', 'Twoje konto');
define('HEAD_TITLE_ACCOUNT_HISTORY', 'Moje zamówienia');
define('HEAD_TITLE_ACCOUNT_EDIT', 'Wyświetl i edytuj moje dane');
define('HEAD_TITLE_ADDRESS_BOOK', 'Książka adresowa');
define('HEAD_TITLE_ACCOUNT_PASSWORD', 'Zmienić hasło');
define('HEAD_TITLE_ALLCOMMENTS', 'Wszystkie komentarze ');
define('HEAD_TITLE_CONTACT_US','Kontakty ze sklepem '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML sitemap - '.STORE_NAME);
define('HEAD_TITLE_404','Error 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Error 403 - '.STORE_NAME);

// text for gender
define('MALE', 'Mężczyzna');
define('FEMALE', 'Kobieta');


// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Twój koszyk jest pusty');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Pełna lista produktów');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Wybierz');
define('PULL_DOWN_COUNTRY', 'Powierzchnia:');
define('TYPE_BELOW', 'Wybór poniżej');

// javascript messages
define('JS_ERROR', 'Błąd podczas wypełniania formularza!\n\nProszę zmienić:\n\n');


define('JS_FIRST_NAME', '* Pole \'Imię\' musi zawierać co najmniej ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' znaków.\n');
define('JS_LAST_NAME', '* Pole \'Nazwisko\' musi zawierać co najmniej ' . ENTRY_LAST_NAME_MIN_LENGTH . ' znaków.\n');


define('JS_ERROR_SUBMITTED', 'Ta forma jest już wypełniona. Naciśnij Ok.');


define('CATEGORY_COMPANY', 'Firma');
define('CATEGORY_PERSONAL', ' Twoje dane');
define('CATEGORY_ADDRESS', 'Twój adres');
define('CATEGORY_CONTACT', 'Dane kontaktowe');
define('CATEGORY_OPTIONS', 'Newslettery');

define('ENTRY_COMPANY', 'Nazwa firmy:');
define('ENTRY_COMPANY_ERROR', 'dsfds');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER', 'Płeć:');
define('ENTRY_GENDER_ERROR', 'Musisz określić swoją płeć.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME', 'Imię:');
define('ENTRY_TEXT_FOM_IMAGE', 'Wprowadź tekst z obrazu');
define('ENTRY_FIRST_NAME_ERROR', 'Pole Imię musi zawierać co najmniej ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' znaków.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Nazwisko:');
define('ENTRY_LAST_NAME_ERROR', 'Pole Nazwisko musi zawierać co najmniej ' . ENTRY_LAST_NAME_MIN_LENGTH . ' znaków.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Data urodzenia:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Data urodzenia musi być wpisana w następującym formacie: DD/MM/RRRR (na przykład 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (przykład 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Pole E-Mail musi zawierać co najmniej ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' znaków.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Niepoprawnie wprowadziłeś E-Mail, spróbuj jeszcze raz.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Wprowadzony adres E-Mail jest już zarejestrowany w naszym sklepie, spróbuj podać inny adres E-Mail.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Adres:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Pole Adres i numer domu musi zawierać co najmniej ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' znaków.');
define('ENTRY_STREET_ADDRESS_TEXT', '* Przykład: ul. Kazimierza Wielkiego 10/5');
define('ENTRY_SUBURB', 'Powiat:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Kod pocztowy:');
define('ENTRY_POST_CODE_ERROR', 'Pole Kod pocztowy musi zawierać co najmniej ' . ENTRY_POSTCODE_MIN_LENGTH . ' znaków.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Miasto:');
define('ENTRY_CITY_ERROR', 'Pole Miasto musi zawierać co najmniej ' . ENTRY_CITY_MIN_LENGTH . ' znaków.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Województwo:');
define('ENTRY_STATE_ERROR', 'Pole Województwo musi zawierać co najmniej' . ENTRY_STATE_MIN_LENGTH . ' znaków.');
define('ENTRY_STATE_ERROR_SELECT', 'Wybierz województwo.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Kraj:');
define('ENTRY_COUNTRY_ERROR', 'Wybierz kraj.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Numer telefonu:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Pole Numer telefonu musi zawierać co najmniej ' . ENTRY_TELEPHONE_MIN_LENGTH . ' znaków.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Faks:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Otrzymuj informacje o zniżkach, nagrodach, prezentach:');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Subskrybuj');
define('ENTRY_NEWSLETTER_NO', 'Zrezygnować z subskrypcji');
define('ENTRY_PASSWORD', 'Hasło:');
define('ENTRY_PASSWORD_ERROR', 'Twoje Hasło musi zawierać co najmniej ' . ENTRY_PASSWORD_MIN_LENGTH . ' znaków.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Pole Potwierdź hasło musi pasować do pola Hasło.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Potwierdź hasło:');
define('ENTRY_PASSWORD_CURRENT', 'Aktualne hasło:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Pole Hasło musi zawierać co najmniej ' . ENTRY_PASSWORD_MIN_LENGTH . ' znaków.');
define('ENTRY_PASSWORD_NEW', 'Nowe hasło:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Twoje nowe hasło musi zawierać co najmniej ' . ENTRY_PASSWORD_MIN_LENGTH . ' znaków.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Pole Potwierdź hasło musi pasować do pola Nowe hasło.');

define('FORM_REQUIRED_INFORMATION', '* Są wymagane');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Strony:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Wyświetlono <b>%d</b> - <b>%d</b> z <b>%d</b> pozycji');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Wyświetlono <b>%d</b> - <b>%d</b> (łącznie <b>%d</b> zamówień)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Wyświetlono <b>%d</b> - <b>%d</b> (łącznie <b>%d</b> ofert specjalnych)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Wyświetlono <b>%d</b> - <b>%d</b> (łącznie <b>%d</b> towarów rekomendowanych)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Poprzednia');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Następna strona');
define('PREVNEXT_TITLE_PAGE_NO', 'Strona %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Poprzednie %d stron');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Następne %d stron');
define('PREVNEXT_BUTTON_PREV', '<<');
define('PREVNEXT_BUTTON_NEXT', '>>');

define('PREVNEXT_TITLE_PPAGE', 'Strona');

define('ART_XSELL_TITLE', 'Według tematu');
define('PROD_XSELL_TITLE', 'Powiązane towary');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Dodaj adres');
define('IMAGE_BUTTON_BACK', 'Powrót');
define('IMAGE_BUTTON_CHECKOUT', 'Złóż zamówienie');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Potwierdź zamówienie');
define('IMAGE_BUTTON_CONTINUE', 'Dalej');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continue Shopping');
define('IMAGE_BUTTON_DELETE', 'Wyczyść');
define('IMAGE_BUTTON_LOGIN', 'Przejdź');
define('IMAGE_BUTTON_IN_CART', 'W koszyku');
define('IMAGE_BUTTON_ADDTO_CART', 'Kupić');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Kupić');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'Do koszyka');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Wypróbuj za darmo!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Kup ten sklep internetowy już teraz!');
define('MESSAGE_BUY_TEMPLATE1', 'Niestety Twój 14-dniowy okres testowy dobiegł końca. Proszę ');
define('MESSAGE_BUY_TEMPLATE2', 'zapłać');
define('MESSAGE_BUY_TEMPLATE3', ' za ten sklep lub stwórz kolejną stronę testową.');

define('CUSTOM_PANEL_EDIT', 'Edytuj ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'towary');
define('CUSTOM_PANEL_EDIT_MANUF', 'producent');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'artykuł');
define('CUSTOM_PANEL_EDIT_SECTION', 'sekcja');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'kategoria');
define('CUSTOM_PANEL_EDIT_SEO', 'seotext');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Panel administracyjny');
define('CUSTOM_PANEL_ADD', 'Dodaj');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Produkt');
define('CUSTOM_PANEL_ADD_PAGE', 'Strona');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Rabat');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Kategoria');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statystyki');
define('CUSTOM_PANEL_ADD_ONLINE', 'Na stronie: ');
define('CUSTOM_PANEL_PALETTE', 'Paleta strony');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Kolor tekstu');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Tło nagłówka');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Tło stopka');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Kolor linku');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Kolor przycisku');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Kolor tła');
define('CUSTOM_PANEL_ORDERS', 'Zamówienia');
define('CUSTOM_PANEL_ORDERS_NEW', 'Nowość: ');


define('CUSTOM_PANEL_DATE1', 'dzień');
define('CUSTOM_PANEL_DATE2', 'dni');
define('CUSTOM_PANEL_DATE3', 'dni');


define('IMAGE_BUTTON_UPDATE', 'Zaktualizuj');
define('IMAGE_REDEEM_VOUCHER', 'Zastosuj');

define('SMALL_IMAGE_BUTTON_VIEW', 'Przejrzyj');


define('ICON_ERROR', 'Błąd');
define('ICON_SUCCESS', 'Gotowe');
define('ICON_WARNING', 'Uwaga');

define('TEXT_GREETING_PERSONAL', 'Witamy, <span class="greetUser">%s!</span> Chcesz zobaczyć <a href="%s"><u>nowe produkty</u></a> w naszym sklepie?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Witamy');
define('TEXT_GREETING_GUEST', 'Witamy, <span class="greetUser">DROGI UŻYTKOWNIKU!</span><br> Jeżeli jesteś naszym stałym klientem, <a href="%s"><u>wpisz Swoje dane</u></a> dla wejścia.  Jeżeli jesteś z nami po raz pierwszy i chcesz robić zakupy, <a href="%s"><u>załóż konto</u></a>.');

define('TEXT_SORT_PRODUCTS', 'Sortowanie:');
define('TEXT_DESCENDINGLY', 'malejąco');
define('TEXT_ASCENDINGLY', 'rosnąco');
define('TEXT_BY', ', kolumna ');

define('TEXT_UNKNOWN_TAX_RATE', 'Stawka podatku jest nieznana');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'Uwaga: Sklep jest zamknięty z powodów technicznych do: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'Uwaga: Sklep jest zamknięty z powodów technicznych.');

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Ostrzeżenie: katalog do zainstalowania sklepu nie został usunięty: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Usuń ten katalog ze względów bezpieczeństwa.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Ostrzeżenie: Plik konfiguracyjny jest dostępny do zapisywania: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. Jest to potencjalne zagrożenie bezpieczeństwa - ustaw niezbędne uprawnienia do tego pliku.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Ostrzeżenie: katalog sesji nie istnieje: ' . tep_session_save_path() . '. Sesje nie będą działać, dopóki ten katalog nie zostanie utworzony.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Ostrzeżenie: nie można uzyskać dostępu do katalogu sesji: ' . tep_session_save_path() . '. Sesje nie będą działać, dopóki nie zostaną ustawione niezbędne prawa dostępu.');
define('WARNING_SESSION_AUTO_START', 'Ostrzeżenie: funkcja session.auto_start jest włączona - wyłącz tę opcję w pliku php.ini i zrestartuj serwer WWW.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'Wprowadziłeś nieprawidłową datę ważności karty płatniczej.<br>Spróbuj ponownie.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Podałeś nieprawidłowy numer karty płatniczej.<br>Spróbuj ponownie.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Pierwsze cyfry Twojej karty płatniczej: %s<br>Jeżeli podałeś prawidłowy numer karty płatniczej, informujemy, że nie akceptujemy tego typu kart płatniczych.<br>Jeżeli podałeś zły numer karty kredytowej, spróbuj ponownie.');


define('BOX_LOGINBOX_HEADING', 'Logowanie');
define('IMAGE_BUTTON_LOGIN', 'Zaloguj się');
define('RENDER_LOGIN_WITH', 'Login with');

define('LOGIN_BOX_MY_CABINET', 'Twoje konto');
define('MY_ORDERS_VIEW', 'Moje zamówienia');
define('MY_ACCOUNT_PASSWORD', 'Zmienić hasło');
define('LOGIN_BOX_ADDRESS_BOOK', 'Książka adresowa');
define('LOGIN_BOX_LOGOFF', 'Wyloguj się');

define('LOGIN_FROM_SITE', 'Zaloguj się');


define('LOGO_IMAGE_TITLE','Logo');

// VJ Guestbook for OSC v1.0 begin
define('BOX_INFORMATION_GUESTBOOK', 'Księga gości');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('GUESTBOOK_TEXT_MIN_LENGTH', '10'); //[TODO] move to config db table
define('JS_GUESTBOOK_TEXT', '* Pole \'Twoja wiadomość\' musi zawierać ' . GUESTBOOK_TEXT_MIN_LENGTH . ' znaków.\n');
define('JS_GUESTBOOK_NAME', '* Musisz wypełnić pole \'Twoje imię\'.\n');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_DISPLAY_NUMBER_OF_GUESTBOOK_ENTRIES', 'Wyświetlono <b>%d</b> - <b>%d</b> (ogółem <b>%d</b> zapisów)');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('IMAGE_BUTTON_SIGN_GUESTBOOK', 'Dodaj zapis');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_GUESTBOOK_DATE_ADDED', 'Data: %s');
define('TEXT_NO_GUESTBOOK_ENTRY', 'Obecnie nie ma wpisów w księdze gości. Bądź pierwszy!');
// VJ Guestbook for OSC v1.0 end


// Article Manager
define('BOX_ALL_ARTICLES', 'Wszystkie artykuły');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', '<font color="#5a5a5a">Wyświetlono <b>%d</b> - <b>%d</b> (ogółem <b>%d</b> aktualności)</font>');
define('NAVBAR_TITLE_DEFAULT', 'Artykuły');
define('TEXT_NAVIGATION_BRANDS','Wszyscy producenci');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT', ''); //define('PRICES_LOGGED_IN_TEXT', '<b><font style="color:#CE1930">Cena: </b></font><a href="create_account.tpl.php">tylko hurt</a>');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO', 'Minimalna ilość zamówienia: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART', 'Minimalna ilość zamówienia: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO', 'Krok: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART', 'Krok: '); // order_detail.php
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT', '');
define('ERROR_PRODUCTS_QUANTITY_INVALID', 'Próbujesz umieścić do koszyka niewłaściwą ilość towaru: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT', '');
define('ERROR_PRODUCTS_UNITS_INVALID', 'Próbujesz umieścić do koszyka niewłaściwą ilość towaru: ');

// Comments 

define('ADD_COMMENT_HEAD_TITLE', 'Zostaw opinię');

// Poll Box Text
define('_RESULTS', 'Wyniki');
define('_VOTE', 'Głosuj');
define('_VOTES', 'Głosów:');

define('PREV_NEXT_PRODUCT', 'Produkt ');
define('PREV_NEXT_CAT', ' kategorie');
define('PREV_NEXT_MB', ' producenta ');


// boxes names

define('BOX_HEADING_CATEGORIES', 'Rozdziały');
define('BOX_HEADING_INFORMATION', 'Informacja');
define('BOX_HEADING_MANUFACTURERS', 'Producenci');
define('BUTTON_SHOPPING_CART_OPEN', 'Open Shopping Cart');
define('BOX_HEADING_SPECIALS', 'Rabaty');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Rabaty %s');
define('BOX_OPEN_SEARCH_FORM', 'Open Search Form');
define('BOX_HEADING_SEARCH', 'Szukaj');
define('BOX_HEADING_WHATS_NEW', 'Nowości');
define('BOX_HEADING_FEATURED', 'Rekomendowane');
define('BOX_HEADING_ARTICLES', 'Artykuły');
define('BOX_HEADING_LINKS', 'Wymiana linków');
define('BOX_HEADING_SHOPPING_ENTER', 'Otwórz koszyk');
define('HELP_HEADING', 'Konsultant');
define('BOX_HEADING_WISHLIST', 'Lista życzeń');
define('BOX_HEADING_BESTSELLERS', 'Bestsellery');
define('BOX_HEADING_CURRENCY', 'Waluta');
define('_POLLS', 'Ankiety');
define('BOX_HEADING_CALLBACK', 'Zamówić połączenie');
define('BOX_HEADING_CONSULTANT', 'Konsultant online');
define('BOX_HEADING_PRODUCTS', 'Produktów');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Dodaj kategorie produktów.');
define('BOX_HEADING_LASTVIEWED', 'Obejrzane produkty');

define('BOX_BESTSELLERS_NO', 'W tej sekcji jeszcze nie ma liderów sprzedaży');

define('SHIPPING_OPTIONS', 'Sposoby i koszt dostawy:');

define('LOW_STOCK_TEXT1', 'Produkt na magazynie się kończy: ');
define('LOW_STOCK_TEXT2', 'Kod produktu: ');
define('LOW_STOCK_TEXT3', 'Aktualna ilość: ');
define('LOW_STOCK_TEXT4', 'Link na produkt: ');
define('LOW_STOCK_TEXT5', 'Bieżąca wartość zmiennej Limit ilości towarów w magazynie: ');

// wishlist box text in includes/boxes/wishlist.php

define('BOX_TEXT_NO_ITEMS', 'Lista życzeń jest pusta.');


define('TOTAL_TIME', 'Czas wykonania: ');
define('TOTAL_CART', 'Razem');

// otf 1.71 defines needed for Product Option Type feature.
define('TEXT_PREFIX', 'txt_');
define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES

//define('NAVBAR_TITLE', 'Koszyk');
define('SUB_TITLE_FROM', 'Od:');
define('SUB_TITLE_REVIEW', 'Tekst wiadomości:');
define('SUB_TITLE_RATING', 'Ocena:');

// Product tabs

define('ALSO_PURCHASED', 'Kupowane razem z tym produktem');


// Paginator
define('FORWARD', 'Dalej');
define('COMP_PROD_NAME', 'Nazwa');
define('COMP_PROD_IMG', 'Zdjęcie');
define('COMP_PROD_PRICE', 'Cena');
define('COMP_PROD_CLEAR', 'Wyczyść wszystko');
define('COMP_PROD_BACK', 'Wróć');
define('COMP_PROD_ADD_TO', 'Dodaj elementy do porównania!');
define('TEXT_PASSWORD_FORGOTTEN', 'Nie pamiętasz hasła?');
define('QUICK_ORDER', 'Szybkie zamówienie');
define('QUICK_ORDER_SUCCESS', 'Wniosek został wysłany, wkrótce menedżer skontaktuje się z Tobą');
define('QUICK_ORDER_BUTTON', 'Kup jednym kliknięciem');
define('SEND_MESSAGE', 'Wysłać');

define('LABEL_NEW', 'Nowość');
define('LABEL_TOP', 'TOP');
define('LABEL_SPECIAL', 'Promocja');

define('FILTER_BRAND', 'Marka');
define('FILTER_ALL', 'wszystkie');
define('WISHLIST_PC', 'szt.');

define('FOOTER_INFO', 'Informacje');
define('FOOTER_CATEGORIES', 'Sekcje');
define('FOOTER_ARTICLES', 'Artykuły');
define('FOOTER_CONTACTS', 'Kontakt');
define('FOOTER_SITEMAP', 'Mapa strony');
define('FOOTER_DEVELOPED', 'Rozbudowa sklepu');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Pokaż wszystkie kategorie');
define('SHOW_ALL_ARTICLES','Pokaż wszystkie artykuły');

define('TEXT_HEADING_PRICE', 'Katalog:');
define('TEXT_HEADING_CATALOG', 'Katalog');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Attributes');
define('TEXT_PRODUCTS_QTY', 'Products Quantity');

define('MAIN_NEWS', 'Wiadomości');
define('MAIN_NEWS_ALL', 'Wszystkie wiadomości');
define('MAIN_NEWS_SUBSCRIBE', 'Subskrybuj <span>na wiadomości</span>');
define('MAIN_NEWS_SUBSCRIBE_BUT', 'Subskrybuj');
define('MAIN_NEWS_EMAIL', 'Twój e-mail');

define('MAIN_BESTSELLERS', 'TOP Sprzedaży');
define('MAIN_REVIEWS', 'Opinie klientów');
define('MAIN_REVIEWS_ALL', 'Wszystkie opinie');
define('MAIN_MOSTVIEWED', 'TOP wyświetleń');

define('LIST_TEMP_INSTOCK', 'Dostępny');
define('LIST_TEMP_OUTSTOCK', 'Niedostępny');

define('HIGHSLIDE_CLOSE', 'Zamknij');
define('BUTTON_CANCEL', 'Anuluj');
define('BUTTON_SEND', 'Wyślij');

define('LISTING_PER_PAGE', 'Na stronie:');
define('LISTING_SORT_NAME', 'Alfabetycznie, A-Z');
define('LISTING_SORT_PRICE1', 'od najniższej');
define('LISTING_SORT_PRICE2', 'od najwyższej');
define('LISTING_SORT_NEW', 'najnowsze');
define('LISTING_SORT_POPULAR', 'popularne');
define('LISTING_SORT_LIST', 'lista');
define('LISTING_SORT_COLUMNS', 'kolumny');

define('PROD_DRUGIE', 'Podobne produkty');
define('PROD_FILTERS', 'Filtry');

define('PAGE404_TITLE', '404 Błąd');
define('PAGE404_DESC', 'Nie odnaleziono strony');
define('PAGE404_REASONS', 'Może być kilka powodów:');
define('PAGE404_REASON1', 'Strona została przeniesiona lub zmieniona');
define('PAGE404_REASON2', 'Strona już nie istnieje na tym serwerze');
define('PAGE404_REASON3', 'URL jest nieprawidłowy');
define('PAGE404_BACK1', 'Wróć');
define('PAGE404_BACK2', 'Powrót do strony głównej');

// 403
define('PAGE403_DESC', 'Nie masz uprawnień, aby wyświetlić tę stronę..');
define('PAGE403_TITLE','Błąd 403');
define('PAGE403_TO_HOME_PAGE','Powrót do strony głównej');

define('SB_SUBSCRIBER', 'Obserwujący');
define('SB_EMAIL_NAME', 'Zapisz się do newslettera');
define('SB_EMAIL_USER', 'Użytkownik');
define('SB_EMAIL_WAS_SUBSCRIBED', 'Subskrybujesz');
define('SB_EMAIL_ALREADY','już subskrybujesz nasze biuletyny.');
define("LOAD_MORE_BUTTON", "Pokaż więcej");
define("TIME_LEFT", "Do końca wersji próbnej");

define('CLO_NEW_PRODUCTS', 'Ostatnio dodane produkty');
define('CLO_FEATURED', 'Te produkty mogą Cię zainteresować');
define('CLO_DESCRIPTION_BESTSELLERS', 'TOP Sprzedaży');
define('CLO_DESCRIPTION_SPECIALS', 'Rabaty');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Najczęściej oglądane');

define('SHOPRULES_AGREE', 'Zgadzam się z');
define('SHOPRULES_AGREE2', 'zasadami sklepu');

define("SHOW_ALL_SUBCATS", "zobacz wszystkie");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Darmowa dostawa");


//home
define('HOME_BOX_HEADING_SEARCH', 'szukaj...');
define('HEADER_TITLE_OR', 'lub');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'dodaj');
define('HOME_IMAGE_BUTTON_IN_CART', 'do koszyka');
define('HOME_ARTICLE_READ_MORE', 'czytaj więcej');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'zapisz się na nowy newsletter, a <span> uzyskaj zniżkę 25% </span> na jeden produkt w zamówieniu online');
define('HOME_MAIN_NEWS_EMAIL', 'Wpisz swój e-mail');
define('HOME_FOOTER_DEVELOPED', 'tworzenie sklepu internetowego');
define('HOME_FOOTER_CATEGORIES', 'Katalog');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Dodaj komentarz do produktu');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Imię');
define('HOME_ADD_COMMENT_FORM', 'Komentarz');
define('HOME_REPLY_TO_COMMENT', 'Odpowiedz, aby dodać komentarz');
define('HOME_PROD_DRUGIE', 'Oglądane produkty');
define("HOME_LOAD_MORE_INFO", "więcej");
define("HOME_LOAD_ROLL_UP", "zwinąć");
define("HOME_TITLE_LEFT_CATEGORIES", "towary");
define('HOME_BOX_HEADING_SELL_OUT', 'Wyprzedaż');
define("HOME_LOAD_MORE_BUTTON", "pobierz więcej produktów");
define("HOME_BOX_HEADING_WISHLIST", "Ulubione produkty");
define("HOME_PROD_VENDOR_CODE", "numer artykułu ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Promocje');
define('HOME_FOOTER_SOCIAL_TITLE', 'jesteśmy w sieciach społecznościowych:');
define('HOME_HEADER_SOME_LIST', 'Zobacz także');

define('SEARCH_LANG', 'pl/');


define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'Wszystkie wyniki');
define('ENTER_KEY', 'Enter keywords');
define('TEXT_LIMIT_REACHED', 'Maximum products in compares reached: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Product was successfully added to your cart!');
define('CHOOSE_ADDRESS', 'Wybierz adres');


//default2

define('DEMO2_TOP_PHONES', 'Telefony');
define('DEMO2_TOP_CALLBACK', 'Oddzwonienie');
define('DEMO2_TOP_ONLINE', 'Czat online');
define('DEMO2_SHOPPING_CART', 'Koszyk');
define('DEMO2_LOGIN_WITH', 'Logowanie z ');
define('DEMO2_WISHLIST_LINK', 'Lista życzeń');
define('DEMO2_FOOTER_CATEGORIES', 'Katalog produktów');
define('DEMO2_MY_INFO', 'Edytuj dane');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Zmień adres wysyłki');
define('DEMO2_LEFT_CAT_TITLE', 'Kategorie');
define('DEMO2_LEFT_ALL_GOODS', 'Wszystkie towary');
define('DEMO2_TITLE_SLIDER_LINK', 'Zobacz wszystko');
define('DEMO2_READ_MORE', 'Czytaj dalej <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Zwinąć <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Wszystkie kategorie');
define('DEMO2_SHOW_ALL_ARTICLES','Wszystkie artykuły');
define('DEMO2_BTN_COME_BACK','Wróć');
define('DEMO2_SHARE_TEXT','Udostępnij to');
define('DEMO2_QUICK_ORDER_BUTTON', 'Jednym kliknięciem');


















