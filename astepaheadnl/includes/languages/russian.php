<?php
/*
  $Id: russian.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'en_US.ISO_8859-1');  
@setlocale(LC_TIME, 'ru_RU.UTF-8');
define('OG_LOCALE', 'ru_RU');
define('GO_COMPARE', 'В сравнении');
define('IN_WHISHLIST', 'В желаниях');
define('COMPARE', 'Сравнить');
define('WHISH', 'Желания');

// HMCS: Begin Autologon   ******************************************************************
// HMCS: End Autologon     ******************************************************************
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // DATE_FORMAT_SHORTthis is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y г.'); // this is used for strftime()
//define('DATE_FORMAT_SHORT', '%d/%m/%Y');
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
define('DATE_FORMAT', DEFAULT_DATE_FORMAT); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
define('TEXT_DAY_1','Понедельник');
define('TEXT_DAY_2','Вторник');
define('TEXT_DAY_3','Среда');
define('TEXT_DAY_4','Четверг');
define('TEXT_DAY_5','Пятница');
define('TEXT_DAY_6','Суббота');
define('TEXT_DAY_7','Воскресенье');
define('TEXT_DAY_SHORT_1','Пн');
define('TEXT_DAY_SHORT_2','Вт');
define('TEXT_DAY_SHORT_3','Ср');
define('TEXT_DAY_SHORT_4','Чт');
define('TEXT_DAY_SHORT_5','Пт');
define('TEXT_DAY_SHORT_6','Сб');
define('TEXT_DAY_SHORT_7','ВС');
define('TEXT_MONTH_BASE_1','Январь');
define('TEXT_MONTH_BASE_2','Февраль');
define('TEXT_MONTH_BASE_3','Март');
define('TEXT_MONTH_BASE_4','Апрель');
define('TEXT_MONTH_BASE_5','Май');
define('TEXT_MONTH_BASE_6','Июнь');
define('TEXT_MONTH_BASE_7','Июль');
define('TEXT_MONTH_BASE_8','Август');
define('TEXT_MONTH_BASE_9','Сентябрь');
define('TEXT_MONTH_BASE_10','Октябрь');
define('TEXT_MONTH_BASE_11','Ноябрь');
define('TEXT_MONTH_BASE_12','Декабрь');
define('TEXT_MONTH_1','Января');
define('TEXT_MONTH_2','Февраля');
define('TEXT_MONTH_3','Марта');
define('TEXT_MONTH_4','Апреля');
define('TEXT_MONTH_5','Мая');
define('TEXT_MONTH_6','Июня');
define('TEXT_MONTH_7','Июля');
define('TEXT_MONTH_8','Августа');
define('TEXT_MONTH_9','Сентября');
define('TEXT_MONTH_10','Октября');
define('TEXT_MONTH_11','Ноября');
define('TEXT_MONTH_12','Декабря');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'UAH');

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="LTR" lang="ru"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Интернет-магазин');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Регистрация');
define('HEADER_TITLE_CHECKOUT', 'Оформить заказ');
define('HEADER_TITLE_TOP', 'Главная');
define('HEADER_TITLE_LOGOFF', 'Выход');

define('HEAD_TITLE_LOGIN', 'Авторизация');
define('HEAD_TITLE_COMPARE', 'Сравнение');
define('HEAD_TITLE_WISHLIST', 'Понравившиеся товары');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Забытый пароль');

define('HEAD_TITLE_CHECKOUT', 'Оформление заказа');
define('HEAD_TITLE_CHECKOUT_SUCCESS', 'Ваш заказ успешно оформлен!');
define('HEAD_TITLE_ACCOUNT', 'Ваш кабинет');
define('HEAD_TITLE_ACCOUNT_HISTORY', 'Мои заказы');
define('HEAD_TITLE_ACCOUNT_EDIT', 'Просмотр и редактирование моих данных');
define('HEAD_TITLE_ADDRESS_BOOK', 'Адресная книга');
define('HEAD_TITLE_ACCOUNT_PASSWORD', 'Изменить пароль');
define('HEAD_TITLE_ALLCOMMENTS', 'Все комментарии');
define('HEAD_TITLE_CONTACT_US','Контакты магазина '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML карта сайта - '.STORE_NAME);
define('HEAD_TITLE_404','Ошибка 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Ошибка 403 - '.STORE_NAME);

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Корзина пуста');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Полный список товаров');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Выберите');
define('PULL_DOWN_COUNTRY', 'Область:');
define('TYPE_BELOW', 'Выбор ниже');

// javascript messages
define('JS_ERROR', 'Ошибки при заполнении формы!\n\nИсправьте пожалуйста:\n\n\n');


define('JS_FIRST_NAME', '* Поле \'Имя\' должно содержать не менее ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' символов.\n');
define('JS_LAST_NAME', '* Поле \'Фамилия\' должно содержать не менее \' . ENTRY_LAST_NAME_MIN_LENGTH . \' символов.\\n\\n');


define('JS_ERROR_SUBMITTED', 'Эта форма уже заполнена. Нажимайте Ok.');


define('CATEGORY_COMPANY', 'Организация');
define('CATEGORY_PERSONAL', 'Ваши персональные данные');
define('CATEGORY_ADDRESS', 'Ваш адрес');
define('CATEGORY_CONTACT', 'Контактная информация');
define('CATEGORY_OPTIONS', 'Рассылка');

define('ENTRY_COMPANY', 'Название компании:');
define('ENTRY_COMPANY_ERROR', 'dsfds');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_FIRST_NAME', 'Имя:');
define('ENTRY_TEXT_FOM_IMAGE', 'Введите текст с картинки');
define('ENTRY_FIRST_NAME_ERROR', 'Поле Имя должно содержать как минимум ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' символа.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Фамилия:');
define('ENTRY_LAST_NAME_ERROR', 'Поле Фамилия должно содержать как минимум ' . ENTRY_LAST_NAME_MIN_LENGTH . ' символа.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Дата рождения:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Дату рождения необходимо вводить в следующем формате: DD/MM/YYYY (пример 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (пример 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Поле E-Mail должно содержать как минимум ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' символов.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Ваш E-Mail адрес указан неправильно, попробуйте ещё раз.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Введённый Вами E-Mail уже зарегистрирован в нашем магазине, попробуйте указать другой E-Mail адрес.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Адрес:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Поле Адрес и номер дома должно содержать как минимум ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' символов.');
define('ENTRY_STREET_ADDRESS_TEXT', '* Пример: ул. Гетьмана 1Б, офис. 2');
define('ENTRY_SUBURB', 'Район:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Почтовый индекс:');
define('ENTRY_POST_CODE_ERROR', 'Поле Почтовый индекс должно содержать как минимум ' . ENTRY_POSTCODE_MIN_LENGTH . ' символа.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Город:');
define('ENTRY_CITY_ERROR', 'Поле Город должно содержать как минимум ' . ENTRY_CITY_MIN_LENGTH . ' символа.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Область:');
define('ENTRY_STATE_ERROR', 'Поле Область должно содержать как минимум ' . ENTRY_STATE_MIN_LENGTH . ' символа.');
define('ENTRY_STATE_ERROR_SELECT', 'Выберите область.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Страна:');
define('ENTRY_COUNTRY_ERROR', 'Выберите страну.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Телефон:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Поле Телефон должно содержать как минимум ' . ENTRY_TELEPHONE_MIN_LENGTH . ' символа.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Факс:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Получать новости');
define('ENTRY_NEWSLETTER_TEXT', 'Получать информацию о скидках, призах, подарках:');
define('ENTRY_NEWSLETTER_YES', 'Подписаться');
define('ENTRY_NEWSLETTER_NO', 'Отказаться от подписки');
define('ENTRY_PASSWORD', 'Пароль:');
define('ENTRY_PASSWORD_ERROR', 'Ваш пароль должен содержать как минимум ' . ENTRY_PASSWORD_MIN_LENGTH . ' символов.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Поле Подтвердите пароль должно совпадать с полем Пароль.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Подтвердите пароль:');
define('ENTRY_PASSWORD_CURRENT', 'Текущий пароль:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Поле Пароль должно содержать как минимум ' . ENTRY_PASSWORD_MIN_LENGTH . ' символов.');
define('ENTRY_PASSWORD_NEW', 'Новый пароль:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Ваш Новый пароль должен содержать как минимум ' . ENTRY_PASSWORD_MIN_LENGTH . ' символов.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Поля Подтвердите пароль и Новый пароль должны совпадать.');

define('FORM_REQUIRED_INFORMATION', '* Обязательно для заполнения');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Страницы:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Показано <b>%d</b> - <b>%d</b> из <b>%d</b> позиций');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Показано <b>%d</b> - <b>%d</b> (всего <b>%d</b> заказов)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Показано <b>%d</b> - <b>%d</b> (всего <b>%d</b> специальных предложений)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Показано <b>%d</b> - <b>%d</b> (всего <b>%d</b> рекомендуемых товаров)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'предыдущая');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Следующая страница');
define('PREVNEXT_TITLE_PAGE_NO', 'Страница %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Предыдущие %d страниц');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Следующие %d страниц');
define('PREVNEXT_BUTTON_PREV', '<<');
define('PREVNEXT_BUTTON_NEXT', '>>');
define('TEXT_CLOSE_BUTTON', 'Close');

define('PREVNEXT_TITLE_PPAGE', 'Страница');

define('ART_XSELL_TITLE', 'По теме');
define('PROD_XSELL_TITLE', 'Сопутствующие товары');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Добавить адрес');
define('IMAGE_BUTTON_BACK', 'Назад');
define('IMAGE_BUTTON_CHECKOUT', 'Оформить заказ');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Подтвердить заказ');
define('IMAGE_BUTTON_CONTINUE', 'Продолжить');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Продолжить покупки');
define('IMAGE_BUTTON_DELETE', 'Удалить');
define('IMAGE_BUTTON_LOGIN', 'Войти');
define('IMAGE_BUTTON_IN_CART', 'В корзине');
define('IMAGE_BUTTON_ADDTO_CART', 'Купить');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Добавить в корзину');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'В корзину');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Попробуйте этот шаблон бесплатно!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Купите этот магазин!');
define('MESSAGE_BUY_TEMPLATE1', 'К сожалению ваш тестовый 14-дневный период окончен. Пожалуйста, ');
define('MESSAGE_BUY_TEMPLATE2', 'купите');
define('MESSAGE_BUY_TEMPLATE3', ' данный магазин или создайте еще один тестовый сайт.');

define('CUSTOM_PANEL_EDIT', 'Редактировать ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'товар');
define('CUSTOM_PANEL_EDIT_MANUF', 'производителя');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'статью');
define('CUSTOM_PANEL_EDIT_SECTION', 'раздел');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'категорию');
define('CUSTOM_PANEL_EDIT_SEO', 'сеотекст');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Админпанель');
define('CUSTOM_PANEL_ADD', 'Добавить');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Товар');
define('CUSTOM_PANEL_ADD_PAGE', 'Страница');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Скидка');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Категория');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Статистика');
define('CUSTOM_PANEL_ADD_ONLINE', 'На сайте: ');
define('CUSTOM_PANEL_PALETTE', 'Палитра сайта');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Цвет текста');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Фон шапки');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Фон подвала');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Цвет ссылок');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Цвет кнопок');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Цвет фона');
define('CUSTOM_PANEL_ORDERS', 'Заказы');
define('CUSTOM_PANEL_ORDERS_NEW', 'Новые: ');


define('CUSTOM_PANEL_DATE1', 'день');
define('CUSTOM_PANEL_DATE2', 'дня');
define('CUSTOM_PANEL_DATE3', 'дней');


define('IMAGE_BUTTON_UPDATE', 'Обновить');
define('IMAGE_REDEEM_VOUCHER', 'Применить');

define('SMALL_IMAGE_BUTTON_VIEW', 'Смотреть');


define('ICON_ERROR', 'Ошибка');
define('ICON_SUCCESS', 'Выполнено');
define('ICON_WARNING', 'Внимание');

define('TEXT_GREETING_PERSONAL', 'Добро пожаловать, <span class="greetUser">%s!</span> Вы хотите посмотреть какие <a href="%s"><u>новые товары</u></a> поступили в наш магазин?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Добро пожаловать');
define('TEXT_GREETING_GUEST', 'Добро пожаловать, <span class="greetUser">УВАЖАЕМЫЙ ГОСТЬ!</span><br> Если Вы наш постоянный клиент, <a href="%s"><u>введите Ваши персональные данные</u></a> для входа. Если Вы у нас впервые и хотите сделать покупки, Вам необходимо <a href="%s"><u>зарегистрироваться</u></a>.');

define('TEXT_SORT_PRODUCTS', 'Сортировка:');
define('TEXT_DESCENDINGLY', 'по убыванию');
define('TEXT_ASCENDINGLY', 'по возрастанию');
define('TEXT_BY', ', колонка ');

define('TEXT_UNKNOWN_TAX_RATE', 'Неизвестна налоговая ставка');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'Внимание: Магазин закрыт по техническим причинам до: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'Внимание: Магазин закрыт по техническим причинам');
define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Предупреждение: Не удалена директория установки магазина: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Пожалуйста, удалите эту директорию для безопасности.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Предупреждение: Файл конфигурации доступен для записи: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. Это - потенциальный риск безопасности - пожалуйста, установите необходимые права доступа к этому файлу.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Предупреждение: директория сессий не существует: ' . tep_session_save_path() . '. Сессии не будут работать пока эта директория не будет создана.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Предупреждение: Нет доступа к каталогу сессий: ' . tep_session_save_path() . '. Сессии не будут работать пока не установлены необходимые права доступа.');
define('WARNING_SESSION_AUTO_START', 'Предупреждение: опция session.auto_start включена - пожалуйста, выключите данную опцию в файле php.ini и перезапустите веб-сервер.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'Вы указали неверную дату истечения срока действия кредитной карточки.<br>Попробуйте ещё раз.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Вы указали неверный номер кредитной карточки.<br>Попробуйте ещё раз.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Первые цифры Вашей кредитной карточки: %s<br>Если Вы указали номер своей кредитной карточки правильно, сообщаем Вам, что мы не принимаем к оплате данный тип кредитных карточек.<br>Если Вы указали номер кредитной карточки неверно, попробуйте ещё раз.');


define('BOX_LOGINBOX_HEADING', 'Вход');
define('IMAGE_BUTTON_LOGIN', 'Войти');
define('RENDER_LOGIN_WITH', 'Войти с помощью');

define('LOGIN_BOX_MY_CABINET', 'Ваш кабинет');
define('MY_ORDERS_VIEW', 'Мои заказы');
define('MY_ACCOUNT_PASSWORD', 'Изменить пароль');
define('LOGIN_BOX_ADDRESS_BOOK', 'Адресная книга');
define('LOGIN_BOX_LOGOFF', 'Выход');

define('LOGIN_FROM_SITE', 'Войти');

define('SORT_BY','Сортировать по');
define('LOGO_IMAGE_TITLE','Логотип');

// VJ Guestbook for OSC v1.0 begin
define('BOX_INFORMATION_GUESTBOOK', 'Гостевая книга');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('GUESTBOOK_TEXT_MIN_LENGTH', '10'); //[TODO] move to config db table
define('JS_GUESTBOOK_TEXT', '* Поле \'Ваше сообщение\' должно содержать как минимум ' . GUESTBOOK_TEXT_MIN_LENGTH . ' символов.\n');
define('JS_GUESTBOOK_NAME', '* Вы должны заполнить поле \'Ваше имя\'.\n');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_DISPLAY_NUMBER_OF_GUESTBOOK_ENTRIES', 'Показано <b>%d</b> - <b>%d</b> (всего <b>%d</b> записей)');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('IMAGE_BUTTON_SIGN_GUESTBOOK', 'Добавить запись');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_GUESTBOOK_DATE_ADDED', 'Дата: %s');
define('TEXT_NO_GUESTBOOK_ENTRY', 'Пока нет ни одной записи в гостевой книге. Будьте первыми!');
// VJ Guestbook for OSC v1.0 end


// Article Manager
define('BOX_ALL_ARTICLES', 'Все статьи');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', '<font color="#5a5a5a">Показано <b>%d</b> - <b>%d</b> (всего <b>%d</b> новостей)</font>');
define('NAVBAR_TITLE_DEFAULT', 'Статьи');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT', '-'); //define('PRICES_LOGGED_IN_TEXT', '<b><font style="color:#CE1930">Цена: </b></font><a href="create_account.tpl.php">только опт</a>');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO', 'Минимум единиц для заказа: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART', 'Минимум единиц для заказа: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO', 'Шаг: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART', 'Шаг: '); // order_detail.php
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT', '');
define('ERROR_PRODUCTS_QUANTITY_INVALID', 'Вы пытаетесь положить в корзину неверное количество товара: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT', '');
define('ERROR_PRODUCTS_UNITS_INVALID', 'Вы пытаетесь положить в корзину неверное количество товара: ');

// Comments 

define('ADD_COMMENT_HEAD_TITLE', 'Оставить свой отзыв');

// Poll Box Text
define('_RESULTS', 'Результаты');
define('_VOTE', 'Голосовать');
define('_VOTES', 'Голосов:');

define('PREV_NEXT_PRODUCT', 'Товар ');
define('PREV_NEXT_CAT', ' категории ');
define('PREV_NEXT_MB', ' производителя ');


// Русские названия боксов 

define('BOX_HEADING_CATEGORIES', 'Разделы');
define('BOX_HEADING_INFORMATION', 'Информация');
define('BOX_HEADING_MANUFACTURERS', 'Производители');
define('BOX_HEADING_SPECIALS', 'Скидки');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Скидки %s');
define('BOX_HEADING_SEARCH', 'Поиск');
define('BOX_OPEN_SEARCH_FORM', 'Открыть поиск');
define('BOX_HEADING_WHATS_NEW', 'Новинки');
define('BOX_HEADING_FEATURED', 'Рекомендуемые');
define('BOX_HEADING_ARTICLES', 'Статьи');
define('BOX_HEADING_LINKS', 'Обмен ссылками');
define('BOX_HEADING_SHOPPING_ENTER', 'Войти в корзину');
define('BUTTON_SHOPPING_CART_OPEN', 'Открыть корзину');
define('HELP_HEADING', 'Консультант');
define('BOX_HEADING_WISHLIST', 'Отложенные товары');
define('BOX_HEADING_BESTSELLERS', 'Лидеры продаж');
define('BOX_HEADING_CURRENCY', 'Валюта');
define('_POLLS', 'Опросы');
define('BOX_HEADING_CALLBACK', 'Перезвоните мне');
define('BOX_HEADING_CONSULTANT', 'Онлайн-консультант');
define('BOX_HEADING_PRODUCTS', 'товаров');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Добавьте категории товаров.');
define('BOX_HEADING_LASTVIEWED', 'Просмотренные товары');

define('BOX_BESTSELLERS_NO', 'В данном разделе пока нет лидеров продаж');

// Способы и стоимость доставки в корзине
define('SHIPPING_OPTIONS', 'Способы и стоимость доставки:');


define('LOW_STOCK_TEXT1', 'Товар на складе заканчивается: ');
define('LOW_STOCK_TEXT2', 'Код товара: ');
define('LOW_STOCK_TEXT3', 'Текущее количество: ');
define('LOW_STOCK_TEXT4', 'Ссылка на товар: ');
define('LOW_STOCK_TEXT5', 'Текущее значение переменной Лимит количества товара на складе: ');

// wishlist box text in includes/boxes/wishlist.php

define('BOX_TEXT_NO_ITEMS', 'Нет отложенных товаров.');

define('TOTAL_CART', 'Итого');
define('TOTAL_TIME', 'Время исполнения: ');

// otf 1.71 defines needed for Product Option Type feature.
define('TEXT_PREFIX', 'txt_');
define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES

//define('NAVBAR_TITLE', 'Корзина');
define('SUB_TITLE_FROM', 'От:');
define('SUB_TITLE_REVIEW', 'Текст сообщения:');
define('SUB_TITLE_RATING', 'Рейтинг:');

// Product tabs

define('ALSO_PURCHASED', 'Также с этим товаром заказывали');


// Paginator
define('FORWARD', 'Вперед');
define('COMP_PROD_NAME', 'Название');
define('COMP_PROD_IMG', 'Картинка');
define('COMP_PROD_PRICE', 'Цена');
define('COMP_PROD_CLEAR', 'Очистить все');
define('COMP_PROD_BACK', 'Вернуться');
define('COMP_PROD_ADD_TO', 'Добавьте товары к сравнению!');
define('TEXT_PASSWORD_FORGOTTEN', 'Забыли пароль?');
define('TEXT_PASSWORD_FORGOTTEN_DO', 'Восстановить пароль');
define('QUICK_ORDER', 'Быстрый заказ');
define('QUICK_ORDER_SUCCESS', 'Заявка отправлена, в скором времени с Вами свяжется менеджер');
define('QUICK_ORDER_BUTTON', 'Купить в один клик');
define('SEND_MESSAGE', 'Отправить');

define('LABEL_NEW', 'Новинка');
define('LABEL_TOP', 'ТОП');
define('LABEL_SPECIAL', 'Акция');

define('FILTER_BRAND', 'Бренд');
define('FILTER_ALL', 'все');
define('WISHLIST_PC', 'шт.');

define('FOOTER_INFO', 'Информация');
define('FOOTER_CATEGORIES', 'Разделы');
define('FOOTER_ARTICLES', 'Статьи');
define('FOOTER_CONTACTS', 'Контакты');
define('FOOTER_SITEMAP', 'Карта сайта');
define('FOOTER_DEVELOPED', 'Создание интернет-магазина');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Показать все категории');
define('SHOW_ALL_ARTICLES','Показать все статьи');
define('TEXT_NAVIGATION_BRANDS','Все производители');


define('TEXT_HEADING_PRICE', 'Каталог:');
define('TEXT_HEADING_CATALOG', 'Каталог');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Атрибуты');
define('TEXT_PRODUCTS_QTY', 'Количество товаров');

define('MAIN_NEWS', 'Новости');
define('MAIN_NEWS_ALL', 'Все новости');
define('MAIN_NEWS_SUBSCRIBE', 'Подписаться <span>на новости</span>');
define('MAIN_NEWS_SUBSCRIBE_BUT', 'Подписаться');
define('MAIN_NEWS_EMAIL', 'Ваш email');

define('MAIN_BESTSELLERS', 'ТОП продаж');
define('MAIN_REVIEWS', 'Отзывы покупателей');
define('MAIN_REVIEWS_ALL', 'Все отзывы');
define('MAIN_MOSTVIEWED', 'ТОП просмотров');

define('LIST_TEMP_INSTOCK', 'В наличии');
define('LIST_TEMP_OUTSTOCK', 'Нет в наличии');

define('HIGHSLIDE_CLOSE', 'Закрыть');
define('BUTTON_CANCEL', 'Вернуться');
define('BUTTON_SEND', 'Отправить');

define('LISTING_PER_PAGE', 'На страницу:');
define('LISTING_SORT_NAME', 'по алфавиту, А-Я');
define('LISTING_SORT_PRICE1', 'дешевле сверху');
define('LISTING_SORT_PRICE2', 'дороже сверху');
define('LISTING_SORT_NEW', 'новые сверху');
define('LISTING_SORT_POPULAR', 'популярные');
define('LISTING_SORT_LIST', 'списком');
define('LISTING_SORT_COLUMNS', 'колонками');

define('PROD_DRUGIE', 'Похожие товары');
define('PROD_FILTERS', 'Фильтры');

define('PAGE404_TITLE', '404 Ошибка');
define('PAGE404_DESC', 'Страница не найдена');
define('PAGE404_REASONS', 'Могут быть несколько причин:');
define('PAGE404_REASON1', 'Страница перемещена или переименована');
define('PAGE404_REASON2', 'Страницы больше не существует на этом cайте');
define('PAGE404_REASON3', 'URL не соответствует действительности');
define('PAGE404_BACK1', 'Вернуться назад');
define('PAGE404_BACK2', 'Вернуться на главную');

// 403
define('PAGE403_DESC', 'У вас нет разрешения для просмотра этой страницы.');
define('PAGE403_TITLE','Ошибка 403');
define('PAGE403_TO_HOME_PAGE','Вернутся на главную страницу');

define('SB_SUBSCRIBER', 'Подписчик');
define('SB_EMAIL_NAME', 'Заявка на рассылку');
define('SB_EMAIL_USER', 'Пользователь');
define('SB_EMAIL_WAS_SUBSCRIBED', 'подписался на рассылку');
define('SB_EMAIL_ALREADY','уже подписан на рассылку');
define("LOAD_MORE_BUTTON", "Показать еще");
define("TIME_LEFT", "До конца пробной версии");

define('CLO_NEW_PRODUCTS', 'Недавно добавленные товары');
define('CLO_FEATURED', 'Эти товары вас заинтересуют');
define('CLO_DESCRIPTION_BESTSELLERS', 'ТОП продаж');
define('CLO_DESCRIPTION_SPECIALS', 'Скидки');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Наиболее просматриваемые');

define('SHOPRULES_AGREE', 'Регистрируясь Вы соглашаетесь с');
define('SHOPRULES_AGREE2', 'условиями использования магазина');

define("SHOW_ALL_SUBCATS", "смотреть все");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Бесплатная доставка");


//home
define('HOME_BOX_HEADING_SEARCH', 'поиск...');
define('HEADER_TITLE_OR', 'или');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'добавить');
define('HOME_IMAGE_BUTTON_IN_CART', 'в корзине');
define('HOME_ARTICLE_READ_MORE', 'читать больше');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'подпишись на новую рассылку и <span>получите скидку 25%</span> на один товар в онлайн-заказе');
define('HOME_MAIN_NEWS_EMAIL', 'Введите ваш e-mail');
define('HOME_FOOTER_DEVELOPED', 'создание интернет магазина');
define('HOME_FOOTER_CATEGORIES', 'Каталог');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Добавь коментарий к товару');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Имя');
define('HOME_ADD_COMMENT_FORM', 'Коментарий');
define('HOME_REPLY_TO_COMMENT', 'Ответить на комментарий');
define('HOME_PROD_DRUGIE', 'Просмотренные товары');
define("HOME_LOAD_MORE_INFO", "подробнее");
define("HOME_LOAD_ROLL_UP", "свернуть");
define("HOME_TITLE_LEFT_CATEGORIES", "товары");
define('HOME_BOX_HEADING_SELL_OUT', 'Распродажа');
define("HOME_LOAD_MORE_BUTTON", "загрузить еще изделия");
define("HOME_BOX_HEADING_WISHLIST", "Любимые товары");
define("HOME_PROD_VENDOR_CODE", "артикул ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Акции');
define('HOME_FOOTER_SOCIAL_TITLE', 'мы в социальных сетях:');
define('HOME_HEADER_SOME_LIST', 'Смотрите также');


define('VK_LOGIN', 'Логин');
define('SHOW_RESULTS', 'Все результаты');
define('ENTER_KEY', 'Введите текст');
define('TEXT_LIMIT_REACHED', 'Достигнуто максимальное количество товаров для сравнения: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Товар успешно добавлен в корзину!');
define('CHOOSE_ADDRESS', 'Выберите адрес');

//default2

define('DEMO2_TOP_PHONES', 'Телефоны');
define('DEMO2_TOP_CALLBACK', 'Callback');
define('DEMO2_TOP_ONLINE', 'Онлайн-чат');
define('DEMO2_SHOPPING_CART', 'Корзина');
define('DEMO2_LOGIN_WITH', 'Войти через ');
define('DEMO2_WISHLIST_LINK', 'Список желаний');
define('DEMO2_FOOTER_CATEGORIES', 'Каталог товаров');
define('DEMO2_MY_INFO', 'Изменить данные');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Изменить адрес доставки');
define('DEMO2_TEXT_CART_EMPTY', '<i class="fa fa-shopping-cart" aria-hidden="true"></i>Добавьте хотя бы один товар');
define('DEMO2_LEFT_CAT_TITLE', 'Категории');
define('DEMO2_LEFT_ALL_GOODS', 'Все товары');
define('DEMO2_TITLE_SLIDER_LINK', 'Смотреть все');
define('DEMO2_READ_MORE', 'Читать дальше <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Cвернуть <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Все категории');
define('DEMO2_SHOW_ALL_ARTICLES','Все статьи');
define('DEMO2_BTN_COME_BACK','Вернуться назад');
define('DEMO2_SHARE_TEXT','Поделиться');
define('DEMO2_QUICK_ORDER_BUTTON', 'В один клик');


define('SEARCH_LANG', 'ru/');

















