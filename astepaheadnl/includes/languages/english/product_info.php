<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Product not found!');
define('TEXT_DATE_ADDED', 'This product was added to our catalog on %s.');
define('TEXT_DATE_AVAILABLE', '<span>This product will be in stock on %s.</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Product has been successfully added to your wishlist');
define('TEXT_COLOR', 'Color');
define('TEXT_SHARE', '');
define('TEXT_REVIEWSES', 'reviews');  
define('TEXT_REVIEWSES2', 'Reviews');  
define('TEXT_DESCRIPTION', 'Description');
define('TEXT_ATTRIBS', 'Characteristics');
define('TEXT_PAYM_SHIP', 'Shipping details');
define('SHORT_DESCRIPTION', 'Short description');
define('TEXT_PRICE_WITHOUT_VAT', 'Excl. VAT');

//home
define('HOME_TEXT_PAYM_SHIP', 'Warranty');
?>