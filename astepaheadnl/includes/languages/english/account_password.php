<?php
/*
  $Id: account_password.php,v 1.1.1.1 2003/09/18 19:04:31 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My account');
define('NAVBAR_TITLE_2', 'Change Password');

define('HEADING_TITLE', 'Change Password');

define('MY_PASSWORD_TITLE', 'Change Password');

define('SUCCESS_PASSWORD_UPDATED', 'Your password has been updated.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Your current password did not match the password in our records. Please try again.');
?>
