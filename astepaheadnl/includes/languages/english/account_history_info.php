<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My account');
define('NAVBAR_TITLE_2', 'My orders');
define('NAVBAR_TITLE_3', 'Information about your order');

define('HEADING_TITLE', 'Information about your order');

define('HEADING_ORDER_NUMBER', 'Order #%s');
define('HEADING_ORDER_DATE', 'Date:  ');
define('HEADING_ORDER_TOTAL', 'Total:  ');

define('HEADING_DELIVERY_ADDRESS', 'Delivery address');
define('HEADING_SHIPPING_METHOD', 'Delivery charges');

define('HEADING_PRODUCTS', 'Products');
define('HEADING_TAX', 'Tax');
define('HEADING_TOTAL', 'Total');

define('HEADING_PAYMENT_METHOD', 'Payment Method');

define('HEADING_ORDER_HISTORY', 'Process');

define('TABLE_HEADING_DOWNLOAD_DATE', 'Link expires: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' downloads remaining');
define('TABLE_HEADING_DATE_ADDED', 'Date:');
define('TABLE_HEADING_STATUS', 'Status:');
define('TABLE_HEADING_COMMENTS', 'Comments:');
?>
