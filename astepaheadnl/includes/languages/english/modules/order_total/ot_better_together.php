<?php
//
// +----------------------------------------------------------------------+
// | Better Together discount strings                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 That Software Guy                                 |
// +----------------------------------------------------------------------+
// | Released under the GNU General Public License.                       |
// +----------------------------------------------------------------------+
//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE', 'Better together');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION', 'Better together module');
define('TWOFER_PROMO_STRING', 'Buy this item and get other for free!');
define('TWOFER_QUALIFY_STRING', "You can get other %s for free");
define('OFF_STRING_PCT', '-%s');  // e.g. at 50% off
define('OFF_STRING_CURR', '-%s');  // e.g. $20 off
define('SECOND', ' second ');  // if both same
define('FREE_STRING', ' for free');  // i.e. amount off
// Reverse defs
define('REV_GET_ANY', 'Buy item from ');
define('REV_GET_THIS', 'Buy  ');
define('REV_GET_DISC', ', get this item ');
// No context (off product info page)
define('FREE', " for free");
define('GET_YOUR_PROD', ", get you product ");
define('GET_YOUR_CAT', ", get item from ");
