<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');

if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
  define('HEADING_TITLE', $topics['topics_name']);
  define('TEXT_NO_ARTICLES', 'There are currently no articles in this topic.');
  define('TEXT_ARTICLES', 'Below is a list of articles with the most recent ones listed first.');
  define('TEXT_DATE_ADDED', 'Published:');
  define('TEXT_TOPIC', 'Topic:');
  define('TEXT_BY', 'by');
} elseif ($topic_depth == 'top') {
  define('HEADING_TITLE', 'All Articles');
  define('TEXT_CURRENT_ARTICLES', 'Current Articles');
  define('TEXT_UPCOMING_ARTICLES', 'Upcoming Articles');
  define('TEXT_NO_ARTICLES', 'There are currently no articles listed.');
  define('TEXT_DATE_ADDED', 'Published:');
  define('TEXT_DATE_EXPECTED', 'Expected:');
  define('TEXT_TOPIC', 'Topic:');
  define('TEXT_BY', 'by');
} elseif ($topic_depth == 'nested') {
  define('HEADING_TITLE', 'Articles');
}
  define('TOTAL_ARTICLES', 'Total articles');
  define('HEADING_TITLE', 'Articles');
