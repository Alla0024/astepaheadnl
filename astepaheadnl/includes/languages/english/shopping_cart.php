<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Cart Contents');
define('HEADING_TITLE', 'Shopping cart');
define('TABLE_HEADING_REMOVE', 'Remove');
define('TABLE_HEADING_REMOVE_FROM', 'from cart');
define('TABLE_HEADING_QUANTITY', 'Qty.');
define('TABLE_HEADING_IMAGE', 'Image');
define('TABLE_HEADING_NAME', 'Name');
define('TABLE_HEADING_PRICE', 'Price');
define('TABLE_HEADING_PRICE_TOTAL', 'Subtotal');
define('TABLE_HEADING_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Product(s)');
define('TABLE_HEADING_TOTAL', 'Total');
define('TEXT_CART_EMPTY', 'Your Shopping Cart is empty!');
define('SUB_TITLE_COUPON', 'Discount code');
define('SUB_TITLE_COUPON_SUBMIT', 'Submit');
define('SUB_TITLE_COUPON_VALID', 'You have the correct discount code!');
define('SUB_TITLE_COUPON_INVALID', 'You have an invalid discount code!');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Unfortunately, the chosen quantity is not in stock.');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Unfortunately, the chosen quantity is not in stock.');
?>