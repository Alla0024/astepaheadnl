<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Authorization');
define('HEADING_TITLE', 'Welcome, Please Sign In');
define('TEXT_NEW_CUSTOMER', 'Create an account');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'By creating an account at ' . STORE_NAME . ' you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.');
define('TEXT_RETURNING_CUSTOMER', 'Login');
define('TEXT_PASSWORD_FORGOTTEN', 'Password forgotten? Click here.');
define('TEXT_LOGIN_ERROR', '<b>Error:</b>: No match for e-mail Address and/or password.');
// guest_account start
define('NO_CUSTOMER', 'There is no such user in our database');
define('NO_PASS', 'Wrong password');
define('SOC_VK', 'VK');
define('SOC_SITE', 'site');
define('SOC_ENTER_FROM_OTHER', 'Login from another social network or enter your login/password.');
define('SOC_NOW_FROM', 'You are trying to login from');
define('SOC_NEED_FROM', 'You was registered via');
define('SOC_LOGIN_THX', 'You was successfully login');