<?php
/*
  $Id: account_edit.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My account');
define('NAVBAR_TITLE_2', 'My details');

define('HEADING_TITLE', 'My details');

define('MY_ACCOUNT_TITLE', 'My details');

define('SUCCESS_ACCOUNT_UPDATED', 'Your information has been updated.');
?>
