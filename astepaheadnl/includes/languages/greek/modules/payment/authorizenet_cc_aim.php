<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_PUBLIC_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_DESCRIPTION', '<a href="https://www.authorize.net" target="_blank" style="text-decoration: underline; font-weight: bold;">Επισκεφθείτε τον ιστότοπο του Authorize.net</a>');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_OWNER', 'Ιδιοκτήτης Πιστωτικής Κάρτας:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_NUMBER', 'Αριθμός πιστωτικής κάρτας:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_EXPIRES', 'Ημερομηνία λήξης πιστωτικής κάρτας:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_CVC', 'Κωδικός επαλήθευσης κάρτας (CVC):');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_TITLE', 'Υπήρξε σφάλμα κατά την επεξεργασία της πιστωτικής σας κάρτας');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_GENERAL', 'Δοκιμάστε ξανά και εάν τα προβλήματα παραμένουν, δοκιμάστε έναν άλλο τρόπο πληρωμής.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_DECLINED', 'Αυτή η συναλλαγή με πιστωτική κάρτα έχει απορριφθεί. Δοκιμάστε ξανά και εάν τα προβλήματα παραμένουν, δοκιμάστε άλλη πιστωτική κάρτα ή μέθοδο πληρωμής');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_INVALID_EXP_DATE', 'Η ημερομηνία λήξης της πιστωτικής κάρτας δεν είναι έγκυρη. Ελέγξτε τα στοιχεία της κάρτας και δοκιμάστε ξανά.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_EXPIRED', 'Η πιστωτική κάρτα έχει λήξει. Δοκιμάστε ξανά με άλλη κάρτα ή μέθοδο πληρωμής.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_CVC', 'Ο αριθμός ελέγχου πιστωτικής κάρτας (CVC) δεν είναι έγκυρος. Ελέγξτε τα στοιχεία της κάρτας και προσπαθήστε ξανά.');
?>
