<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Τραπεζική μεταφορά');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', 'Τα τραπεζικά μας στοιχεία:<br><br>Όνομα τράπεζας: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . '<br>Τραπεζικός λογαριασμός: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . '<br>ΝΧΙ: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . '<br>Κουτί / τιμολόγιο: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . '<br>ΙΝΝ: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . '<br>Παραλήπτης: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . '<br>ΔΕΗ: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . '<br>Σκοπός πληρωμής: &nbsp;&nbsp;&nbsp;' .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . '<br><br>Μετά την πληρωμή της παραγγελίας, φροντίστε να μας ενημερώσετε μέσω e-mail <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS
  . '</a> για το γεγονός της πληρωμής. Η παραγγελία σας θα αποσταλεί αμέσως μετά την επιβεβαίωση της πληρωμής.<br><br><a href=kvitan.php target=_blank><b><span>Απόδειξη πληρωμής</a></b>');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Τα τραπεζικά μας στοιχεία:\n\nΌνομα τράπεζας: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')
  . "<br>Τραπεζικός λογαριασμός: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')
  . "<br>ΝΧΙ: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')
  . "<br>Κουτί / τιμολόγιο: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')
  . "<br>ΙΝΝ: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')
  . "<br>Παραλήπτης: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')
  . "\nΔΕΗ: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
  . "\nΣκοπός πληρωμής: " .
  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')
  . "\n\nΜετά την πληρωμή της παραγγελίας, φροντίστε να μας ενημερώσετε μέσω e-mail " . STORE_OWNER_EMAIL_ADDRESS . " για το γεγονός της πληρωμής. Η παραγγελία σας θα αποσταλεί αμέσως μετά την επιβεβαίωση της πληρωμής.");
