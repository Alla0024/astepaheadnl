<?php
/*
  $Id: webmoney_merchant.php 1778 2008-01-09 23:37:44Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_OSHAD_TEXT_TITLE', 'Oshad');
define('MODULE_PAYMENT_OSHAD_TEXT_PUBLIC_TITLE', 'Oshadbank Visa/Mastercard');
define('MODULE_PAYMENT_OSHAD_TEXT_DESCRIPTION', 'Αφού κάνετε κλικ στο κουμπί Επιβεβαίωση παραγγελίας, θα μεταβείτε στον ιστότοπο του συστήματος πληρωμών για να πληρώσετε την παραγγελία, αφού ολοκληρωθεί η πληρωμή της παραγγελίας σας.');
define('MODULE_PAYMENT_OSHAD_TEXT_ADMIN_DESCRIPTION', 'Oshad..');