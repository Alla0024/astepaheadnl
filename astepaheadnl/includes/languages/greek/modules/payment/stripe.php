<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define( 'MODULE_PAYMENT_STRIPE_TEXT_TITLE', 'λωρίδα');
define('MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION', 'Stripe - ηλεκτρονικές πληρωμές');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER', 'Ιδιοκτήτης Πιστωτικής Κάρτας:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER', 'Αριθμός πιστωτικής κάρτας:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM', 'Λήξη MM:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY', 'Λήξη YY:');
define('MODULE_PAYMENT_STRIPE_TEXT_ERROR', 'Error Credit Card!');