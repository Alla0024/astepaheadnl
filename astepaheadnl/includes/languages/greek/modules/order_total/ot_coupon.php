<?php
/*
  $Id: ot_coupon.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Κουπόνια έκπτωσης');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Κουπόνια δώρων / κουπόνια έκπτωσης');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Εκπτωτικό κουπόνι');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Μη έγκυρος κωδικός κουπονιού');
define('ERROR_MINIMUM_CART_AMOUNT', 'Το ελάχιστο ποσό καλαθιού για αυτό το κουπόνι είναι: %s');
define('ERROR_INVALID_STARTDATE_COUPON', 'Αυτό το κουπόνι δεν είναι διαθέσιμο ακόμα');
define('ERROR_INVALID_FINISDATE_COUPON', 'Αυτό το κουπόνι έχει λήξει');
define('ERROR_INVALID_USES_COUPON', 'Αυτό το κουπόνι θα μπορούσε να χρησιμοποιηθεί  ');
define('TIMES', ' times.');
define('ERROR_INVALID_USES_USER_COUPON', 'Χρησιμοποιήσατε το κουπόνι με στον μέγιστο αριθμό επιτρεπόμενων  ανά πελάτη.');
define('TEXT_ENTER_COUPON_CODE', 'Καταχωρίστε τον κωδικό εξόφλησης&nbsp;&nbsp;');