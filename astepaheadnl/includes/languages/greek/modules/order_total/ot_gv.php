<?php
/*
  $Id: ot_gv.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_ORDER_TOTAL_GV_TITLE', 'Δωροεπιταγές');
  define('MODULE_ORDER_TOTAL_GV_HEADER', 'Κουπόνια δώρων / κουπόνια έκπτωσης');
  define('MODULE_ORDER_TOTAL_GV_DESCRIPTION', 'Δωροεπιταγές');
  define('MODULE_ORDER_TOTAL_GV_USER_PROMPT', 'Επιλέξτε το υπόλοιπο του λογαριασμού Δωροκουπόνι ->&nbsp;');
  define('TEXT_ENTER_GV_CODE', 'Καταχωρίστε τον κωδικό εξόφλησης&nbsp;&nbsp;');
?>