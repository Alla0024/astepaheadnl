<?php
//
// +----------------------------------------------------------------------+
// | Better Together discount strings                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 That Software Guy                                 |
// +----------------------------------------------------------------------+
// | Released under the GNU General Public License.                       |
// +----------------------------------------------------------------------+
//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE', 'Καλύτερα μαζί');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION', 'Καλύτερη μαζί προσφορά');
define('TWOFER_PROMO_STRING', 'Αγοράστε αυτό το προιόν και κερδίστε!');
define('TWOFER_QUALIFY_STRING', "Μπορείτε να λάβετε και άλλα% s δωρεάν");
define('OFF_STRING_PCT', '-%s');  // e.g. at 50% off
define('OFF_STRING_CURR', '-%s');  // e.g. $20 off
define('SECOND', ' second ');  // if both same
define('FREE_STRING', ' for free');  // i.e. amount off
// Reverse defs
define('REV_GET_ANY', 'Αγοράστε στοιχείο από ');
define('REV_GET_THIS', 'Αγορά');
define('REV_GET_DISC', ', get this item ');
// No context (off product info page)
define('FREE', " for free");
define('GET_YOUR_PROD', ", να σας πάρει προϊόν ");
define('GET_YOUR_CAT', ", πάρτε προιόν από ");
