<?php
/*
  $Id: template_configuration.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'ΠροσαρμογήTemplates');
define('TABLE_HEADING_TEMPLATE', 'Ονομα');
define('TABLE_HEADING_TEMPLATE_FOLDER', 'Folder');
define('TABLE_HEADING_ACTION', 'Δράση');
define('TABLE_HEADING_ACTIVE', 'Κατάσταση');
define('TABLE_HEADING_COLOR', 'Χρώμα');

define('TABLE_HEADING_DISPLAY_COLUMN_LEFT', 'Εμφάνιση αριστερής στήλης?');
define('SLIDER_SIZE_CONTENT', 'Τοποθετώντας ένα ρυθμιστικό');
define('SLIDER_RIGHT', 'Στη δεξιά στήλη');
define('SLIDER_CONTENT_WIDTH', 'Με πλάτος περιεχομένου');
define('SLIDER_CONTENT_WIDTH_100', 'Πλάτος σελίδας(100%)');

define('GENERAL_MODULES', 'Τα κύρια τμήματα του site');
define('HEADER_MODULES', 'Κεφαλίδες μπλοκ');
define('LEFT_MODULES', 'Αποκλεισμός στην αριστερή στήλη');
define('MAINPAGE_MODULES', 'Αποκλεισμός στην κύρια σελίδα');
define('FOOTER_MODULES', 'Υποσέλιδα μπλοκ');
define('OTHER_MODULES', 'Άλλα μπλοκ');

#from BD table infobox_configuration:

##FOOTER BOXES
define('F_ARTICLES_BOTTOM', 'Άρθρα στο υποσέλιδο');
define('F_FOOTER_CATEGORIES_MENU', 'Κατηγορίες στο υποσέλιδο');
define('F_TOP_LINKS', 'Σελιδες νικος πληροφοριών  στο υποσέλιδο');

##HEADER BOXES
define('H_LOGIN', 'Σύνδεση');
define('H_COMPARE', 'Σύγκριση');
define('H_LANGUAGES', 'Γλώσσες');
define('H_CURRENCIES', 'Νόμισμα');
define('H_ONLINE', 'Online συνομιλία');
define('H_SEARCH', 'Ψάξιμο');
define('H_SHOPPING_CART', 'Καλάθι αγορών');
define('H_WISHLIST', 'Λίστα επιθυμιών');
define('H_TEMPLATE_SELECT', 'Επιλογή προτύπου');
define('H_TOP_MENU', 'Μενού κατηγορίας');
define('H_CALLBACK', 'Επανάκληση');
define('H_TOP_LINKS', 'Αρχικό μενού');

##OTHER_MODULES
/*define('O_LOGIN', 'Σύνδεση');
define('O_TEMPLATE_SELECT', 'Template Selection');
define('O_SHOPPING_CART', 'Shop cart');
define('O_SEARCH', 'Search');
define('O_ONLINE', 'Online chat');
define('O_COMPARE', 'Comparison');
define('O_CURRENCIES', 'Currency');
define('O_LANGUAGES', 'Languages');
define('O_TOP_LINKS', 'Top menu');
define('O_CALLBACK', 'Callback');
define('O_TOP_MENU', 'Category menu');*/
define('O_FILTER', 'Filter');
define('LIST_FILTER', 'Filter');

##LEFT_MODULES
define('L_NEWSDESK_CATEGORIES', 'Κατηγορία γραφείου ειδήσεων');
define('L_NEWSDESK_LATEST', 'Τελευταία Νέα');
define('L_FEATURED', 'Προτεινόμενα');
define('L_NEWSBOX', 'Κουτί ειδήσεων');
define('L_WHATS_NEW', 'Τι νέα μας');
define('L_SPECIALS', 'Προσφορές');
define('L_MANUFACTURERS', 'Κατασκευαστές');
define('L_BESTSELLERS', 'Κορυφαίες Πωλήσεις');
define('L_ARTICLES', 'Άρθρα');
define('L_POLLS', 'Δημοσκοπήσεις');
define('L_FILTER', 'Φίλτρο');
define('L_BANNER_1', 'Διαφήμιση 1');
define('L_BANNER_2', 'Διαφήμιση 2');
define('L_BANNER_3', 'Διαφήμιση 3');
define('L_SUPER', 'Κατηγορία');
define('L_SUPER_TOPIC', 'Τμήματα άρθρων');

##MAINPAGE_MODULES
define('M_ARTICLES_MAIN', 'Άρθρα');
define('M_BANNER_LONG', 'Πανό');
define('M_BEST_SELLERS', 'Κορυφαίες πωλήσεις');
define('M_BROWSE_CATEGORY', 'Κατηγορία');
define('M_DEFAULT_SPECIALS', 'Προσφορές ');
define('M_FEATURED', 'Προτεινόμενα');
define('M_LAST_COMMENTS', 'Πρόσφατα σχόλια');
define('M_VIEW_PRODUCTS', 'Προβολές προϊόντων');
define('M_MAINPAGE', 'Κείμενο της κύριας σελίδας');
define('M_MANUFACTURERS', 'Κατασκευαστές');
define('M_MOST_VIEWED', 'Περισσότερες εμφανίσεις');
define('M_NEW_PRODUCTS', 'Νέο προϊόν');
define('M_SLIDE_MAIN', 'Ολισθητής εγω ');
define('M_BANNER_1', 'Πανω 1');

##MAINPAGE_MODULES
define('G_HEADER_1', 'Οριζόντια γραμμή κεφαλίδας 1');
define('G_HEADER_2', 'Οριζόντια γραμμή κεφαλίδας 2');
define('G_LEFT_COLUMN', 'Αριστερή στήλη');
define('G_FOOTER_1', 'Οριζόντια γραμμή υποσέλιδου 1');
define('G_FOOTER_2', 'Οριζόντια γραμμή υποσέλιδου 2');

##
define('P_FILTER', 'Filter');
?>