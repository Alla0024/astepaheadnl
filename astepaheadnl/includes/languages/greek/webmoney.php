<?php
/*
  $Id: webmoney.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_WEBMONEY_TEXT_TITLE', 'WebMoney');
define('MODULE_PAYMENT_WEBMONEY_TEXT_DESCRIPTION', 'Τα δεδομένα μας στο σύστημα WebMoney:<br><br>Our WM ID: &nbsp;&nbsp;&nbsp;<b>' . (defined('MODULE_PAYMENT_WEBMONEY_1') ? MODULE_PAYMENT_WEBMONEY_1 : '---') . '</b><br>Our R αριθμός πορτοφολιών: &nbsp;&nbsp;&nbsp;<b>' . (defined('MODULE_PAYMENT_WEBMONEY_2') ? MODULE_PAYMENT_WEBMONEY_2 : '---') . '</b><br>Ο αριθμός του πορτοφολιού μας Ζ: &nbsp;&nbsp;&nbsp;<b>' . (defined('MODULE_PAYMENT_WEBMONEY_3') ? MODULE_PAYMENT_WEBMONEY_3 : '---') . '</b><br><br>Για την πληρωμή της παραγγελίας πρέπει να μεταφέρετε χρήματα στο πορτοφόλι R ή Z στο σύστημα WebMoney, και στη σημείωση υποδεικνύουν <b> τον αριθμό της παραγγελίας σας</b><br><br><span>Ο αριθμός της παραγγελίας σας θα αποσταλεί στη διεύθυνση ηλεκτρονικού ταχυδρομείου σας</span><br><br><span>Η μεταφορά πρέπει να είναι ΧΩΡΙΣ την προστασία της συναλλαγής</span><br><br>Πώς να κάνετε μια μεταφορά: <br> Για να πραγματοποιήσετε μια μεταφορά, ανοίξτε το WM Keeper και κάντε δεξί κλικ στο πορτοφόλι από το οποίο θέλετε να μεταφέρετε και, στη συνέχεια, επιλέξτε από το μενού μεταφοράς χρημάτων - στο WebMoney πορτοφόλι. Στο παράθυρο διαλόγου που εμφανίζεται, εισαγάγετε το ποσό μεταφοράς και τον αριθμό του πορτοφολιού μας και στη σημείωση καθορίστε <b> τον αριθμό της παραγγελίας σας </b>');
define('MODULE_PAYMENT_WEBMONEY_TEXT_EMAIL_FOOTER', "Τα δεδομένα μας στο σύστημα WebMoney:\n\nOur WM ID: " . (defined('MODULE_PAYMENT_WEBMONEY_1') ? MODULE_PAYMENT_WEBMONEY_1 : '---') . "\nOur R purse number: " . (defined('MODULE_PAYMENT_WEBMONEY_2') ? MODULE_PAYMENT_WEBMONEY_2 : '---') . "\τον αριθμό του πορτοφολιού μας Z: " . (defined('MODULE_PAYMENT_WEBMONEY_3') ? MODULE_PAYMENT_WEBMONEY_3 : '---') . "\n\nΓια να πληρώσετε τη διαταγή πρέπει να μεταφέρετε χρήματα στο πορτοφόλι R ή Z στο σύστημα WebMoney και στο σημείωμα να το υποδείξετε <b> τον αριθμό της παραγγελίας σας\n\nΟ αριθμός της παραγγελίας σας θα αποσταλεί στη διεύθυνση ηλεκτρονικού ταχυδρομείου σας\n\nΗ μεταφορά πρέπει να είναι ΧΩΡΙΣ την προστασία της συναλλαγής\n\nΠώς να κάνετε μια μεταφορά:\nΓια να πραγματοποιήσετε μια μεταφορά, ανοίξτε το WM Keeper και κάντε δεξί κλικ στο πορτοφόλι από το οποίο θέλετε να μεταφέρετε και, στη συνέχεια, επιλέξτε από το μενού μεταφοράς χρημάτων - στο WebMoney πορτοφόλι. Στο παράθυρο διαλόγου που εμφανίζεται, εισαγάγετε το ποσό μεταφοράς και τον αριθμό του πορτοφολιού μας και στη σημείωση καθορίστε τον αριθμό της παραγγελίας σας");