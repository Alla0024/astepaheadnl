<?php
/*
  $Id: gv_send.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Αποστολή δωροεπιταγής');
define('NAVBAR_TITLE', 'Αποστολή δωροεπιταγής');
define('EMAIL_SUBJECT', 'από ' . STORE_NAME);
define('HEADING_TEXT','<br>Πληκτρολογήστε παρακάτω τις λεπτομέρειες του δελτίου δώρων που θέλετε να στείλετε.<br>');
define('ENTRY_NAME', 'Ονομα παραλήπτη:');
define('ENTRY_EMAIL', 'Η ηλεκτρονική διεύθυνση του αποδέκτη:');
define('ENTRY_MESSAGE', 'Μήνυμα πρός το παραλήπτη:');
define('ENTRY_AMOUNT', 'Αξία δωροεπιταγής:');
define('ERROR_ENTRY_AMOUNT_CHECK', '&nbsp;&nbsp;<span class="errorText">Invalid Amount</span>');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK', '&nbsp;&nbsp;<span class="errorText">Invalid Email Address</span>');
define('MAIN_MESSAGE', 'Έχετε αποφασίσει να δημοσιεύσετε ένα δωροκουπόνι αξίας% s στο% s ποιος\'s email η διεύθυνση είναι %s<br><br>Θα διαβαστεί το κείμενο που συνοδεύει το μήνυμα ηλεκτρονικού ταχυδρομείουd<br><br>Dear %s<br><br>
                        Έχετε αποσταλεί ένα δελτίο δώρων αξίας% s από% s');
define('PERSONAL_MESSAGE', '%s says');
define('TEXT_SUCCESS', 'Συγχαρητήρια, το δελτίο δώρου σας έχει αποσταλεί με επιτυχία');
define('EMAIL_SEPARATOR', '----------------------------------------------------------------------------------------');
define('EMAIL_GV_TEXT_HEADER', 'CΣυγχαρητήρια, Έχετε λάβει ένα κουπόνι δώρου αξίας% s');
define('EMAIL_GV_TEXT_SUBJECT', 'Ένα δώρο από το% s');
define('EMAIL_GV_FROM', 'Αυτό το κουπόνι δώρων σας έχει σταλεί από το% s');
define('EMAIL_GV_MESSAGE', 'Με ένα μήνυμα που λέει');
define('EMAIL_GV_SEND_TO', 'Γεια, %s');
define('EMAIL_GV_REDEEM', 'Για να εξαργυρώσετε αυτό το Κουπόνι δώρων, κάντε κλικ στον παρακάτω σύνδεσμο. Καταχωρίστε επίσης τον κωδικό εξαργύρωσης που είναι% s. Σε περίπτωση που έχετε προβλήμα.');
define('EMAIL_GV_LINK', 'Για να εξαργυρώσετε, πατήστε ');
define('EMAIL_GV_FIXED_FOOTER', 'Αν έχετε προβλήματα με την εξαργύρωση του Κουπονιού δώρων χρησιμοποιώντας τον αυτοματοποιημένο σύνδεσμο παραπάνω, ' . "\n" .
                                'μπορείτε επίσης να εισάγετε τον κωδικό δωροεπιταγής κατά τη διάρκεια της διαδικασίας πληρωμής στο κατάστημά μας.' . "\n\n");
define('EMAIL_GV_SHOP_FOOTER', '');