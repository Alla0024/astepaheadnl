<?php
/*
  $Id: gv_redeem.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Εξαργύρωση δωροεπιταγή');
define('HEADING_TITLE', 'Εξαργύρωση δωροεπιταγή');
define('TEXT_INFORMATION', '');
define('TEXT_INVALID_GV', 'Ο αριθμός του δελτίου δώρων μπορεί να είναι άκυρος ή έχει ήδη εξαργυρωθεί. Για να επικοινωνήσετε με τον ιδιοκτήτη του καταστήματος, χρησιμοποιήστε τη σελίδα επικοινωνίας');
define('TEXT_VALID_GV', 'Συγχαρητήρια, έχετε εξαργυρώσει ένα δελτίο δώρων αξίας% s');
?>