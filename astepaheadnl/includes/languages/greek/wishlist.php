<?php
/*
  $Id: wishlist.php,v 3.0  2005/04/20 Dennis Blake
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Η λίστα επιθυμιών μου περιέχει:');
define('BOX_TEXT_PRICE', 'Τιμή');
define('BOX_TEXT_PRODUCT', 'όνομα προϊόντος');
define('BOX_TEXT_IMAGE', 'Εικόνα');
define('BOX_TEXT_NO_ITEMS', 'Δεν υπάρχουν προϊόντα στην Λίστα επιθυμιών σας. <br><br>');
define('TEXT_EMAIL', 'Email: ');
define('TEXT_MESSAGE', 'Μήνυμα: ');
define('TEXT_ITEM_IN_CART', 'Προιόντα στο καλάθι');
define('TEXT_ITEM_NOT_AVAILABLE', 'Το προιόν δεν είναι πλέον διαθέσιμο');
define('WISHLIST_EMAIL_SUBJECT', 'σας έστειλε τη λίστα επιθυμιών τους ' . STORE_NAME);  //Customers name will be automatically added to the beginning of this.
define('WISHLIST_SENT', 'Η λίστα επιθυμιών σας έχει σταλεί.');
define('WISHLIST_EMAIL_LINK', '

$from_name\'s public wishlist is located here:
$link

Thank you,
' . STORE_NAME); //$from_name = Customers name  $link = public wishlist link

define('WISHLIST_EMAIL_GUEST', 'Ευχαριστώ πολύ,
' . STORE_NAME);

define('ERROR_YOUR_NAME' , 'Παρακαλώ εισάγετε το όνομά σας.');
define('ERROR_YOUR_EMAIL' , 'Παρακαλώ εισαγάγετε το Email σας.');
define('ERROR_VALID_EMAIL' , 'Παρακαλώ εισάγετε την διεύθυνση του  ηλεκτρονικού ταχυδρομείου σας .');
define('ERROR_ONE_EMAIL' , 'Πρέπει να συμπεριλάβετε τουλάχιστον ένα όνομα και ένα email.');
define('ERROR_ENTER_EMAIL' , 'Παρακαλώ να εισάγετε μια διεύθυνση ηλεκτρονικού ταχυδρομείου.');
define('ERROR_ENTER_NAME' , 'Εισαγάγετε το όνομα του παραλήπτη του ηλεκτρονικού ταχυδρομείου.');
define('ERROR_MESSAGE', 'Συμπεριλάβετε εαν θέλετε ένα σύντομο μήνυμα.');
?>
