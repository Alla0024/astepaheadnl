<?php

define('HEADING_TITLE', 'ΔΙΑΣΤΑΥΡΩΜΕΝΗ ΠΩΛΗΣΗ Admin');

define('XSELL_FILTERS', 'Ορίστε φίλτρα:');
define('XSELL_CATEGORIES', 'Κατηγορίες: ');
define('XSELL_MANUFACTURERS', 'κατασκευαστες: ');

define('XSELL_SELECT', 'Go');

define('XSELL_PRODUCT_ID', 'Αναγνωριστικό προϊόντος');
define('XSELL_PRODUCT_MODEL', 'Γρήγορη αναζητηση');
define('XSELL_PRODUCT_NAME', 'Όνομα προιόντος');
define('XSELL_PRODUCT_CURRENT', 'Τρέχουσες πωλήσεις');
define('XSELL_PRODUCT_UPDATE', 'Ανανεωση πωλησεων');

define('XSELL_EDIT', 'Επεξεργασία');
define('XSELL_SORT_ORDER', 'προτεραιότητα');

define('XSELL_NEW_XSELL', 'Κάντε κλικ εδώ για να προσθέσετε μια νεα αγορα');
define('XSELL_NEW_INPUT_PLACEHOLDER', 'Εισαγάγετε το όνομα του προϊόντος');
define('XSELL_SORT_ORDER_XSELL', 'Κάντε κλικ εδώ για να ταξινομήσετε (από πάνω προς τα κάτω) ');
define('XSELL_ADD_NEW', 'Η ρύθμιση Διασταυρωμενες πωλησεις για: ');

define('XSELL_PRODUCT_IMAGE', 'Εικόνα προϊόντος');
define('XSELL_PRODUCT_XSELL', 'Διασταυρωση?');
define('XSELL_PRODUCT_ITEM_NAME', 'Ονομα προιόντος');
define('XSELL_PRODUCT_PRICE', 'Τιμή');
define('XSELL_PRODUCT_SORT_ORDER', 'Order (1=Top)');

define('XSELL_PRODUCT_ALREADY', 'Τα προϊόντα βρίσκονται ήδη σε λειτουργία διασταυρωσης. Κατάργηση, αν χρειαστεί...');

define('XSELL_PRODUCT_XSELL1', 'Διασταυρωμενα προιόντα');
define('XSELL_PRODUCT_ADD', 'Κατάλογος προϊόντων για προσθήκη στην διασταυρωση. Παρακαλώ Έλεγξέ το...');
define('XSELL_PRODUCT_UPDATED', 'Η διασταυρωση  ενημερώνεται');
define('XSELL_PRODUCT_BACK', 'Πίσω στην διασταυρωση admin');

define('XSELL_PRODUCT_SUBMIT', 'υποβολλή');

define('CROSS_SELL_SUCCESS', 'Προϊόντα διασταυρωμενης  Πώλησης Επιτυχής Ενημέρωση για Προϊόν Σταυροειδούς Πώλησης #'.$_GET['προσθέστε _ το σχετικό _ ID προϊόντος']);
define('SORT_CROSS_SELL_SUCCESS', 'Ταξινόμηση με επιτυχία την ενημέρωση για το διασταυρένο Νικος προϊόν #'.$_GET['προσθέστε _ το σχετικό _ ID προϊόντος']);
//define('HEADING_TITLE', 'Cross-Sell (X-Sell) Admin');
define('TABLE_HEADING_PRODUCT_ID', '#');
define('TABLE_HEADING_PRODUCT_MODEL', 'Μοντέλο');
define('TABLE_HEADING_PRODUCT_NAME', 'Ονομα');
define('TABLE_HEADING_CURRENT_SELLS', 'Τρέχουσα διασταυρωμενη πώληση ');
define('TABLE_HEADING_UPDATE_SELLS', 'Ανανεωση διασταυρωσης');
define('TABLE_HEADING_PRODUCT_IMAGE', 'Εικονα');
define('TABLE_HEADING_PRODUCT_PRICE', 'Τιμη');
define('TABLE_HEADING_CROSS_SELL_THIS', 'καντε διασταυρωση προιοντων?');
define('TEXT_EDIT_SELLS', 'Επεξεργασία');
define('TEXT_SORT', 'Ταξινόμηση σειράς');
define('TEXT_SETTING_SELLS', 'Ορίσε ρε την διασταυρωση  για');
define('TEXT_PRODUCT_ID', '#');
define('TEXT_MODEL', 'Μοντελο');
define('TABLE_HEADING_PRODUCT_SORT', 'είδος');
define('TEXT_NO_IMAGE', 'χωρίς εικόνα');
define('TEXT_CROSS_SELL', 'ΔΙΑΣΤΑΥΡΩΣΗ');
define('HEADING_TITLE_SEARCH', 'ΑΝΑΖΗΤΗΣΗ: ');
define('TEXT_RECIPROCAL_LINK', 'Διασταυρούμενη σύνδεση?');

// version 2_7_3
define('HEADING_TITLE_XSELL', 'ΔΙΑΣΤΑΥΡΩΣΗ');
define('HEADING_TITLE_NEW_XSELL', 'ΠΡΟΣΘΕΣΕ ΔΙΑΣΤΑΥΡΩΣΗ');
define('HEADING_TITLE_EDIT_XSELL', 'Επεξεργαστείτε τη διασταυρωση');

define('TEXT_ADD_XSELLS', 'Προσθέστε διασταυρωση προιόντων');
define('TEXT_EDIT_XSELLS', 'Επεξεργαστείτε τη διασταυρωση επιτελους');
define('TEXT_SEARCH_MODEL' , 'Αναζήτηση βάσει ονόματος');
define('IMAGE_WITH_XSELLS','Εμφάνιση προϊόντων με κωδικό');
define('IMAGE_WITHOUT_XSELLS','Εμφάνιση όλων των προϊόντων');

define('TEXT_XSELL_DISC', 'Εκπτωση');

?>