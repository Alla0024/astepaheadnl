<?php
/*
  $Id: create_account_success.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Δημιουργία λογαριασμού');
define('NAVBAR_TITLE_2', 'Επιτυχής');
define('HEADING_TITLE', 'Ο λογαριασμός σας έχει δημιουργηθεί!');
define('TEXT_ACCOUNT_CREATED', 'Συγχαρητήρια! Ο νέος σας λογαριασμός δημιουργήθηκε με επιτυχία! Τώρα μπορείτε να επωφεληθείτε από τα προνόμια των μελών για να βελτιώσετε την εμπειρία ηλεκτρονικών αγορών σας μαζί μας.<br>Έχει αποσταλεί επιβεβαίωση στη διεύθυνση ηλεκτρονικού ταχυδρομείου που παρέχεται. Εάν δεν το έχετε λάβει μέσα στην ώρα, παρακαλώ <a href="' . tep_href_link(FILENAME_CONTACT_US) . '">επικοινωνήστε μαζί μας</a>.');
?>