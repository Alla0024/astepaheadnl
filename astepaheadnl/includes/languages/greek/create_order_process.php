<?php
/*
  $Id: create_order_process.php,v 1.1 2003/09/24 14:33:18 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_CREATE', 'Ελέγξτε τα στοιχεία του πελάτη');
define('TEXT_OP_PRICE', 'Τιμή');
define('TEXT_OP_TAX', 'Φόρος');
define('TEXT_OP_SHIPPING', 'Αποστολή');
define('TEXT_OP_TOTAL', 'Συνολικό ποσό');
?>