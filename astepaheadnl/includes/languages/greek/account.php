<?php
/*
  $Id: account.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'My Account');
define('HEADING_TITLE', 'My Account Information');


define('MY_ACCOUNT_TITLE', 'My Account');

define('MY_ACCOUNT_PASSWORD', 'Change my account password.');

define('MY_ORDERS_VIEW', 'View the orders I have made.');

define('MY_NAVI', 'Menu');
define('MY_INFO', 'My information');
define('EDIT_ADDRESS_BOOK', 'Edit my shipping address');
