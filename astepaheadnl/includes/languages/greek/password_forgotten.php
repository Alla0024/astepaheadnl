<?php
/*
  $Id: password_forgotten.php,v 1.8 2003/06/09 22:46:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Σύνδεση');
define('NAVBAR_TITLE_2', 'Ξεχάσατε τον κωδικό πρόσβασης');
define('HEADING_TITLE', 'I\'Ξεχάσατε τον κωδικό πρόσβασης!');
define('TEXT_MAIN', 'Εάν έχετε ξεχάσει τον κωδικό πρόσβασής σας, πληκτρολογήστε την παρακάτω διεύθυνση ηλεκτρονικού ταχυδρομείου και θα σας στείλουμε ένα μήνυμα ηλεκτρονικού ταχυδρομείου που περιέχει τον νέο σας κωδικό πρόσβασης.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Σφάλμα: Η διεύθυνση EMail δεν βρέθηκε στα αρχεία μας, δοκιμάστε ξανά.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Νέος Κωδικός');
define('EMAIL_PASSWORD_REMINDER_BODY', 'Ένας νέος κωδικός πρόσβασης ζητήθηκε από ' . $REMOTE_ADDR . '.' . "\n\n" . 'Ο νέος σας κωδικός πρόσβασης\'' . STORE_NAME . '\' is:' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'Επιτυχία: Ένας νέος κωδικός πρόσβασης έχει σταλεί στη διεύθυνση ηλεκτρονικού ταχυδρομείου σας.');