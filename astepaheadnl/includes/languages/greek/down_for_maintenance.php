<?php
/*
  Created by: Linda McGrath osCOMMERCE@WebMakers.com
  
  Update by: fram 05-05-2003
  Updated by: Donald Harriman - 08-08-2003 - MS2

  down_for_maintenance.php v1.1
  
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Δεν λειτουργεί λόγω συντήρησης');
define('HEADING_TITLE', 'Δεν λειτουργεί λόγω συντήρησης ...');
define('DOWN_FOR_MAINTENANCE_TEXT_INFORMATION', 'Ο ιστότοπος είναι προς το παρόν εκτός λειτουργίας για συντήρηση. Παρακαλούμε δοκιμάστε αργότερα.');
define('TEXT_MAINTENANCE_ON_AT_TIME', 'The Admin / Ο Webmaster έχει ενεργοποιήσει τη συντήρηση στη διεύθυνση : ');
define('TEXT_MAINTENANCE_PERIOD', 'Ο Webmaster έχει ενεργοποιήσει τη συντήρηση στη διεύθυνση : ');
define('DOWN_FOR_MAINTENANCE_STATUS_TEXT', 'Για να επαληθεύσετε την κατάσταση του ιστότοπου ... Κάντε κλικ εδώ:');
?>