<?php
/*
  $Id: checkout_process.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('EMAIL_TEXT_SUBJECT', 'Διαδικασία παραγγελίας');
define('EMAIL_TEXT_ORDER_NUMBER', 'Αριθμός παραγγελίας:');
define('EMAIL_TEXT_INVOICE_URL', 'Λεπτομερές δελτίο:');
define('EMAIL_TEXT_DATE_ORDERED', 'Ημερομηνία παραγγελίας:');
define('EMAIL_TEXT_PRODUCTS', 'Προϊόντα');
define('EMAIL_TEXT_DELIVERY_ADDRESS', 'Διεύθυνση παράδοσης');
define('EMAIL_TEXT_BILLING_ADDRESS', 'διεύθυνση χρέωσης');
define('EMAIL_TEXT_PAYMENT_METHOD', 'Μέθοδος πληρωμής');
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_CUSTOMER_NAME', 'Πελάτης:');
define('EMAIL_TEXT_CUSTOMER_EMAIL_ADDRESS', 'email:');
define('EMAIL_TEXT_CUSTOMER_TELEPHONE', 'Τηλέφωνο:');