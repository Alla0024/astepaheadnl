<?php
/*
  $Id: index.php,v 1.1 2003/06/11 17:38:00 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');
define('TABLE_HEADING_DATE_EXPECTED', 'Date Expected');
define('TABLE_HEADING_DEFAULT_SPECIALS', 'Specials for %s');

if ( ($category_depth == 'products') || (isset($_GET['manufacturers_id'])) ) {
  define('HEADING_TITLE', 'Let\'s See What We Have Here');
  define('TABLE_HEADING_IMAGE', '');
  define('TABLE_HEADING_MODEL', 'Μοντέλο');
  define('TABLE_HEADING_PRODUCTS', 'Ονομα προϊόντος');
  define('TABLE_HEADING_MANUFACTURER', 'Κατασκευαστής');
  define('TABLE_HEADING_QUANTITY', 'Ποσότητα');
  define('TABLE_HEADING_PRICE', 'Τιμή');
  define('TABLE_HEADING_WEIGHT', 'Βάρος');
  define('TEXT_ALL_MANUFACTURERS', 'Όλοι οι κατασκευαστές');
} elseif ($category_depth == 'top') {
  define('HEADING_TITLE', 'Τι νέο υπάρχει?');
} elseif ($category_depth == 'ένθετα') {
  define('HEADING_TITLE', 'Κατηγορίες');
}
 	// BOF Wolfen featured sets
  define('TABLE_HEADING_PRICE', 'Τιμή');
  define('TEXT_NO_FEATURED_PRODUCTS', 'Δεν υπάρχουν προτεινόμενα προϊόντα');
 	// EOF Wolfen featured sets
?>