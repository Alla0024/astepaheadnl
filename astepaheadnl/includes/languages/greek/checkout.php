<?php
/*
  One Page Checkout, Version: 1.08

  I.T. Web Experts
  http://www.itwebexperts.com

  Copyright (c) 2009 I.T. Web Experts

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Ελέγξτε την παραγγελία');
define('NAVBAR_TITLE_1', 'Ελέγξτε την παραγγελία');
define('HEADING_TITLE', 'Ελέγξτε την παραγγελία');
define('TABLE_HEADING_SHIPPING_ADDRESS', 'Διεύθυνση αποστολής');
define('TABLE_HEADING_BILLING_ADDRESS', 'Οι πληροφορίες σας:');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Ονομα');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Τιμή');
define('TABLE_HEADING_PRODUCTS', 'καλάθι');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Σύνολο');
define('ENTRY_TELEPHONE', 'Αριθμός τηλεφώνου');
define('ENTRY_COMMENT', 'Σχόλια');
define('TABLE_HEADING_SHIPPING_METHOD', 'Τρόπος αποστολής');
define('TABLE_HEADING_PAYMENT_METHOD', 'Τρόπος πληρωμής');
define('TEXT_CHOOSE_SHIPPING_METHOD', '');
define('TEXT_SELECT_PAYMENT_METHOD', '');
define('TEXT_ERROR_SHIPPING_METHOD', 'Εισαγάγετε τη ΔΙΕΥΘΥΝΣΗ ΣΑΣ ΝΑΥΤΙΛΙΑ για να δείτε εισαγωγικά <b>%s</b>');
define('TEXT_ENTER_SHIPPING_INFORMATION', 'Αυτή τη στιγμή είναι η μόνη διαθέσιμη μέθοδος αποστολής για χρήση σε αυτήν την παραγγελία.');
define('TEXT_ENTER_PAYMENT_INFORMATION', 'Αυτή είναι αυτή τη στιγμή η μόνη διαθέσιμη μέθοδος πληρωμής για τη χρήση αυτής της παραγγελίας.');
define('TABLE_HEADING_COMMENTS', 'Σχόλια:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Να συνεχίσει την ολοκλήρωση της παραγγελίας');
define('EMAIL_SUBJECT', 'Welcome to ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Dear %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Dear %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Dear %s' . "\n\n");
define('EMAIL_WELCOME', 'Welcome to <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'Τώρα έχετε πρόσβαση σε ορισμένες πρόσθετες λειτουργίες που είναι διαθέσιμες μόνο για εγγέγραμμενους χρήστες  <br /><Li> <b> Cart </b> - Όλα τα προϊόντα που προστίθενται στο καλάθι, παραμένουν εκεί μέχρι να τα διαγράψετε ή να τα παραγγείλετε<br /><Li> <b> Βιβλίο διευθύνσεων </b> - Τώρα μπορούμε να στείλουμε τα προϊόντα μας στη διεύθυνση που καθορίσατε στη "Διεύθυνση παράδοσης"".<br /><Li> <b> Ιστορικό παραγγελιών </b> - έχετε την ευκαιρία να δείτε το ιστορίκο των παραγγελιών στο κατάστημά μας.');
define('EMAIL_CONTACT', 'Εάν έχετε ερωτήσεις, στείλτε μας email: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '');

// Start - CREDIT CLASS Gift Voucher Contribution
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Ως μέρος της υποδοχής μας σε νέους πελάτες, σας έχουμε στείλει ένα δελτίο ηλεκτρονικού δώρου αξίας% s');
define('EMAIL_GV_REDEEM', 'Ο κωδικός εξαργύρωσης του δελτίου ηλεκρονικού δώρου είναι% s, μπορείτε να εισάγετε τον κωδικό εξαργύρωσης κατά τον έλεγχο κατά την πραγματοποίηση αγοράς');
define('EMAIL_GV_LINK', 'ή ακολουθώντας αυτόν τον σύνδεσμο ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Συγχαρητήρια, για να κάνετε την πρώτη σας επίσκεψη στο ηλεκτρονικό μας κατάστημα μια πιο ικανοποιητική εμπειρία, σας στέλνουμε ένα ηλεκτρονικό εκτωτικό κουπόνι.' . "\n" .
										' Παρακάτω υπάρχουν λεπτομέρειες σχετικά με το κουπόνι έκπτωσης που δημιουργήθηκε μόνο για εσάς' . "\n");
define('EMAIL_COUPON_REDEEM', 'Για να χρησιμοποιήσετε το κουπόνι, εισαγάγετε τον κωδικό εξαργύρωσης που είναι% s κατά την ολοκλήρωση του ελέγχου κατά την πραγματοποίηση αγοράς');
// End - CREDIT CLASS Gift Voucher Contribution
define('TEXT_PLEASE_SELECT', 'Επιλογή');
define('TEXT_PASSWORD_FORGOTTEN', 'Ξεχάσατε τον κωδικό?');
define('IMAGE_LOGIN', 'Σύνδεση');
define('TEXT_HAVE_COUPON_KGT', 'Έχετε ένα κουπόνι;');
define('TEXT_DIFFERENT_SHIPPING', 'Διαφορετική από τη διεύθυνση χρέωσης;');
define('TEXT_DIFFERENT_BILLING', 'Διαφορετική διεύθυνση αποστολής;');
// Points/Rewards Module V2.1rc2a EOF
define('TEXT_MIN_SUM', 'MINIMUM ORDER PRICE');
define('TEXT_NEW_CUSTOMER', 'Νέος πελάτης');
define('TEXT_LOGIN_SOCIAL', 'Σύνδεση με Facebook ');
define('TEXT_REGISTRATION_OFF', 'Τοποθετήστε μια παραγγελία χωρίς εγγραφή');

define('TEXT_EMAIL_EXISTS', 'Αυτό το μήνυμα ηλεκτρονικού ταχυδρομείου είναι ήδη εγγεγραμμένο, παρακαλώ');
define('TEXT_EMAIL_EXISTS2', 'Συνδεθείτε');
define('TEXT_EMAIL_EXISTS3', 'μέσω του ηλεκτρονικού σας ταχυδρομείου');
define('TEXT_EMAIL_WRONG', 'εισάγατε λάθος email');
define('TEXT_ORDER_PROCESSING', 'επεξεργασία παραγγελιών, παρακαλώ περιμένετε ...');
define('TEXT_EMAIL_LOGIN', 'Σύνδεση');
define('TEXT_EMAIL_PASS', 'Κωδικός πρόσβασης');

define('TEXT_CHANGE_ADDRESS', 'Αλλαξε διεύθυνση');
define('ADDRESS_BOOK', 'Βιβλίο διευθύνσεων');
define('BILLING_ADDRESS_THE_SAME', 'την ίδια');

define('CH_JS_REFRESH', 'Aνανεωση');
define('CH_JS_REFRESH_METHOD', 'Τρόπος ανανεωσης:');
define('CH_JS_SETTING_METHOD', 'Μέθοδος ρύθμισης:');
define('CH_JS_SETTING_ADDRESS', 'Ορισμός διεύθυνσης:');
define('CH_JS_SETTING_ADDRESS_BIL', 'Τιμολόγηση');
define('CH_JS_SETTING_ADDRESS_SHIP', 'Αποστολή');

define('CH_JS_ERROR_SCART', 'Παρουσιάστηκε σφάλμα κατά την ανανέωση του καλαθιού αγορών, ενημερώστε μας');
define('CH_JS_ERROR_SOME1', 'Ανανεωση');
define('CH_JS_ERROR_SOME2', 'Παρουσιάστηκε σφάλμα, παρακαλούμε ενημερώστε');

define('CH_JS_ERROR_SET_SOME1', 'Υπήρξε μια ρύθμιση σφάλματος');
define('CH_JS_ERROR_SET_SOME2', 'μέθοδο, παρακαλώ ενημερώστε');
define('CH_JS_ERROR_SET_SOME3', 'σχετικά με αυτό το σφάλμα.');

define('CH_JS_ERROR_REQ_BIL', 'Συμπληρώστε τα υποχρεωτικά πεδία στην ενότητα "Διεύθυνση πληρωμής"');
define('CH_JS_ERROR_ERR_BIL', 'Ελέγξτε τα πεδία στην ενότητα "Διεύθυνση πληρωμής"');
define('CH_JS_ERROR_REQ_SHIP', 'Please fill in required fields in section "Shipping Address"');
define('CH_JS_ERROR_ERR_SHIP', 'Please check fields in section "Shipping Address"');
define('CH_JS_ERROR_ADDRESS', 'Σφάλμα διεύθυνσης');
define('CH_JS_ERROR_PMETHOD', 'Σφάλμα κατά την επιλογή της μεθόδου πληρωμής');
define('CH_JS_ERROR_SELECT_PMETHOD', 'Επιλέξτε τρόπο πληρωμής');
define('CH_JS_CHECK_EMAIL', 'Ελέγξτε τη διεύθυνση ηλεκτρονικού ταχυδρομείου σας');
define('CH_JS_ERROR_EMAIL', 'Παρουσιάστηκε σφάλμα κατά την επαλήθευση των διευθύνσεων ηλεκτρονικού ταχυδρομείου');
?>