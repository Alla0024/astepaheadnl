<?php
/*
  $Id: packingslip.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TABLE_HEADING_COMMENTS', 'Σχόλια');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Μοντέλο');
define('TABLE_HEADING_PRODUCTS', 'Προϊόντα');

define('ENTRY_SOLD_TO', 'Πουλήθηκε σε :');
define('ENTRY_SHIP_TO', 'ΑΠΟΣΤΟΛΗ ΠΡΟΣ:');
define('ENTRY_PAYMENT_METHOD', 'Μέθοδος πληρωμής:');
?>
