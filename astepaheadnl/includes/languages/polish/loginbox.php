<?php
/*
  WebMakers.com Added: loginbox.php
  Originally written by: Aubrey Kilian <aubrey@mycon.co.za>
  Re-written by Linda McGrath osCOMMERCE@WebMakers.com
  http://www.thewebmakerscorner.com

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('BOX_LOGINBOX_HEADING', 'Logowanie');
define('IMAGE_BUTTON_LOGIN', 'Zaloguj');
define('LOGIN_BOX_ADDRESS_BOOK','Książka adresowa');
define('LOGIN_BOX_LOGOFF','Wyloguj');
