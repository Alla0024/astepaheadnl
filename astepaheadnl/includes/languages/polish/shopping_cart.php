<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Zawartość koszyka');
define('HEADING_TITLE', 'Mój koszyk');
define('TABLE_HEADING_REMOVE', 'Usuń');
define('TABLE_HEADING_REMOVE_FROM', 'z koszyka');
define('TABLE_HEADING_QUANTITY', 'Ilość');
define('TABLE_HEADING_IMAGE', 'Zdjęcie');
define('TABLE_HEADING_NAME', 'Nazwa');
define('TABLE_HEADING_PRICE', 'Cena');
define('TABLE_HEADING_PRICE_TOTAL', 'Cena');
define('TABLE_HEADING_MODEL', 'Kod produktu');
define('TABLE_HEADING_PRODUCTS', 'Towary');
define('TABLE_HEADING_TOTAL', 'Cena');
define('TEXT_CART_EMPTY', 'Twój koszyk jest pusty!');
define('SUB_TITLE_COUPON', 'Kupon');
define('SUB_TITLE_COUPON_SUBMIT', 'Zastosuj');
define('SUB_TITLE_COUPON_VALID', 'Gratulacje! <br />Wprowadziłeś odpowiedni kupon!');
define('SUB_TITLE_COUPON_INVALID', 'Podałeś nieprawidłowy kupon!');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'Wybrane towary ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' są niedostępne w naszym magazynie w ilości wystarczającej dla Twojego zamówienia.<br>Zmień liczbę wybranych produktów (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), dziękujemy');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Wybrane towary ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' są niedostępne w naszym magazynie w ilości wystarczającej dla Twojego zamówienia.<br>Można je jednak kupić i sprawdzić ilość dostępną do dostawy etapowej w procesie realizacji zamówienia.');
