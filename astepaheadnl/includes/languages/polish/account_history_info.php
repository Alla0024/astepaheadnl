<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Moje dane');
define('NAVBAR_TITLE_2', 'Historia zamówień');
define('NAVBAR_TITLE_3', 'Informacja o zamówieniach');
define('HEADING_TITLE', 'Informacja o zamówieniach');
define('HEADING_ORDER_NUMBER', 'Zamówienie #%s');
define('HEADING_ORDER_DATE', 'Data');
    define('HEADING_ORDER_TOTAL', 'Koszt zamówienia:');
define('HEADING_DELIVERY_ADDRESS', 'Adres dostawy');
define('HEADING_SHIPPING_METHOD', 'Sposób wysyłki');
define('HEADING_PRODUCTS', 'Towar');
define('HEADING_TAX', 'Podatek');
define('HEADING_TOTAL', 'Razem');
define('HEADING_PAYMENT_METHOD', 'Sposób płatności');
define('HEADING_ORDER_HISTORY', 'Historia zamówienia');
    define('TABLE_HEADING_DOWNLOAD_DATE', 'Link jest ważny do: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' raz można pobrać plik');
define('TABLE_HEADING_DATE_ADDED', 'Data');
define('TABLE_HEADING_STATUS', 'Status zamówienia');
define('TABLE_HEADING_COMMENTS', 'Komentarze');
