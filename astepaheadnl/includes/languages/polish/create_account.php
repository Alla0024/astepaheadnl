<?php
/*
  $Id: create_account.tpl.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// guest_account start
if ($guest_account == false) { // Not a Guest Account
  define('NAVBAR_TITLE', 'Rejestracja');
} else {
  define('NAVBAR_TITLE', 'Złożenie zamówienie');
}
// guest_account end
define('HEADING_TITLE', 'Moje dane');
define('CR_ENTER', 'Zaloguj się');
define('CR_THX', 'Dziękujemy, jesteś zarejestrowany!');
define('CR_IF', 'jeśli jesteś już zarejestrowany.');
// guest_account end
define('TEXT_ORIGIN_LOGIN', '<span><span><small><b>UWAGA:</b></small></span>&nbsp;Musisz wypełnić formularz rejestracyjny. To umożliwi ci do złożenia zamówienia w naszym sklepie internetowym,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jeśli jesteś już zarejestrowany na naszej stronie internetowej, wprowadź swój login i Hasło&nbsp;</span><a href="%s"><u>tutaj</u></a>.');
define('EMAIL_SUBJECT', 'Witamy w ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Drogi %s! <br><br>');
define('EMAIL_GREET_MS', 'Droga %s! <br><br>');
define('EMAIL_GREET_NONE', 'Drogi %s! <br><br>');
define('EMAIL_WELCOME', 'Witamy w naszym sklepie internetowym <b>' . STORE_NAME . '</b>. <br><br>');
define('EMAIL_CONTACT', 'Jeśli masz jakieś pytania, napisz: ' . STORE_OWNER_EMAIL_ADDRESS . '.<br><br>');
define('EMAIL_TEXT', 'Teraz możesz robić zakupy.' . "\n");
define('EMAIL_WARNING', '');

/* ICW Credit class gift voucher begin */
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Ponadto informujemy, że otrzymasz kartę podarunkową na kwotę %s');
define('EMAIL_GV_REDEEM', 'Kod twojej karty podarunkowej%s. Możesz użyć swoją kartę podarunkową, gdy płacisz za zamówienie. Wartość nominalna karty podarunkowej jest obliczana jako zapłata za całe zamówienie lub za część.');
define('EMAIL_GV_LINK', 'Kliknij tutaj, aby aktywować swoją kartę podarunkową: ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Gratulujemy! Jesteś zarejestrowany w naszym sklepie, mamy przyjemność podarować ci kartę podarunkową.' . "\n" .
                                        ' Ta karta jest ważna tylko dla ciebie' . "\n");
define('EMAIL_COUPON_REDEEM', 'Aby skorzystać z karty podarunkowej, musisz podać kod karty w procesie składania zamówienia, aby otrzymać zniżkę.' . "\n" . 'Kod twojej karty podarunkowej: %s');
define('CR_LOGIN', 'Login(e-mail)');
define('CR_PASS', 'Hasło');
define('CR_ADD_EMAIL', 'Aby ukończyć rejestrację<br /> wprowadź swój <b>e-mail:</b>');
define('CR_SUBMIT', 'Potwierdź');
/* ICW Credit class gift voucher end */
