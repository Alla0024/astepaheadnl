<?php
/*
  $Id: password_forgotten.php,v 1.8 2003/06/09 22:46:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Zaloguj się');
define('NAVBAR_TITLE_2', 'Odzyskiwanie hasła');
define('HEADING_TITLE', 'Zapomniałem hasło!');
define('TEXT_MAIN', 'Jeśli zapomniałeś hasło, podaj swój adres e-mail, a my wyślemy ci hasło na podany adres e-mail.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', '<span><b>Błąd:</b></span> Adres e-mail nie pasuje do Twojego konta, spróbuj ponownie.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Twoje hasło');
    define('EMAIL_PASSWORD_REMINDER_BODY', 'Zapytanie o nowe hasło zostało otrzymane od
 ' . $REMOTE_ADDR . '.' . "\n\n" . 'Twoje nowe hasło w \'' . STORE_NAME . '\' :' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'Gotowe: twoje nowe hasło zostało wysłane do ciebie na twój e-mail.');
