<?php
/*
  $Id: address_book_process.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Moje dane');
define('NAVBAR_TITLE_2', 'Kontakty');
define('NAVBAR_TITLE_ADD_ENTRY', 'Nowy kontakt');
define('NAVBAR_TITLE_MODIFY_ENTRY', 'Edycja');
define('NAVBAR_TITLE_DELETE_ENTRY', 'Usuń kontakt');
define('HEADING_TITLE_ADD_ENTRY', 'Nowy kontakt');
define('HEADING_TITLE_MODIFY_ENTRY', 'Edycja');
define('HEADING_TITLE_DELETE_ENTRY', 'Usuń kontakt');
define('DELETE_ADDRESS_TITLE', 'Usuń adres');
define('DELETE_ADDRESS_DESCRIPTION', 'Na pewno chcesz usunąć wybrany adres?');
define('NEW_ADDRESS_TITLE', 'Nowy adres');
define('SELECTED_ADDRESS', 'Wybrany adres');
define('SET_AS_PRIMARY', 'Ustaw jako domyślny.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'Wybrany adres został usunięty.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Twoje kontakty zostały zaktualizowane.');
define('WARNING_PRIMARY_ADDRESS_DELETION', 'Adres, ustawiony jako domyślny, nie może być usunięty. Ustaw inny adres jako domyślny i spróbuj ponownie.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'Nie masz książki adresowej.');
define('ERROR_ADDRESS_BOOK_FULL', 'Twoja książka adresowa jest pełna. Usuń adres, którego nie potrzebujesz i dopiero wtedy możesz dodać nowy adres.');

