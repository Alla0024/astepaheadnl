<?php
/*
  $Id: specials.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Zniżka');
define('HEADING_TITLE', 'Zniżka');
define('TEXT_INFO_CATEGORY', 'Wszystkie kategorie');
define('TEXT_INFO_MANUFACTURERS', 'Wszystkie marki');
define('TEXT_INFO_ONLY_DISCOUNT', 'Tylko ze zniżką');
?>
