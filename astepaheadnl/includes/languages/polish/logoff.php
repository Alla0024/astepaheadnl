<?php
/*
  $Id $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Wyloguj');
define('NAVBAR_TITLE', 'Wyloguj');
define('TEXT_MAIN', 'Pomyślnie się wylogowałeś.<br>Zawartość koszyka została zapisana.');
