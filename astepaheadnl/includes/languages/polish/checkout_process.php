<?php
/*
  $Id: checkout_process.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('EMAIL_TEXT_SUBJECT', 'Twoje zamówienie');
define('EMAIL_TEXT_ORDER_NUMBER', 'Numer zamówienia:');
define('EMAIL_TEXT_INVOICE_URL', 'Informacja o zamówieniu:');
define('EMAIL_TEXT_DATE_ORDERED', 'Data zamówienia:');
define('EMAIL_TEXT_PRODUCTS', 'Zamówiłeś:');
define('EMAIL_TEXT_DELIVERY_ADDRESS', 'Adres dostawy');
define('EMAIL_TEXT_BILLING_ADDRESS', 'Adres kupującego');
define('EMAIL_TEXT_PAYMENT_METHOD', 'Sposób płatności');
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_CUSTOMER_NAME', 'Kupujący:');
define('EMAIL_TEXT_CUSTOMER_EMAIL_ADDRESS', 'Email:');
define('EMAIL_TEXT_CUSTOMER_TELEPHONE', 'Telefon:');
