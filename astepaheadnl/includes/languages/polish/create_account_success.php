<?php
/*
  $Id: create_account_success.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Rejestracja');
define('NAVBAR_TITLE_2', 'Sukces');
define('HEADING_TITLE', 'Twoje konto zostało pomyślnie założone!');
define('TEXT_ACCOUNT_CREATED', 'Gratulacje! Twoja rejestracja została pomyślnie zakończona! Otrzymasz przywileje zarejestrowanego użytkownika.<br><br>Potwierdzenie rejestracji wstępnej przesłano na e-mail adres wskazany podczas rejestracji.');
