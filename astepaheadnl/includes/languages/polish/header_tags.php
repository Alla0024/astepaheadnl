<?php
// WebMakers.com Added: Header Tags Generator v2.0
// Add META TAGS and Modify TITLE
//
// DEFINITIONS FOR /includes/languages/english/header_tags.php

// Define your email address to appear on all pages
define('HEAD_REPLY_TAG_ALL','');

// для всіх сторінок:
define('HEAD_TITLE_TAG_ALL',''.STORE_NAME);
define('HEAD_DESC_TAG_ALL',''.STORE_NAME);
define('HEAD_KEY_TAG_ALL','');

// головна сторінка:
define('HTTA_DEFAULT_ON','1'); // Include HEAD_TITLE_TAG_ALL in Title
define('HTKA_DEFAULT_ON','1'); // Include HEAD_KEY_TAG_ALL in Keywords
define('HTDA_DEFAULT_ON','0'); // Include HEAD_DESC_TAG_ALL in Description
define('HEAD_TITLE_TAG_DEFAULT',$headers_mainpage['articles_head_title_tag']); //define('HEAD_TITLE_TAG_DEFAULT','Solomono Template - ELE');
define('HEAD_DESC_TAG_DEFAULT',$headers_mainpage['articles_head_desc_tag']); // define('HEAD_DESC_TAG_DEFAULT','demo-wersja sklepu internetowego');
define('HEAD_KEY_TAG_DEFAULT',$headers_mainpage['articles_head_keywords_tag']); // define('HEAD_KEY_TAG_DEFAULT','Solomono, sklep internetowy');

// картка товара:
define('HTTA_PRODUCT_INFO_ON','0');
define('HTKA_PRODUCT_INFO_ON','0');
define('HTDA_PRODUCT_INFO_ON','0');

// статті (завжди перед тегами статей):
define('HEAD_TITLE_ARTICLE_TAG_ALL','');
define('HEAD_DESC_ARTICLE_TAG_ALL','');
define('HEAD_KEY_ARTICLE_TAG_ALL','');

// статті (якщо теги в базі пусті):
define('HTTA_ARTICLES_ON','1'); // Include HEAD_TITLE_TAG_ALL in Title
define('HTKA_ARTICLES_ON','0'); // Include HEAD_KEY_TAG_ALL in Keywords
define('HTDA_ARTICLES_ON','1'); // Include HEAD_DESC_TAG_ALL in Description
define('HEAD_TITLE_TAG_ARTICLES','');
define('HEAD_DESC_TAG_ARTICLES','');
define('HEAD_KEY_TAG_ARTICLES','');
