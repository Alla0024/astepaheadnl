<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Złożenie zamówienia');
define('NAVBAR_TITLE_2', 'Pomyślnie');
define('HEADING_TITLE', 'Twoje zamówienie zostało złożone');
define('TEXT_SUCCESS', 'Twoje zamówienie zostało pomyślnie złożone!');
define('TEXT_THANKS_FOR_SHOPPING', 'Twoje zamówienie zostało pomyślnie złożone!');
define('TABLE_HEADING_COMMENTS', 'W najbliższym czasie skontaktujemy się ze tobą');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Ten link jest ważny do: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' raz można pobrać plik.');
define('SMS_NEW_ORDER', 'Masz nowe zamówienie');
define('TEXT_SUCCESS_INFO', 'Wszystkie dalsze instrukcje i szczegóły dotyczące płatności zostały wysłane do Ciebie pocztą');
