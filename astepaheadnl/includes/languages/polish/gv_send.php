<?php
/*
  $Id: gv_send.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Wysłać kartę podarunkową');
define('NAVBAR_TITLE', 'Wysłać kartę podarunkową');
define('EMAIL_SUBJECT', 'Wiadomość od sklepu internetowego');
define('HEADING_TEXT','<br>Aby wysłać certyfikat, musisz wypełnić poniższy formularz.<br>');
define('ENTRY_NAME', 'Imię odbiorcy:');
define('ENTRY_EMAIL', 'Adres e-mail odbiorcy:');
define('ENTRY_MESSAGE', 'Wiadomość:');
define('ENTRY_AMOUNT', 'Kwota karty podarunkowej:');
define('ERROR_ENTRY_AMOUNT_CHECK', '&nbsp;&nbsp;<span class="errorText">Nieprawidłowa kwota</span>');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK', '&nbsp;&nbsp;<span class="errorText">Zły adres e-mail</span>');
define('MAIN_MESSAGE', 'Postanowiłeś wysłać kartę podarunkową na kwotę %s swojemu przyjacielowi %s, jego e-mail adres: %s<br><br>Odbiorca karty podarunkowej otrzyma następujący komunikat:<br><br>Drogi %s<br><br>
                        Otrzymałeś certyfikat na sumę %s, nadawca: %s');
define('PERSONAL_MESSAGE', '%s pisze:');
define('TEXT_SUCCESS', 'Gratulacje, twoja karta podarunkowa została pomyślnie wysłana.');
define('EMAIL_SEPARATOR', '----------------------------------------------------------------------------------------');
define('EMAIL_GV_TEXT_HEADER', 'Gratulacje, otrzymałeś kartę podarunkową na kwotę %s');
define('EMAIL_GV_TEXT_SUBJECT', 'Prezent od %s');
define('EMAIL_GV_FROM', 'Nadawca tej karty podarunkowej - %s');
define('EMAIL_GV_MESSAGE', 'Wiadomość od nadawcy: ');
define('EMAIL_GV_SEND_TO', 'Witam, %s');
define('EMAIL_GV_REDEEM', 'Aby aktywować kartę podarunkowej, otwórz poniższy link. Kod karty podarunkowej: %s');
define('EMAIL_GV_LINK', 'Kliknij tutaj, aby aktywować kartę podarunkową ');
define('EMAIL_GV_FIXED_FOOTER', 'Jeśli masz problemy z aktywacją karty podarunkowej za pomocą powyższego linku, ' . "\n" .
                                'Zalecamy wprowadzenie kodu karty podarunkowej podczas składania zamówienia, a nie za pomocą powyższego linku.' . "\n\n");
define('EMAIL_GV_SHOP_FOOTER', '');
