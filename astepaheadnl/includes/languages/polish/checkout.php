<?php
/*
  One Page Checkout, Version: 1.08

  I.T. Web Experts
  http://www.itwebexperts.com

  Copyright (c) 2009 I.T. Web Experts

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Złożenie zamówienia');
define('NAVBAR_TITLE_1', 'Złożenie zamówienia');
define('HEADING_TITLE', 'Złożenie zamówienia');
define('TABLE_HEADING_SHIPPING_ADDRESS', 'Adres dostawy');
define('TABLE_HEADING_BILLING_ADDRESS', 'Twoje dane');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Nazwa');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Cena za szt.');
define('TABLE_HEADING_PRODUCTS', 'Koszyk');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Razem');
define('ENTRY_TELEPHONE', 'Telefon');
define('ENTRY_COMMENT', 'Komentarz do zamówienia');
define('TABLE_HEADING_SHIPPING_METHOD', 'Sposób dostawy');
define('TABLE_HEADING_PAYMENT_METHOD', 'Sposób płatności');
define('TEXT_CHOOSE_SHIPPING_METHOD', '');
define('TEXT_SELECT_PAYMENT_METHOD', '');
define('TEXT_ERROR_SHIPPING_METHOD', 'Wprowadź ADRES WYSYŁKI, aby zobaczyć cytaty <b>%s</b>');
define('TEXT_ENTER_SHIPPING_INFORMATION', ''); //This is currently the only shipping method available to use on this order.
define('TEXT_ENTER_PAYMENT_INFORMATION', '');
define('TABLE_HEADING_COMMENTS', 'Komentarze:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Kontynuuj składanie zamówienia');
define('EMAIL_SUBJECT', 'Witamy w naszym sklepie internetowym ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Drogi %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Droga %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Drogi(a) %s <br /><br/>');
define('EMAIL_WELCOME', 'Witamy w naszym sklepie internetowym  <b>' . STORE_NAME . '</b>. <br /><br/>');
define('EMAIL_TEXT', 'Teraz masz dostęp do funkcji, które są dostępne dla zarejestrowanych użytkowników:' . "\n\n" . '<li><b>Koszyk</b> - Wszelkie produkty dodane do koszyka pozostają tam, dopóki ich nie usuniesz lub nie złożysz zamówienia.' . "\n" . '<li><b>Książka adresowa</b> - Teraz możemy wysłać nasze produkty na adres wskazany w sekcji "Adres dostawy".' . "\n" . '<li><b>Historia zamówień</b> - Masz możliwość przeglądania historii zamówień w naszym sklepie.<br><br>');
define('EMAIL_CONTACT', 'Jeśli masz jakieś pytania, napisz: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '');
// Start - CREDIT CLASS Gift Voucher Contribution
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'As part of our welcome to new customers, we have sent you an e-Gift Voucher worth %s');
define('EMAIL_GV_REDEEM', 'The redeem code for the e-Gift Voucher is %s, you can enter the redeem code when checking out while making a purchase');
define('EMAIL_GV_LINK', 'or by following this link ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Congratulations, to make your first visit to our online shop a more rewarding experience we are sending you an e-Discount Coupon.' . "\n" .
										' Below are details of the Discount Coupon created just for you' . "\n");
define('EMAIL_COUPON_REDEEM', 'To use the coupon enter the redeem code which is %s during checkout while making a purchase');
// End - CREDIT CLASS Gift Voucher Contribution
define('TEXT_PLEASE_SELECT', 'Proszę wybrać');
define('TEXT_PASSWORD_FORGOTTEN', 'Zapomniałeś hasła?');
define('IMAGE_LOGIN', 'Zaloguj się');
define('TEXT_HAVE_COUPON_KGT', 'Masz kupon?');
define('TEXT_DIFFERENT_SHIPPING', 'Czy adres dostawy jest inny niż adres płatności?');
define('TEXT_DIFFERENT_BILLING', 'Inny adres wysyłki?');
// Points/Rewards Module V2.1rc2a BOF

define('TEXT_MIN_SUM', 'MINIMALNA ILOŚĆ ZAMÓWIENIA');
define('TEXT_NEW_CUSTOMER', 'Nowy klient');
define('TEXT_LOGIN_SOCIAL', 'Zarejestruj się przez media społecznościowe');
define('TEXT_REGISTRATION_OFF', 'Złóż zamówienie bez rejestracji');


define('TEXT_EMAIL_EXISTS', 'Ten e-mail już jest wykorzystywany, proszę');
define('TEXT_EMAIL_EXISTS2', 'zaloguj się');
define('TEXT_EMAIL_EXISTS3', 'przez dany e-mail');
define('TEXT_EMAIL_WRONG', 'wprowadziłeś niepoprawny e-mail.');
define('TEXT_ORDER_PROCESSING', 'Zamówienie jest przetwarzane, proszę czekać ...');
define('TEXT_EMAIL_LOGIN', 'Login');
define('TEXT_EMAIL_PASS', 'Hasło');

define('TEXT_CHANGE_ADDRESS', 'Zmiana adresu');
define('ADDRESS_BOOK', 'Książka adresowa');
define('BILLING_ADDRESS_THE_SAME', 'Ten sam');

define('CH_JS_REFRESH', 'Aktualizacja');
define('CH_JS_REFRESH_METHOD', 'Aktualizacja sposobu');
define('CH_JS_SETTING_METHOD', 'Ustawianie sposobu');
define('CH_JS_SETTING_ADDRESS', 'Ustawienie adresu');
define('CH_JS_SETTING_ADDRESS_BIL', 'Opłaty');
define('CH_JS_SETTING_ADDRESS_SHIP', 'Dostawy');

define('CH_JS_ERROR_SCART', 'Wystąpił błąd podczas aktualizacji koszyka, proszę poinformuj nas');
define('CH_JS_ERROR_SOME1', 'Podczas aktualizacji');
define('CH_JS_ERROR_SOME2', 'Wystąpił błąd, proszę poinformować nas');

define('CH_JS_ERROR_SET_SOME1', 'Wystąpił błąd podczas instalacji sposobu');
define('CH_JS_ERROR_SET_SOME2', ', proszę poinformować nas');
define('CH_JS_ERROR_SET_SOME3', 'o tym błędzie.');

define('CH_JS_ERROR_REQ_BIL', 'Proszę wypełnić wymagane pola w sekcji "Adres płatności"');
define('CH_JS_ERROR_ERR_BIL', 'Proszę sprawdzić poprawność wprowadzonych danych w sekcji "Adres płatności"');
define('CH_JS_ERROR_REQ_SHIP', 'Proszę wypełnić wszystkie wymagane pola w sekcji "Adres dostawy"');
define('CH_JS_ERROR_ERR_SHIP', 'Proszę sprawdzić poprawność wprowadzonych danych w sekcji "Adres dostawy"');
define('CH_JS_ERROR_ADDRESS', 'Błąd adresu');
define('CH_JS_ERROR_PMETHOD', 'Błąd wyboru sposobu płatności');
define('CH_JS_ERROR_SELECT_PMETHOD', 'Musisz wybrać sposób płatności.');
define('CH_JS_CHECK_EMAIL', 'Sprawdzanie adresu e-mail');
define('CH_JS_ERROR_EMAIL', 'Wystąpił błąd podczas weryfikacji adresu e-mail, poinformuj nas');
