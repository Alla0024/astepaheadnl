<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('HEADING_TITLE', 'KONTAKTY');
define('HEADING_SUBTITLE', 'Skontaktuj się z nami teraz i otrzymaj swój sklep internetowy i łatwe w użyciu cms dla sklepu internetowego!');
define('NAVBAR_TITLE', 'Napisz');
define('TEXT_SUCCESS', 'Twoja wiadomość została wysłana!');
define('EMAIL_SUBJECT', 'Wiadomość od ' . STORE_NAME);
define('ENTRY_NAME', 'Twoje imię i nazwisko:');
define('ENTRY_EMAIL', 'E-mail:');
define('ENTRY_PHONE', 'Twój telefon:');
define('ENTRY_ENQUIRY', 'Wiadomość:');
define('SEND_TO_TEXT', 'List do:');
define('POP_CONTACT_US','Zostaw prośbę, a my za chwilę oddzwonimy do ciebie');
