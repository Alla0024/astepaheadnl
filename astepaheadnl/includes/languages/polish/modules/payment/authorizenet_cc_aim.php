<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_PUBLIC_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_DESCRIPTION', '<a href="https://www.authorize.net" target="_blank" style="text-decoration: underline; font-weight: bold;">Odwiedź stronę Authorize.net</a>');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_OWNER', 'Właściciel karty kredytowej:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_NUMBER', 'Numer karty kredytowej:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_EXPIRES', 'Data ważności karty kredytowej:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_CVC', 'Kod weryfikacji karty (CVC):');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_TITLE', 'Wystąpił błąd podczas przetwarzania Twojej karty kredytowej');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_GENERAL', 'Spróbuj ponownie, a jeśli problemy będą się powtarzać, spróbuj innej metody płatności.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_DECLINED', 'Ta transakcja kartą kredytową została odrzucona. Spróbuj ponownie, a jeśli problemy będą się powtarzać, spróbuj użyć innej karty kredytowej lub metody płatności.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_INVALID_EXP_DATE', 'Data ważności karty kredytowej jest nieprawidłowa. Sprawdź informacje o karcie i spróbuj ponownie.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_EXPIRED', 'Karta kredytowa wygasła. Spróbuj ponownie przy użyciu innej karty lub metody płatności.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_CVC', 'Numer czeku karty kredytowej (CVC) jest nieprawidłowy. Sprawdź informacje o karcie i spróbuj ponownie.');
?>
