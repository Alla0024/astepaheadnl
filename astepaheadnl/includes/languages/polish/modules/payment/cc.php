<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_CC_TEXT_TITLE', 'Karta płatnicza');
define('MODULE_PAYMENT_CC_TEXT_DESCRIPTION', 'Informacja o karcie płatniczej dla testu:<br><br>Numer karty: 4111111111111111<br>Ważna do: data');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_OWNER', 'Posiadacz karty płatniczej:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_NUMBER', 'Numer karty płatniczej:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_EXPIRES', 'Ważna do:');
define('MODULE_PAYMENT_CC_TEXT_ERROR', 'Wprowadziłeś błędny numer karty!');