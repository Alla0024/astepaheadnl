<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Przelew');
define(
    'MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION',
    'Dane bankowe:<br><br>Nazwa banku: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_1') ? MODULE_PAYMENT_BANK_TRANSFER_1 : '')
    . '<br>Numer konta: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_2') ? MODULE_PAYMENT_BANK_TRANSFER_2 : '')
    . '<br>BIC: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_3') ? MODULE_PAYMENT_BANK_TRANSFER_3 : '')
    . '<br>Bank korespondent: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_4') ? MODULE_PAYMENT_BANK_TRANSFER_4 : '')
    . '<br>NIP: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_5') ? MODULE_PAYMENT_BANK_TRANSFER_5 : '')
    . '<br>Odbiorca: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_6') ? MODULE_PAYMENT_BANK_TRANSFER_6 : '')
    . '<br>КПП: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_7') ? MODULE_PAYMENT_BANK_TRANSFER_7 : '')
    . '<br>Tytuł płatności: &nbsp;&nbsp;&nbsp;'
    . (defined('MODULE_PAYMENT_BANK_TRANSFER_8') ? MODULE_PAYMENT_BANK_TRANSFER_8 : '')
    . '<br><br>O fakcie zapłaty przelewem prosimy poinformować nas drogą elektroniczną <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS . '</a> Zamówienie będzie wysłane niezwłocznie po potwierdzeniu zapłaty.<br><br><a href=kvitan.php target=_blank><b><span>Potwierdzenie zapłaty</a></b>');
define(
    'MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Dane bankowe:\n\n Nazwa banku: " . (
        defined('MODULE_PAYMENT_BANK_TRANSFER_1' ) ? MODULE_PAYMENT_BANK_TRANSFER_1 : '')
    . "\nNumer konta: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_2') ? MODULE_PAYMENT_BANK_TRANSFER_2 : '')
    . "\nBIC: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_3') ? MODULE_PAYMENT_BANK_TRANSFER_3 : '')
    . "\nBank korespondent: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_4') ? MODULE_PAYMENT_BANK_TRANSFER_4 : '')
    . "\nNIP: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_5') ? MODULE_PAYMENT_BANK_TRANSFER_5 : '')
    . "\nOdbiorca: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_6') ? MODULE_PAYMENT_BANK_TRANSFER_6 : '')
    . "\nКПП: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')
    . "\nTytuł płatności: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_8') ? MODULE_PAYMENT_BANK_TRANSFER_8 : '')
    . "\n\n O fakcie zapłaty przelewem prosimy poinformować nas drogą elektroniczną " . STORE_OWNER_EMAIL_ADDRESS . " Zamówienie będzie wysłane niezwłocznie po potwierdzeniu zapłaty.");
