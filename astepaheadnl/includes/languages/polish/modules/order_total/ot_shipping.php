<?php
/*
  $Id: ot_shipping.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_SHIPPING_TITLE', 'Dostawa');
define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION', 'Koszty dostawy');
define('FREE_SHIPPING_TITLE', 'Darmowa wysyłka przy zakupie od   >');
define('FREE_SHIPPING_DESCRIPTION', 'Darmowa wysyłka przy zakupie powyżej %s');