<?php
/*
  $Id: ot_coupon.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Kupon');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Karta podarunkowa/Kupon');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Kupon');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Niewłaściwy kod kuponu');
define('ERROR_MINIMUM_CART_AMOUNT', 'Minimalna kwota koszyka dla tego kuponu wynosi: %s');
define('ERROR_INVALID_STARTDATE_COUPON', 'Wymieniony kupon nie jest jeszcze ważny');
define('ERROR_INVALID_FINISDATE_COUPON', 'U danego kuponu wygasła data ważności');
define('ERROR_INVALID_USES_COUPON', 'Kupon został już wykorzystany  ');
define('TIMES', ' raz.');
define('ERROR_INVALID_USES_USER_COUPON', 'Wykorzystałeś kod kuponu maksymalną ilość razy.');
define('TEXT_ENTER_COUPON_CODE', 'Twój kod:&nbsp;&nbsp;');