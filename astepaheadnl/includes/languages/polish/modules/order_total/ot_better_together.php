<?php
//
// +----------------------------------------------------------------------+
// | Better Together discount strings                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 That Software Guy                                 |
// +----------------------------------------------------------------------+
// | Released under the GNU General Public License.                       |
// +----------------------------------------------------------------------+
//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE', 'Dodatkowy rabat');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION', 'Dodatkowy rabat');
define('TWOFER_PROMO_STRING', '1 + 1 gratis!');
define('TWOFER_QUALIFY_STRING', "Otrzymaj drugi %s gratis ");
define('OFF_STRING_PCT', '-%s');  // e.g. at 50% off
define('OFF_STRING_CURR', '-%s');  // e.g. $20 off
define('SECOND', ' drugi ');  // if both same
define('FREE_STRING', ' gratis');  // i.e. amount off
// Reverse defs
define('REV_GET_ANY', 'Kup towar z ');
define('REV_GET_THIS', 'Kup ');
define('REV_GET_DISC', ', otrzymaj ten produkt ');
// No context (off product info page)
define('FREE', " gratis");
define('GET_YOUR_PROD', ", otrzymaj ");
define('GET_YOUR_CAT', ", otrzymaj swój wybór z ");

?>