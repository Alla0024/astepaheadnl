<?php
/*
  $Id: zones.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SHIPPING_ZONES_TEXT_TITLE', 'Taryfy dla strefy');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION', 'podstawowa taryfa strefy');
define('MODULE_SHIPPING_ZONES_TEXT_WAY', 'Dostawa do');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS', 'Kg.');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE', 'Dla wybranego kraju nie ma opcji dostawy ');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE', 'Koszty wysyłki nie mogą zostać określone w tej chwili. ');
