<?php
/*
  $Id: cookie_usage.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Używanie plików cookies');
define('HEADING_TITLE', 'Używanie plików cookies');
define('TEXT_INFORMATION', 'Twoja przeglądarka nie obsługuje plików cookies lub obsługa jest wyłączona.<br><br>Aby współpracować z naszym sklepem, musisz włączyć obsługę plików cookies w przeglądarce.<br><br>Instrukcje dla przeglądarki <b>Internet Explorer</b>:<br><ol><li>Serwis, a następnie Opcje internetowe</li><li>Wybierz zakładkę "Zabezpieczenie", ustaw poziom zabezpieczeń na Średni</li></ol><br><br>Jeśli masz problemy podczas zakupów w naszym sklepie, skontaktuj się z administratorem sklepu, aby rozwiązać problem.');
define('BOX_INFORMATION', 'Aby współpracować z naszym sklepem, musisz włączyć obsługę plików cookies w przeglądarce.<br><br>Jeśli masz problemy podczas zakupów w naszym sklepie, skontaktuj się z administratorem sklepu, aby rozwiązać problem.');
