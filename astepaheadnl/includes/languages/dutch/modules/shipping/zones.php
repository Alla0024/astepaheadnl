<?php 
define('MODULE_SHIPPING_ZONES_TEXT_TITLE','Verzendkosten');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION','zonale basis tarief');
define('MODULE_SHIPPING_ZONES_TEXT_WAY','Levering');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS','Kg.');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE','Voor bepaalde landen is er geen mogelijkheid van levering ');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE','Verzendkosten kan nu niet worden bepaald ');
