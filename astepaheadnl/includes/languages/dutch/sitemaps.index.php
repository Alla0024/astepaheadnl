<?php 
define('GOOGLE_SITEMAPS_PRODUCT_SUCCESS','Gegenereerd Google-Product Met Succes Sitemap');
define('GOOGLE_SITEMAPS_IMAGE_SUCCESS','Google Image Sitemap Gegenereerd');
define('GOOGLE_SITEMAPS_PRODUCT_ERROR','FOUT: Google Product Sitemap-Generatie MISLUKT!');
define('GOOGLE_SITEMAPS_IMAGE_ERROR','FOUT: Google Image Sitemap-Generatie MISLUKT!');
define('GOOGLE_SITEMAPS_CATEGORY_SUCCESS','Gegenereerd Google Categorie Sitemap Succes');
define('GOOGLE_SITEMAPS_CATEGORY_ERROR','FOUT: Google Categorie Sitemap-Generatie MISLUKT!');
define('GOOGLE_SITEMAPS_INDEX_SUCCESS','Gegenereerd Google Sitemap Index Succes');
define('GOOGLE_SITEMAPS_INDEX_ERROR','FOUT: Google Sitemap Index-Generatie MISLUKT!');
define('GOOGLE_SITEMAPS_CONGRATULATION','Van harte GEFELICITEERD! Alle bestanden die zijn gegenereerd met succes.');
define('GOOGLE_SITEMAPS_ALREADY_SUBMITTED','Als u dat nog niet hebt ingediend, wordt de sitemap index van Google klik op de link hieronder.');
define('GOOGLE_SITEMAPS_HIGHLY_RECCOMMEND','Voor u kan ik raden dat u de XML-bestanden te zorgen dat de data correct is.');
define('GOOGLE_SITEMAPS_CONVENIENCE','Voor uw gemak is hier de CRON opdracht voor uw website:');
define('GOOGLE_SITEMAPS_HERE_INDEX','Hier is uw sitemap index: ');
define('GOOGLE_SITEMAPS_HERE_PRODUCT','Hier is uw product sitemap: ');
define('GOOGLE_SITEMAPS_HERE_CATEGORY','Hier is uw categorie sitemap: ');
define('GOOGLE_SITEMAPS_HERE_IMAGE','Hier is uw beeld sitemap: ');
define('GOOGLE_SITEMAPS_HERE_ARTICLES', 'Here is your articles sitemap: ');
define('GOOGLE_SITEMAPS_ARTICLES_SUCCESS', 'Generated Google Articles Sitemap Successfully');
define('GOOGLE_SITEMAPS_ARTICLES_ERROR', 'ERROR: Google Articles Sitemap Generation FAILED!');
