<?php 
define('NAVBAR_TITLE','Autorisatie');
define('HEADING_TITLE','Laat ons in!');
define('TEXT_NEW_CUSTOMER','Account aanmaken');
define('TEXT_NEW_CUSTOMER_INTRODUCTION','Door te registreren bij AStepAhead, kunt u veel  <b>sneller en gemakkelijker</b> bestellen, uw bestellingen volgen en de geschiedenis van uw eerdere aankopen bekijken.');
define('TEXT_RETURNING_CUSTOMER','Inloggen');
define('TEXT_LOGIN_ERROR','<span><b>Fout:</b></span> Ongeldig e-mailadres en/of wachtwoord.');
define('NO_CUSTOMER','Deze gebruiker is niet in de database');
define('NO_PASS','Verkeerde wachtwoord');
define('SOC_VK','Vkontakte');
define('SOC_SITE','site');
define('SOC_ENTER_FROM_OTHER','Komen uit verschillende sociale. netwerk of uw login/wachtwoord.');
define('SOC_NOW_FROM','Nu je probeert te gaan van');
define('SOC_NEED_FROM','Je registreren van');
define('SOC_LOGIN_THX','Bedankt, u bent succesvol ingeschreven!');
