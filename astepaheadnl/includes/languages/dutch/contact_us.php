<?php 
define('HEADING_TITLE','Contact');
define('HEADING_SUBTITLE','Contact');
define('NAVBAR_TITLE','Contact');
define('TEXT_SUCCESS','Uw bericht werd met succes verstuurd!');
define('EMAIL_SUBJECT','Een bericht van '.STORE_NAME);
define('ENTRY_NAME','Volledige naam:');
define('ENTRY_EMAIL','E-Mail:');
define('ENTRY_PHONE','Telefoonnummer:');
define('ENTRY_ENQUIRY','Bericht:');
define('SEND_TO_TEXT','Brief:');
define('POP_CONTACT_US','Laat een aanvraag in en wij bellen U terug');
