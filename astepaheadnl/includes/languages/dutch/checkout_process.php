<?php 
define('EMAIL_TEXT_SUBJECT','Uw bestelling');
define('EMAIL_TEXT_ORDER_NUMBER','Bestel nummer:');
define('EMAIL_TEXT_INVOICE_URL','Bestel informatie:');
define('EMAIL_TEXT_DATE_ORDERED','Order date:');
define('EMAIL_TEXT_PRODUCTS','U besteld:');
define('EMAIL_TEXT_DELIVERY_ADDRESS','Verzendadres');
define('EMAIL_TEXT_BILLING_ADDRESS','Het adres van de koper');
define('EMAIL_TEXT_PAYMENT_METHOD','Betaalmethode');
define('EMAIL_SEPARATOR','------------------------------------------------------');
define('EMAIL_TEXT_CUSTOMER_NAME','Koper:');
define('EMAIL_TEXT_CUSTOMER_EMAIL_ADDRESS','E-mail:');
define('EMAIL_TEXT_CUSTOMER_TELEPHONE','Telefoon:');
