<?php 
define('NAVBAR_TITLE_1','Mijn account');
define('NAVBAR_TITLE_2','Adresgegevens');
define('NAVBAR_TITLE_ADD_ENTRY','Adresgegevens toevoegen');
define('NAVBAR_TITLE_MODIFY_ENTRY','Adresgegevens wijzigen');
define('NAVBAR_TITLE_DELETE_ENTRY','Adresgegevens verwijderen');
define('HEADING_TITLE_ADD_ENTRY','Adresgegevens toevoegen');
define('HEADING_TITLE_MODIFY_ENTRY','Adresgegevens wijzigen');
define('HEADING_TITLE_DELETE_ENTRY','Adresgegevens verwijderen');
define('DELETE_ADDRESS_TITLE','Om het adres te verwijderen');
define('DELETE_ADDRESS_DESCRIPTION','Wilt u de geselecteerde adresgegevens verwijderen?');
define('NEW_ADDRESS_TITLE','Adresgegevens aanpassen');
define('SELECTED_ADDRESS','Het geselecteerde adres');
define('SET_AS_PRIMARY','Als primaire adres gebruiken');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED','De adresgegevens zijn verwijderd.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED','Uw adresgegevens zijn bijgewerkt.');
define('WARNING_PRIMARY_ADDRESS_DELETION','Adresgegevens kunnen niet worden verwijderd. U moet de primaire  status naar een ander adres verplaatsen en probeer het dan opnieuw.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY','Adresgegevens niet gevonden.');
define('ERROR_ADDRESS_BOOK_FULL','U heeft het maximale aantal adressen is. Verwijder adressen om nieuwe toe te voegen.');
