<?php 
define('NAVBAR_TITLE_1','Mijn account');
define('NAVBAR_TITLE_2','Mijn bestellingen');
define('HEADING_TITLE','Mijn bestellingen');
define('TEXT_ORDER_STATUS','Status');
define('TEXT_ORDER_DATE','Datum');
define('TEXT_ORDER_SHIPPED_TO','Levering adres:');
define('TEXT_ORDER_BILLED_TO','Koper:');
define('TEXT_ORDER_COST','Bedrag excl. BTW');
define('TEXT_ORDER_PC','Item(s)');
define('TEXT_NO_PURCHASES','Helaas, je hebt nog niets gekocht in onze winkel.');
