<?php

define('HEADING_TITLE','Dealer formulier');
define('HEADING_SUBTITLE','Wilt u dealer worden bij AStepAhead? Vul dan onderstaand formulier in, <br />het verplicht u nog tot niets. Na aanmelding neemt AStepAhead contact met u op.');
define('NAVBAR_TITLE','Dealer');
define('COMPANY_DETAILS_HEADER', 'Bedrijfsgegevens');
define('COMPANY_NAME','Bedrijfsnaam');
define('KVK_NUMBER','KvK nummer');
define('BTW_NUMBER','BTW nummer');
define('ADDRESS','Adres');
define('VENUE','Plaats');
define('POSTAL_CODE','Postcode');
define('COUNTRY','Land');
define('CONTACT_DETAILS','Contactgegevens');
define('FIRST_NAME','Voornaam');
define('LAST_NAME','Achternaam');
define('EMAIL','E-Mail');
define('PHONE','Telefoonnummer');
define('ENQUIRY','Bericht');
define('SUBSCRIBE_NEWSLETTER', 'Abonneren op onze nieuwsbrief');
define('AGREE_TEXT', 'Door te registreren gaat u akkoord met de');
define('TERMS_CONDITIONS', 'Algemene voorwaarden ');
define('EMAIL_SUBJECT','Dealer van ' . STORE_NAME);
define('TEXT_SUCCESS','Uw aanvraag is ingediend');
