<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Istoric comenzi');
define('NAVBAR_TITLE_3', 'Comanda #%s');

define('HEADING_TITLE', 'Detalii comanda');

define('HEADING_ORDER_NUMBER', 'Comanda #%s');
define('HEADING_ORDER_DATE', 'Data comenzii:');
define('HEADING_ORDER_TOTAL', 'Total:');

define('HEADING_DELIVERY_ADDRESS', 'Adresa livrare');
define('HEADING_SHIPPING_METHOD', 'Transport');

define('HEADING_PRODUCTS', 'Produse');
define('HEADING_TAX', 'Tax');
define('HEADING_TOTAL', 'Total');

define('HEADING_PAYMENT_METHOD', 'Metoda plata');

define('HEADING_ORDER_HISTORY', 'Istoric comanda');

define('TABLE_HEADING_DOWNLOAD_DATE', 'Link expires: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' downloads remaining');
define('TABLE_HEADING_DATE_ADDED', 'Date');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_COMMENTS', 'Comentarii');
?>
