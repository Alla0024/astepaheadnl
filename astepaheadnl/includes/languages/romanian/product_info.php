<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Produs negasit');
define('TEXT_DATE_ADDED', 'Produs adaugat in catalog in %s.');
define('TEXT_DATE_AVAILABLE', '<span>This product will be in stock on %s.</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Produsul a fost adaugat in lista cu preferinte.');
define('TEXT_COLOR', 'Culoare');
define('TEXT_SHARE', 'Share');
define('TEXT_REVIEWSES', 'reviews');  
define('TEXT_REVIEWSES2', 'Reviews');  
define('TEXT_DESCRIPTION', 'Descriere');
define('TEXT_ATTRIBS', 'Caracteristici');
define('TEXT_PAYM_SHIP', 'Detalii comanda');
define('SHORT_DESCRIPTION', '');

?>