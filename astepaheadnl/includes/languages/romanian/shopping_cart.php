<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Continut');
define('HEADING_TITLE', 'Cos cumparaturi');
define('TABLE_HEADING_REMOVE', 'Sterge');
define('TABLE_HEADING_REMOVE_FROM', '');
define('TABLE_HEADING_QUANTITY', 'Cantitate');
define('TABLE_HEADING_IMAGE', 'Imagine');
define('TABLE_HEADING_NAME', 'Nume ');
define('TABLE_HEADING_PRICE', 'Pret');
define('TABLE_HEADING_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Produse');
define('TABLE_HEADING_TOTAL', 'Total');
define('TEXT_CART_EMPTY', 'Cos cumparaturi gol!');
define('SUB_TITLE_COUPON', 'Cupon reducere');
define('SUB_TITLE_COUPON_SUBMIT', 'Trimite');
define('SUB_TITLE_COUPON_VALID', 'Cuponul de reducere este valid. Verifica discountul:');
define('SUB_TITLE_COUPON_INVALID', 'Cuponul de reducere este invalid');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Produsele marcate cu ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' nu sunt disponibile in cantitatea dorita (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ')');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Produsele marcate cu ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' nu este disponibila in cantitatea dorita. ');
?>