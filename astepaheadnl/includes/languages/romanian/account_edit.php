<?php
/*
  $Id: account_edit.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Cont');
define('NAVBAR_TITLE_2', 'Editare cont');

define('HEADING_TITLE', 'Informatii cont');

define('MY_ACCOUNT_TITLE', 'Cont');

define('SUCCESS_ACCOUNT_UPDATED', 'Contul a fost actualizat cu succes');
?>
