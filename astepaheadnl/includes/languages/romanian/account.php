<?php
/*
  $Id: account.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Cont');
define('HEADING_TITLE', 'Detalii cont');


define('MY_ACCOUNT_TITLE', 'Cont');

define('MY_ACCOUNT_PASSWORD', 'Schimbare parola');

define('MY_ORDERS_VIEW', 'Comenzi efectuate');

define('MY_NAVI', 'Meniu');
define('MY_INFO', 'Detalii personale');
define('EDIT_ADDRESS_BOOK', 'Editeaza adresa');
