<?php
 return array (
  'LEFT_modules' => 
  array (
    'L_ARTICLES' => 
    array (
      'id' => 
      array (
        'val' => '13',
        'callable' => 'getTopics',
        'label' => 'ARTICLE_NAME',
      ),
      'limit' => 
      array (
        'val' => '3',
        'label' => 'LIMIT',
      ),
    ),
    'L_BANNER_1' => 
    array (
      'id' => 
      array (
        'val' => '71',
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
    ),
    'L_BANNER_2' => 
    array (
      'id' => 
      array (
        'val' => '72',
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
    ),
    'L_BANNER_3' => 
    array (
      'id' => 
      array (
        'val' => '73',
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
    ),
    'L_FEATURED' => 
    array (
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 10,
      ),
      'cols' => 
      array (
        'label' => 'COLS',
        'val' => '5',
      ),
    ),
  ),
  'HEADER_modules' => 
  array (
    'H_CALLBACK' => 
    array (
      'id' => 
      array (
        'val' => 75,
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
      'id_work' => 
      array (
        'val' => 84,
        'callable' => 'getArticles',
      ),
    ),
    'H_TOP_LINKS' => 
    array (
      'id' => 
      array (
        'val' => 16,
        'callable' => 'getTopics',
        'label' => 'ARTICLE_NAME',
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 7,
      ),
    ),
  ),
  'FOOTER_modules' => 
  array (
    'F_ARTICLES_BOTTOM' => 
    array (
      'id' => 
      array (
        'val' => 13,
        'callable' => 'getTopics',
        'label' => 'ARTICLE_NAME',
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 7,
      ),
    ),
    'F_TOP_LINKS' => 
    array (
      'id' => 
      array (
        'val' => 16,
        'callable' => 'getTopics',
        'label' => 'ARTICLE_NAME',
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 7,
      ),
    ),
  ),
  'MAINPAGE_modules' => 
  array (
    'M_ARTICLES_MAIN' => 
    array (
      'id' => 
      array (
        'val' => 14,
        'callable' => 'getTopics',
        'label' => 'ARTICLE_NAME',
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 3,
      ),
    ),
    'M_BANNER_LONG' => 
    array (
      'id' => 
      array (
        'val' => '70',
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
    ),
    'M_DEFAULT_SPECIALS' => 
    array (
      'cols' => 
      array (
        'label' => 'COLS',
        'val' => 4,
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 10,
      ),
    ),
    'M_FEATURED' => 
    array (
      'cols' => 
      array (
        'label' => 'COLS',
        'val' => 4,
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 10,
      ),
    ),
    'M_VIEW_PRODUCTS' => 
    array (
      'cols' => 
      array (
        'label' => 'COLS',
        'val' => 7,
      ),
    ),
    'M_MAINPAGE' => 
    array (
      'id' => 
      array (
        'val' => 68,
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
    ),
	'M_MAINPAGE_HEADER' => 
    array (
      'id' => 
      array (
        'val' => 68,
        'callable' => 'getArticles',
        'label' => 'ARTICLE_NAME',
      ),
    ),
    'M_MANUFACTURERS' => 
    array (
      'limit' => 
      array (
        'label' => '',
        'val' => 10,
      ),
    ),
    'M_MOST_VIEWED' => 
    array (
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => 4,
      ),
      'cols' => 
      array (
        'label' => 'COLS',
        'val' => 4,
      ),
    ),
    'M_NEW_PRODUCTS' => 
    array (
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => '8',
      ),
    ),
    'M_SLIDE_MAIN' => 
    array (
      'id' => 
      array (
        'val' => '22',
        'callable' => 'getTopics',
        'label' => 'TOPIC_NAME',
      ),
      'limit' => 
      array (
        'label' => 'LIMIT',
        'val' => '5',
      ),
      'height' => 
      array (
        'label' => 'SLIDER_HEIGHT_TITLE',
        'val' => '400',
      ),
      'width' => 
      array (
        'label' => 'SLIDER_WIDTH_TITLE',
        'callable' => 'getWidth',
        'val' => '2',
      ),
    ),
  ),
);
 ?>