<?php 
    $tpl_settings = array(
        'id'=>'left_specials',
        'classes'=>($config['slider']['val']==1)?array('product_slider'):array('front_section'),
        'cols'=>$config['cols']['val'] ?: 1,
        'limit'=>$config['limit']['val'] ?: 5,
        'title'=>BOX_HEADING_SPECIALS,
    );

    include(DIR_WS_MODULES . 'default_specials.php'); 
?>