<?php
/*
  $Id: information.php,v 1.1.1.1 2003/09/18 19:05:51 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2001 osCommerce

  Released under the GNU General Public License
*/

$articles_category = $config['id']['val'] ?: 13;
$articles_limit=$config['limit']['val'] ?: 5;
$art_array = getArticles($articles_category, $articles_limit, false, true);
$all_articles_string = '';

if (is_array($art_array)) {
    foreach ($art_array as $articles) {
        if ($articles['image'] != '') {
            $image = 'getimage/38x38/' . $articles['image'];
            $all_articles_string .= '<li><img class="lazyload" src="images/pixel_trans.png" data-src="' . $image . '" alt="' . $articles["name"] . '" width="38" height="38"><a href="' . $articles['link'] . '"><span>' . $articles['name'] . '</span></a></li>';
        } else {
            $all_articles_string .= '<li><a class="no_mg" href="' . $articles['link'] . '"><span>' . $articles['name'] . '</span></a></li>';
        }
    }


    ?>
    <!-- ARTICLES -->
    <div class="articles">
        <div class="like_h2"><?php echo BOX_HEADING_ARTICLES;?></div>
        <nav>
            <ul>
                <?php echo $all_articles_string;?>
            </ul>
        </nav>
        <a href="<?php echo tep_href_link(FILENAME_ARTICLES, 'tPath=' . $articles_category);?>"><?php echo BOX_ALL_ARTICLES;?></a>
    </div>
    <!-- END ARTICLES -->

    <?php
}
?>            