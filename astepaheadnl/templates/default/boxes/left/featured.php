<?php 
    $tpl_settings = array(
        'id'=>'left_featured',
        'classes'=>($config['slider']['val']==1)?array('product_slider'):array('front_section'),
        'cols'=>$config['cols']['val'] ?: 1,
        'limit'=>$config['limit']['val'] ?: 3,
        'title'=>BOX_HEADING_FEATURED,
    );

    include(DIR_WS_MODULES . 'featured.php'); 
?>