<!-- SLIDER -->   
<?php
$item_limit_mobile = $config['limit_mobile']['val'] ? : 5;
$item_limit = $config['limit']['val'] ? : 5;
$slider_images = getArticles($config['id']['val']?:22, isMobile() ? $item_limit_mobile : $item_limit, true, true);

if (count($slider_images)) : 

  if(count($slider_images)>1) {
    $slider_class = 'id="owl-frontslider" class="owl-carousel"';
    $slide_class = 'owl-lazy';
  } else {
    $slider_class = 'class="single_slide"';
    $slide_class = 'lazyload';
  }
  
  if($config['width']['val']==2)  $banner_width = '1920';
  elseif($config['width']['val']==1) $banner_width = '1410'; 
  else $banner_width = '1045';
    
  if(!isMobile()) {
    $banner_height = $config['height']['val'];
  } else {
    $banner_mobile_width = '395';
    $banner_height = (int)($banner_mobile_width*$config['height']['val']/$banner_width);  
    $banner_width = $banner_mobile_width; 
  }

  $banner_size= '&w='.$banner_width.'&h='.$banner_height; 
  $banner_size2= $banner_width.'x'.$banner_height; 
  $ratio = (int)(100/$banner_width*$banner_height);
  
?> 

<style>
#owl-frontslider .item, .single_slide .item, #owl-frontslider .item-video{padding-top: <?php echo $ratio; ?>%;}
#owl-frontslider .item > *, .single_slide .item > *, #owl-frontslider .item-video > *{margin-top: -<?php echo $ratio; ?>%;}
@media (max-width:480px) { #owl-frontslider{height:<?php echo $banner_height; ?>px;}}
@media (min-width:481px) { #owl-frontslider{max-height:<?php echo $banner_height; ?>px;}}
</style>                                                               
  <div class="position-relative">
    <div <?php echo $slider_class; ?> style="min-height:<?php echo $banner_height; ?>px;overflow:hidden;">
        <?php foreach ($slider_images as $slide) {
           
              $slide['desc'] = '<div>'.$slide['desc'].'</div>';
              
              if ($slide['image'] == '') { 
                  // get separate banners for each language:
                  $desc_img = explode('src="', $slide['desc']);
                  $desc_img = explode('"', $desc_img[1]);
                  $slide['image'] = $desc_img[0] . $banner_size;
                  if(!empty($desc_img[0])) $slide['desc'] = '';
              } else {
                  // get one banner for all languages:
                  $slide['image'] = '/getimage/'.$banner_size2.'/' . $slide['image'];
              }
              
              $slide['image'] = str_replace(" ","%20",$slide['image']); // prevent lazy load bug

              $direct_link = $slide['direct_link'] != ''?'<a href="'.$slide['link'].'">'.$slide['desc'].'</a>':$slide['desc'];

              if ($slide['code'] != '' && strstr($slide['code'],'youtube')) {  // video:
                  if(count($slider_images)>1) echo '<div class="item-video"><a class="owl-video" href="'.$slide['code'].'"></a></div>';
                  else echo '<div class="item">'.convertYoutube($slide['code']).'</div>';
              } elseif ($slide['code'] != '' && !strstr($slide['code'],'youtube')) {
                  continue; //wrong code
              } else { // image
                  echo '<div class="item '.$slide_class.'" data-src="'.HTTP_SERVER . $slide['image'].'">'.$direct_link.'</div>';
              } 
         
         } ?>    
    </div>
    <?php if(count($slider_images)>1) { ?>
      <ul id="carousel-custom-dots" class="owl-dots">
          <?php foreach ($slider_images as $slide): ?>
              <li class="owl-dot">
                <svg class="fa-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path></svg>
                <svg class="fa-circle-o" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200z"></path></svg>
              </li>
          <?php endforeach; ?>
      </ul>
    <?php } ?>
  </div>   
<?php endif; ?>
<!-- END SLIDER -->