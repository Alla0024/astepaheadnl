  <?php if (count($additional_images) > 1) { ?>
    <div id="sync1" class="owl-carousel">
        <?php foreach ($additional_images as $image): ?>
          <div class="item"><a href="getimage/products/<?php echo $image ?>" data-lightbox="image-1">
            <img class="lazyload" src="images/pixel_trans.png" data-src="<?php echo 'getimage/'.$big_width.'x'.$big_height.'/products/'.$image; ?>" alt="<?php echo htmlentities($products_name); ?>" title="<?php  echo IMAGE_BUTTON_ADDTO_CART.' '.htmlentities($products_name); ?>" /></a>
					</div>
        <?php endforeach ?>
    </div>
    <div class="thumbs_row">
      <div id="sync2" class="thumbs row text-center">
        <?php foreach ($additional_images as $k => $image): ?>
            <div class="col-xs-3"><img class="lazyload" src="images/pixel_trans.png" data-src="<?php echo 'getimage/'.$big_width.'x'.$big_height.'/products/'.$image; ?>" alt="<?php echo htmlentities($products_name).' - '.($k+1); ?>"></div>
          <?php endforeach ?>
      </div>
    </div>
  <?php } else {
       $image = $additional_images[0]?:'default.png';
  ?>
      <div class="item single_image"><a href="getimage/products/<?php echo $image ?>" data-lightbox="image-1">
        <img class="lazyload" src="images/pixel_trans.png" data-src="<?php echo 'getimage/'.$big_width.'x'.$big_height.'/products/'.$image; ?>" alt="<?php echo htmlentities($products_name); ?>" title="<?php echo IMAGE_BUTTON_ADDTO_CART.' '.htmlentities($products_name); ?>" /></a>
			</div>
  <?php }  ?>