<?php

echo tep_draw_form('dealer', tep_href_link(FILENAME_DEALER, tep_get_all_get_params(array('action','language')).'action=send')); ?>
<div class="row">
    <div class="col-sm-12 form-group">
        <h1><?= HEADING_TITLE ?></h1>
        <p><?= HEADING_SUBTITLE; ?></p>
    </div>
    <div class="col-sm-6 form-group clearfix">
        <h4><b><?= COMPANY_DETAILS_HEADER; ?></b></h4>
        <input type="text" name="company_name" class="form-group form-control" placeholder="<?= COMPANY_NAME; ?>" required />
        <input type="text" name="kvk_number" class="form-group form-control" placeholder="<?= KVK_NUMBER; ?>" required />
        <input type="text" name="btw_number" class="form-group form-control" placeholder="<?= BTW_NUMBER; ?>" required />
        <input type="text" name="address" class="form-group form-control" placeholder="<?= ADDRESS; ?>" required />
        <input type="text" name="venue" class="form-group form-control" placeholder="<?= VENUE; ?>" required />
        <input type="text" name="postal_code" class="form-group form-control" placeholder="<?= POSTAL_CODE; ?>" required />
        <input type="text" name="country" class="form-group form-control" placeholder="<?= COUNTRY; ?>" required />
        <h4><b><?= CONTACT_DETAILS; ?></b></h4>
        <input type="text" name="first_name" class="form-group form-control" placeholder="<?= FIRST_NAME; ?>" required />
        <input type="text" name="last_name" class="form-group form-control" placeholder="<?= LAST_NAME; ?>" required />
        <input type="text" name="email" class="form-group form-control" placeholder="<?php echo  EMAIL ?>" required />
        <input type="text" name="phone" class="form-group form-control" placeholder="<?php echo  PHONE ?>" required />
        <textarea name="enquiry" class="form-group form-control" placeholder="<?php echo  ENQUIRY ?>" rows="5" required ></textarea>
        <div>
            <input type="checkbox" id="subscribe_newsletter" name="subscribe_newsletter" checked>
            <label for="subscribe_newsletter"><?= SUBSCRIBE_NEWSLETTER; ?></label>
        </div>
        <div>
            <input type="checkbox" id="terms_conditions" name="terms_conditions" required>
            <label for="terms_conditions"><?= AGREE_TEXT; ?>
                <a href="<?= tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=131')?>" target="_blank">
                    <?= TERMS_CONDITIONS; ?>
                </a>
            </label>
        </div>
        <?php  require_once 'ext/recaptcha/recaptcha.php' ?><br/>
        <button type="submit" class="form-group btn pull-left btn-default"><?php echo SEND_MESSAGE; ?></button>
    </div>
    <div class="col-sm-6 form-group">
        <?= renderArticle('dealer'); ?>
    </div>
</div>
</form>