<div class="row">
	<div class="col-md-6">
		<?php echo tep_draw_form('create_account', tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'), 'post', 'onSubmit="return check_form(create_account);"') . tep_draw_hidden_field('action', 'process') . tep_draw_hidden_field('guest_account', $guest_account); ?>

		<h2><?php echo HEADER_TITLE_CREATE_ACCOUNT; ?></h2>
		<?php echo $messageStack->render('create_account'); ?>

		<div class="form-group">
			<?php echo '<a class="btn btn-default btn-xs" rel="nofollow" href="'.tep_href_link(FILENAME_LOGIN).'">'.CR_ENTER.'</a>, '.CR_IF; ?>
		</div>

		<div class="form-group">
			<input type="text" name="firstname" class="form-control reg_input" placeholder="<?php echo ENTRY_FIRST_NAME; ?>" value="<?php echo  $_POST['firstname']; ?>">
		</div>
    
		<?php if (ACCOUNT_LAST_NAME == 'true'): ?>
			<div class="form-group">
				<input type="text" name="lastname" class="form-control reg_input" placeholder="<?php echo ENTRY_LAST_NAME; ?>" value="<?php echo  $_POST['lastname']; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_DOB == 'true'): ?>
			<div class="form-group">
				<input type="text" name="dob" class="form-control reg_input" placeholder="<?php echo ENTRY_DATE_OF_BIRTH; ?>" value="<?php echo  $_POST['dob']; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_COMPANY == 'true'): ?>
			<div class="form-group">
				<input type="text" name="company" class="form-control reg_input" placeholder="<?php echo ENTRY_COMPANY; ?>" value="<?php echo  $_POST['company']; ?>">
			</div>
		<?php endif ?>

		<div class="form-group">
			<input type="text" name="email_address" class="form-control reg_input" placeholder="<?php echo ENTRY_EMAIL_ADDRESS; ?>" value="<?php echo  $_POST['email_address']; ?>">
		</div>

		<?php if (ACCOUNT_TELE == 'true'): ?>
			<div class="form-group">
				<input type="text" name="telephone" class="form-control reg_input" placeholder="<?php echo ENTRY_TELEPHONE_NUMBER; ?>" value="<?php echo  $_POST['telephone']; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_STREET_ADDRESS == 'true'): ?>
			<div class="form-group">
				<input type="text" name="street_address" class="form-control reg_input" placeholder="<?php echo ENTRY_STREET_ADDRESS; ?>" value="<?php echo  $_POST['street_address']; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_CITY == 'true'): ?>
			<div class="form-group">
				<input type="text" name="city" class="form-control reg_input" placeholder="<?php echo ENTRY_CITY; ?>" value="<?php echo  $_POST['city']; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_SUBURB == 'true'): ?>
			<div class="form-group">
				<input type="text" name="suburb" class="form-control reg_input" placeholder="<?php echo ENTRY_SUBURB; ?>" value="<?php echo  $_POST['suburb']; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_POSTCODE == 'true'): ?>
			<div class="form-group">
				<input type="text" name="postcode" class="form-control reg_input" placeholder="<?php echo ENTRY_POST_CODE; ?>" value="<?php echo  $_POST['postcode']; ?>">
			</div>
		<?php endif ?>



		<!-- select state -->


		<?php

		if (ACCOUNT_COUNTRY == 'true' or ACCOUNT_STATE == 'true') {

      if (ACCOUNT_COUNTRY != 'true') $non_show = 'style="display:none;"';       
      echo '<div class="form-group" '.$non_show.'>'.tep_get_country_list('selectCountry',  isset($country_id) ? $country_id['countries_id'] : STORE_COUNTRY, 'data-zone="'.($_POST['selectRegion']?:STORE_ZONE).'" autocomplete="off" class="checkout_inputs required"').'</div>';
		}

		?>


		<?php if (ACCOUNT_FAX == 'true'): ?>
			<div class="form-group">
				<input type="text" name="fax" class="form-control reg_input" placeholder="<?php echo ENTRY_FAX_NUMBER; ?>" value="<?php echo  $_POST['fax']; ?>">
			</div>
		<?php endif ?>

		<?php if (!$guest_account): ?>
			<div class="form-group">
				<input type="password" name="password" class="form-control reg_input" placeholder="<?php echo ENTRY_PASSWORD; ?>" >
			</div>
			<div class="form-group">
				<input type="password" name="confirmation" class="form-control reg_input" placeholder="<?php echo ENTRY_PASSWORD_CONFIRMATION; ?>">
			</div>
		<?php endif ?>

		<?php if (ACCOUNT_NEWS == 'true'): ?>
			<div class="form-group">
				<?php echo tep_draw_checkbox_field('newsletter', '1', true, 'id="account_newsletter"') ?>
				<label for="account_newsletter"><?php echo ENTRY_NEWSLETTER?></label>
			</div>
		<?php endif ?>
    
			<div class="form-group">
				<?php echo tep_draw_checkbox_field('shoprules', '1', false, 'id="shoprules"') ?>
				<label for="shoprules"><?php echo SHOPRULES_AGREE; ?></label>
        <a href="#" class="ajax_modal_article" data-id="store-rules"><?php echo SHOPRULES_AGREE2; ?></a>
			</div>
        <div class="form-group">
            <?php
            if (defined('GOOGLE_RECAPTCHA_STATUS') && GOOGLE_RECAPTCHA_STATUS !== 'false' && is_file(DIR_WS_EXT . "recaptcha/recaptcha.php")) {
                require_once DIR_WS_EXT . "recaptcha/recaptcha.php";
            }
            ?>
        </div>
                 
<!--		<p><img class="lazyload" src="images/pixel_trans.png" data-src="<?php /*echo tep_href_link(DIR_WS_INCLUDES.'kcaptcha/kindex.php', '', 'SSL');*/?>"></p>

		<p><input type="text" name="keystring"></p>
-->
		<div class="form-group">
			<input type="submit" class="btn btn-default" value="<?php echo SEND_MESSAGE; ?>">
		</div>
		</form>
	</div>
</div>