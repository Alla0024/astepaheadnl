<?php 
    if (tep_session_is_registered('customer_id')) {
        $customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_id='" . $customer_id . "'");
        $customer_array = tep_db_fetch_array($customer_query);
        $val_name = $customer_array['customers_firstname'] . ' ' . $customer_array['customers_lastname'];
        $val_email = $customer_array['customers_email_address'];
    } else {
        $val_name = '';
        $val_email = '';
    } 
       
    echo tep_draw_form('contact_us', tep_href_link(FILENAME_CONTACT_US, tep_get_all_get_params(array('action','language')).'action=send')); ?>

      <div class="row">
        <div class="col-sm-12 form-group">  
          <h1><?php echo  HEADING_TITLE ?></h1>
          <?php //echo renderArticle(76); // contacts main ?>
        </div>
        <div class="col-sm-6 form-group clearfix">  
          <input type="text" name="name" class="form-group form-control" placeholder="<?php echo  ENTRY_NAME ?>" required value="<?php echo  $val_name ?>" />
          <input type="text" name="email" class="form-group form-control" placeholder="<?php echo  ENTRY_EMAIL ?>" required value="<?php echo  $val_email ?>" />
          <input type="text" name="phone" class="form-group form-control" placeholder="<?php echo  ENTRY_PHONE ?>" required />
          <textarea name="enquiry" class="form-group form-control" placeholder="<?php echo  ENTRY_ENQUIRY ?>" rows="5" required ></textarea>
          <?php require_once 'ext/recaptcha/recaptcha.php' ?><br />
            <button type="submit" class="form-group btn pull-left btn-default"><?php echo SEND_MESSAGE; ?></button>
        </div>   
        <div class="col-sm-6 form-group">
          <?php echo renderArticle('contacts'); // contacts  ?>
          <?php //echo renderArticle(96); // google map ?>
        </div>        
      </div>  
    </form>