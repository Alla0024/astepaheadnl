<?php
// QUERIES FOR COLUMNS

$body_classes = array();
$center_margin = '';
$sidebar_left =  $template->getMainconf('MC_SHOW_LEFT_COLUMN')?:false;


$show_breadcrumbs = true;

// FIND CLASSES FOR COLUMNS
if ($_GET['cPath'] == '') {
    $show_breadcrumbs = true;
}

/**
 * Turn off left sidebar for manufacturers page
 */
if(basename($PHP_SELF) === FILENAME_MANUFACTURERS) {
    $sidebar_left = false;
    $center_classes .= 'col-xs-12 col-sm-12 col-md-12 ';
}

if ((is_array($cPath_array) and count($cPath_array)) or isset($_GET['manufacturers_id'])) {
    $sidebar_left = true;
}
// IF PRODUCT INFO
if (isset($_GET['products_id']) || $content == 'checkout' || $content == 'compare') {
    $sidebar_left = false;
    $show_breadcrumbs = true;

}
if (isset($_GET['products_id'])) {
    array_push($body_classes, 'product_page');
}
if ($content == 'checkout') {
    array_push($body_classes, 'checkout_page');
}

// IF MAINPAGE 
if ($content == 'index_default') {
    $show_breadcrumbs = false;
    array_push($body_classes, 'frontpage');
} else {
    array_push($body_classes, 'not-front');
}

//HIDE SIDEBAR_LEFT
if ($page_not_found){
    $sidebar_left = false;
}

if (isMobile()){
    $sidebar_left = false;
    if ($content == 'index_products') {
      $sidebar_left = true;
    }
}

// DEFINE CLASSES FOR COLUMNS
if ($sidebar_left) {
    array_push($body_classes, 'one-sidebar', 'left-sidebar');
    $center_classes .= 'col-xs-12 col-sm-12 col-md-9';
} else {
    $center_margin = '';
    $center_classes .= 'col-xs-12 col-sm-12 col-md-12 ';
}

if (isset($_GET['products_id'])) {
    $squeeze_margin = "margin:0 0 0 0;";
}
if ($body_classes != '' and is_array($body_classes)) {
    $body_class = implode(' ', $body_classes);
}
// echo '<pre>',var_dump($body_class),'</pre>';
if(($content=='product_info' and !$template->show('P_BREADCRUMB')) or ($content=='index_products' and !$template->show('LIST_BREADCRUMB'))) {
    $show_breadcrumbs = false;
}

?>  