<!DOCTYPE html>
<html <?php echo HTML_PARAMS;?> prefix="og: http://ogp.me/ns#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="<?=$template->getMainconf('MC_COLOR_1')?>">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="<?=$template->getMainconf('MC_COLOR_1')?>">
    <?php  

      echo "\n\t" .'<base href="'.HTTP_SERVER . DIR_WS_CATALOG.'">';
      require(DIR_WS_INCLUDES . 'header_tags.php');  // <title> , <meta name="Description", Keywords, Reply-to 



    echo get_noindex_nofollow();
      echo get_canonical();
      outputGoogleVerificationMetaTag();
      echo get_custom_favicon(); // custom favicon       
      echo get_rel_prevnext(); // <link rel="next" and "prev"

       if ($isFilter){
           echo get_rel_alternate_seo_filters(); // <link rel="alternate"
       } else{
           echo get_rel_alternate(); // <link rel="alternate"
       }

      // MINIFY_CSSJS: 0 - not minify, 1 - minify always, 2 - use before minified version, not use minify

      $css_array = array(DIR_WS_JAVASCRIPT . 'jqueryui/css/smoothness/jquery-ui-1.10.4.custom.min.css',
                         DIR_WS_JAVASCRIPT . 'owl-carousel/owl.carousel.css',
                         DIR_WS_JAVASCRIPT . 'owl-carousel/owl.theme.css',
                         DIR_WS_JAVASCRIPT . 'selectize/selectize.css',
                         DIR_WS_TEMPLATES . TEMPLATE_NAME.'/css/bootstrap.min.css',
                         DIR_WS_TEMPLATES . TEMPLATE_NAME.'/stylesheet.css',
                         DIR_WS_TEMPLATES . TEMPLATE_NAME.'/css/responsive.css',
                         DIR_WS_JAVASCRIPT . '/customization_panel.css',
                         DIR_WS_JAVASCRIPT . 'Spectrum/spectrum.min.css');
    $css_nominify_array = [];
   
      if (isset($_GET['products_id']))
        $css_nominify_array[] = 'includes/javascript/lightbox/lightbox.min.css';                   
      if (getenv('APP_ENV')=='production' or getenv('APP_ENV')=='demo')
        $css_array[] = 'includes/javascript/colorpicker.css';

      if(MINIFY_CSSJS==0) { // show all styles SEPARATELY
        if(!empty($css_nominify_array)) $css_array = array_merge($css_array, $css_nominify_array);
        foreach($css_array as $css) echo "\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"".$css."\">";
      } elseif(MINIFY_CSSJS==1) {
          if (file_exists(DIR_WS_EXT . 'minifier/minifier.php')) {
              require_once(DIR_WS_EXT . 'minifier/minifier.php');
              foreach ($css_array as $k => $css) $css_array[$k] = file_get_contents($css);
              $minifierCSS->add(implode(' ', $css_array));
              file_put_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/css/all.min.css', $minifierCSS->minify());
          }
      }
      
      if(MINIFY_CSSJS!=0) {
         if(!isMobile())  echo "\t".'<link rel="stylesheet" type="text/css" href="'.DIR_WS_TEMPLATES . TEMPLATE_NAME.'/css/all.min.css?v='.$versionTimestamp.'">'."\n\t";
         else echo '<style>'.file_get_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/css/all.min.css').'</style>'."\n\t";
         
         if(is_array($css_nominify_array)) 
            foreach($css_nominify_array as $k => $js) 
                echo "\t".'<link rel="stylesheet" type="text/css" href="'.$js.'">'."\n\t";   
      }
      
      if(isset($_COOKIE['LongScriptsLoaded'])) {
          echo '<link rel="stylesheet" type="text/css" href="'.DIR_WS_TEMPLATES . TEMPLATE_NAME.'/css/fonts.css">';
      }
                   
    ?>

    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_WS_TEMPLATES . TEMPLATE_NAME;?>/css/ie.css">
    <![endif]-->

    <meta property="og:locale" content="<?php echo OG_LOCALE;?>"/>
    <meta property="og:title" content="<?php echo $the_title;?>"/>
    <meta property="og:type" content="<?php echo $content=='product_info'?'product':'website'; ?>"/>
    <meta property="fb:app_id" content="<?php echo $fb_app_id; ?>"/>
    <meta property="og:description" content="<?php echo $the_desc;?>"/>
    <meta property="og:url" content="<?=HTTP_SERVER.$_SERVER['REQUEST_URI'];?>"/>  
    <meta property="og:image" content="<?php echo ($product_info['products_image']!='')?(HTTP_SERVER . '/getimage/products/' . urlencode(explode(';', $product_info['products_image'])[0])):HTTP_SERVER.'/'.LOGO_IMAGE ;?>"/>
    <?php
      if ($product_info['products_id']!='') { 
        echo '<meta property="og:type" content="product"/>
              <meta property="og:image:width" content="500"/>
              <meta property="og:image:height" content="500"/>';  
      } 
      echo '<style>
              :root {--sm-text-color: ' . $template->getMainconf('MC_COLOR_1') . ';--sm-link-color: ' . $template->getMainconf('MC_COLOR_2') . ';--sm-background: ' . $template->getMainconf('MC_COLOR_3') . ';--sm-bg-footer: ' . $template->getMainconf('MC_COLOR_5') . ';--sm-bg-header: ' . $template->getMainconf('MC_COLOR_4') . ';--sm-btn-color: ' . $template->getMainconf('MC_COLOR_6') . ';}
              .p_img_href {height: ' . (SMALL_IMAGE_HEIGHT + 10) . 'px;line-height: ' . (SMALL_IMAGE_HEIGHT + 10) . 'px;}
              .p_img_href_list{max-width: ' . SMALL_IMAGE_WIDTH . 'px;}
              .product {height:' . (SMALL_IMAGE_HEIGHT + 175) . 'px;}
              .product_slider {height:' . (SMALL_IMAGE_HEIGHT + 165) . 'px;}
              @media (max-width:415px) {
                .product {height:' . (SMALL_IMAGE_HEIGHT + 158) . 'px;}
                .product_slider {height:' . (SMALL_IMAGE_HEIGHT + 163) . 'px;}
              }
            </style>';
    ?>
</head>

<?php require_once(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/bootstrap.php'); ?>
<body class="<?php echo $body_class; ?>">
<?php require_once(DIR_WS_MODULES . 'check_buy.php'); ?><!----><?php //require_once(DIR_WS_MODULES . 'edit_page.php'); ?>
<?php require $template->requireBox('H_COMPARE'); ?>
<?php require $template->requireBox('H_WISHLIST'); ?>

<div class="page-wrap">
    <header>
        <!-- TOP HEADER -->
        <?php if(!isMobile()) { ?>
        <div class="top_header hidden-xs visible-sm-block">
            <div class="container container_top_header clearfix">
                <?php require $template->requireBox('H_LANGUAGES'); ?>
                <?php require $template->requireBox('H_CURRENCIES'); ?>
                <?php require $template->requireBox('H_LOGIN'); ?>
                <?php require $template->requireBox('H_TOP_LINKS', $config); ?>
            </div>
        </div>
        <!-- END TOP HEADER -->
        
        <!-- MIDDLE HEADER -->
        <div class="middle_header hidden-xs visible-sm-block">
            <div class="container container_middle_header">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <?php require $template->requireBox('H_LOGO'); ?>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12">
                        <div class="call_me clearfix">
                            <div class="phones_header">
                                <?php echo renderArticle('phones'); ?>
                            </div>
                            <?php require $template->requireBox('H_CALLBACK'); ?>
                         <!--   <div class="time_work">
                                <div class="callback_round">
                                  <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512">
                                    <path d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z"></path>
                                  </svg>
                                </div>
                                <?php echo renderArticle('time_work'); ?>
	                            <?php require $template->requireBox('H_ONLINE'); ?>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="search_site">    
                                    <?php require $template->requireBox('H_SEARCH',$config); ?>
                                    <?php require $template->requireBox('H_SHOPPING_CART'); ?>

                                    <!-- SHOPPING CART LESS 768 PX -->
                                    <?php echo $cart_output_mobile; ?>
                                    <!-- END SHOPPING CART LESS 768 PX -->
                                </div>
                            </div>
	                        <!--<div class="col-xs-4 visible-lg"></div>-->
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                        <!-- SHOPPING CART -->
                        <?php echo $cart_output; ?>
                        <!-- END SHOPPING CART -->
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- END MIDDLE HEADER -->

        <!-- HORIZONTAL MENU  -->
        <?php require $template->requireBox('H_TOP_MENU'); ?>
        <!-- END HORIZONTAL MENU -->
    </header>


    <main>
        <?php include DIR_WS_MODULES . "AlertMessage.php"; ?>
        <?php $instagramPath = '/ext/instagram/insta_feed.php';
        $instagramConfig = $template->checkConfig('MAINPAGE', 'M_INSTAGRAM');
        if (file_exists($rootPath.$instagramPath) && $template->show('M_INSTAGRAM') && !empty($instagramConfig['url']['val'])) {
            include $rootPath.$instagramPath;
            if ($content=='index_default') {?>
                <div class="container instagram_posts">
                    <div id="<?= $instaBlockId?>" class="row"></div>
                </div>
            <?php }
        }?>
        <!-- SLIDER  -->
        <?php if ($content=='index_default' && $file=$template->getFiles('MAINPAGE', 'M_SLIDE_MAIN',$config)) {
                if ($config['width']['val']!=0) {
                  if ($config['width']['val']==1 ) $content_class = 'container'; // slider width: 1140px or in right column
                  elseif ($config['width']['val']==2) $content_class = 'container-fluid padd-0'; // slider width: 100%; 
          ?>
                  <div class="<?php echo $content_class; ?>">
                      <?php require_once $file; ?>
                  </div>
        <?php   }
              } ?>
        <!-- END SLIDER  -->
        <div class="container">
            <div class="row">
                <!-- CENTER CONTENT -->
                <div class="<?php echo $center_classes?> right_content">
                    <!-- BREADCRUMBS -->
                        <?php if ($show_breadcrumbs) echo $breadcrumb->trail(' ');?>
                    <!-- END BREADCRUMBS -->

                    <!-- CONTENT -->
                    <?php
                    if (file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/' . $content . '.tpl.php')) {
                        require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/' . $content . '.tpl.php'); // content from current template (if exists)
                    } else {
                        require(DIR_WS_CONTENT . $content . '.tpl.php'); // content from default template
                    }
                    ?>
                    <!-- END CONTENT -->

                </div>
                <!-- END CENTER CONTENT -->

                <!-- COLUMN LEFT -->
                <?php if ($sidebar_left): ?>
                    <div id="sidebar-left" class="col-sm-3 sidebar left_content">
                        <aside>
                            <?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
                        </aside>
                    </div>
                    <button type="button" class="sidebar-toggle-back btn-link hidden-xs hidden-sm">
                        <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path>
                        </svg>
                    </button>
                <?php endif ?>
                <!-- END COLUMN LEFT -->
            </div>
        </div>
    </main>
</div>

<footer>
    <div class="top_footer">
        <div class="container">
            <div class="row row_menu_contacts_footer">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <?php require $template->requireBox('F_TOP_LINKS',$config); ?>     
                        <?php require $template->requireBox('F_FOOTER_CATEGORIES_MENU'); ?>  
                        <?php require $template->requireBox('F_ARTICLES_BOTTOM',$config); ?>     
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="contacts_info_footer">
                        <?php
                        $footer_contacts=renderArticle('contacts_footer');
                        if ($footer_contacts!='') { ?>
                            <!-- FOOTER CONTACTS -->
                            <div class="h3"><?php echo FOOTER_CONTACTS;?></div>
                            <a href="#" rel="nofollow"  class="toggle-xs" data-target="#footer_contacts"></a>
                            <div class="phones" id="footer_contacts"><?php echo $footer_contacts;?></div>
                            <!-- END FOOTER CONTACTS -->
                        <?php } ?>
<!--  F_SOCIAL   -->
                        <?php if (SOCIAL_WIDGETS_ENABLED=='true' and $template->show('F_SOCIAL')) { ?>
                            <!-- SOCIAL BUTTONS -->
                            <div class="social_group_footer">
                               <p><a href="https://www.facebook.com/A-Step-Ahead-707771095933274" target="_blank"><img alt="" class="img-responsive  lazyload" src="https://astepahead.nl/images/Facebook-logo-footer.png" /></a>&nbsp; <a href="https://www.linkedin.com/company/a-stepahead/about/" target="_blank"><img alt="" class="img-responsive  lazyload" src="https://astepahead.nl/images/LinkedIn-logo-footer.png" /></a>&nbsp; <a href="#" target="_blank"><img alt="" class="img-responsive  lazyload" src="https://astepahead.nl/images/Instagram-logo-footer.png" /></a></p>
                            </div>
                            <!-- END SOCIAL BUTTONS -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FOOTER COPYRIGHT -->
    <div class="bottom_footer">
        <div class="container">
            <div class="row row_copyright">
                <div class="col-sm-3 col-xs-12">
                    <div class="copyright">                                                                  
                        <p><?php echo FOOTER_COPYRIGHT;?> <?php echo date('Y');?> <?php echo STORE_NAME;?>. <?php echo FOOTER_ALLRIGHTS;?></p>
                        <a href="<?php echo tep_href_link('sitemap.html');?>"><?php echo FOOTER_SITEMAP;?></a>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
<!-- F_MONEY_SYSTEM   -->
                    <?php if($template->show('F_MONEY_SYSTEM')): ?>
                    <div class="money_systems">
                        <nav>
                            <ul>
                                <li>
                                   
                                        <img class="lazyload" src="images/pixel_trans.png" data-src="<?php echo DIR_WS_TEMPLATES . TEMPLATE_NAME;?>/images/master_card.png" alt="master_card" height="1" width="1">
                                  
                                </li>
                                <li>
                                  
                                        <img class="lazyload" src="images/pixel_trans.png" data-src="<?php echo DIR_WS_TEMPLATES . TEMPLATE_NAME;?>/images/visa.png" alt="visa" height="1" width="1">
                                    
                                </li>
                                <li>
                                  
                                        <img class="lazyload" src="images/pixel_trans.png" data-src="<?php echo DIR_WS_TEMPLATES . TEMPLATE_NAME;?>/images/paypal.png" alt="paypal" height="1" width="1">
                                  
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <?php endif ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="link_webstudio">
                        <p><?php echo FOOTER_DEVELOPED;?></p>
                        <a href="https://solomono.net<?php echo $solomono_link; ?>">SoloMono.net</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END FOOTER COPYRIGHT -->
</footer>


<?php require(DIR_WS_INCLUDES . '/javascript/javascript.php'); ?>
<?php if (isset($javascript) && file_exists(DIR_WS_JAVASCRIPT . basename($javascript))) require(DIR_WS_JAVASCRIPT . basename($javascript)); ?>

</body>
</html>