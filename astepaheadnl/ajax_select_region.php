<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
    require('includes/application_top.php');
        
    function get_region($country_id, $name = 'selectRegion', $default_region = 0){
        $regions_array = array(array('id' => '', 'text' => PULL_DOWN_COUNTRY));
        $regions = tep_get_country_zones($country_id);
        $size = sizeof($regions);

        for ($i=0; $i<$size; $i++) {

            $regions_array[] = array('id' => $regions[$i]['id'], 'text' => $regions[$i]['text']);
        }

        echo '<div class="form-group">';
       
        if($size>1) echo tep_draw_pull_down_menu($name, $regions_array,$default_region,'class="fulllength checkout_inputs required"');
        else echo tep_draw_input_field($name,$default_region,'class="form-control checkout_inputs required" placeholder="'.ENTRY_STATE_ERROR_SELECT.'"');
      
        echo '</div>';
    }
    
    if(isset($_GET['country_id']) && ACCOUNT_STATE == "true") {
        get_region($_GET['country_id'], $name = $_GET['zone_field_name'], $_GET['default_region']);
    }

    if($_GET['name']=='sendto') $_SESSION['sendto'] = $_SESSION['customer_default_address_id'] = $_GET['val']; 
    if($_GET['name']=='billto') $_SESSION['billto'] = $_SESSION['customer_default_address_id'] = $_GET['val'];  
    
}

?>