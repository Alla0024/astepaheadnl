<?php
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') require('includes/application_top.php');
    require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOPPING_CART);

    $any_out_of_stock = 0;
    $disabled = '';
    $stock_text = '';
    $products = $cart->get_products();
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {

        // Push all attributes information in an array
        if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
            foreach($products[$i]['attributes'] as $option => $value) {
                $attributes = tep_db_query("SELECT  popt.products_options_name, 
                                                    poval.products_options_values_name, 
                                                    pa.options_values_price, 
                                                    pa.price_prefix
                                               FROM " . TABLE_PRODUCTS_OPTIONS . " popt, " .
                                                        TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " .
                                                        TABLE_PRODUCTS_ATTRIBUTES . " pa
                                                  WHERE pa.products_id = '" . $products[$i]['id'] . "'
                                                    and pa.options_id = '" . $option . "'
                                                    and pa.options_id = popt.products_options_id
                                                    and pa.options_values_id = '" . (int)$value . "'
                                                    and pa.options_values_id = poval.products_options_values_id
                                                    and popt.language_id = '" . $languages_id . "'
                                                    and poval.language_id = '" . $languages_id . "'");

                $attributes_values = tep_db_fetch_array($attributes);
                if ($value == PRODUCTS_OPTIONS_VALUE_TEXT_ID) {
                    $attr_value = $products[$i]['attributes_values'][$option]
                    .tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . TEXT_PREFIX . $option . ']',  $products[$i]['attributes_values'][$option]);;
                } else {
                    $attr_value = $attributes_values['products_options_values_name']
                    .tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . $option . ']', $value);
                }

                $products[$i][$option]['products_options_name'] = $attributes_values['products_options_name'];
                $products[$i][$option]['options_values_id'] = $value;
                $products[$i][$option]['products_options_values_name'] = $attr_value;
                $products[$i][$option]['options_values_price'] = $attributes_values['options_values_price'];
                $products[$i][$option]['price_prefix'] = $attributes_values['price_prefix'];
            }
        }

        $products_name = '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '">' . $products[$i]['name'] . '</a>';

        if (STOCK_CHECK == 'true') {
            $stock_check = tep_check_stock($products[$i]['id'], $products[$i]['quantity']);
            if (tep_not_null($stock_check)) {
                $any_out_of_stock = 1;
                $products_name .= $stock_check;
            }
        }

        if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
            reset($products[$i]['attributes']);
            $products_name .= '<ul class="attributes_list">';
            foreach($products[$i]['attributes'] as $option => $value) {
                $products_name .= '<li><span>'.$products[$i][$option]['products_options_name'].'</span> : '.$products[$i][$option]['products_options_values_name'] . '</li>';
            }
            $products_name .= '</ul>';
        }

        $html[$i]['id'] = $products[$i]['id'];
        $products_image = explode(';', $products[$i]['image']);
        $html[$i]['image'] = $products_image[0];
        $html[$i]['name'] = $products_name;
        $html[$i]['price'] = $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']));
        $html[$i]['qty'] = $products[$i]['quantity'];
        $html[$i]['price_full'] = $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $products[$i]['quantity']);
    }
    
    
    // Products, signed as *** are not enought...
    if ($any_out_of_stock == 1){
        if (STOCK_ALLOW_CHECKOUT == 'true') {
            $stock_text = '<div class="warning_mess">'.OUT_OF_STOCK_CAN_CHECKOUT.'</div>';
        } else {
            $stock_text = '<div class="warning_mess">'.OUT_OF_STOCK_CANT_CHECKOUT.'</div>';
            $disabled = ' style="pointer-events: none;background:#999;"';
        }
    }

    $content = CONTENT_POPUP_CART;
    if (file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/' . $content . '.tpl.php')) {
        require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/' . $content . '.tpl.php');
    } else {
        require(DIR_WS_CONTENT . $content . '.tpl.php');
    }
?>
