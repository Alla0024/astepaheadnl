<?php
/*
  $Id: modules.php,v 1.2 2003/09/24 15:18:15 wilt Exp $
  ++++ modified as USPS Methods 2.5 08/02/03 by Brad Waite and Fritz Clapp ++++
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('includes/application_top.php');
$set = (isset($_GET['set']) ? $_GET['set'] : '');

if (tep_not_null($set)) {
    switch ($set) {
        case 'shipping':
            $module_type = 'shipping';
            $module_directory = DIR_FS_CATALOG_MODULES . 'shipping/';
            $module_key = 'MODULE_SHIPPING_INSTALLED';
            define('HEADING_TITLE', HEADING_TITLE_MODULES_SHIPPING);
            break;
        case 'ordertotal':
            $module_type = 'order_total';
            $module_directory = DIR_FS_CATALOG_MODULES . 'order_total/';
            $module_key = 'MODULE_ORDER_TOTAL_INSTALLED';
            define('HEADING_TITLE', HEADING_TITLE_MODULES_ORDER_TOTAL);
            break;
        case 'payment':
            $module_type = 'payment';
            $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
            $module_key = 'MODULE_PAYMENT_INSTALLED';
            define('HEADING_TITLE', HEADING_TITLE_MODULES_PAYMENT);
            break;
        default:
            $module_type = 'payment';
            $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
            $module_key = 'MODULE_PAYMENT_INSTALLED';
            define('HEADING_TITLE', HEADING_TITLE_MODULES_PAYMENT);
            break;
    }
}

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
    switch ($action) {
        case 'edit':

            $class = $_GET['module'];
            $file = $class.'.php';

            include (DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $file);
            include ($module_directory . $file);

            if (tep_class_exists($class)) {
                $module = new $class;

                $module_info = array(
                    'code' => $module->code,
                    'title' => $module->title,
                    'description' => $module->description,
                    'status' => $module->check()
                );

                $module_keys = $module->keys();
                $keys_extra = array();
                for ($j = 0, $k = sizeof($module_keys); $j < $k; $j++) {
                    $key_value_query = tep_db_query("select configuration_title, configuration_value, configuration_key, configuration_description, use_function, set_function from " . TABLE_CONFIGURATION . " where configuration_key = '" . $module_keys[$j] . "'");
                    $key_value = tep_db_fetch_array($key_value_query);
                    if (defined(strtoupper($configuration['configuration_key'] . '_TITLE'))) $keys_extra[$module_keys[$j]]['title'] = constant(strtoupper($configuration['configuration_key'] . '_TITLE'));
                    else $keys_extra[$module_keys[$j]]['title'] = $key_value['configuration_title'];
                    $keys_extra[$module_keys[$j]]['value'] = $key_value['configuration_value'];
                    $keys_extra[$module_keys[$j]]['description'] = $key_value['configuration_description'];
                    $keys_extra[$module_keys[$j]]['use_function'] = $key_value['use_function'];
                    $keys_extra[$module_keys[$j]]['set_function'] = $key_value['set_function'];
                }

                $module_info['keys'] = $keys_extra;
                $mInfo = new objectInfo($module_info);

                $heading = array();
                $contents = array();

                $keys = '';
                reset($mInfo->keys);

                foreach ($mInfo->keys as $key => $value) {
                    // while (list($key, $value) = each($mInfo->keys)) {

                    if(defined(strtoupper($key.'_TITLE'))) $value['title'] = constant(strtoupper($key.'_TITLE'));
                    if(defined(strtoupper($key.'_DESC'))) $value['description'] = constant(strtoupper($key.'_DESC'));

                    $keys .= '<b>' . $value['title'] . '</b>';
                    if($value['description']!='') $keys .= '<br>' . $value['description'] . '<br>';

                    if ($value['set_function']) {
                        eval('$keys .= ' . $value['set_function'] . "'" . $value['value'] . "', '" . $key . "');");
                    } else {
                        $keys .= tep_draw_input_field('configuration[' . $key . ']', $value['value'], 'class="form-control"');
                    }
                    $keys .= '<br>';
                }

                $keys = substr($keys, 0, strrpos($keys, '<br>'));

                $contents = array('form' => tep_draw_form('modules', FILENAME_MODULES, 'set=' . $set . '&module=' . $_GET['module'] . '&action=save'));
                $contents[] = array('text' => $keys);

                $params = array(
                    'submitButton'  => array('name' => IMAGE_UPDATE, 'class' => 'ajax btn btn-info'),
                    'cancelButton'  => array('name' => IMAGE_CANCEL, 'class' => 'btn btn-default'),
                );

                ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo TEXT_CLOSE_BUTTON; ?>"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="editModalLabel"><?php print $mInfo->title; ?></h4>
                    </div>

                    <?php
                    $box = new box;
                    echo $box->infoBoxModal($contents, $params);
                    ?>
                </div>
                <?php
            }

            exit;
            break;

        case 'save':

            $configuration = $_POST['configuration'];

            $sort_order = '';
            foreach ($configuration as $key => $value) {
                // while (list($key, $value) = each($configuration)) {


                if( strpos($key, '_SORT_ORDER') !== false ) {
                    $sort_order = $value;
                }

                if( is_array( $value ) ) {
                    $value = implode( ", ", $value);
                    $value = preg_replace ("/, --none--/", "", $value);
                }

                tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . $value . "' where configuration_key = '" . $key . "'");
            }

// Start MZMT
            // Runs the module's Update method if it exists. This allows updating install data after
            //   the installation is complete. The method code needs to determine when it should run.
            $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
            $class = $_GET['module'];
            if( file_exists( $module_directory . $class . $file_extension ) ) {
                include_once DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $class . $file_extension;
                include_once $module_directory . $class . $file_extension;
                $module = new $class;
                if( method_exists( $module, 'update' ) )  {
                    // Initialize the Update metnod with the configuration data being passed
                    $module->update( $_POST['configuration'] );
                }
            }
// End MZMT


            $params = [
                'module_code' => $_GET['module'],
                'order' => $sort_order
            ];

            get_modules_page_panel_html($params);

            print json_encode(array(
                'updated_cols' => array(
                    'sort_order' => $sort_order,
                ),
                'modal' => array(
                    'hide',
                ),
            ));
            exit;

            break;

        case 'installconfirm':

            $file_extension_new = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
            $class_new = basename($_GET['module']);

            if (file_exists($module_directory . $class_new . $file_extension_new)) {

                include (DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $class_new . $file_extension_new);
                include($module_directory . $class_new . $file_extension_new);
                $new_const_array = array_map(function($key){return "'$key'";},call_user_func($class_new.'::keys'));
                $new_const_list = implode(',',$new_const_array);
                tep_db_query("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key in ({$new_const_list})");
//          $module_new = new $class_new;
                call_user_func($class_new.'::install');
                $new_const_query = tep_db_query("SELECT configuration_value as cv, configuration_key as ck FROM " . TABLE_CONFIGURATION . " WHERE configuration_key in ($new_const_list)");
                while ($const = tep_db_fetch_array($new_const_query)){
                    if (!defined($const['ck'])) define($const['ck'],$const['cv']);
                }
                $installed_mod = explode(';', tep_db_fetch_array(tep_db_query("SELECT configuration_value as cv FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_".strtoupper($module_type)."_INSTALLED'"))['cv']);
                if(!in_array($class_new.$file_extension_new, $installed_mod)) $installed_mod[] = $class_new . $file_extension_new;
                $new_installed_mod = implode(';', $installed_mod);
                tep_db_query("UPDATE ".TABLE_CONFIGURATION." SET configuration_value = '".$new_installed_mod."' WHERE configuration_key = 'MODULE_".strtoupper($module_type)."_INSTALLED'");

            }

            print json_encode(array(
                'updated_panel' => get_modules_page_panel_html(),
                'modal' => array(
                    'hide',
                ),
            ));

            exit;
            break;

        case 'install':

            $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
            $class = basename($_GET['module']);
            if (file_exists($module_directory . $class . $file_extension)) {

                $definedConst = get_defined_constants(true)['user']?:[];
                include (DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $class . $file_extension);
                $newDefinedConst = get_defined_constants(true)['user'];
                $diff = array_diff_key($newDefinedConst,$definedConst);
                $module_title = reset($diff);

                include($module_directory . $class . $file_extension);
            }

            ?>

            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="<?php echo TEXT_CLOSE_BUTTON; ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ajaxModalLabel"><?php print !empty($module_title)?$module_title:HEADING_TITLE; ?></h4>
                </div>

                <div class="modal-body">
                    <form action="<?php print tep_href_link(FILENAME_MODULES, tep_get_all_get_params(array('action')) . 'action=installconfirm', 'NONSSL'); ?>" method="post">
                        <p class="text-center m-b-none"><?php print TEXT_INSTALL_INTRO; ?></p>
                    </form>
                </div>

                <div class="modal-footer">
                    <button class="ajax btn btn-success"><?php print TEXT_MODAL_INSTALL_ACTION ?></button>
                    <button class="btn btn-default" data-dismiss="modal"><?php print TEXT_MODAL_CANCEL_ACTION; ?></button>
                </div>
            </div>

            <?php

            exit;
            break;

        case 'confirm':
            $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
            $class = basename($_GET['module']);
            if (file_exists($module_directory . $class . $file_extension)) {
                include (DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $class . $file_extension);
                include($module_directory . $class . $file_extension);
                $module = new $class;
            }

            ?>
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="<?php echo TEXT_CLOSE_BUTTON; ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ajaxModalLabel"><?php print !empty($module->title)?$module->title:HEADING_TITLE; ?></h4>
                </div>

                <div class="modal-body">
                    <form action="<?php print tep_href_link(FILENAME_MODULES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm', 'NONSSL'); ?>" method="post">
                        <input type="text" hidden value="<?php print $class; ?>" name="module">
                        <p class="text-center m-b-none"><?php print TEXT_DELETE_INTRO; ?></p>
                    </form>
                </div>

                <div class="modal-footer">
                    <button class="ajax btn btn-danger"><?php print TEXT_MODAL_DELETE_ACTION; ?></button>
                    <button class="btn btn-default" data-dismiss="modal"><?php print TEXT_MODAL_CANCEL_ACTION; ?></button>
                </div>
            </div>

            <?php
            exit;
            break;

        case 'deleteconfirm':

            $file_extension_remove = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
            $class_remove = basename($_GET['module']);
            if (file_exists($module_directory . $class_remove . $file_extension_remove)) {
                include (DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $class_remove . $file_extension_remove);

                include($module_directory . $class_remove . $file_extension_remove);
                $module_remove = new $class_remove;
                $module_remove->remove();
            }

            print json_encode(array(
                'updated_panel' => get_modules_page_panel_html(),
                'modal' => array(
                    'hide',
                ),
            ));

            exit;
            break;
    }
}
?>

<?php

/**
 * header
 */

include_once('html-open.php');
include_once('header.php');

?>

<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
    <div class="modal-dialog modal-lg" role="document">
    </div>
</div>

<!-- content -->
<div class="container app-content-body p-b-none">
    <div class="hbox hbox-auto-xs hbox-auto-sm">
        <!-- main -->
        <div class="col">

            <div class="wrapper-md wrapper_767">
                <div class="bg-light lter ng-scope">
                    <h1 class="m-n font-thin h3"><?php echo HEADING_TITLE; ?></h1>
                </div>
            </div>

            <div class="wrapper-md wrapper_767">
                <?php print get_modules_page_panel_html(); ?>
            </div>

        </div>
    </div>
</div>

<!-- /content -->

<?php

/**
 * footer
 */

include_once('footer.php');
include_once('html-close.php');

?>

<?php

/**
 * Создает html панели страницы "Модули"
 * @return string - готовый html панели страницы "Модули"
 */
function get_modules_page_panel_html($params = []) {

    global $language;
    global $module_directory;
    global $PHP_SELF;
    global $module_type;
    global $module_key;
    global $set;
    global $admin_check;

    ob_start();

//    //require_once ('../'.DIR_WS_INCLUDES.'filenames.php');
//   debug(get_included_files());
    ?>

    <div class="panel panel-default">
        <!--    <div class="table-responsive">-->
        <table class="table table-bordered table-hover table-condensed bg-white-only b-t b-light <?php print $_GET['set'] == 'payment'?'payment':'shipping'?>_modules">
            <thead>
            <tr>
                <th class="v-middle"><?php echo TABLE_HEADING_MODULES; ?></th>
                <?php if ($_GET['set'] == 'payment') { ?>
                    <th class="text-center v-middle payment_module_description"><?php echo TABLE_HEADING_MODULE_DESCRIPTION; ?></th>
                <?php } ?>
                <th class="text-center v-middle"><?php echo TABLE_HEADING_SORT_ORDER; ?></th>
                <th class="text-center v-middle"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
            </tr>
            </thead>
            <tbody>

            <?php

            $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
            $directory_array = array();
            if ($dir = @dir($module_directory)) {
                while ($file = $dir->read()) {
                    if (!is_dir($module_directory . $file)) {
                        if (substr($file, strrpos($file, '.')) == $file_extension) {
                            $directory_array[] = $file;
                        }
                    }
                }

                sort($directory_array);
                $dir->close();
            }

            $installed_modules = array();

            $modules_check_query = tep_db_query("SELECT  GROUP_CONCAT(configuration_value SEPARATOR ';') AS modules
                                                 FROM `configuration`
                                                 WHERE configuration_key LIKE 'MODULE_%_INSTALLED'");

            $inst_modules = explode(';', tep_db_fetch_array($modules_check_query)['modules']);
            for ($i = 0, $n = sizeof($directory_array); $i < $n; $i++) {


                $file = $directory_array[$i];
                $class = substr($file, 0, strrpos($file, '.'));

                if(in_array($file, $inst_modules)) {
                    include(DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $file);

                    include_once($module_directory . $file);


                    if (tep_class_exists($class)) {

                        $admin_check = true; // for some modules like customshipper.php
                        $module = new $class;


                        //  if ($module->check() > 0 ) {

                        if ($params['module_code'] == $module->code) {
                            $installed_modules[$i]['file'] = $file;
                            $installed_modules[$i]['order'] = $params['order'];
                        } else {
                            $installed_modules[$i]['file'] = $file;
                            $installed_modules[$i]['order'] = $module->sort_order;
                        }
                        //  }
                        if (($module->enabled == 'True' or $module->enabled == 'true' or $module->enabled == true or $module->enabled == 1) and $module->check() > 0) {
                            $installed_modules[$i]['enabled'] = 1;
                        } else {
                            $installed_modules[$i]['enabled'] = 0;
                        }

                        $installed_modules[$i]['title'] = $module->title;
                        $installed_modules[$i]['description'] = $module->description;
                        $installed_modules[$i]['sort_order'] = $module->sort_order;
                        $installed_modules[$i]['check'] = $module->check();
                        $installed_modules[$i]['code'] = $module->code;
                    }
                }
                else
                {
                    $definedConst = get_defined_constants(true)['user']?:[];
                    include(DIR_FS_CATALOG_LANGUAGES . $language . '/modules/' . $module_type . '/' . $file);
                    $newDefinedConst = get_defined_constants(true)['user'];
                    $diff_keys = array_diff_key($newDefinedConst, $definedConst);
                    $installed_modules[$i]['title'] = reset($diff_keys);
                    $installed_modules[$i]['enabled'] = 0;
                    $installed_modules[$i]['description'] = "";
                    $installed_modules[$i]['file'] = $file;
                    $installed_modules[$i]['sort_order'] = 0;
                    $installed_modules[$i]['order'] = 0;
                    $installed_modules[$i]['check'] = 0;
                    $installed_modules[$i]['code'] = $class;
                }
            }
            //debug($installed_modules);

            // sort by string:
            /*  usort($installed_modules, function ($a, $b) {
              return strcmp($a["order"], $b["order"]);
            });  */

            // sort by numbers (order):
            /*usort($installed_modules,function($a,$b){
                return $a["order"] > $b["order"];
            }); */

            // sort by two columns: "enabled" and "order"
            array_multisort(array_column($installed_modules, 'enabled'),  SORT_DESC,
                array_column($installed_modules, 'order'), SORT_ASC,
                $installed_modules);

            foreach($installed_modules as $module) {

                if($module['enabled']) $installed_enabled_modules[] = $module['file'];

                ?>
                <tr <?php echo ($module['enabled']==1)?'':'style="opacity:0.5;"'; ?>>
                    <td data-label="<?php echo TABLE_HEADING_MODULES; ?>:" class="col-name-title_<?php print $_GET['set'] == 'payment'?'payment':'shipping'?>"><?php echo $module['title']; ?></td>
                    <?php if ($_GET['set'] == 'payment') { ?>
                        <td data-label="<?php echo TABLE_HEADING_MODULE_DESCRIPTION; ?>:" class="value_payment_module_description"><?php print $module['description']; ?></td>
                    <?php } ?>
                    <td data-label="<?php echo TABLE_HEADING_SORT_ORDER; ?>:" class="text-center v-middle col-name-sort_order">
                        <?php
                        if (is_numeric($module['sort_order'])) {
                            echo $module['sort_order'];
                        }
                        ?>
                    </td>
                    <td data-label="<?php echo TABLE_HEADING_ACTION; ?>:" class="text-center v-middle">

                        <?php
                        if ($module['check'] == '1') {
                            ?>

                            <a class="ajax-modal btn-link btn-link-icon" href="<?php print tep_href_link(FILENAME_MODULES, 'set=' . $set .'&module='. $module['code'] . '&action=edit'); ?>" data-toggle="tooltip" data-placement="right" title="<?php print IMAGE_EDIT; ?>" data-original-title="<?php print IMAGE_EDIT; ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <?php
                            /*
                             * actionDelete є тимчасовий клас
                             */
                            ?>
                            <a class="ajax-modal m-l-sm btn-link btn-link-icon" href="<?php print tep_href_link(FILENAME_MODULES, 'set=' . $set . '&module=' . $module['code'] . '&action=confirm'); ?>" data-toggle="tooltip" data-placement="right" title="<?php print IMAGE_MODULE_REMOVE; ?>" data-original-title="<?php print IMAGE_MODULE_REMOVE; ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>

                            <?php
                        } else {
                            ?>
                            <?php
                            /*
                             * actionInstall є тимчасовий клас
                             */
                            ?>
                            <a class="ajax-modal btn-link btn-link-icon" href="<?php print tep_href_link(FILENAME_MODULES, 'set=' . $set . '&module=' . $module['code'] . '&action=install'); ?>" data-toggle="tooltip" data-placement="right" title="<?php print IMAGE_MODULE_INSTALL; ?>" data-original-title="<?php print IMAGE_MODULE_INSTALL; ?>">
                                <i class="fa fa-plus text-base"></i>
                            </a>

                            <?php
                        }
                        ?>
                    </td>
                </tr>

            <?php }

            //            $installed_modules = array_column($installed_modules, 'file');
            $installed_modules = $installed_enabled_modules?:[];
                $check_query = tep_db_query("SELECT `configuration_value` FROM " . TABLE_CONFIGURATION . " WHERE `configuration_key` = '" . $module_key . "'");
                if (tep_db_num_rows($check_query)) {
                    $check = tep_db_fetch_array($check_query);

                    if ($check['configuration_value'] != implode(';', $installed_modules)) {

                        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . implode(';', $installed_modules) . "', last_modified = now() where configuration_key = '" . $module_key . "'");
                    }
                } else {
                    tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Installed Modules', '" . $module_key . "', '" . implode(';', $installed_modules) . "', 'This is automatically updated. No need to edit.', '6', '0', now())");
                }

            ?>

            </tbody>
        </table>
        <!--    </div>-->
        <!--
    <footer class="panel-footer">
      <div class="row m-b">
        <div class="col-sm-12 text-xs">
          <?php echo TEXT_MODULE_DIRECTORY . ' ' . $module_directory; ?>
        </div>
    </footer>  -->
    </div>

    <?php

    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}
?>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
