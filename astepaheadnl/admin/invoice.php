<?php
/*
  $Id: invoice.php,v 1.2 2003/09/24 15:18:15 wilt Exp $
  
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');

$oID = $_GET['oID'];
$orderId = tep_db_input(tep_db_prepare_input($oID));
$customerNumberSql = 'SELECT o.customers_id,
                             i.invoice_number
                     FROM ' . TABLE_ORDERS . ' o
                     LEFT JOIN invoices i on i.order_id = o.orders_id
                     WHERE o.orders_id = ' . $orderId;
$customer_number_query = tep_db_query($customerNumberSql);
$customer_number = tep_db_fetch_array($customer_number_query);
/*
  if ($customer_number['customers_id'] != $customer_id) {
    tep_redirect(tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'));
  }
*/
$payment_info_query = tep_db_query("select payment_info from " . TABLE_ORDERS . " where orders_id = '" . tep_db_input(tep_db_prepare_input($oID)) . "'");
$payment_info = tep_db_fetch_array($payment_info_query);
$payment_info = $payment_info['payment_info'];

//  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_INVOICE);

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$oID = tep_db_prepare_input($_GET['oID']);
$orders_query = "select orders_id from " . TABLE_ORDERS . " where orders_id = '" . tep_db_input($oID) . "'";

include(DIR_WS_CLASSES . 'order.php');
$order = new order($oID);

if ($_GET['pdf'] == 'true') {
    require_once '../' . DIR_WS_INCLUDES . 'mpdf/examples/vendor/autoload.php';
    define('_MPDF_TTFONTPATH', '../' . DIR_WS_INCLUDES . 'mpdf/examples/vendor/mpdf/mpdf/ttfonts');
    $mpdf = new \Mpdf\Mpdf([
        'margin_left' => 15,
        'margin_right' => 15,
        'margin_bottom' => 55,
        'margin_footer' => 10
    ]);

    ob_start();
    require_once "invoice.tpl.php";
    $output = ob_get_contents();
    ob_end_clean();
    $mpdf->WriteHTML($output);


    $mpdf->Output();
    require(DIR_WS_INCLUDES . 'application_bottom.php');

    exit();
}

?>
<style>
    body {
        font-family: Arial;
    }

    table {
        width: 90%;
        margin: 20px auto !important;
        border-collapse: collapse;
        font-size: 14px;
    }

    .logo {
        width: 200px;
    }

    .logo_text {
        font-size: 30px;
        background-color: #1b302f;
        color: white;
        padding: 5px 10px;
        font-weight: 100;
        width: 150px;
        text-align: center;
    }

    .products th {
        background-color: #333;
        color: #fff;
        font-weight: normal;
        padding: 15px;
    }

    .products td {
        padding: 15px;
        text-align: center;
    }

    .products tr {
        border: 1px solid #f5f5f5;
    }

    .footer th {
        background-color: #0090b5;
        padding: 23px;
    }

    .footer {
        margin-bottom: 20px;
    }
</style>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE_PRINT_ORDER . $oID; ?></title>
    <base href="<?php echo HTTP_SERVER . DIR_WS_CATALOG; ?>">
    <link rel="stylesheet" type="text/css" href="admin/includes/print.css">
</head>

<body marginwidth="10" marginheight="10" topmargin="10" bottommargin="10" leftmargin="10" rightmargin="10">

<table>
    <tbody>
    <tr>
        <td>
            <div class="logo"><img src="<?= HTTP_SERVER . "/" . LOGO_IMAGE ?>"></div>
        </td>
        <td align="right">
            <div class="logo_text"><?= DOCUMENT_TITLE; ?></div>
        </td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 145px;"><?= ADDRESS_TITLE ?></td>
        <td>:</td>
        <td style="text-align: left"><?= $order->customer["company"] . ' ' . $order->customer["name"] ?><br>
            <?= $order->customer["street_address"] . ', ' . $order->customer["postcode"] ?><br>
            <?= $order->customer["country"] ?></td>
    </tr>
    <tr>
        <td style="width: 145px;"><?= TEL_TITLE ?></td>
        <td>:</td>
        <td><?= $order->customer["telephone"]; ?></td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 115px;">* <?= INVOICE_NUMBER_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;">W<?= $customer_number["invoice_number"] ?></td>

        <td style="width: 115px;"><?= INVOICE_DATE_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= date('d-m-y', strtotime($order->info['date_purchased'])) ?></td>

        <td style="width: 115px;"><?= INVOICE_VAT_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= INVOICE_VAT ?></td>

        <td style="width: 100px;"></td>
    </tr>
    <tr>
        <td style="width: 115px;">* <?= INVOICE_DEBITEUR_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= $customer_number["customers_id"] ?></td>

        <td style="width: 115px;"><?= INVOICE_EXPIRATION_DATE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= date('d-m-y', strtotime($order->info['date_purchased']) + 1209600) // + 14 days          ?> </td>

        <td style="width: 115px;"><?= INVOICE_SHEET_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= INVOICE_SHEET_NUMBER ?></td>

        <td style="width: 100px;"></td>
    </tr>
    </tbody>
</table>

<table class="products">
    <thead>
    <th><?php echo ENTRY_QTY; ?></th>
    <th><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></th>
    <th><?php echo TABLE_HEADING_PRODUCTS; ?></th>
    <th><?php echo TABLE_HEADING_REF; ?></th>
    <th><?php echo TABLE_HEADING_INVOICE_PRODUCT_PRICE; ?></th>
    <th><?php echo TABLE_HEADING_TOTAL_EXCLUDING_TAX; ?></th>
    </thead>
    <tbody>
    <?php for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) { ?>
        <tr>
            <td><?php echo $order->products[$i]['qty'] ?></td>
            <td><?php echo $order->products[$i]['model'] ?></td>
            <td><?php echo $order->products[$i]['name'] ?></td>
            <td></td>
            <td><?php echo $currencies->format($order->products[$i]['final_price'], true, $order->info['currency'], $order->info['currency_value']) ?></td>
            <td><?php echo $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) ?></td>
        </tr>
    <?php } ?>
    <tr></tr>
    <tr>
        <td colspan="6"><?= SECOND_PART_TITLE ?></td>
    </tr>
    <?php
    $lng = tep_get_languages();
    foreach ($lng as $lngItem) {
        includeLanguages(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $lngItem['directory'] . '/modules/order_total/ot_subtotal.json');
        includeLanguages(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $lngItem['directory'] . '/modules/order_total/ot_total.json');
    }
    $excludeTotals = [
        MODULE_ORDER_TOTAL_SUBTOTAL_TITLE,
        MODULE_ORDER_TOTAL_TOTAL_TITLE,
        'B.T.W. 21.0%',
        'Total Excl. VAT:',
        'Total incl. VAT',

    ];
    for ($k = 0, $n = sizeof($order->totals); $k < $n; $k++) {
        $check = true;
        foreach ($excludeTotals as $exclude) {
            if (stripos($order->totals[$k]['title'], $exclude) !== false) {
                $check = false;
                break;
            }
        }
        switch ($order->totals[$k]['class']) {
            case 'ot_tax':
                $VatTax = str_replace('.', '', $order->totals[$k]['text']);
                $VatTax = (float)str_replace(['€', ','], ['', '.'], $VatTax);
                break;

            case 'ot_total':
                $totals = str_replace('.', '', $order->totals[$k]['text']);
                $totals = (float)str_replace(['<b>€', ','], ['', '.'], $totals);
                break;
        }
        if ($check) {
            $i++;
            ?>
            <tr>
                <td></td>
                <td></td>
                <td><?php echo $order->totals[$k]['title']; ?></td>
                <td></td>
                <td></td>
                <td><?php echo $order->totals[$k]['text']; ?></td>
            </tr>
            <?php
        }
    }
    ?>
    </tbody>
</table>

<table class="products">
    <thead>
    <th><?php echo CREDIT_LIMIT; ?></th>
    <th><?php echo OTHER; ?></th>
    <th><?php echo SUBTOTAL; ?></th>
    <th><?php echo VAT_SUBTOTAL; ?></th>
    <th><?php echo INVOICE_TOTAL; ?></th>
    </thead>
    <tbody>
    <tr>
        <td><?php echo $currencies->format(0, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo $currencies->format(0, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo $currencies->format($totals - $VatTax, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo $currencies->format($VatTax, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo '<b>' . $currencies->format($totals, true, $order->info['currency'], $order->info['currency_value']) . '</b>'; ?></td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td><?= PAYMENT_CONDITION_TITLE ?> : <?= $order->info["payment_method"] ?></td>
    </tr>
    <tr>
        <td><?= PAYMENT_TERM_TITLE ?> : <?= PAYMENT_TERM ?></td>
    </tr>
    <tr>
        <td><?= CREDIT_LIMITATION_TITLE ?> : <?= CREDIT_LIMITATION ?></td>
    </tr>
    <tr>
        <td><?= COMPLAINTS ?></td>
    </tr>
    <tr>
        <td style="text-align: right"><?= INVOICE_CREATED ?></td>
    </tr>
    </tbody>
</table>

<table class="footer">
    <thead>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
    <tr>
        <td><?= FOOTER_BLOCK1 ?></td>
        <td><?= FOOTER_BLOCK2 ?></td>
        <td><?= FOOTER_PHONE ?><br/>
            <?= FOOTER_EMAIL ?><br/>
            <?= FOOTER_SITE ?></td>
        <td><?= FOOTER_CC ?><br/>
            <?= FOOTER_IBAN ?><br/>
            <?= FOOTER_BIC ?><br/>
            <?= FOOTER_VAT ?><br/></td>
    </tr>
    </tbody>
</table>

<br />
<br />
<br />

</body>

</html>
