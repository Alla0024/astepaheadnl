<?php

require('includes/application_top.php');

if (file_exists('../ext/yml_import/yml_import.php') && defined('YML_MODULE_ENABLED') && constant('YML_MODULE_ENABLED') == 'true'){
    require ('../ext/yml_import/yml_import.php');
}else{
    $location = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($location);
    header('Location: '.implode('/',$location));
}