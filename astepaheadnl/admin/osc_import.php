<?php
/*
  Page for module OSC Import
*/

require('includes/application_top.php');

include_once('html-open.php');
include_once('header.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE; ?></title>
    <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
    <link rel="stylesheet" href="includes/solomono/css/overwrite.css" type="text/css"/>
<!--    <script language="javascript" src="includes/menu.js"></script>-->
    <script language="javascript" src="includes/general.js"></script>

<!--    <script type="text/javascript" src="../includes/javascript/lib/jquery-3.3.1.min.js"></script>-->
<!--    <script type="text/javascript" src="includes/javascript/jquery-ui-1.9.2.custom.min.js"></script>-->
<!--    <link rel="stylesheet" href="../includes/javascript/jqueryui/css/smoothness/jquery-ui-1.10.4.custom.min.css">-->
<!--    <script type="text/javascript" src="includes/autocomplete/ac.js"></script>-->
<!--    <link type="text/css" rel="stylesheet" href="includes/autocomplete/ac.css" />-->
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>solomono/css/new_importexport.css?t=<?=filesize(__DIR__ . DIRECTORY_SEPARATOR . DIR_WS_INCLUDES.'solomono/css/new_importexport.css')?>" type="text/css"/>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" >
<!-- header //-->
<!-- header_smend //-->
<!-- body //-->
<h1 class="importexport-title">OSC Import</h1>
<?php
if (defined('OSC_IMPORT_MODULE_ENABLED') && constant('OSC_IMPORT_MODULE_ENABLED') == 'true') : ?>
    <div class="importexport">
        <?php require_once("../ext/osc_import/osc_import.php"); ?>
    </div>
<?php endif; ?>

<!-- body_smend //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_smend //-->
</body>
</html>
<?php
include_once('footer.php');
include_once('html-close.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
