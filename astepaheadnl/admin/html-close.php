</div><!-- / app -->
<?php
if (getenv('APP_ENV') == 'production' or getenv('APP_ENV') == 'demo') {
//    if (getenv('APP_ENV') == 'production') $buy_tpl_link = 'https://solomono.net/oplata-a-121.html?from=' . $_SERVER['SERVER_NAME']; elseif (getenv('APP_ENV') == 'demo') $buy_tpl_link = 'https://solomono.net';
    if (getenv('APP_ENV') == 'production') $buy_tpl_link = 'https://solomono.net/?from=' . $_SERVER['SERVER_NAME'] . '#select_packages';
    elseif (getenv('APP_ENV') == 'demo') $buy_tpl_link = 'https://solomono.net';
    ?><?php $to_date = get_created(14); ?>
    <div class="buy_template" style="position: fixed;bottom: 0; width: 100%;background: rgba(0,0,0,0.6);z-index: 990;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="row">
                        <div class="col-xs-1 col-md-5"></div>
                        <?php if ($to_date): ?>
                            <div class="col-xs-6 col-md-7">
                                <div id="clock" style="font-size: 23px; color: white; padding-top: 20px;"><?php echo TIME_LEFT;?>
                                    <span date-to="<?php echo $to_date;?>" data-lan="<?php echo $language;?>"></span>
                                </div>
                            </div>
                            <script src="/includes/javascript/jquery.countdown/jquery.countdown.min.js"></script>
                            <script src="/includes/javascript/jquery.countdown/initialization.js"></script>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 hidden-xs">
                    <a style="display: block;font-size: 16px;line-height: 24px;padding: 8px 30px;margin: 16px;text-align: center; background: #f8e342;color:black!important;" target="_blank" href="<?php echo $buy_tpl_link; ?>" class="btn-primary buy_template_link"><?php echo IMAGE_BUTTON_BUY_TEMPLATE; ?></a>
                </div>
                <div class="visible-xs">
                    <a style="display: block;background: #007bfe;position: fixed;bottom: 150px;right: 15px;width: 65px;height: 65px;border-radius: 65px!important;text-align: center;box-shadow: 0 7px 22px 0 rgba(34,36,43,.3);" target="_blank" href="<?php echo $buy_tpl_link; ?>" class="btn-primary buy_template_link">
                        <svg style="width: 26px;margin-top: 10px;" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false"  role="img" viewBox="0 0 512 512">
                            <path fill="#fff" d="M256 416c114.9 0 208-93.1 208-208S370.9 0 256 0 48 93.1 48 208s93.1 208 208 208zM233.8 97.4V80.6c0-9.2 7.4-16.6 16.6-16.6h11.1c9.2 0 16.6 7.4 16.6 16.6v17c15.5.8 30.5 6.1 43 15.4 5.6 4.1 6.2 12.3 1.2 17.1L306 145.6c-3.8 3.7-9.5 3.8-14 1-5.4-3.4-11.4-5.1-17.8-5.1h-38.9c-9 0-16.3 8.2-16.3 18.3 0 8.2 5 15.5 12.1 17.6l62.3 18.7c25.7 7.7 43.7 32.4 43.7 60.1 0 34-26.4 61.5-59.1 62.4v16.8c0 9.2-7.4 16.6-16.6 16.6h-11.1c-9.2 0-16.6-7.4-16.6-16.6v-17c-15.5-.8-30.5-6.1-43-15.4-5.6-4.1-6.2-12.3-1.2-17.1l16.3-15.5c3.8-3.7 9.5-3.8 14-1 5.4 3.4 11.4 5.1 17.8 5.1h38.9c9 0 16.3-8.2 16.3-18.3 0-8.2-5-15.5-12.1-17.6l-62.3-18.7c-25.7-7.7-43.7-32.4-43.7-60.1.1-34 26.4-61.5 59.1-62.4zM480 352h-32.5c-19.6 26-44.6 47.7-73 64h63.8c5.3 0 9.6 3.6 9.6 8v16c0 4.4-4.3 8-9.6 8H73.6c-5.3 0-9.6-3.6-9.6-8v-16c0-4.4 4.3-8 9.6-8h63.8c-28.4-16.3-53.3-38-73-64H32c-17.7 0-32 14.3-32 32v96c0 17.7 14.3 32 32 32h448c17.7 0 32-14.3 32-32v-96c0-17.7-14.3-32-32-32z"/>
                        </svg>
                        <?php echo IMAGE_BUTTON_BUY_TEMPLATE_MOB;?>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</body>
</html>
