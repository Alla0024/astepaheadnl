<?php 
define('HEADING_TITLE_MODULES_PAYMENT','I Moduli Di Pagamento');
define('HEADING_TITLE_MODULES_SHIPPING','I Moduli Di Spedizione');
define('HEADING_TITLE_MODULES_ORDER_TOTAL','I Moduli Di Ordini');
define('TEXT_INSTALL_INTRO','Davvero si desidera installare questo modulo?');
define('TEXT_DELETE_INTRO','Vuoi davvero cancellare questo modulo?');
define('TABLE_HEADING_MODULES','I moduli');
define('TABLE_HEADING_MODULE_DESCRIPTION','Descrizione');
define('TABLE_HEADING_SORT_ORDER','L\'Ordinamento');
define('TABLE_HEADING_ACTION','L\'azione');
define('TEXT_MODULE_DIRECTORY','Repertorio dei Moduli:');
define('TEXT_CLOSE_BUTTON','Chiudere');
define('MODULE_PAYMENT_CC_STATUS_TITLE','Consentire il modulo di pagamento carta di credito');
define('MODULE_PAYMENT_CC_STATUS_DESC','Si desidera ricevere i pagamenti tramite carta di credito?');
define('MODULE_PAYMENT_CC_EMAIL_TITLE','Indirizzo E-Mail');
define('MODULE_PAYMENT_CC_EMAIL_DESC','Se viene specificato un indirizzo e-mail, poi all\'indirizzo e-mail verranno inviate le medie dei numeri di carta di credito (nel database verrà memorizzato il numero completo della carta di credito, ad eccezione dei dati medi cifre)');
define('MODULE_PAYMENT_CC_ZONE_TITLE','Zona');
define('MODULE_PAYMENT_CC_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di pagamento sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_PAYMENT_CC_ORDER_STATUS_ID_TITLE','Lo stato dell\'ordine');
define('MODULE_PAYMENT_CC_ORDER_STATUS_ID_DESC','Gli ordini effettuati con l\'uso di questo modulo di pagamento prenderanno stato specificato.');
define('MODULE_PAYMENT_CC_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_PAYMENT_CC_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_PAYMENT_COD_STATUS_TITLE','Consentire il modulo di pagamento in contanti alla consegna');
define('MODULE_PAYMENT_COD_STATUS_DESC','Si desidera consentire l\'utilizzo del modulo in caso di ordinazione?');
define('MODULE_PAYMENT_COD_ZONE_TITLE','Zona');
define('MODULE_PAYMENT_COD_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di pagamento sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_TITLE','Lo stato dell\'ordine');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_DESC','Gli ordini effettuati con l\'uso di questo modulo di pagamento prenderanno stato specificato.');
define('MODULE_PAYMENT_COD_SORT_ORDER_TITLE','Il tipo di ordinamento.');
define('MODULE_PAYMENT_COD_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_PAYMENT_FREECHARGER_STATUS_TITLE','Consentire il modulo download Gratuito');
define('MODULE_PAYMENT_FREECHARGER_STATUS_DESC','Si desidera consentire il modulo di download gratuito?');
define('MODULE_PAYMENT_FREECHARGER_ZONE_TITLE','Zona');
define('MODULE_PAYMENT_FREECHARGER_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di pagamento sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_PAYMENT_FREECHARGER_ORDER_STATUS_ID_TITLE','Lo stato dell\'ordine');
define('MODULE_PAYMENT_FREECHARGER_ORDER_STATUS_ID_DESC','Gli ordini effettuati con l\'uso di questo modulo di pagamento prenderanno stato specificato.');
define('MODULE_PAYMENT_FREECHARGER_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_PAYMENT_FREECHARGER_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_PAYMENT_LIQPAY_STATUS_TITLE','Consentire il modulo di pagamento LiqPAY');
define('MODULE_PAYMENT_LIQPAY_STATUS_DESC','Consentire il modulo di pagamento LiqPAY?');
define('MODULE_PAYMENT_LIQPAY_ID_TITLE','Merchant ID');
define('MODULE_PAYMENT_LIQPAY_ID_DESC','Inserisci il tuo идентификационныый camera (merchant id).');
define('MODULE_PAYMENT_LIQPAY_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_PAYMENT_LIQPAY_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_PAYMENT_LIQPAY_ZONE_TITLE','L\'area di pagamento');
define('MODULE_PAYMENT_LIQPAY_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di pagamento è disponibile solo per gli acquirenti di una determinata zona.');
define('MODULE_PAYMENT_LIQPAY_SECRET_KEY_TITLE','Merchant password (firma)');
define('MODULE_PAYMENT_LIQPAY_SECRET_KEY_DESC','In questa opzione consente di specificare la password (firma) specificato nelle impostazioni del sito LiqPAY.');
define('MODULE_PAYMENT_LIQPAY_ORDER_STATUS_ID_TITLE','Specificare lo stato di pagamento dell\'ordine');
define('MODULE_PAYMENT_LIQPAY_ORDER_STATUS_ID_DESC','Specificare lo stato di pagamento dell\'ordine');
define('MODULE_PAYMENT_LIQPAY_DEFAULT_ORDER_STATUS_ID_TITLE','Indicare lo stato dell\' ordine predefinito');
define('MODULE_PAYMENT_LIQPAY_DEFAULT_ORDER_STATUS_ID_DESC','Indicare lo stato dell\' un ordine predefinito');
define('MODULE_PAYMENT_BANK_TRANSFER_STATUS_TITLE','Pagamento anticipato sul conto');
define('MODULE_PAYMENT_BANK_TRANSFER_STATUS_DESC','Si desidera utilizzare il modulo di pagamento anticipato sul conto? 1 - si, 0 - no');
define('MODULE_PAYMENT_BANK_TRANSFER_1_TITLE','Il nome della banca');
define('MODULE_PAYMENT_BANK_TRANSFER_1_DESC','Digitare il nome della banca');
define('MODULE_PAYMENT_BANK_TRANSFER_2_TITLE','Conto corrente');
define('MODULE_PAYMENT_BANK_TRANSFER_2_DESC','Inserisci il tuo conto corrente');
define('MODULE_PAYMENT_BANK_TRANSFER_3_TITLE','MFI');
define('MODULE_PAYMENT_BANK_TRANSFER_3_DESC','Inserire la MFI Banca');
define('MODULE_PAYMENT_BANK_TRANSFER_4_TITLE','Cor./conto');
define('MODULE_PAYMENT_BANK_TRANSFER_4_DESC','Inserisci Cor./conto della banca');
define('MODULE_PAYMENT_BANK_TRANSFER_5_TITLE','INN');
define('MODULE_PAYMENT_BANK_TRANSFER_5_DESC','Inserire la partita IVA della banca');
define('MODULE_PAYMENT_BANK_TRANSFER_6_TITLE','Il destinatario');
define('MODULE_PAYMENT_BANK_TRANSFER_6_DESC','Il destinatario del pagamento');
define('MODULE_PAYMENT_BANK_TRANSFER_7_TITLE','PPC');
define('MODULE_PAYMENT_BANK_TRANSFER_7_DESC','Inserisci PPC');
define('MODULE_PAYMENT_BANK_TRANSFER_8_TITLE','La nomina di pagamento');
define('MODULE_PAYMENT_BANK_TRANSFER_8_DESC','Specificare la destinazione di pagamento');
define('MODULE_PAYMENT_BANK_TRANSFER_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_PAYMENT_BANK_TRANSFER_SORT_ORDER_DESC','L\'ordinamento del modulo');
define('MODULE_PAYMENT_WEBMONEY_STATUS_TITLE','Pagamento tramite WebMoney');
define('MODULE_PAYMENT_WEBMONEY_STATUS_DESC','Si desidera utilizzare il modulo di Pagamento tramite WebMoney? 1 - si, 0 - no');
define('MODULE_PAYMENT_WEBMONEY_1_TITLE','Il WM Id');
define('MODULE_PAYMENT_WEBMONEY_1_DESC','Inserisci il Tuo WM id');
define('MODULE_PAYMENT_WEBMONEY_2_TITLE','Il Tuo numero di R portafoglio');
define('MODULE_PAYMENT_WEBMONEY_2_DESC','Inserisci il Tuo numero di R portafoglio');
define('MODULE_PAYMENT_WEBMONEY_3_TITLE','Il Tuo numero di Z portafoglio');
define('MODULE_PAYMENT_WEBMONEY_3_DESC','Inserisci il Tuo numero di Z portafoglio');
define('MODULE_PAYMENT_WEBMONEY_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_PAYMENT_WEBMONEY_SORT_ORDER_DESC','L\'ordinamento del modulo');
define('MODULE_SHIPPING_EXPRESS_STATUS_TITLE','Consentire il modulo di consegna del corriere');
define('MODULE_SHIPPING_EXPRESS_STATUS_DESC','Si desidera consentire il modulo di consegna del corriere?');
define('MODULE_SHIPPING_EXPRESS_COST_TITLE','Il costo');
define('MODULE_SHIPPING_EXPRESS_COST_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_EXPRESS_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_EXPRESS_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_EXPRESS_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_EXPRESS_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_EXPRESS_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_EXPRESS_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_FLAT_STATUS_TITLE','Consentire il modulo di consegna del corriere');
define('MODULE_SHIPPING_FLAT_STATUS_DESC','Si desidera consentire il modulo di consegna del corriere?');
define('MODULE_SHIPPING_FLAT_COST_TITLE','Il costo');
define('MODULE_SHIPPING_FLAT_COST_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_FLAT_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_FLAT_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_FREESHIPPER_STATUS_TITLE','Consentire il trasporto libero');
define('MODULE_SHIPPING_FREESHIPPER_STATUS_DESC','Si desidera consentire modulo spedizione gratuita?');
define('MODULE_SHIPPING_FREESHIPPER_COST_TITLE','Il costo');
define('MODULE_SHIPPING_FREESHIPPER_COST_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_FREESHIPPER_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_FREESHIPPER_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_FREESHIPPER_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_FREESHIPPER_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_FREESHIPPER_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_FREESHIPPER_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_ITEM_STATUS_TITLE','Consentire il modulo per unità di');
define('MODULE_SHIPPING_ITEM_STATUS_DESC','Si desidera consentire il modulo per unità?');
define('MODULE_SHIPPING_ITEM_COST_TITLE','Il costo di spedizione');
define('MODULE_SHIPPING_ITEM_COST_DESC','Il costo di spedizione sarà moltiplicato per il numero di unità di prodotto nell\'ordine.');
define('MODULE_SHIPPING_ITEM_HANDLING_TITLE','Il costo');
define('MODULE_SHIPPING_ITEM_HANDLING_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_ITEM_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_ITEM_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_NWPOCHTA_STATUS_TITLE','Consentire il modulo di Nuova Posta');
define('MODULE_SHIPPING_NWPOCHTA_STATUS_DESC','Si desidera consentire il modulo di Nuova Posta?');
define('MODULE_SHIPPING_NWPOCHTA_COST_TITLE','Il costo');
define('MODULE_SHIPPING_NWPOCHTA_CUSTOM_NAME_TITLE','Spec. Il nome');
define('MODULE_SHIPPING_NWPOCHTA_CUSTOM_NAME_DESC','Lasciare il campo vuoto se si desidera utilizzare il nome predefinito');
define('MODULE_SHIPPING_NWPOCHTA_COST_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_NWPOCHTA_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_NWPOCHTA_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_NWPOCHTA_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_NWPOCHTA_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_NWPOCHTA_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_NWPOCHTA_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_CUSTOMSHIPPER_STATUS_TITLE','Consentire il modulo di consegna del corriere');
define('MODULE_SHIPPING_CUSTOMSHIPPER_NAME_TITLE','Il nome del vettore');
define('MODULE_SHIPPING_CUSTOMSHIPPER_WAY_TITLE','Descrizione del vettore');
define('MODULE_SHIPPING_CUSTOMSHIPPER_COST_TITLE','Il costo');
define('MODULE_SHIPPING_CUSTOMSHIPPER_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_CUSTOMSHIPPER_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_CUSTOMSHIPPER_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_CUSTOMSHIPPER_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_PERCENT_STATUS_TITLE','Consentire il modulo di interesse gratuita');
define('MODULE_SHIPPING_PERCENT_STATUS_DESC','Si desidera consentire il modulo di interesse di spedizione?');
define('MODULE_SHIPPING_PERCENT_RATE_TITLE','La percentuale di');
define('MODULE_SHIPPING_PERCENT_RATE_DESC','Il costo di consegna di questo modulo in percentuale del costo totale dell\'ordine, i valori da 01 a .99');
define('MODULE_SHIPPING_PERCENT_LESS_THEN_TITLE','Piano il valore per gli ordini fino a');
define('MODULE_SHIPPING_PERCENT_LESS_THEN_DESC','Piana costo di spedizione per gli ordini, per un valore al valore indicato.');
define('MODULE_SHIPPING_PERCENT_FLAT_USE_TITLE','Piana di tasso di interesse il valore');
define('MODULE_SHIPPING_PERCENT_FLAT_USE_DESC','Piana costo di spedizione in percentuale del costo totale della prenotazione, valido per tutti gli ordini.');
define('MODULE_SHIPPING_PERCENT_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_PERCENT_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_PERCENT_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_PERCENT_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_PERCENT_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_PERCENT_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_SAT_STATUS_TITLE','Consentire il modulo di consegna del corriere');
define('MODULE_SHIPPING_SAT_STATUS_DESC','Si desidera consentire il modulo di consegna del corriere?');
define('MODULE_SHIPPING_SAT_COST_TITLE','Il costo');
define('MODULE_SHIPPING_SAT_COST_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_SAT_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_SAT_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_SAT_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_SAT_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_SAT_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_SAT_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_TABLE_STATUS_TITLE','Consentire il modulo \"Senza spese di spedizione\"');
define('MODULE_SHIPPING_TABLE_STATUS_DESC','Si desidera consentire il modulo di consegna \"Senza spese di spedizione\"?');
define('MODULE_SHIPPING_TABLE_COST_TITLE','Il costo di spedizione');
define('MODULE_SHIPPING_TABLE_COST_DESC','Il costo di spedizione viene calcolato in base al peso totale dell\'ordine o totale dell\'ordine. Ad esempio: 25:8.50,50:5.50,ecc... Questo vuol dire che fino a 25 spedizione avrà un costo di 8.50, da 25 a 50 costerà 5.50, ecc');
define('MODULE_SHIPPING_TABLE_MODE_TITLE','Il metodo di calcolo');
define('MODULE_SHIPPING_TABLE_MODE_DESC','Il costo di calcolo delle spese di spedizione sulla base del peso totale dell\'ordine (weight) o in base al costo totale dell\'ordine (price).');
define('MODULE_SHIPPING_TABLE_HANDLING_TITLE','Il costo');
define('MODULE_SHIPPING_TABLE_HANDLING_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_SHIPPING_TABLE_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_TABLE_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_TABLE_ZONE_TITLE','Zona');
define('MODULE_SHIPPING_TABLE_ZONE_DESC','Se è stata selezionata l\'area di, questo modulo di consegna sarà visibile solo per gli acquirenti della zona selezionata.');
define('MODULE_SHIPPING_TABLE_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_TABLE_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_ZONES_STATUS_TITLE','Consentire il modulo tariffe per la zona');
define('MODULE_SHIPPING_ZONES_STATUS_DESC','Si desidera consentire il modulo tariffe per la zona?');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_TITLE','Tassa');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_SHIPPING_ZONES_COUNTRIES_1_TITLE','Paese 1 zona');
define('MODULE_SHIPPING_ZONES_COUNTRIES_1_DESC','Elenco dei paesi separati da una virgola per la zona 1.');
define('MODULE_SHIPPING_ZONES_COST_1_TITLE','Il costo di spedizione per 1 zona');
define('MODULE_SHIPPING_ZONES_COST_1_DESC','Il costo di spedizione attraverso la virgola per la zona 1 sulla base di massimizzare il valore dell\'ordine. Ad esempio: 3:8.50,7:10.50,... Questo significa che il costo di spedizione per ordini di peso inferiore a 3 kg. costerà 8.50 per gli acquirenti provenienti da paesi 1 della zona.');
define('MODULE_SHIPPING_ZONES_HANDLING_1_TITLE','Il costo per 1 zona');
define('MODULE_SHIPPING_ZONES_HANDLING_1_DESC','Il costo di utilizzo di questo metodo di spedizione.');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_STATUS_TITLE','Consentire il modulo di Accompagnamento di sconto');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_STATUS_DESC','Si desidera consentire il modulo di Accompagnamento di sconto?');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_SORT_ORDER_DESC','L\'ordinamento del modulo');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_INC_TAX_TITLE','Include Tax');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_INC_TAX_DESC','Utilizzare tassa');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_CALC_TAX_TITLE','Re-calculate Tax');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_CALC_TAX_DESC','Ricalcolare l\'imposta');
define('MODULE_ORDER_TOTAL_COUPON_STATUS_TITLE','Mostra tutto');
define('MODULE_ORDER_TOTAL_COUPON_STATUS_DESC','Vuoi mostrare il valore nominale cedola?');
define('MODULE_ORDER_TOTAL_COUPON_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_COUPON_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_TITLE','Prendere in considerazione la spedizione');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_DESC','Includere nel calcolo spese di spedizione.');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_TITLE','Considerare l\'imposta');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_DESC','Includere nel calcolo della tassa.');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_TITLE','Ricalcolare l\'imposta');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_DESC','Ricalcolare l\'imposta.');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_TITLE','Tassa');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_DESC','Utilizzare tasse per i tagliandi.');
define('MODULE_ORDER_TOTAL_GV_STATUS_TITLE','Mostra tutto');
define('MODULE_ORDER_TOTAL_GV_STATUS_DESC','Vuoi mostrare il valore nominale di un buono regalo?');
define('MODULE_ORDER_TOTAL_GV_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_GV_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_ORDER_TOTAL_GV_QUEUE_TITLE','L\'attivazione di certificati');
define('MODULE_ORDER_TOTAL_GV_QUEUE_DESC','Si desidera attivare manualmente acquistato i certificati di regalo?');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_TITLE','Prendere in considerazione la spedizione');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_DESC','Includere nel calcolo spese di spedizione.');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_TITLE','Considerare l\'imposta');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_DESC','Includere nel calcolo della tassa.');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_TITLE','Ricalcolare l\'imposta');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_DESC','Ricalcolare l\'imposta.');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_TITLE','Tassa');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_TITLE','Tassa di certificato');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_DESC','Aggiungere l\'imposta per gli acquisti di gift card.');
define('MODULE_ORDER_TOTAL_GV_ORDER_STATUS_ID_TITLE','Lo stato dell\'ordine');
define('MODULE_ORDER_TOTAL_GV_ORDER_STATUS_ID_DESC','Gli ordini effettuati con l\'utilizzo di gift card, valida per l\'intero importo, sarà stato specificato.');
define('MODULE_LEV_DISCOUNT_STATUS_TITLE','Mostra di sconto');
define('MODULE_LEV_DISCOUNT_STATUS_DESC','Consentire sconti?');
define('MODULE_LEV_DISCOUNT_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_LEV_DISCOUNT_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_LEV_DISCOUNT_TABLE_TITLE','La percentuale di sconto');
define('MODULE_LEV_DISCOUNT_TABLE_DESC','Impostare il prezzo di limiti e le percentuali di sconto, separati da una virgola.');
define('MODULE_LEV_DISCOUNT_INC_SHIPPING_TITLE','Prendere in considerazione la spedizione');
define('MODULE_LEV_DISCOUNT_INC_SHIPPING_DESC','Includere nel calcolo spese di spedizione.');
define('MODULE_LEV_DISCOUNT_INC_TAX_TITLE','Considerare l\'imposta');
define('MODULE_LEV_DISCOUNT_INC_TAX_DESC','Includere nel calcolo della tassa.');
define('MODULE_LEV_DISCOUNT_CALC_TAX_TITLE','Ricalcolare l\'imposta');
define('MODULE_LEV_DISCOUNT_CALC_TAX_DESC','Ricalcolare l\'imposta.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_TITLE','Mostra a basso costo ordine');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_DESC','Desideri pubblicare un basso costo di ordine?');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_TITLE','Consentire a basso costo ordine');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_DESC','Si desidera consentire il modulo a basso costo ordine?');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_TITLE','A basso costo per ordini fino a');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_DESC','Valore basso degli ordini per ordini fino a tale grandezza.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_TITLE','Supplemento');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_DESC','Supplemento');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_TITLE','Tenere conto di pagamento all\'ordine');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_DESC','Tenere conto pagamento alle seguenti ordini.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_TITLE','Tassa');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_DESC','Utilizzare la tassa di soggiorno.');
define('MODULE_PAYMENT_DISC_STATUS_TITLE','Consentire il modulo');
define('MODULE_PAYMENT_DISC_STATUS_DESC','Attivare il modulo?');
define('MODULE_PAYMENT_DISC_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_PAYMENT_DISC_SORT_ORDER_DESC','L\'ordinamento del modulo deve essere inferiore modulo di Tutto.');
define('MODULE_PAYMENT_DISC_PERCENTAGE_TITLE','Sconto');
define('MODULE_PAYMENT_DISC_PERCENTAGE_DESC','Importo minimo di ordine per ottenere lo sconto.');
define('MODULE_PAYMENT_DISC_MINIMUM_TITLE','Importo minimo di ordine');
define('MODULE_PAYMENT_DISC_MINIMUM_DESC','Importo minimo di ordine per ottenere lo sconto.');
define('MODULE_PAYMENT_DISC_TYPE_TITLE','Metodo di pagamento');
define('MODULE_PAYMENT_DISC_TYPE_DESC','Qui è necessario specificare il nome della classe del modulo di pagamento, classe moruo imparare nel file del modulo, ad esempio /includes/modules/payment/webmoney.php. Dall\'alto si vede class webmoney, quindi se vogliamo dare uno sconto in caso di pagamento tramite WebMoney, scriviamo webmoney.');
define('MODULE_PAYMENT_DISC_INC_SHIPPING_TITLE','Prendere in considerazione la spedizione');
define('MODULE_PAYMENT_DISC_INC_SHIPPING_DESC','Includere la spedizione nei calcoli');
define('MODULE_PAYMENT_DISC_INC_TAX_TITLE','Considerare l\'imposta');
define('MODULE_PAYMENT_DISC_INC_TAX_DESC','Includere una tassa calcoli.');
define('MODULE_PAYMENT_DISC_CALC_TAX_TITLE','Considerato tassa');
define('MODULE_PAYMENT_DISC_CALC_TAX_DESC','Tener conto fiscale nel calcolo dello sconto.');
define('MODULE_QTY_DISCOUNT_STATUS_TITLE','Mostra uno sconto del numero di');
define('MODULE_QTY_DISCOUNT_STATUS_DESC','Si desidera consentire sconti dal numero?');
define('MODULE_QTY_DISCOUNT_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_QTY_DISCOUNT_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_QTY_DISCOUNT_RATE_TYPE_TITLE','Tipo di sconto');
define('MODULE_QTY_DISCOUNT_RATE_TYPE_DESC','Selezionare il tipo di sconto - tasso (percentuale) o piatto (flat rate)');
define('MODULE_QTY_DISCOUNT_RATES_TITLE','Sconto');
define('MODULE_QTY_DISCOUNT_RATES_DESC','Lo sconto è considerato sulla base del numero totale ordinati unità di prodotto. Ad esempio: 10:5,20:10, ecc... vuol dire che ordinando 10 o più unità di prodotto, il cliente riceve uno sconto del 5% o $5; 20 o più unità - sconto del 10% o $10; a seconda del tipo di');
define('MODULE_QTY_DISCOUNT_INC_SHIPPING_TITLE','Prendere in considerazione la spedizione');
define('MODULE_QTY_DISCOUNT_INC_SHIPPING_DESC','Includere nel calcolo spese di spedizione.');
define('MODULE_QTY_DISCOUNT_INC_TAX_TITLE','Considerare l\'imposta');
define('MODULE_QTY_DISCOUNT_INC_TAX_DESC','Includere nel calcolo della tassa.');
define('MODULE_QTY_DISCOUNT_CALC_TAX_TITLE','Ricalcolare l\'imposta');
define('MODULE_QTY_DISCOUNT_CALC_TAX_DESC','Ricalcolare l\'imposta.');
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_TITLE','Visualizza il recapito');
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_DESC','Si desidera visualizzare il costo di spedizione?');
define('MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_TITLE','Consentire il trasporto libero');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_DESC','Si desidera consentire la spedizione gratuita?');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_TITLE','Spedizione gratuita per ordini superiori a');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_DESC','Per ordini di oltre tale valore, la spedizione è gratuita..');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_TITLE','Spedizione gratuita per ordini');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_DESC','Specificare per quali ordini sarà valida spedizione gratuita.');
define('MODULE_ORDER_TOTAL_SUBTOTAL_STATUS_TITLE','Mostra il valore della merce');
define('MODULE_ORDER_TOTAL_SUBTOTAL_STATUS_DESC','Si desidera visualizzare il prezzo del prodotto?');
define('MODULE_ORDER_TOTAL_SUBTOTAL_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_SUBTOTAL_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_ORDER_TOTAL_TAX_STATUS_TITLE','Mostra tassa');
define('MODULE_ORDER_TOTAL_TAX_STATUS_DESC','Si vuole mostrare la tassa di soggiorno?');
define('MODULE_ORDER_TOTAL_TAX_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_TAX_SORT_ORDER_DESC','L\'ordinamento del modulo.');
define('MODULE_ORDER_TOTAL_TOTAL_STATUS_TITLE','Mostra tutto');
define('MODULE_ORDER_TOTAL_TOTAL_STATUS_DESC','Si desidera visualizzare il costo totale dell\'ordine?');
define('MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER_TITLE','L\'ordinamento');
define('MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER_DESC','L\'ordinamento del modulo.');
