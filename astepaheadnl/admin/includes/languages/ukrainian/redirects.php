<?php
define('HEADING_TITLE', 'Перенаправлення');
define('TEXT_REDIRECT_FROM', 'Перенаправити з');
define('TEXT_REDIRECT_TO', 'Перенаправити на');
define('TABLE_HEADING_STATUS', 'Статус');
define('TEXT_DATE_ADDED', 'Дата додавання');
define('TEXT_DATE_UPDATED', 'Дата оновлення');