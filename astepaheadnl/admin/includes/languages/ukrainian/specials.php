<?php
/*
  $Id: specials.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Знижки');

define('TABLE_HEADING_PRODUCTS', 'Товари');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Ціна');
define('TABLE_HEADING_STATUS', 'Стан');
define('TABLE_HEADING_ACTION', 'Дія');
define('TABLE_HEADING_MODEL', 'Модель');

define('TEXT_SPECIALS_PRODUCT', 'Товар:');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Спеціальна ціна:');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Діє До:');
define('TEXT_SPECIALS_PRICE_TIP', '<b>Примечание:</b><ul><li>Вы можете ввести процент скидки в поле "Специальная Цена", например: <b>20%</b></li><li>Если Вы вводите новую цену, десятичный разделитель должен быть \'.\' (точка), например: <b>49.99</b></li></ul>');

//TotalB2B start
define('TEXT_SPECIALS_CUSTOMERS', 'Customer Special Price:');
define('TEXT_ENTER_NAME','Введіть назву або код товару:');
define('TEXT_SPECIALS_GROUPS', 'Group Special Price:');
//TotalB2B end


define('TEXT_INFO_DATE_ADDED', 'Дата додавання:');
define('TEXT_INFO_LAST_MODIFIED', 'Остання зміна:');
define('TEXT_INFO_NEW_PRICE', 'Нова Ціна:');
define('TEXT_INFO_ORIGINAL_PRICE', 'Вихідна Ціна:');
define('TEXT_INFO_PERCENTAGE', 'Відсоток:');
define('TEXT_INFO_EXPIRES_DATE', 'Діє до:');
define('TEXT_INFO_STATUS_CHANGE', 'Змінити статус:');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Видалити Знижку');
define('TEXT_INFO_DELETE_INTRO', 'Ви дійсно хочете видалити спеціальнцю ціну для товару?');

define('TABLE_HEADING_NAME', 'Ім\'я');
define('TABLE_HEADING_PRICE', 'Ціна');
define('TABLE_HEADING_PRICE_DISCOUNT', 'Ціна зі знижкою');
define('TABLE_HEADING_DISCOUNT_PERCENT', 'Знижка в %');
define('TABLE_HEADING_DATE_ADD_DISCOUNT', 'Дата створення знижки');
define('TABLE_HEADING_EXPIRES_DATE', 'Діє до');
define('TABLE_HEADING_ATTR', 'Атрибути');

define('TEXT_INFO_ONLY_DISCOUNT', 'Тілько зі знижкою');
define('TEXT_INFO_CATEGORY', 'Всі категориї');
define('TEXT_INFO_MANUFACTURERS', 'Всі бренди');


//Button
define('BUTTON_CANCEL_NEW', 'відмінити');
define('BUTTON_EDIT_NEW', 'змінити');
define('BUTTON_DELETE_NEW', 'видалити');
define('BUTTON_NEW_PRODUCT_NEW', 'новий товар');

?>