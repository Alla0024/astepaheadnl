<?php 
define('HEADING_TITLE','Configuration d\'un modèle de');
define('TABLE_HEADING_TEMPLATE','Le nom de');
define('TABLE_HEADING_TEMPLATE_FOLDER','Dossier');
define('TABLE_HEADING_ACTION','L\'action');
define('TABLE_HEADING_ACTIVE','Le statut de');
define('TABLE_HEADING_COLOR','Couleur');
define('TABLE_HEADING_DISPLAY_COLUMN_LEFT','Afficher la colonne de gauche?');
define('SLIDER_SIZE_CONTENT','Hébergement curseur');
define('SLIDER_RIGHT','Dans la colonne de droite');
define('SLIDER_CONTENT_WIDTH','Selon la largeur du contenu');
define('SLIDER_CONTENT_WIDTH_100','La largeur de la page(100%)');
define('GENERAL_MODULES','Les blocs de base du site');
define('HEADER_MODULES','Les blocs de casquettes');
define('LEFT_MODULES','Les blocs dans la colonne de gauche');
define('MAINPAGE_MODULES','Les blocs sur la page principale');
define('FOOTER_MODULES','Les blocs de sous-sol');
define('OTHER_MODULES','Les autres blocs');
define('ARTICLE_NAME','Le titre de l\'article');
define('TOPIC_NAME','Le nom de la catégorie');
define('LIMIT','La limite');
define('LIMIT_MOBILE', 'Limiter le mobile');
define('COLS','Nombre de. les colonnes');
define('SLIDER_WIDTH_TITLE','Largeur');
define('SLIDER_HEIGHT_TITLE','Hauteur');

define('F_ARTICLES_BOTTOM','Article dans le sous-sol');
define('F_FOOTER_CATEGORIES_MENU','La catégorie dans le sous-sol');
define('F_TOP_LINKS','Инфостраницы dans le sous-sol');
define('F_MONEY_SYSTEM', 'Afficher les systèmes de paiement');
define('F_SOCIAL', 'Afficher les réseaux sociaux de pied de page');

define('H_LOGIN','Connexion');
define('H_LOGO','Logo');
define('H_COMPARE','Comparaison');
define('H_LANGUAGES','Langues');
define('H_CURRENCIES','La monnaie');
define('H_ONLINE','Show Consultant en ligne');
define('H_SEARCH','Recherche');
define('H_SHOPPING_CART','Panier');
define('H_WISHLIST','Liste de souhaits');
define('H_TEMPLATE_SELECT','Le choix d\'un modèle');
define('H_TOP_MENU','Un menu de catégories');
define('H_CALLBACK','Rappelez-moi');
define('H_TOP_LINKS','Le menu du haut');
define('H_TOGGLE_MOBILE_VISIBLE', 'Visibilité par catégorie');
define('H_LOGIN_FB', 'Afficher la connexion via Facebook');



define('O_FILTER','Les filtres');
define('LIST_FILTER','Les filtres');
define('L_FEATURED','Recommandés');
define('L_WHATS_NEW','Nouveautés');
define('L_SPECIALS','Rabais');
define('L_MANUFACTURERS','Les fabricants');
define('L_BESTSELLERS','Top des ventes');
define('L_ARTICLES','Article');
define('L_POLLS','Les sondages');
define('L_FILTER','Les filtres');
define('L_BANNER_1','Bannière 1');
define('L_BANNER_2','Bannière 2');
define('L_BANNER_3','Bannière 3');
define('L_SUPER','Catégorie');
define('L_SUPER_TOPIC','Sections D\'Articles');
define('M_ARTICLES_MAIN','Nouvelles');
define('M_BANNER_LONG','Bannière longue');
define('M_BEST_SELLERS','Top des ventes');
define('M_BROWSE_CATEGORY','Catégorie');
define('M_DEFAULT_SPECIALS','Rabais');
define('M_FEATURED','Recommandés');
define('M_LAST_COMMENTS','Les derniers commentaires');
define('M_VIEW_PRODUCTS','Vus les marchandises');
define('M_MAINPAGE','Le texte principal');
define('M_MANUFACTURERS','Les fabricants');
define('M_MOST_VIEWED','Vues de haut');
define('M_NEW_PRODUCTS','Nouveautés');
define('M_SLIDE_MAIN','Curseur');
define('M_BANNER_1','Bannière 1');
define('M_CATEGORIES_TABS','Taba catégories');
define('M_CATEGORIES_TABS_NEW','Nouveautés');
define('M_CATEGORIES_TABS_FEATURED','Recommandés');
define('M_CATEGORIES_TABS_SPECIAL','Rabais');
define('M_CATEGORIES_TABS_BEST_SELLERS','Top des ventes');
define('M_CATEGORIES_TABS_NEW_PRODUCTS','Nouveautés');
define('G_HEADER_1','Bande horizontale dans le chapeau 1');
define('G_HEADER_2','Bande horizontale dans le chapeau 2');
define('G_LEFT_COLUMN','La colonne de gauche');
define('G_FOOTER_1','La barre horizontale en bas de 1');
define('G_FOOTER_2','La barre horizontale en bas de 2');
define('M_BANNER_BLOCK','Double bannière sur la');
define('ADD_MODULE_MODULES','Ajouter un module');
define('MAINCONF_MODULES','Les réglages de base');
define('MC_COLOR_1', 'Couleur du texte');
define('MC_COLOR_2', 'Couleur du lien');
define('MC_COLOR_3', 'Couleur de fond');
define('MC_COLOR_4', 'Fond de casquettes');
define('MC_COLOR_5', 'Fond de sous-sol');
define('MC_COLOR_6', 'Couleur du bouton');
define('MC_SHOW_LEFT_COLUMN','Afficher la colonne de gauche');
define('MC_PRODUCT_QNT_IN_ROW','Le nombre de produits en ligne');
define('MAX_DISPLAY_SEARCH_RESULTS_TITLE','Le nombre de produits sur la page');
define('MC_THUMB_WIDTH','La largeur d\'une petite image');
define('MC_THUMB_HEIGHT','La hauteur de la petite image');
define('MC_SHOW_THUMB2','Changer l\'image lors du survol');
define('MC_THUMB_FIT','Étirer l\'image de la marchandise');

define('LISTING_MODULES','La liste des produits');
define('LIST_MODEL','Montrer le code de l\'article');
define('LIST_BREADCRUMB', 'Montrer les miettes de pain');
define('LIST_CONCLUSION', 'Afficher le format de sortie du produit');
define('LIST_QUANTITY_PAGE', 'Afficher le nombre de produits sur la page');
define('LIST_SORTING', 'Montrer le tri des marchandises');
define('LIST_LOAD_MORE', 'Afficher le bouton "Afficher plus"');
define('LIST_NUMBER_OF_ROWS', 'Montrer la pagination');



define('PRODUCT_INFO_MODULES','Page de l\'article');
define('P_MODEL','Montrer le code de l\'article');
define('P_BREADCRUMB', 'Montrer les miettes de pain');
define('P_SOCIAL_LIKE', 'Afficher les goûts via les réseaux sociaux');
define('P_PRESENCE', 'Afficher la disponibilité du produit');
define('P_BUY_ONE_CLICK', 'Montrer "Acheter en un clic"');
define('P_ATTRIBUTES', 'Afficher les attributs du produit');
define('P_SHARE', 'Afficher le partage sur les réseaux sociaux');
define('P_COMPARE', 'Afficher la marque de comparaison');
define('P_WISHLIST', 'Afficher la marque de la liste de souhaits');
define('P_RATING', 'Afficher l\'évaluation du produit');
define('P_SHORT_DESCRIPTION', 'Afficher la description courte');
define('P_RIGHT_SIDE', 'Afficher la colonne de droite');
define('P_TAB_DESCRIPTION', 'Afficher l\'onglet de description');
define('P_TAB_CHARACTERISTICS', 'Afficher l\'onglet de fonctionnalité');
define('P_TAB_COMMENTS', 'Afficher les commentaires');
define('P_TAB_PAYMENT_SHIPPING', 'Afficher l\'onglet paiement et livraison');
define('P_WARRANTY', 'Garantie');
define('P_DRUGIE', 'Afficher des produits similaires');
define('P_XSELL', 'Afficher les produits associés');
define('P_SHOW_QUANTITY_INPUT', 'Afficher le champ "Quantité de marchandises"');
define('P_FILTER','Les filtres');


define('LIST_SHOW_PDF_LINK', 'Afficher le lien PDF');


define('LIST_DISPLAY_TYPE', 'Format de sortie du produit');


define('INSTAGRAM_URL', 'Lien curseur');
define('M_INSTAGRAM', 'Instagram');
