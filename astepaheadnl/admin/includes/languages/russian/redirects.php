<?php
define('HEADING_TITLE', 'Перенаправления');
define('TEXT_REDIRECT_FROM', 'Перенаправление с');
define('TEXT_REDIRECT_TO', 'Перенаправить на');
define('TABLE_HEADING_STATUS', 'Статус');
define('TEXT_DATE_ADDED', 'Дата добавления');
define('TEXT_DATE_UPDATED', 'Дата обновления');