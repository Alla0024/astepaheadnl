<?php
/*
  $Id: html_output.php,v 1.1.1.1 2003/09/18 19:03:44 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

////
// The HTML href link wrapper function

  function tep_href_link($page = '', $parameters = '', $connection = 'NONSSL') {
    if ($page == '') {
      die('</td></tr></table></td></tr></table><br><br><font color="#ff0000"><b>Error!</b></font><br><br><b>Unable to determine the page link!<br><br>Function used:<br><br>tep_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</b>');
    }
      $link = HTTP_SERVER . DIR_WS_ADMIN;
/*    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_ADMIN;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == 'true') {
        $link = HTTPS_SERVER . DIR_WS_ADMIN;
      } else {
        $link = HTTP_SERVER . DIR_WS_ADMIN;
      }
    } else {
      die('</td></tr></table></td></tr></table><br><br><font color="#ff0000"><b>Error!</b></font><br><br><b>Unable to determine connection method on a link!<br><br>Known methods: NONSSL SSL<br><br>Function used:<br><br>tep_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</b>');
    }*/
    if ($parameters == '') {
      $link = $link . $page . '?' . SID;
    } else {
      $link = $link . $page . '?' . $parameters . '&' . SID;
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

    return $link;
  }
  
  function tep_hide_session_id() {
    global $session_started, $SID;

    if (($session_started == true) && tep_not_null($SID)) {
      return tep_draw_hidden_field(tep_session_name(), tep_session_id());
    }
  }
  
  function tep_catalog_href_link($page = '', $parameters = '', $connection = 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_CATALOG;
/*    if ($connection == 'NONSSL') {
      $link = HTTP_CATALOG_SERVER . DIR_WS_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL_CATALOG == 'true') {
        $link = HTTPS_CATALOG_SERVER . DIR_WS_CATALOG;
      } else {
        $link = HTTP_CATALOG_SERVER . DIR_WS_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br><br><font color="#ff0000"><b>Error!</b></font><br><br><b>Unable to determine connection method on a link!<br><br>Known methods: NONSSL SSL<br><br>Function used:<br><br>tep_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</b>');
    }*/
    if ($parameters == '') {
      $link .= $page;
    } else {
      $link .= $page . '?' . $parameters;
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

    return $link;
  }

////
// The HTML image wrapper function
  function tep_image($src, $alt = '', $width = '', $height = '', $params = '') {
    $image = '<img src="' . $src . '" border="0" alt="' . $alt . '"';
    if ($alt) {
      $image .= ' title=" ' . $alt . ' "';
    }
    if ($width) {
      $image .= ' width="' . $width . '"';
    }
    if ($height) {
      $image .= ' height="' . $height . '"';
    }
    if ($params) {
      $image .= ' ' . $params;
    }
    $image .= '>';

    return $image;
  }

////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
//  function tep_image_submit($image, $alt = '', $parameters = '') {
//    global $language;
//
//    $image_submit = '<input type="image" src="' . tep_output_string(DIR_WS_LANGUAGES . $language . '/images/buttons/' . $image) . '" border="0" alt="' . tep_output_string($alt) . '"';
//
//    if (tep_not_null($alt)) $image_submit .= ' title=" ' . tep_output_string($alt) . ' "';
//
//    if (tep_not_null($parameters)) $image_submit .= ' ' . $parameters;
//
//    $image_submit .= '>';
//
//    return $image_submit;
//  }
  function tep_image_submit($image, $alt = '', $parameters = '') {
    global $language;

    $image_submit = '<input type="submit" value="' . tep_output_string($alt) . '"';

//    if (tep_not_null($alt)) $image_submit .= ' title=" ' . tep_output_string($alt) . ' "';
//
    if (tep_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= '>';

    return $image_submit;
  }

////
// Draw a 1 pixel black line
  function tep_black_line() {
    return tep_image(DIR_WS_IMAGES . 'pixel_black.gif', '', '100%', '1');
  }

////
// Output a separator either through whitespace, or with an image
  function tep_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return tep_image(DIR_WS_IMAGES . $image, '', $width, $height);
  }

////
// Output a function button in the selected language
  function tep_image_button($image, $alt = '', $params = '') {
    global $language;

    return tep_image(DIR_WS_LANGUAGES . $language . '/images/buttons/' . $image, $alt, '', '', $params);
  }

function tep_text_button($text) {

    return "<input type='button' value='$text' />";
}


////
// javascript to dynamically update the states/provinces list when the country is changed
// TABLES: zones
  function tep_js_zone_list($country, $form, $field) {
    $countries_query = tep_db_query("select distinct zone_country_id from " . TABLE_ZONES . " order by zone_country_id");
    $num_country = 1;
    $output_string = '';
    while ($countries = tep_db_fetch_array($countries_query)) {
      if ($num_country == 1) {
        $output_string .= '  if (' . $country . ' == "' . $countries['zone_country_id'] . '") {' . "\n";
      } else {
        $output_string .= '  } else if (' . $country . ' == "' . $countries['zone_country_id'] . '") {' . "\n";
      }

      $states_query = tep_db_query("select zone_name, zone_id from " . TABLE_ZONES . " where zone_country_id = '" . $countries['zone_country_id'] . "' order by zone_name");

      $num_state = 1;
      while ($states = tep_db_fetch_array($states_query)) {
        if ($num_state == '1') $output_string .= '    ' . $form . '.' . $field . '.options[0] = new Option("' . PLEASE_SELECT . '", "");' . "\n";
        $output_string .= '    ' . $form . '.' . $field . '.options[' . $num_state . '] = new Option("' . $states['zone_name'] . '", "' . $states['zone_id'] . '");' . "\n";
        $num_state++;
      }
      $num_country++;
    }
    $output_string .= '  } else {' . "\n" .
                      '    ' . $form . '.' . $field . '.options[0] = new Option("' . TYPE_BELOW . '", "");' . "\n" .
                      '  }' . "\n";

    return $output_string;
  }

////
// Output a form
  function tep_draw_form($name, $action, $parameters = '', $method = 'post', $params = '') {
    $form = '<form name="' . tep_output_string($name) . '" action="';
    if (tep_not_null($parameters)) {
      $form .= tep_href_link($action, $parameters);
    } else {
      $form .= tep_href_link($action);
    }
    $form .= '" method="' . tep_output_string($method) . '"';
    if (tep_not_null($params)) {
      $form .= ' ' . $params;
    }
    $form .= '>';

    return $form;
  }

////
// Output a form input field
  function tep_draw_input_field($name, $value = '', $parameters = '', $required = false, $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '" ';

    if (isset($GLOBALS[$name]) && ($reinsert_value == true) && is_string($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }
// input 4 admin NAME

function tep_lol($name, $value = '', $parameters = '', $required = false, $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '" size="70" ';

    if (isset($GLOBALS[$name]) && ($reinsert_value == true) && is_string($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

////
// Output a form password field
  function tep_draw_password_field($name, $value = '', $required = false) {
    $field = tep_draw_input_field($name, $value, 'maxlength="40"', $required, 'password', false);

    return $field;
  }

////
// Output a form filefield
  function tep_draw_file_field($name, $required = false) {
    $field = tep_draw_input_field($name, '', 'size="10"', $required, 'file');

    return $field;
  }





//Admin begin
////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
//  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '') {
//    $selection = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';
//
//    if (tep_not_null($value)) $selection .= ' value="' . tep_output_string($value) . '"';
//
//    if ( ($checked == true) || (isset($GLOBALS[$name]) && is_string($GLOBALS[$name]) && ($GLOBALS[$name] == 'on')) || (isset($value) && isset($GLOBALS[$name]) && (stripslashes($GLOBALS[$name]) == $value)) || (tep_not_null($value) && tep_not_null($compare) && ($value == $compare)) ) {
//      $selection .= ' CHECKED';
//    }
//
//    $selection .= '>';
//
//    return $selection;
//  }
//
////
// Output a form checkbox field
//  function tep_draw_checkbox_field($name, $value = '', $checked = false, $compare = '') {
//    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $compare);
//  }
//
////
// Output a form radio field
//  function tep_draw_radio_field($name, $value = '', $checked = false, $compare = '') {
//    return tep_draw_selection_field($name, 'radio', $value, $checked, $compare);
//  }
////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '', $parameter = '') {
    $selection = '<input type="' . $type . '" name="' . $name . '"';
    if ($value != '') {
      $selection .= ' value="' . $value . '"';
    }
    if ( ($checked == true) || ($GLOBALS[$name] == 'on') || ($value && ($GLOBALS[$name] == $value)) || ($value && ($value == $compare)) ) {
      $selection .= ' CHECKED';
    }
    if ($parameter != '') {
      $selection .= ' ' . $parameter;
    }
    $selection .= '>';

    return $selection;
  }

////
// Output a form checkbox field
  function tep_draw_checkbox_field($name, $value = '', $checked = false, $compare = '', $parameter = '') {
    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $compare, $parameter);
  }

////
// Output a form radio field
  function tep_draw_radio_field($name, $value = '', $checked = false, $compare = '', $parameter = '') {
    return tep_draw_selection_field($name, 'radio', $value, $checked, $compare, $parameter);
  }
//Admin end

////
// Output a form textarea field
  function tep_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {
    $field = '<textarea name="' . tep_output_string($name) . '" wrap="' . tep_output_string($wrap) . '" cols="' . tep_output_string($width) . '" rows="' . tep_output_string($height) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= tep_output_string_protected(stripslashes($GLOBALS[$name]));
    } elseif (tep_not_null($text)) {
      $field .= tep_output_string_protected($text);
   }

    $field .= '</textarea>';

    return $field;
  }

function arr2ini(array $a, array $parent = array()){
    $out = '';
    foreach ($a as $k => $v) {
        if (is_array($v)) {
            $sec = array_merge((array) $parent, (array) $k);
            $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
            $out .= arr2ini($v, $sec);
        }
        else $out .= "$k=$v" . PHP_EOL;
    }
    return $out;
}

////
// Output a form hidden field
  function tep_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    } elseif (isset($GLOBALS[$name]) && is_string($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

////
// Output a form pull down menu
  function tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {
    $field = '<select class="ajaxSelect form-control" name="' . tep_output_string($name) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

    foreach ($values as $value) {
      $field .= '<option value="' . tep_output_string($value['id']) . '"';
      if ($default == $value['id']) {
        $field .= ' selected';
      }

        $text = tep_output_string($value['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;'));

        if ($default == $value['id']) {
            $text = str_replace("&nbsp;", "", $text);
        }

      $field .= '>' . $text . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;
    return $field;
  }
function tep_draw_pull_down_menu_new($name, $values, $default='', $parameters='', $required=false, $mode=false)
{
    $field = '<ul class="menu-cats treeline">';
    $nesting = 0;
    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);
    foreach ($values as $value) {

        if ($nesting == $value['level']) {
            $field .= '<li><a style="display:block" href="' . HTTP_SERVER . DIR_WS_ADMIN . 'categories.php?cPath=' . tep_output_string($value['id']) . '"';
            if ($default == $value['id']) $field .= ' class=active';

            $field .= '><span>' . tep_output_string($value['text'], array(
                    '"' => '&quot;',
                    '\'' => '&#039;',
                    '<' => '&lt;',
                    '>' => '&gt;'
                )) . '</span></a></li>';
        } elseif ($nesting < $value['level']) {
            $field .= '<li class="menu-cats-nesting"><ul><li><a style="display:block" href="' . HTTP_SERVER . DIR_WS_ADMIN . 'categories.php?cPath=' . tep_output_string($value['id']) . '"';
            if ($default == $value['id']) $field .= ' class=active';

            $field .= '><span>' . tep_output_string($value['text'], array(
                    '"' => '&quot;',
                    '\'' => '&#039;',
                    '<' => '&lt;',
                    '>' => '&gt;'
                )) . '</span></a></li>';
        } elseif ($nesting > $value['level']){
            $category_link = "";
            for ($i = 0; $i < ($nesting - $value['level']); $i++) {
                $category_link .= "</ul></li>";
            }
            $field .= $category_link.'<li><a style="display:block" href="' . HTTP_SERVER . DIR_WS_ADMIN . 'categories.php?cPath=' . tep_output_string($value['id']) . '"';
            if ($default == $value['id']) $field .= ' class=active';

            $field .= '><span>' . tep_output_string($value['text'], array(
                    '"' => '&quot;',
                    '\'' => '&#039;',
                    '<' => '&lt;',
                    '>' => '&gt;'
                )) . '</span></a></li>';
        }
        $nesting = $value['level'];
    }
        if ($required == true) $field .= TEXT_FIELD_REQUIRED;
        $field.="</ul>";
        return $field;

}

/**
 * Функция возвращает стилизованый дроп-даун
 *
 * @param $name - значение атрибута 'name'
 * @param $values - значения, из которых будет построен список
 * @param string $default - id выбранного значения
 * @param string $parameters - другие атрибуты дроп-дауна
 * @param bool $required - обязательно ли выбирать значение в этом дроп-дауне
 * @return string
 */
function new_tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {
  $attr_name = tep_output_string($name);

  $select_attr_class = 'form-control';
  if (tep_not_null($parameters)) {
    preg_match('#class="([^"\']*)"#', $parameters, $matches);

    if (sizeof($matches) > 0) {
      $select_attr_class = $matches[1];
    }

    $parameters = ' ' . $parameters;
  }

  if (empty($default) && isset($GLOBALS[$name])) {
    $default = stripslashes($GLOBALS[$name]);
  }

  $options = '';
  foreach ($values as $value) {
    if ($default == $value['id']) {
      $attr_class .= ' selected';
    } else {
      $attr_class = '';
    }

    $attr_value = tep_output_string($value['id']);
    $text = tep_output_string($value['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;'));

    $options .= <<< EOF
<option{$attr_class} value="{$attr_value}">{$text}</option>
EOF;
  }

  if ($required == true) {
    $attr_required = TEXT_FIELD_REQUIRED;
  } else {
    $attr_required = '';
  }

  $field = <<< EOF
<select class="{$select_attr_class}" name="{$attr_name}"{$parameters}{$attr_required}>
  {$options}
</select>
EOF;

  return $field;
}

/**
 * Возвращает стилизованый чекбокс
 * @param $name
 * @param string $value
 * @param bool $checked
 * @param string $compare
 * @param string $parameter
 * @return string
 */
function new_tep_draw_checkbox_field($name, $value = '', $checked = false, $compare = '', $parameter = '') {
  return new_tep_draw_selection_field($name, 'checkbox', $value, $checked, $compare, $parameter);
}

function new_tep_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '', $parameter = '') {
  $selection = '<input type="' . $type . '" name="' . $name . '"';

  if ($value != '' && $type != 'checkbox') {
    $selection .= ' value="' . $value . '"';
  }

  if ( ($checked == true) || ($GLOBALS[$name] == 'on') || ($value && ($GLOBALS[$name] == $value)) || ($value && ($value == $compare)) ) {
    $selection .= ' CHECKED';
  }

  $label_parameter = '';
  if ($parameter != '') {
    preg_match('#data-toggle="tooltip" title="[^"\']*"#is', $parameter, $matches);

    if (sizeof($matches) > 0) {
      $parameter = str_replace($matches[0], '', $parameter);
      $label_parameter = ' ' . $matches[0];
    }

    $selection .= ' ' . trim($parameter);
  }

  $selection .= '>';

  if ($type == 'checkbox') {
    if($value=='1') $value = '';
    $selection = '<label class="checkbox i-checks inline"' . $label_parameter . '>' . $selection . '<i></i>' . $value . '</label>';
  }

  return $selection;
}

/**
 * @param MenuItemConfiguration[] $subMenuItems
 * @return string
 */
function drawSubMenuTabs($subMenuItems) {
    $html = '<div class="nav_table">';
    foreach ($subMenuItems as $subMenuItemFilename => $subMenuItem) {
       $isActive = $subMenuItem->isActive() ? "active" : "";
       $disabled = !$subMenuItem->isAccessible() ? "disabled" : "";
       $html .= '
            <a href="' . tep_href_link($subMenuItemFilename) .'" class="' . $isActive . ' link ' . $disabled . '">
                ' . $subMenuItem->getTitle() . '
            </a>
       ';
    }
    $html .= '</div>';
    return $html;
}

function renderTooltip($text) {

    if(isMobile()) {
      return '';
    }

    return '
        <i class="fa fa-question-circle"
           data-toggle="tooltip" 
           data-placement="right"
           title="' . $text . '"
        ></i>
    ';
}