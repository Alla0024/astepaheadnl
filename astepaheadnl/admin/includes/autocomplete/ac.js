  // Solomono jQuery Autocomplete module for osCommerce. 2017
  // full descriprion of UI autocomplete here: http://api.jqueryui.com/autocomplete/

      jQuery(document).ready(function() {
      	$("input[name=keywords]").autocomplete({
      		source: 'includes/autocomplete/ac.php', // link to php source
          delay: 0, // delay in milliseconds between when a keystroke occurs and when a search is performed
      		minLength: 2, // minimum number of characters a user must type before a search is performed
          select: function( event, ui ) {  // what to do when we click on product  
            //window.location = "product_info.php?products_id="+ui.item.id;
            $('*[name=products_id]').val(ui.item.id);
            $('input[name=keywords]').val('#' + ui.item.model + ' ('+ui.item.price+') ' + ui.item.name);
            return false;
          }, 
          create: function() { // custom style for products listing in search box
              $(this).data('ui-autocomplete')._resizeMenu = function() {this.menu.element.outerWidth(700);};
              $(this).data('ui-autocomplete')._renderItem = function( ul, item ) {
                return $("<li>")
                  .data("ui-autocomplete-item", item)
                  .append('<a><img src="../getimage/products/' + item.image + '&w=40&h=40" /><span class="ac_name"><b>#' + item.model + '</b> ' + item.name + '</span> <span class="ac_price">' + item.price + '</span></a>')
                  .appendTo(ul);
               };
          }
      	});
      });