<?php

$subMenuItems[FILENAME_IMPORT_EXPORT.'?import_type='.basename(FILENAME_EASYPOPULATE, '.php')] = MenuItemConfiguration::create()
    ->setTitle(BOX_EXEL_IMPORT_EXPORT . " " . renderTooltip(TOOLTIP_EXPORT_IMPORT_CSV))
    ->setAccessClosure(function () {
        $file = __DIR__ . "/../../../../../ext/import/import.php";
        return file_exists($file) &&
               defined('EXCEL_IMPORT_MODULE_ENABLED') &&
               constant('EXCEL_IMPORT_MODULE_ENABLED') == 'true';
    })
    ->setIsActiveClosure(function(){
        return isset($_GET['import_type']) && $_GET['import_type'] === basename(FILENAME_EASYPOPULATE, '.php');
    });


$subMenuItems[FILENAME_IMPORT_EXPORT.'?import_type='.basename(FILENAME_PROM, '.php')] = MenuItemConfiguration::create()
    ->setTitle(BOX_PROM_IMPORT_EXPORT . " " . renderTooltip(TOOLTIP_EXPORT_IMPORT_PROM))
    ->setAccessClosure(function () {
        $file = __DIR__ . "/../../../../../ext/prom_excel/prom.php";
        return file_exists($file) &&
               defined('PROM_EXCEL_MODULE_ENABLED') &&
               constant('PROM_EXCEL_MODULE_ENABLED') == 'true';
    })
    ->setIsActiveClosure(function(){
        return isset($_GET['import_type']) && $_GET['import_type'] === basename(FILENAME_PROM, '.php');
    });


$subMenuItems[FILENAME_IMPORT_EXPORT.'?import_type='.basename(FILENAME_YML, '.php')] = MenuItemConfiguration::create()
    ->setTitle(BOX_CATALOG_YML)
    ->setAccessClosure(function () {
        $file = __DIR__ . "/../../../../../ext/yml_import/yml_import.php";
        return file_exists($file) &&
               defined('YML_MODULE_ENABLED') &&
               constant('YML_MODULE_ENABLED') == 'true';
    })
    ->setIsActiveClosure(function(){
        return isset($_GET['import_type']) && $_GET['import_type'] === basename(FILENAME_YML, '.php');
    });


echo drawSubMenuTabs($subMenuItems);