<?php

$subMenuItems[FILENAME_SPECIALS] = MenuItemConfiguration::create()
    ->setTitle(BOX_CATALOG_SPECIALS . " " . renderTooltip(TOOLTIP_SPECIALS))
    ->setAccessClosure(function () {
        return true;
    })
    ->setIsActiveClosure(function(){
        return (bool)strstr($_SERVER['PHP_SELF'], FILENAME_SPECIALS);
    });


$subMenuItems[FILENAME_SALEMAKER] = MenuItemConfiguration::create()
    ->setTitle(BOX_CATALOG_SALEMAKER . " " . renderTooltip(TOOLTIP_SALES_MAKERS))
    ->setAccessClosure(function () {
        return true;
    })
    ->setIsActiveClosure(function(){
        return (bool)strstr($_SERVER['PHP_SELF'], FILENAME_SALEMAKER);
    });


echo drawSubMenuTabs($subMenuItems);