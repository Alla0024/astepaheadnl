<?php

define('CLIENT_DATA_CONFIGURATION_GROUP_ID', 5);
define('SHIPPING_PACKING_CONFIGURATION_GROUP_ID', 7);
define('CHECKOUT_CONFIGURATION_GROUP_ID', 7575);
define('PRODUCTS_LISTING_SETTINGS', 8);
define('SOLO_MODULES_CONFIGURATION_GROUP_ID', 277);

?>

<!-- aside -->
<aside id="aside" class="app-aside bg-dark">

  <!-- fix width -->
  <div class="fix-width <?= $menu_location == '2' ? 'collapse-menu' : '';?>">
    <div class="aside-wrap">
      <div class="navi-wrap wrapper-md no-padder-t-b">
	      <!-- search -->
	      <div class="menu-form">
		      <div class="menu_search_form" style="display: <?= $menu_location == '2' ? 'none' : 'inline-block';?>;">
			      <form action="" method="post" class="search">
				      <input type="search" name="" placeholder="Поиск по категориям" class="input" />
				      <!--<input type="submit" name="" value="" class="submit" />-->
				      <span>
				      <svg width="13px" height="13px" viewBox="0 0 13 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
    <title>search-icon</title>
    <desc>Created with Sketch.</desc>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="newmade-admin-detailed" transform="translate(-251.000000, -67.000000)" stroke="#C6C8CC" stroke-width="1.5">
            <g id="Container">
                <g id="Sidebar" transform="translate(0.000000, 50.000000)">
                    <g id="Item">
                        <g id="Expand" transform="translate(8.000000, 8.000000)">
                            <g id="search-icon" transform="translate(243.000000, 9.000000)">
                                <path d="M4.87495226,0.749999998 C3.7359127,0.75 2.7046627,1.21170635 1.95818453,1.95818453 C1.21170635,2.7046627 0.75,3.7359127 0.75,4.875 C0.75,6.0140873 1.21170635,7.0453373 1.95818453,7.79181547 C2.7046627,8.53829365 3.7359127,9 4.875,9 C6.0140873,9 7.0453373,8.53829365 7.79181547,7.79181547 C8.53829365,7.0453373 9,6.0140873 9,4.875 C8.9999275,3.73597729 8.53820079,2.70475161 7.79172459,1.95827541 C7.04524839,1.21179921 6.01402271,0.750072502 4.87495226,0.749999998 Z" id="Path"></path>
                                <line x1="8.25" y1="8.25" x2="12" y2="12" id="Path" stroke-linecap="round"></line>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
			      </span>
			      </form>
		      </div>
		      <div class="open-close" style="display: inline-block;">
			      <button id="close-menu">
				      <svg width="15px" height="16px" viewBox="0 0 15 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					      <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
					      <title>arrow-back-outline</title>
					      <desc>Created with Sketch.</desc>
					      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.7" stroke-linecap="round" stroke-linejoin="round">
						      <g id="newmade-admin-detailed" transform="translate(-250.000000, -103.000000)" stroke="#FFFFFF" stroke-width="2">
							      <g id="Container">
								      <g id="Sidebar" transform="translate(0.000000, 50.000000)">
									      <g id="List" transform="translate(0.000000, 38.000000)">
										      <g id="Item">
											      <g id="Expand" transform="translate(242.000000, 8.000000)">
												      <g id="arrow-back-outline" transform="translate(8.000000, 8.000000)">
													      <polyline id="Path" points="9 11 5 7 9 3"></polyline>
													      <line x1="6" y1="7" x2="14" y2="7" id="Path"></line>
													      <line x1="1" y1="0" x2="1" y2="14" id="Path"></line>
												      </g>
											      </g>
										      </g>
									      </g>
								      </g>
							      </g>
						      </g>
					      </g>
				      </svg>
			      </button>
			      <button id="open-menu" style="display: none">
				      <svg width="15px" height="16px" viewBox="0 0 15 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					      <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
					      <title>arrow-back-outline</title>
					      <desc>Created with Sketch.</desc>
					      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.7" stroke-linecap="round" stroke-linejoin="round">
						      <g id="newmade-admin-detailed" transform="translate(-250.000000, -103.000000)" stroke="#FFFFFF" stroke-width="2">
							      <g id="Container">
								      <g id="Sidebar" transform="translate(0.000000, 50.000000)">
									      <g id="List" transform="translate(0.000000, 38.000000)">
										      <g id="Item">
											      <g id="Expand" transform="translate(242.000000, 8.000000)">
												      <g id="arrow-back-outline" transform="translate(8.000000, 8.000000)">
													      <polyline id="Path" points="9 11 5 7 9 3"></polyline>
													      <line x1="6" y1="7" x2="14" y2="7" id="Path"></line>
													      <line x1="1" y1="0" x2="1" y2="14" id="Path"></line>
												      </g>
											      </g>
										      </g>
									      </g>
								      </g>
							      </g>
						      </g>
					      </g>
				      </svg>
			      </button>
		      </div>
	      </div>
	      <!-- search_end // -->
        <!-- nav -->
        <nav ui-nav class="navi clearfix">
          <ul class="nav">
            <li class="item-menu tooltip-left_menu <?php print tep_is_active_menu() || tep_is_active_menu(FILENAME_DEFAULT)?'active':''; ?>">
	           <span class="tooltip-left_menu-text"><?php print TEXT_MENU_HOME; ?></span>
              <a href="<?php print tep_href_link(FILENAME_DEFAULT); ?>">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16px" height="16px" viewBox="0 0 16 16" version="1.1">
                      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="newmade-admin-detailed" transform="translate(-20.000000, -65.000000)" fill="#FFFFFF" fill-rule="nonzero">
                              <g id="Sidebar" transform="translate(0.000000, 50.000000)">
                                  <g id="List">
                                      <g id="Item">
                                          <g id="Icon" transform="translate(20.000000, 15.000000)">
                                              <g id="home-sharp" transform="translate(0.000000, 1.000000)">
                                                  <path d="M12.5,1 L11.5,1 C11.2238576,1 11,1.22385763 11,1.5 L11,2.51715728 C11,2.62761423 10.9104569,2.71715728 10.8,2.71715728 C10.7469567,2.71715728 10.6960859,2.69608591 10.6585786,2.65857864 L8.35355339,0.353553391 C8.15829124,0.158291245 7.84170876,0.158291245 7.64644661,0.353553391 L0.707106781,7.29289322 C0.545346323,7.45465368 0.545346323,7.7169192 0.707106781,7.87867966 C0.784786893,7.95635977 0.890143733,8 1,8 L1.5,8 C1.77614237,8 2,8.22385763 2,8.5 L2,13.5 C2,13.7761424 2.22385763,14 2.5,14 L5.5,14 C5.77614237,14 6,13.7761424 6,13.5 L6,9.5 C6,9.22385763 6.22385763,9 6.5,9 L9.5,9 C9.77614237,9 10,9.22385763 10,9.5 L10,13.5 C10,13.7761424 10.2238576,14 10.5,14 L13.5,14 C13.7761424,14 14,13.7761424 14,13.5 L14,8.5 C14,8.22385763 14.2238576,8 14.5,8 L15,8 C15.2287638,8 15.4142136,7.81455027 15.4142136,7.58578644 C15.4142136,7.47593017 15.3705733,7.37057333 15.2928932,7.29289322 L13.1464466,5.14644661 C13.0526784,5.05267842 13,4.92550146 13,4.79289322 L13,1.5 C13,1.22385763 12.7761424,1 12.5,1 Z" id="Path"/>
                                              </g>
                                          </g>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </g>
                  </svg>
                  <span><?php print TEXT_MENU_HOME; ?></span>

	              <span style="display: none">
		              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				         viewBox="0 0 448 448" style="enable-background:new 0 0 448 448;" xml:space="preserve">
<g>
	<g>
		<g>
			<polygon points="225.813,126.187 302.293,202.667 0,202.667 0,245.333 302.293,245.333 225.813,321.813 256,352 384,224 256,96
							"/>
			<rect x="405.333" y="96" width="42.667" height="256"/>
		</g>
	</g>
</g>
</svg>
	              </span>
              </a>
            </li>
            <?php

            /*
             * Товары
             */
            $filenames = array(
                FILENAME_CATEGORIES,
                FILENAME_NEW_PRODUCTS_ATTRIBUTES,
                FILENAME_SALEMAKER,
                FILENAME_IMPORT_EXPORT,
                FILENAME_XSELL_PRODUCTS,
                FILENAME_SPECIALS,
                FILENAME_PROM,
                FILENAME_YML,
                FILENAME_PRODUCTS_MULTI,
                FILENAME_MANUFACTURERS,
                FILENAME_QUICK_UPDATES,
                FILENAME_FEATURED,
              );

            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';
            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
              <li class="item-menu tooltip-left_menu <?=$isActive ?'active':''?>" data-title="<?php print TEXT_MENU_PRODUCTS; ?>">
	              <span class="tooltip-left_menu-text"><?php print TEXT_MENU_PRODUCTS; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/products-icon.svg" border="0" alt="">
                    <span><?php print TEXT_MENU_PRODUCTS; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive?' style="display:block"':''?>>

                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_CATEGORIES) == true) {
                    ?>
                    <li<?php print tep_is_active_menu(FILENAME_CATEGORIES)?' class="active"':''; ?>>
                      <a href="<?php print tep_href_link(FILENAME_CATEGORIES); ?>">
                        <span><?php print TEXT_MENU_CATALOGUE; ?></span>
                      </a>
	                    <span class="tovar-svg" style="display: none">
		                    <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 512 512" width="15px" class=""><g><path d="m256 0c-141.164062 0-256 114.835938-256 256s114.835938 256 256 256 256-114.835938 256-256-114.835938-256-256-256zm0 0" fill="#607d8b" data-original="#607D8B" class="active-path" style="fill:#535559" data-old_color="#607d8b"/><g fill="#fafafa"><path d="m277.332031 128c0 11.78125-9.550781 21.332031-21.332031 21.332031s-21.332031-9.550781-21.332031-21.332031 9.550781-21.332031 21.332031-21.332031 21.332031 9.550781 21.332031 21.332031zm0 0" data-original="#FAFAFA" class="" style="fill:#000000" data-old_color="#FAFAFA"/><path d="m304 405.332031h-96c-11.796875 0-21.332031-9.554687-21.332031-21.332031s9.535156-21.332031 21.332031-21.332031h26.667969v-128h-16c-11.796875 0-21.335938-9.558594-21.335938-21.335938 0-11.773437 9.539063-21.332031 21.335938-21.332031h37.332031c11.796875 0 21.332031 9.558594 21.332031 21.332031v149.335938h26.667969c11.796875 0 21.332031 9.554687 21.332031 21.332031s-9.535156 21.332031-21.332031 21.332031zm0 0" data-original="#FAFAFA" class="" style="fill:#000000" data-old_color="#FAFAFA"/></g></g> </svg>
	                    </span>
                    </li>
                    <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_PRODUCTS_MULTI) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_PRODUCTS_MULTI) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_PRODUCTS_MULTI); ?>">
                              <span><?php print BOX_CATALOG_CATEGORIES_PRODUCTS_MULTI; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_QUICK_UPDATES) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_QUICK_UPDATES) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_QUICK_UPDATES); ?>">
                              <span><?php print BOX_CATALOG_QUICK_UPDATES; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_FEATURED) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_FEATURED) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_FEATURED); ?>">
                              <span><?php print BOX_CATALOG_FEATURED . " " . renderTooltip(TOOLTIP_PRODUCTS_FEATURED); ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_XSELL_PRODUCTS) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_XSELL_PRODUCTS) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_XSELL_PRODUCTS); ?>">
                              <span><?php print BOX_CATALOG_XSELL_PRODUCTS . " " . renderTooltip(TOOLTIP_PRODUCTS_RELATED); ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_NEW_PRODUCTS_ATTRIBUTES) == true) {
                    ?>
                    <li<?php print tep_is_active_menu(FILENAME_NEW_PRODUCTS_ATTRIBUTES)?' class="active"':''; ?>>
                      <a href="<?php print tep_href_link(FILENAME_NEW_PRODUCTS_ATTRIBUTES); ?>">
                        <span><?php print TEXT_MENU_ATTRIBUTES; ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  $isMenuItemActive = strstr(basename($PHP_SELF),FILENAME_SPECIALS) ||
                                      strstr(basename($PHP_SELF),FILENAME_SALEMAKER);

                  if (new_tep_admin_check_boxes(FILENAME_SPECIALS) == true) {
                      ?>
                      <li<?=$isMenuItemActive && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_SPECIALS); ?>">
                              <span><?php print BOX_CATALOG_SPECIALS; ?></span>
                          </a>
                      </li>
                      <?php
                  }


                  if (new_tep_admin_check_boxes(FILENAME_MANUFACTURERS) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_MANUFACTURERS) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_MANUFACTURERS); ?>">
                              <span><?php print BOX_CATALOG_MANUFACTURERS; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  $isMenuItemActive = strstr(basename($PHP_SELF),FILENAME_IMPORT_EXPORT);


                   ?>
                        <li<?=$isMenuItemActive && $menu_location != '0' ? ' class="active"' : ''?>>
                        <a href="<?php print tep_href_link(FILENAME_IMPORT_EXPORT); ?>">
                          <span><?php print IMPORT_EXPORT_MENU_BOX; ?></span>
                        </a>
	                        <span class="tovar-svg" style="display: none">
		                        <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 512 512" width="15px" class=""><g><path d="m256 0c-141.164062 0-256 114.835938-256 256s114.835938 256 256 256 256-114.835938 256-256-114.835938-256-256-256zm0 0" fill="#607d8b" data-original="#607D8B" class="active-path" style="fill:#535559" data-old_color="#607d8b"/><g fill="#fafafa"><path d="m277.332031 128c0 11.78125-9.550781 21.332031-21.332031 21.332031s-21.332031-9.550781-21.332031-21.332031 9.550781-21.332031 21.332031-21.332031 21.332031 9.550781 21.332031 21.332031zm0 0" data-original="#FAFAFA" class="" style="fill:#000000" data-old_color="#FAFAFA"/><path d="m304 405.332031h-96c-11.796875 0-21.332031-9.554687-21.332031-21.332031s9.535156-21.332031 21.332031-21.332031h26.667969v-128h-16c-11.796875 0-21.335938-9.558594-21.335938-21.335938 0-11.773437 9.539063-21.332031 21.335938-21.332031h37.332031c11.796875 0 21.332031 9.558594 21.332031 21.332031v149.335938h26.667969c11.796875 0 21.332031 9.554687 21.332031 21.332031s-9.535156 21.332031-21.332031 21.332031zm0 0" data-original="#FAFAFA" class="" style="fill:#000000" data-old_color="#FAFAFA"/></g></g> </svg>
	                       </span>
                      </li>
                </ul>
              </li>
              <?php
            }

            /*
             * Заказы
             */
            $filenames = array(
                FILENAME_ORDERS,
                FILENAME_ORDERS_STATUS,
                FILENAME_CREATE_ORDER,

              );
            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';

            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
              <li class="item-menu tooltip-left_menu <?=$isActive ?'active':''?>" data-title="<?php print TEXT_MENU_ORDERS; ?>">
	              <span class="tooltip-left_menu-text"><?php print TEXT_MENU_ORDERS; ?></span>
                <a href class="auto">
                    <b class="badge new_badge"><?php print $orders_awaiting; ?></b>
	                <img src="images/icons-sidebare/orders-icon.svg" border="0" alt="" class="img-filter_1">
                    <span><?php print TEXT_MENU_ORDERS; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive?' style="display:block"':''?>>
<!--                  <li class="nav-sub-header">-->
<!--                    <a href>-->
<!--                      <span>--><?php //print TEXT_MENU_ORDERS; ?><!--</span>-->
<!--                    </a>-->
<!--                  </li>-->
                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_ORDERS) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_ORDERS) && $menu_location != '0' && !isset($_GET['action'])? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_ORDERS); ?>">
                        <span><?php print TEXT_MENU_ORDERS_LIST; ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_ORDERS_STATUS) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_ORDERS_STATUS) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_ORDERS_STATUS); ?>">
                        <span><?php print BOX_LOCALIZATION_ORDERS_STATUS; ?></span>
                      </a>
                    </li>
                  <?php
                  }

                  if (true) {
                  ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_ORDERS) && $menu_location != '0' && isset($_GET['action']) && $_GET['action'] == 'create_order_form_user_selection'? ' class="active"' : ''?>>
                    <a href="<?php print tep_href_link(FILENAME_ORDERS,'page=1&perPage=25&action=create_order_form_user_selection'); ?>">
                      <span><?php print BOX_MANUAL_ORDER_CREATE_ORDER; ?></span>
                    </a>
                  </li>
                  <?php
                  }


                  ?>
                </ul>
              </li>
              <?php
            }

            /*
             * Клиенты
             */
            $filenames = array(
                FILENAME_CUSTOMERS,
                FILENAME_GROUPS,
                FILENAME_CREATE_ACCOUNT,
              );
            $isActive = (
                    in_array(basename($PHP_SELF),$filenames) ||
                    (basename($PHP_SELF) === FILENAME_CONFIGURATION && $_GET['gID'] == CLIENT_DATA_CONFIGURATION_GROUP_ID )
                ) && $menu_location != '0';

            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
              <li class="item-menu tooltip-left_menu <?=$isActive ?'active':''?>" data-title="<?php print BOX_HEADING_CUSTOMERS; ?>">
	              <span class="tooltip-left_menu-text"><?php print BOX_HEADING_CUSTOMERS; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/clients-icon.svg" border="0" alt="" class="img-filter_2">
                    <span><?php print BOX_HEADING_CUSTOMERS; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive?' style="display:block"':''?>>
<!--                  <li class="nav-sub-header">-->
<!--                    <a href>-->
<!--                      <span>--><?php //print BOX_HEADING_CUSTOMERS; ?><!--</span>-->
<!--                    </a>-->
<!--                    </a>-->
<!--                  </li>-->
                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_CUSTOMERS) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_CUSTOMERS) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_CUSTOMERS); ?>">
                        <span><?php print TEXT_MENU_CLIENTS_LIST; ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_GROUPS) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_GROUPS) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_GROUPS); ?>">
                        <span><?php print TEXT_MENU_CLIENTS_GROUPS; ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_CREATE_ACCOUNT) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_CREATE_ACCOUNT) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_CREATE_ACCOUNT); ?>">
                        <span><?php print TEXT_MENU_ADD_CLIENT; ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  ?>

                    <li <?=(isset($_GET['gID']) && $_GET['gID'] == CLIENT_DATA_CONFIGURATION_GROUP_ID && $menu_location != '0' ?' class="active"':'')?>>
                        <a href="<?=tep_href_link(FILENAME_CONFIGURATION, 'gID=' . CLIENT_DATA_CONFIGURATION_GROUP_ID, 'NONSSL')?>">
                            <span><?=CUSTOMER_DETAILS_CONF_TITLE?></span>
                        </a>
                    </li>

                </ul>
              </li>
              <?php
            }

            /*
             * Контент
             */
            $filenames = array(
                FILENAME_ARTICLES,
                FILENAME_EMAIL_CONTENT,
                FILENAME_REVIEWS,
                'image_explorer.php',
                FILENAME_LANGUAGES_TRANSLATER,
                FILENAME_SEO_FILTER,
                FILENAME_KEYWORDS,
                FILENAME_REDIRECTS,
              );
            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';
            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
              <li class="item-menu tooltip-left_menu <?=$isActive ?'active':''?>" data-title="<?php print BOX_HEADING_INFORMATION; ?>">
	              <span class="tooltip-left_menu-text"><?php print BOX_HEADING_INFORMATION; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/content-icon.svg" border="0" alt="" class="img-filter_3">
                    <span><?php print BOX_HEADING_INFORMATION; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive?' style="display:block"':''?>>
<!--                  <li class="nav-sub-header">-->
<!--                    <a href>-->
<!--                      <span>--><?php //print BOX_HEADING_INFORMATION; ?><!--</span>-->
<!--                    </a>-->
<!--                  </li>-->
                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_ARTICLES) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_ARTICLES) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_ARTICLES); ?>">
                        <span><?php print TEXT_MENU_PAGES; ?></span>
                      </a>
                    </li>
                    <?php
                  }



                  if (new_tep_admin_check_boxes(FILENAME_EMAIL_CONTENT) == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_EMAIL_CONTENT) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_EMAIL_CONTENT); ?>">
                        <span><?php print TEXT_MENU_EMAIL_CONTENT . " " . renderTooltip(TOOLTIP_EMAIL_TEMPLATE); ?></span>
                      </a>
                    </li>
                    <?php
                  }
                  if (new_tep_admin_check_boxes('image_explorer.php') == true) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),'image_explorer.php') && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link('image_explorer.php'); ?>">
                        <span><?php print TEXT_MENU_CKFINDER . " " . renderTooltip(TOOLTIP_FILE_MANAGER); ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_SEO_FILTER) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_SEO_FILTER) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_SEO_FILTER); ?>">
                              <span><?php print BOX_CATALOG_SEO_FILTER; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (defined('COMMENTS_MODULE_ENABLED') && COMMENTS_MODULE_ENABLED == 'true') {

                      if (new_tep_admin_check_boxes(FILENAME_REVIEWS) == true) {
                          ?>
                          <li<?=strstr(basename($PHP_SELF),FILENAME_REVIEWS) && $menu_location != '0' ? ' class="active"' : ''?>>
                              <a href="<?php print tep_href_link(FILENAME_REVIEWS); ?>">
                                  <span><?php print TEXT_MENU_REVIEWS; ?></span>
                              </a>
                          </li>
                          <?php
                      }
                  }

                  if (new_tep_admin_check_boxes(FILENAME_KEYWORDS)) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_KEYWORDS) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_KEYWORDS,'order=search_count-desc'); ?>">
                              <span><?php print BOX_TOOLS_KEYWORDS; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  ?>

                    <li<?=strstr(basename($PHP_SELF),FILENAME_LANGUAGES_TRANSLATER) && $menu_location != '0' ? ' class="active"' : ''?>>
                        <a href="<?= tep_href_link(FILENAME_LANGUAGES_TRANSLATER,'file='.$language.'.php') ?>">
                            <span><?= TEXT_TRANSLATER_TITLE ?></span>
                        </a>
                    </li>

                    <li<?=strstr(basename($PHP_SELF),FILENAME_REDIRECTS) && $menu_location != '0' ? ' class="active"' : ''?>>
                        <a href="<?= tep_href_link(FILENAME_REDIRECTS) ?>">
                            <span><?= TEXT_REDIRECTS_TITLE . " " . renderTooltip(TOOLTIP_REDIRECTS) ?></span>
                        </a>
                    </li>

                </ul>
              </li>
              <?php
            }

            /*
             * Модули
             */
            $filenames = array(
                FILENAME_POLLS,
                FILENAME_CURRENCIES,
                FILENAME_COUPON_ADMIN,
                FILENAME_LANGUAGES,
                FILENAME_MODULES,
                FILENAME_SHIP2PAY,
              );


            $isActive = (
                    in_array(basename($PHP_SELF),$filenames) ||
                    (basename($PHP_SELF) === FILENAME_CONFIGURATION && $_GET['gID'] == SOLO_MODULES_CONFIGURATION_GROUP_ID )
                ) && $menu_location != '0';

            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
              <li class="item-menu tooltip-left_menu <?php print $isActive  ? 'active' : ''; ?>" data-title="<?php print BOX_HEADING_MODULES; ?>">
	              <span class="tooltip-left_menu-text"><?php print BOX_HEADING_MODULES; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/modules-icon.svg" border="0" alt="" class="img-filter_4">
                    <span><?php print BOX_HEADING_MODULES; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive && $menu_location != '0' ? ' style="display:block"':''?>>
<!--                  <li class="nav-sub-header">-->
<!--                    <a href>-->
<!--                      <span>--><?php //print BOX_HEADING_MODULES; ?><!--</span>-->
<!--                    </a>-->
<!--                  </li>-->
                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_MODULES) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_MODULES) && $menu_location != '0' && isset($_GET['set']) && $_GET['set'] == 'payment' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_MODULES, 'set=payment', 'NONSSL'); ?>">
                              <span><?php print BOX_MODULES_PAYMENT . " " . renderTooltip(TOOLTIP_MODULES_PAYMENT); ?></span>
                          </a>
                      </li>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_MODULES) && $menu_location != '0' && isset($_GET['set']) && $_GET['set'] == 'shipping' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_MODULES, 'set=shipping', 'NONSSL'); ?>">
                              <span><?php print BOX_MODULES_SHIPPING . " " . renderTooltip(TOOLTIP_MODULES_SHIPPING); ?></span>
                          </a>
                      </li>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_MODULES) && $menu_location != '0' && isset($_GET['set']) && $_GET['set'] == 'ordertotal' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_MODULES, 'set=ordertotal', 'NONSSL'); ?>">
                              <span><?php print BOX_MODULES_ORDER_TOTAL . " " . renderTooltip(TOOLTIP_MODULES_TOTALS); ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_SHIP2PAY) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_SHIP2PAY) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_SHIP2PAY); ?>">
                              <span><?php print BOX_MODULES_SHIP2PAY . " " . renderTooltip(TOOLTIP_MODULES_ZONE); ?></span>
                          </a>
                      </li>
                      <?php
                    }
                  }

                  if (LANGUAGE_SELECTOR_MODULE_ENABLED == 'true') {
                      if (new_tep_admin_check_boxes(FILENAME_LANGUAGES) == true) {
                          ?>
                          <li<?=strstr(basename($PHP_SELF),FILENAME_LANGUAGES) && $menu_location != '0'? ' class="active"' : ''?>>
                              <a href="<?php print tep_href_link(FILENAME_LANGUAGES); ?>">
                                  <span><?php print BOX_LOCALIZATION_LANGUAGES . " " . renderTooltip(TOOLTIP_MODULES_LANGUAGES); ?></span>
                              </a>
                          </li>
                          <?php
                      }
                  }


                  if (new_tep_admin_check_boxes(FILENAME_CURRENCIES) == true) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_CURRENCIES) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_CURRENCIES); ?>">
                              <span><?php print BOX_CURRENCIES_CONFIG . " " . renderTooltip(TOOLTIP_MODULES_CURRENCY); ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  if (CUPONES_MODULE_ENABLED == 'true') {
                      if (new_tep_admin_check_boxes(FILENAME_COUPON_ADMIN) == true) {
                          ?>
                          <li<?=strstr(basename($PHP_SELF),FILENAME_COUPON_ADMIN) && $menu_location != '0' ? ' class="active"' : ''?>>
                              <a href="<?php print tep_href_link(FILENAME_COUPON_ADMIN); ?>">
                                  <span><?php print BOX_COUPONS . " " . renderTooltip(TOOLTIP_MODULES_COUPONS); ?></span>
                              </a>
                          </li>
                          <?php
                      }
                  }

                  if (new_tep_admin_check_boxes(FILENAME_POLLS) == true) {
                    ?>

                    <li<?=strstr(basename($PHP_SELF),FILENAME_POLLS) && $menu_location != '0' && !isset($_GET['action']) ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_POLLS, '', 'NONSSL'); ?>">
                        <span><?php print BOX_POLLS_POLLS . " " . renderTooltip(TOOLTIP_MODULES_POOLS); ?></span>
                      </a>
                    </li>
                    <?php
                  }

                  if (new_tep_admin_check_boxes(FILENAME_CONFIGURATION) == true) {
                      ?>
                      <li<?= isset($_GET['gID']) && $_GET['gID'] == 277 && $menu_location != '0' ? ' class="active"' : '' ?>>
                          <a href="<?php print tep_href_link(FILENAME_CONFIGURATION, 'gID=' . 277, 'NONSSL'); ?>">
                              <span><?php print TEXT_MENU_SITE_MODULES . " " . renderTooltip(TOOLTIP_MODULES_SOLOMONO); ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  ?>
                </ul>
              </li>
              <?php

            /*
             * Дизайн
             */
            $filenames = array(FILENAME_TEMPLATE_CONFIGURATION);
            $isActive = in_array(basename($PHP_SELF),$filenames);
            if (new_tep_admin_check_boxes_parent($filenames) and new_tep_admin_check_boxes(FILENAME_TEMPLATE_CONFIGURATION) == true) {
                $template_id_select_query = tep_db_query("SELECT template_id FROM " . TABLE_TEMPLATE . "  WHERE template_name = '" . DEFAULT_TEMPLATE . "'");
                $template_id_select = tep_db_fetch_array($template_id_select_query);
              ?>
              <li class="item-menu tooltip-left_menu <?=$isActive ?'active':''?>" data-title="<?php print BOX_HEADING_DESIGN_CONTROLS; ?>">
	              <span class="tooltip-left_menu-text"><?php print BOX_HEADING_DESIGN_CONTROLS; ?></span>
                <a class="auto" href="<?php print tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=edit_template&id=' . $template_id_select['template_id'], 'NONSSL'); ?>">
	                <img src="images/icons-sidebare/design-icon.svg" border="0" alt="" class="img-filter_5">
                    <span><?php print BOX_HEADING_DESIGN_CONTROLS; ?></span>
                </a>
              </li>
              <?php
            }

            /*
             * Настройки
             */
            $filenames = array(
                FILENAME_CONFIGURATION,
                FILENAME_TAX_CLASSES,
                FILENAME_TAX_RATES,
                FILENAME_GEO_ZONES,
            );

            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';
            if(
                basename($PHP_SELF) == FILENAME_CONFIGURATION &&
                (
                        $_GET['gID'] == CLIENT_DATA_CONFIGURATION_GROUP_ID
                     || $_GET['gID'] == SOLO_MODULES_CONFIGURATION_GROUP_ID
                )
            ) {
                $isActive = false;
            }
            if (new_tep_admin_check_boxes(FILENAME_CONFIGURATION)) { ?>
              <li class="item-menu tooltip-left_menu <?= $isActive?'active':''; ?>" data-title="<?php print BOX_HEADING_CONFIGURATION; ?>">
	              <span class="tooltip-left_menu-text"><?php print BOX_HEADING_CONFIGURATION; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/settings-icon.svg" border="0" alt="" class="img-filter_6">
                    <span><?php print BOX_HEADING_CONFIGURATION; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive ?' style="display:block"':''?>>
                  <?php

                  define('UNCOMPLETED_ORDERS_CONFIGURATION_GROUP_ID', 6501);
                  define('XML_EXPORT_CONFIGURATION_GROUP_ID', 26230);

                  $settings_configuration = '';
                  $query = tep_db_query("SELECT configuration_group_id AS cgID, configuration_group_key AS cgKey, configuration_group_title AS cgTitle FROM " . TABLE_CONFIGURATION_GROUP . " WHERE visible = '1' ORDER BY sort_order");
                  while ($setting_configuration = tep_db_fetch_array($query)) {
                    if ($setting_configuration['cgID'] == 902 && SMSINFORM_MODULE_ENABLED != 'true') {
                      continue;
                    } else if ($setting_configuration['cgID'] == 26230 && EXCEL_IMPORT_MODULE_ENABLED != 'true') {
                      continue;
                    } else if ($setting_configuration['cgID'] == 401) {
                      continue;
                    } else if ($setting_configuration['cgID'] == CLIENT_DATA_CONFIGURATION_GROUP_ID) {
                      continue;
                    } else if ($setting_configuration['cgID'] == SHIPPING_PACKING_CONFIGURATION_GROUP_ID) {
                      continue;
                    } else if ($setting_configuration['cgID'] == UNCOMPLETED_ORDERS_CONFIGURATION_GROUP_ID) {
                      continue;
                    } else if ($setting_configuration['cgID'] == XML_EXPORT_CONFIGURATION_GROUP_ID) {
                      continue;
                    } else if ($setting_configuration['cgID'] == CHECKOUT_CONFIGURATION_GROUP_ID) {
                        continue;
                    } else if ($setting_configuration['cgID'] == PRODUCTS_LISTING_SETTINGS) {
                        continue;
                    }
                    else if ($setting_configuration['cgID'] == 277) {
                      //$setting_configuration['cgKey'] = 'TEXT_MENU_SITE_MODULES';
                        continue;
                    }

                    $settings_configuration .= '<li'.(isset($_GET['gID']) && $_GET['gID'] == $setting_configuration['cgID'] && $menu_location != '0' ?' class="active"':'').'><a href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $setting_configuration['cgID'], 'NONSSL') . '"><span>' . constant(strtoupper($setting_configuration['cgKey'])) . '</span></a></li>';
                  }

                  print $settings_configuration;

                  $isMenuItemActive = strstr(basename($PHP_SELF),FILENAME_TAX_CLASSES) ||
                                      strstr(basename($PHP_SELF),FILENAME_TAX_RATES) ||
                                      strstr(basename($PHP_SELF),FILENAME_GEO_ZONES);

                      ?>
                      <li<?=$isMenuItemActive && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_TAX_CLASSES); ?>">
                              <span><?php print BOX_MENU_TAXES; ?></span>
                          </a>
                      </li>

                </ul>
              </li>
              <?php
            }

            /*
             * Инструменты
             */
            $filenames = array(
                FILENAME_BACKUP,
                FILENAME_TOTAL_CONFIGURATION,
                FILENAME_MAIL,
                FILENAME_NEWSLETTERS,
                FILENAME_MYSQL_PERFORMANCE,
//                FILENAME_GOOGLE_SITEMAP,
                FILENAME_RECOVER_CART_SALES
              );

            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';
            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
                <li class="item-menu tooltip-left_menu <?= $isActive?'active':''; ?>" data-title="<?php print BOX_HEADING_TOOLS; ?>">
	                <span class="tooltip-left_menu-text"><?php print BOX_HEADING_TOOLS; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/tools-icon.svg" border="0" alt="" class="img-filter_7">
                    <span><?php print BOX_HEADING_TOOLS; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive ?' style="display:block"':''?>>
<!--                  <li class="nav-sub-header">-->
<!--                    <a href>-->
<!--                      <span>--><?php //print BOX_HEADING_TOOLS; ?><!--</span>-->
<!--                    </a>-->
<!--                  </li>-->
                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_BACKUP)) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_BACKUP) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_BACKUP); ?>">
                        <span><?php print TEXT_MENU_BACKUP; ?></span>
                      </a>
                    </li>
                    <?php
                  }
                  if (new_tep_admin_check_boxes(FILENAME_TOTAL_CONFIGURATION) && $login_email_address == 'admin@solomono.net') {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_TOTAL_CONFIGURATION) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_TOTAL_CONFIGURATION); ?>">
                              <span><?php print TEXT_MENU_TOTAL_CONFIG; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  $isMenuItemActive = strstr(basename($PHP_SELF),FILENAME_MAIL) ||
                                      strstr(basename($PHP_SELF),FILENAME_NEWSLETTERS);

                  if (new_tep_admin_check_boxes(FILENAME_MAIL)) {
                    ?>
                      <li<?=$isMenuItemActive && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_MAIL); ?>">
                        <span><?php print BOX_MENU_TOOLS_EMAILS?></span>
                      </a>
                    </li>
                    <?php
                  }


                  if (new_tep_admin_check_boxes(FILENAME_MYSQL_PERFORMANCE)) {
                    ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_MYSQL_PERFORMANCE) && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_MYSQL_PERFORMANCE); ?>">
                        <span><?php print TEXT_MENU_SLOW_QUERIES_LOGS; ?></span>
                      </a>
                    </li>
                    <?php
                  }

                    ?>
                    <li>
                      <a id="menu-clear-image-cache" href="<?php print tep_href_link(FILENAME_CLEAR_IMAGE_CACHE); ?>">
                        <span><?php print BOX_CLEAR_IMAGE_CACHE . " " . renderTooltip(TOOLTIP_CLEAR_CACHE); ?></span>
                      </a>
                    </li>
                </ul>
              </li>
              <?php
            }

            /*
             * Отчеты
             */
            $filenames = array(
                FILENAME_STATS_PRODUCTS_VIEWED,
                FILENAME_STATS_PRODUCTS_PURCHASED,
                FILENAME_STATS_PRODUCTS_PURCHASED_BY_CATEGORY,
                FILENAME_STATS_CUSTOMERS,
                FILENAME_STATS_MONTHLY_SALES,
                FILENAME_STATS_NOPHOTO,
                FILENAME_STATS_OPENED_BY,
                FILENAME_STATS_ZEROQTY,
                FILENAME_STATS_LAST_MODIFIED,
                FILENAME_STATS_RECOVER_CART_SALES
            );

            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';
            if (new_tep_admin_check_boxes_parent($filenames)) { ?>
                <li class="item-menu tooltip-left_menu <?= $isActive ? 'active':''; ?>" data-title="<?php print BOX_HEADING_REPORTS; ?>">
	                <span class="tooltip-left_menu-text"><?php print BOX_HEADING_REPORTS; ?></span>
                <a href class="auto">
	                <img src="images/icons-sidebare/charts-icon.svg" border="0" alt="" class="img-filter_8">
                    <span><?php print BOX_HEADING_REPORTS; ?></span>
                </a>
                <ul class="nav nav-sub dk"<?=$isActive ?' style="display:block"':''?>>
                  <?php

                  if (new_tep_admin_check_boxes(FILENAME_STATS_MONTHLY_SALES)) {
                      ?>
                      <li<?=strstr(basename($PHP_SELF),FILENAME_STATS_MONTHLY_SALES) && $menu_location != '0' ? ' class="active"' : ''?>>
                          <a href="<?php print tep_href_link(FILENAME_STATS_MONTHLY_SALES); ?>">
                              <span><?php print TEXT_MENU_SALES; ?></span>
                          </a>
                      </li>
                      <?php
                  }

                  $isMenuItemActive = strstr(basename($PHP_SELF),FILENAME_STATS_PRODUCTS_VIEWED) ||
                                      strstr(basename($PHP_SELF),FILENAME_STATS_PRODUCTS_PURCHASED) ||
                                      strstr(basename($PHP_SELF),FILENAME_STATS_ZEROQTY) ||
                                      strstr(basename($PHP_SELF),FILENAME_STATS_PRODUCTS_PURCHASED_BY_CATEGORY);


                  if (new_tep_admin_check_boxes(FILENAME_STATS_PRODUCTS_PURCHASED)) {
                    ?>
                      <li<?=$isMenuItemActive && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_STATS_PRODUCTS_PURCHASED); ?>">
                        <span><?php print BOX_PRODUCTS_STATS_MENU_ITEM . " " . renderTooltip(TOOLTIP_STATS_VIEWED_PRODUCTS); ?></span>
                      </a>
                    </li>
                    <?php
                  }


                  $isMenuItemActive = strstr(basename($PHP_SELF),FILENAME_STATS_CUSTOMERS) ||
                                      strstr(basename($PHP_SELF),FILENAME_STATS_OPENED_BY);


                  if (new_tep_admin_check_boxes(FILENAME_STATS_CUSTOMERS)) {
                    ?>
                      <li<?=$isMenuItemActive && $menu_location != '0' ? ' class="active"' : ''?>>
                      <a href="<?php print tep_href_link(FILENAME_STATS_CUSTOMERS); ?>">
                        <span><?php print TEXT_MENU_CLIENTS . " " . renderTooltip(TOOLTIP_STATS_CLIENTS_ORDERS); ?></span>
                      </a>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
              </li>
              <?php
            }

            /*
             * Админы
             */
            $filenames = array(FILENAME_ADMIN_MEMBERS);
            $isActive = in_array(basename($PHP_SELF),$filenames) && $menu_location != '0';

            if (new_tep_admin_check_boxes_parent($filenames) and new_tep_admin_check_boxes(FILENAME_ADMIN_MEMBERS) == true) { ?>
                <li class="tooltip-menu item-menu tooltip-left_menu <?=strstr(basename($PHP_SELF),FILENAME_ADMIN_MEMBERS) && $menu_location != '0' ? 'active' : ''?>" data-title="<?php print BOX_HEADING_ADMINISTRATOR; ?>">
	                <span class="tooltip-left_menu-text"><?php print BOX_HEADING_ADMINISTRATOR; ?></span>
                <a class="auto" href="<?php print tep_href_link(FILENAME_ADMIN_MEMBERS); ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16px" height="16px" viewBox="0 0 16 16" version="1.1">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="newmade-admin-detailed" transform="translate(-20.000000, -1049.000000)">
                                <g id="Container">
                                    <g id="Sidebar" transform="translate(0.000000, 50.000000)">
                                        <g id="List" transform="translate(0.000000, 38.000000)">
                                            <g id="Item-" transform="translate(0.000000, 945.000000)">
                                                <g id="Icon" transform="translate(20.000000, 16.000000)">
                                                    <g id="people-outline">
                                                        <g id="Front" transform="translate(2.000000, 1.000000)">
                                                            <path d="M9,5 C9,6.65685425 7.65685425,8 6,8 C4.34314575,8 3,6.65685425 3,5 L9,5 L9,5 Z M6,2 C6.88854726,2 7.68687122,2.3862919 8.23619452,3.00009834 L3.76380548,3.00009834 C4.31312878,2.3862919 5.11145274,2 6,2 Z" id="Combined-Shape" fill="#FFFFFF"/>
                                                            <path d="M6,10 C3.29984714,10 0.703079604,11.1560714 6.36646291e-12,13 C-0.00886927601,13.185942 -0.014231454,13.3504779 -0.0160865339,13.4936077 C-0.0195770851,13.7696797 0.201321913,13.9963799 0.477392742,13.999958 C0.479552207,13.999986 0.48171183,14 0.483871476,14 L11.5158528,14 C11.7920355,14.0000731 12.0159259,13.7761827 12.0159259,13.5 C12.0159259,13.4978666 12.0159122,13.4957332 12.0158849,13.4936 C12.014053,13.350472 12.008758,13.1859387 12,13 C11.297742,11.12 8.70097445,10 6,10 Z" id="Path" fill="#FFFFFF"/>
                                                            <polygon id="Rectangle" fill="#FFD400" points="2 0 4.28571429 2.40740741 6 0.555555556 7.71428571 2.40740741 10 0 9.42857143 4 2.57142857 4"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <span><?php print BOX_HEADING_ADMINISTRATOR; ?></span>
                </a>
              </li>
              <?php
            }

            ?>
          </ul>
        </nav>
        <!-- nav -->
      </div>
    </div>
  </div>
  <!-- /fix width -->
</aside>
<div id="overflow_admin"></div>
<!-- / aside -->