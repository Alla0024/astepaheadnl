<?php
/**
 * Created by PhpStorm.
 * User: ILIYA
 * Date: 07.09.2017
 * Time: 14:06
 */

namespace admin\includes\solomono\app\models\orders_status;


use admin\includes\solomono\app\core\Model;

class orders_status extends Model {

    protected $prefix_id = 'orders_status_id';
    private $default = DEFAULT_ORDERS_STATUS_ID;
    protected $allowed_fields = [
        'orders_status_name' => [
            'label' => TABLE_HEADING_ORDERS_STATUS,
            'type' => 'text',
        ],
        'orders_status_text' => [
            'label' => 'Order status text',
            'type' => 'textarea',
            'ckeditor' => true,
            'show'=>false
        ],
        'default' => [
            'label' => TABLE_HEADING_DEFAULT,
            'type' => 'status',
            'class' => 'check_all',
            'hideInForm' => true,
        ],
        'orders_status_color' => [
            'label' => TABLE_HEADING_COLOR,
            'type' => 'color',
            'class' => 'color',
            'change' => true,
            'hideInForm' => true,
        ],
        'orders_status_show' => [
            'label' => TABLE_HEADING_STATUS_SHOW,
            'type' => 'status',
            'hideInForm' => true,
        ]
    ];

    public function select($id = false) {
        $sql = "SELECT
                `{$this->prefix_id}` as id,
                `orders_status_name`,
                `orders_status_text`,
                `orders_status_color`,
                `orders_status_show`,
                `language_id`,
                 CASE `{$this->prefix_id}`
                WHEN '{$this->default}' THEN '1'
                ELSE '0'
                END as `default`
              FROM `{$this->table}`";
        if ($id) {
            return $sql . " WHERE {$this->prefix_id} = '{$id}'";
        }
        $sql .= " WHERE `language_id`='{$this->language_id}'";

        return $sql;
    }

    public function selectOne($id) {
        $sql = $this->select($id);
        if ($id) {
            $this->data['data'] = $this->getResultKey($sql, 'language_id');
        }
        $this->getLanguages();
    }

    private function InsertUpdate($data,$action,$id,$table=null) {
        $table = is_null($table) ?$this->table:$table ;
        $this->getLanguages();
        $languages = $this->data['languages'];

        foreach ($languages as $k => $v) {
            $query = '';
            foreach ($data as $key => $value) {
                $value = tep_db_prepare_input($value[$k]);
                $query .= "`{$key}` = " . '\'' . tep_db_input($value) . '\', ';
            }
            $query .= "`{$this->prefix_id}`= {$id}, `language_id`={$k}";

            if ($action == 'update') {
                $sql = "INSERT INTO `{$table}` SET $query ON DUPLICATE KEY UPDATE {$query}";
            } elseif ($action == 'insert') {
                $sql = "INSERT INTO `{$table}` SET $query";
            }
            if (!tep_db_query($sql)) {
                return false;
            }
        }
        return true;
    }

    public function insert($data) {
        $id = mysqli_fetch_assoc(tep_db_query("select max({$this->prefix_id})+1 as next_id from `{$this->table}`"))['next_id']?:1;
        return $this->InsertUpdate($data,__FUNCTION__, $id);

    }

    public function update($data) {
        $id = $data['id'];
        unset($data['id']);
        return $this->InsertUpdate($data,__FUNCTION__, $id);
    }

    public function updateDefault($id) {
        $sql = ("UPDATE " . TABLE_CONFIGURATION . " SET `configuration_value`='{$id}' WHERE `configuration_key` = 'DEFAULT_ORDERS_STATUS_ID'");
        if (!tep_db_query($sql)) {
            return false;
        }
        return true;
    }




}