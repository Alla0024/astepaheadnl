<?php
/**
 * Created by PhpStorm.
 * User: ILIYA
 * Date: 02.08.2017
 * Time: 20:03
 */

namespace admin\includes\solomono\app\models\specials;


class manufacturers {

    public static function getManufacturers(){
        $menu='';
        $result='';

        $sql="SELECT manufacturers_id as id,manufacturers_name from manufacturers order by manufacturers_name";
        $sql=tep_db_query($sql);
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }

        foreach ($result as $manufacturer) {
            $menu .= '<option value="' . $manufacturer['id'] . '">' . $manufacturer['manufacturers_name'] . '</option>';
        }
        return $menu;
    }
}