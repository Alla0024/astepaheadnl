<?php
//debug($data);
//debug($data['address_book']);
//debug($data['customers_default_address_id']);
//debug($action);

?>

<?php $action_form="update_$action";
$def_add_id=$data['customers_default_address_id']; ?>
<div class="row">
    <div class="col-sm-12">
       <?=AD_CHOOSE_ADDRESS?>:
        <select class="form-control" style="width: initial;display: inline-block;" id="select_address_book">
            <?php foreach ($data['address_book'] as $k=>$v): ?>
                <option value="<?=$k;?>" <?=$k==$def_add_id ? 'selected' : '';?>><?=$v['entry_street_address'];?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<form class="form-horizontal <?=$action?>" action="<?=($_SERVER['SCRIPT_URL']?:$_SERVER['SCRIPT_NAME']) . '?action=' . $action_form;?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?=$data['id'];?>">
    <input type="hidden" name="address_book_id" value="<?=$def_add_id;?>">
    <div class="row">
        <div class="col-md-6">
            <h2><?=AD_FIO?></h2>
            <?php foreach ($data['customer_cabinet'] as $k=>$v): ?>
                <?php $allowed_fields=$data['allowed_fields']['customer_cabinet'][$k] ?>
                <?php if (!$allowed_fields || $allowed_fields['hideInForm']===true)
                    continue; ?>
                <div class="form-group">
                    <label for="<?=$k;?>" class="col-sm-3 control-label"><?=$allowed_fields['label'];?>:</label>
                    <div class="col-sm-9">
                        <input type="<?=$allowed_fields['type']?>" value="<?=$v?>" name="<?=$k;?>" placeholder="<?=$allowed_fields['placeholder'] ? : $allowed_fields['label'];?>" class="form-control" id="<?=$k;?>">
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-md-6">
            <h2><?=AD_SUBSCRIBE;?></h2>
            <?php foreach ($data['customer_subscribe'] as $k=>$v): ?>
                <?php $allowed_fields=$data['allowed_fields']['customer_subscribe'][$k]; ?>
                <?php if (!$allowed_fields || $allowed_fields['hideInForm']===true)
                    continue; ?>
                <?php if ($allowed_fields['type']=='status'): ?>
                    <input class="cmn-toggle cmn-toggle-round" <?=$v ? 'checked' : '';?> type="checkbox" name="<?=$k?>" id="cmn-toggle-<?=$k?>">
                    <label style="display: inline" for="cmn-toggle-<?=$k?>"><?=$allowed_fields['label']?></label>
                <?php endif; ?>
            <?php endforeach; ?>
            <h2><?=AD_CHANGE_PASSWORD;?></h2>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label"><?=AD_NEW_PASSWORD;?>:</label>
                <div class="col-sm-9">
                    <input type="password" name="password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password_confirm" class="col-sm-3 control-label"><?=AD_CONFIRM_PASSWORD;?>:</label>
                <div class="col-sm-9">
                    <input type="password" name="password_confirm" class="form-control">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-xs-12" id="address_book">
                    <?php foreach ($data['address_book'] as $id_add_book=>$arr): ?>
                        <div data-address_id="<?=$id_add_book;?>" class="fadeIn animated" style="display:<?=$id_add_book==$def_add_id ? 'block' : 'none';?> ">
                            <h2><?=AD_BOOK?></h2>
                            <?php if ($id_add_book==$def_add_id): ?>
                                <div class="alert alert-danger" role="alert"><?=AD_DEL?></div>
                            <?php else: ?>
                                <div class="alert alert-success" role="alert">
                                    <div>
                                        <p><?=AD_BY_DEFAULT;?>
                                            <input type="checkbox" name="customers_default_address_id">
                                        </p>
                                    </div>
                                    <div>
                                        <?=AD_WANT_TO_DEL;?>
                                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#del_<?=$id_add_book?>" aria-expanded="false" aria-controls="collapseExample">
                                            Сюда!
                                        </button>
                                        <div>
                                            <span class="collapse" id="del_<?=$id_add_book?>"> <?=AD_SURE;?>
                                             <button class="btn btn-danger" type="button" data-toggle="collapse" data-target="#del_<?=$id_add_book?>" aria-expanded="false" aria-controls="collapseExample">
                                            no
                                        </button>
                                                  <button class="btn btn-danger" type="button" data-id="<?=$id_add_book?>" onclick="del.call(this)">yes</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php foreach ($arr as $k=>$v): ?>
                                <?php $allowed_fields=$data['allowed_fields']['address_book'][$k]; ?>
                                <?php if (!$allowed_fields || $allowed_fields['hideInForm']===true || $data['account_fields'][$k] == 'false')
                                    continue; ?>
                                <?php $name=$k . "[{$id_add_book}]"; ?>
                                <div class="form-group">
                                    <label for="<?=$name;?>" class="col-sm-3 control-label"><?=$allowed_fields['label'];?>
                                        :</label>
                                    <div class="col-sm-9">
                                        <?php if ($allowed_fields['type']=='select'): ?>
                                            <select class="form-control" name="<?=$name;?>" id="<?=$name;?>"<?= $v == 0 ? ' style="display:none" disabled' : ''?>>
                                                <?php foreach ($data['option'][$k] as $sk=>$sv): ?>
                                                    <?php if (is_array($sv)): ?>
                                                        <option value="<?=$sk;?>" data-country-id="<?=$sv['zone_country_id']?>" <?=$sk==$v ? 'selected' : '';?> ><?=$sv['zone_name'];?></option>
                                                    <?php else: ?>
                                                        <option value="<?=$sk;?>" <?=$sk==$v ? 'selected' : '';?> ><?=$sv;?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                            <input type="text" value="<?=$data['address_book'][$id_add_book]['entry_state']?>" class="form-control" name="<?=$name;?>" data-select-id="<?=$name;?>" <?= $v != 0 ? ' style="display:none" disabled' : ''?>>
                                        <?php else: ?>
                                            <input type="<?=$allowed_fields['type']?>" value="<?=$v?>" name="<?=$name;?>" placeholder="<?=$allowed_fields['label'];?>" class="form-control" id="<?=$name;?>">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-xs-12">
                    <button class="col-xs-12 btn btn-primary" type="button" data-toggle="collapse" data-target="#address_book_list" aria-expanded="false" aria-controls="collapseExample">
                       <?=AD_LIST;?>
                    </button>
                    <div class="col-xs-12 collapse" id="address_book_list">
                        <?php foreach ($data['address_book'] as $k=>$v): ?>
                            <div data-adress_id="<?=$v['address_book_id'];?>">
                                <h5><?=$v['entry_firstname'] . ' ' . $v['entry_lastname'];?> <?=$v['address_book_id']==$def_add_id ? '<span style="font-weight: bold">' . AD_DEFAULT . '</span>' : ''?></h5>
                                <p><?=$v['entry_phone'] . ' ' . $v['entry_fax'];?></p>
                                <p><?=$data['option']['entry_country_id'][$v['entry_country_id']] . ' ' . ($v['entry_zone_id']?$data['option']['entry_zone_id'][$v['entry_zone_id']]['zone_name']:$v['entry_state']);?></p>
                            </div>
                            <hr>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h2><?=DISCOUNT_OPTIONS;?></h2>
            <?php
            $allowed_fields = $data['allowed_fields']['customer_discount'];
            $cd_data = $data['customer_discount'];
            ?>
            <div class="form-group">
                <label for="<?=$name;?>" class="col-sm-3 control-label"><?=$allowed_fields['customers_discount']['label'];?>:</label>
                <div class="col-sm-9">
                    <input type="<?=$allowed_fields['customers_discount']['type']?>" value="<?=$cd_data['customers_discount']?>" name="customers_discount" placeholder="<?=$allowed_fields['customers_discount']['label'];?>" class="form-control" id="customers_discount">
                </div>
            </div>
            <div class="form-group">
                <label for="customer_discount" class="col-sm-3 control-label"><?=$allowed_fields['customers_groups_id']['label'];?>:</label>
                <div class="col-sm-9">
                    <select class="form-control" name="customers_groups_id" id="customers_groups_id">
                        <?php foreach ($data['option']['customers_groups_id'] as $sk=>$sv): ?>
                            <option value="<?=$sk;?>" <?=$sk==$cd_data['customers_groups_id'] ? 'selected' : '';?> ><?=$sv;?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="checkbox" id="customer_discount_notify" name="customer_discount_notify"><?=CHECK_NOTIFY_CUSTOMER?></label>
            </div>
        </div>
        <div class="col-xs-12 text-right">
            <input type="submit" value="OK" class="btn">
            <a class="btn btn-default" href="orders.php?cID=<?php echo $data['id']; ?>"><?=BUTTON_ORDERS_NEW?></a>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=TEXT_MODAL_CANCEL_ACTION?></button>
        </div>
    </div>
</form>
<script>
    $(document).ready(function(){
        $('select[id^=entry_zone_id] option').hide();
        $('select[id^=entry_zone_id] option[data-country-id='+$('select[id^=entry_country_id]').val()+']').show();
        $(document).on('change','select[id^=entry_country_id]',function(){
            var country_id = $(this).val();
            var address_id = $(this).closest('#address_book').children().data('address_id');
            $('select[id="entry_zone_id['+address_id+']"] option').hide();
            if ($('select[id="entry_zone_id['+address_id+']"] option[data-country-id='+country_id+']').length) {
                $('select[id="entry_zone_id['+address_id+']"]').prop('disabled',false).show();
                $('input[name="entry_zone_id['+address_id+']"]').prop('disabled',true).hide().val('');
                $('select[id="entry_zone_id['+address_id+']"] option[data-country-id=' + country_id + ']').show();
                $('select[id="entry_zone_id['+address_id+']"] option').prop('selected', false);
                $('select[id="entry_zone_id['+address_id+']"] option[data-country-id=' + country_id + ']:first').prop('selected', true);
            }else{
                $('select[id="entry_zone_id['+address_id+']"]').prop('disabled',true).hide();
                $('input[name="entry_zone_id['+address_id+']"]').prop('disabled',false).val('').show();

            }
        })
    })
</script>