<?php
//debug($data);
//debug($action);
//exit;
?>

<?php $action_form = (!empty($data['data'])) ? "update_$action" : "insert_$action"; ?>

<form class="form-horizontal" action="<?php echo ($_SERVER['SCRIPT_URL']?:$_SERVER['SCRIPT_NAME']) . '?action=' . $action_form;?>" method="post" enctype="multipart/form-data">
    <?php if (!empty($data['data'])): ?>
        <input type="hidden" name="id" value="<?php echo current($data['data'])['id']?>">
    <?php endif; ?>
    <div class="col-md-3">
        <h3><?php echo TEXT_GENERAL_SETTING;?></h3>
        <?php foreach ($data['allowed_fields'] as $field_name => $option): ?>
            <?php if (isset($option['general'])): ?>
                <?php $current = !empty($data['data']) ? current($data['data']) : null; ?>
                <?php $val = $current[$field_name] !== '' ? $current[$field_name] : ''; ?>
                <div class="form-group">
                    <label for="<?php echo $field_name;?>" class="col-sm-12 control-label"><?php echo $option['label'];?>:</label for="<?php echo $field_name;?>" class="col-sm-12 control-label">
                    <div class="col-sm-12">
                        <?php if ($option['general'] == 'file'): ?>
                            <input type="<?php echo $option['general']?>" name="<?php echo $field_name?>" id="<?php echo $field_name?>" class="form-control">
                            <?php $path = DIR_WS_IMAGES . $val; ?>
                            <?php if (file_exists(DIR_FS_CATALOG . $path) && !is_dir(DIR_FS_CATALOG . $path)): ?>
                                <img src="/<?php echo $path;?>" style="max-width: 60px;">
                            <?php else: ?>
                                <span><?php echo TEXT_IMAGE_NONEXISTENT?></span>
                            <?php endif; ?>
                        <?php elseif ($option['general'] == 'select'): ?>
                            <select class="form-control" name="<?php echo $field_name;?>" id="<?php echo $field_name;?>">
                                <?php foreach ($data['option'][$field_name] as $k=>$v): ?>
                                    <?php $selected = $k==$data['data']['1'][$field_name]; ?>
                                    <?php $selected = $selected ?: (isset($v['selected']) ? $v['selected'] : false); ?>
                                    <option value="<?php echo $k;?>" <?php echo  $selected?'selected':''; ?> ><?php echo $v['val'];?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php elseif ($option['general'] == 'radio'): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" <?php echo ($val==1) ?'checked':'';?> name="<?php echo $field_name?>" value="1">
                                    true
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input <?php echo $val?> type="radio" <?php echo empty($val) ?'checked':'';?> name="<?php echo $field_name?>" value="0">
                                    false
                                </label>
                            </div>
                        <?php elseif ($option['general'] == 'disabled'): ?>
                            <input type="text" <?php echo $option['general'];?> value="<?php echo $val?>" id="<?php echo $field_name?>" class="form-control">
                        <?php else: ?>
                            <input type="<?php echo $option['general']?>" value="<?php echo $val?>" name="<?php echo $field_name?>" class="form-control" id="<?php echo $field_name?>">
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div class="col-md-9" style="padding-top: 55px;">
        <?php foreach ($data['languages'] as $k => $v): ?>
            <?php $class = $v == reset($data['languages']) ? ' class="active"' : ''; ?>
            <div data-lang="<?php echo $k;?>" <?php echo ' class="active"'?>>
                 <?php foreach ($data['allowed_fields'] as $field_name => $option): ?>
                    <?php if (!isset($option['type']) || $option['hideInForm']===true)continue; ?>
                        <?php $val = (!empty($data['data'][$k][$field_name])) ? $data['data'][$k][$field_name] : ''; ?>
                        <?php $field_lan = $field_name . '[' . $k . ']'; ?>
                        <div class="form-group">
	                        <label for="<?php echo $field_lan;?>" class="col-sm-3 control-label"><img src="/admin/images/flags/<?php echo $data['languages'][$k]['code']?>.svg" class="country-flag"> (<?php echo $data['languages'][$k]['code']?>):</label>
                            <div class="col-sm-9">
                                <?php if ($option['type'] == 'textarea'): ?>
                                    <textarea class="<?php echo $option['ckeditor'] === true ? 'ck_replacer' : ''?> form-control" rows="<?php echo $option['rows'] ?: 6?>" name="<?php echo $field_lan?>"><?php echo $val?></textarea>
                                <?php else: ?>
                                    <input type="<?php echo $option['type']?>" value="<?php echo $val?>" name="<?php echo $field_lan?>" class="form-control" id="<?php echo $field_lan?>">
                                <?php endif; ?>
                            </div>
                        </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if (!empty($data['AllProductsByAttrVal'])): ?>
    <div class="col-md-12">
        <h3><?php echo TEXT_PRODUCTS_ON_ATTRIBUTES_VAL?>:</h3>
            <div class="wrapper_products_attributes_val">
                <table class="table table-striped" style="margin-bottom: 0">
                    <?php foreach ($data['AllProductsByAttrVal'] as $k=>$v): ?>
                        <tr>
                            <td><?php echo $v['products_id']?></td>
                            <td>
                                <a target="_blank" style="color: #337ab7;" href="/admin/products.php?pID=<?php echo $v['products_id']?>&action=new_product"><?php echo $v['products_name']?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
    </div>
    <?php endif; ?>
    <div class="form-group text-right">
        <div class="col-sm-12">
            <input type="submit" value="OK" class="btn">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo TEXT_MODAL_CANCEL_ACTION?></button>
        </div>
    </div>
</form>