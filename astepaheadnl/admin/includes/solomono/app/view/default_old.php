<?php
debug($data);
//debug($action);

?>
<?php if (!isset($_GET['ajax_load'])): ?>
    <div class="container-fluid">
        <div class="wrapper-title">
            <div class="bg-light lter b-b wrapper-md ng-scope">
                <h1 class="m-n font-thin h3"><?=HEADING_TITLE;?></h1>
            </div>
        </div>
        <table id="own_table" class="table table-hover table-bordered bg-white-only b-t b-light <?=$action;?>">
            <thead>
            <tr>
                <?php foreach ($data['allowed_fields'] as $key => $value): ?>
                    <th data-table="<?=$key?>" <?=($data['allowed_fields'][$key]['sort'] == true) ? ' class="sorting"' : '';?>><?=trim($data['allowed_fields'][$key]['label']);?>
                        <?php if (!empty($data['allowed_fields'][$key]['filter'])) : ?>
                            <input type="text" class="search">
                        <?php endif; ?>
                    </th>
                <?php endforeach; ?>
                <th align="center" class="add_row">
                    <a href="<?=$_SERVER['SCRIPT_NAME'] . '?action=' . $action . '&new=row'?>" id="add" data-toggle="tooltip" data-placement="top" title="<?=TEXT_MODAL_ADD_ACTION?>">
                        <i class="fa fa-plus"></i>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < count($data['data']); $i++): ?>
                <tr data-id="<?=$id = $data['data'][$i]['id'];?>">
                    <?php foreach ($data['allowed_fields'] as $key => $value): ?>
                        <td data-name="<?=$key;?>" <?=isset($value['class']) ? "class='" . $value['class'] . "'" : '';?>>
                            <?php if ($key === 'status'): ?>
                                <div class="status">
                                    <input id="cmn-toggle-<?=$id;?>" class="cmn-toggle cmn-toggle-round" <?=($data['data'][$i]['status']) ? 'checked' : ''?> type="checkbox">
                                    <label for="cmn-toggle-<?=$id;?>"></label>
                                </div>
                            <?php elseif ($value['type'] == 'file') : ?>
                                <img src="/<?=(!is_dir(DIR_FS_CATALOG . DIR_WS_IMAGES . $data['data'][$i][$key])) ? DIR_WS_IMAGES . $data['data'][$i][$key] : ''?>" alt="<?=$data['data'][$i][$key];?>">
                            <?php else: ?>
                                <?=$data['data'][$i][$key];?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                    <td align="center">
                        <button class="btn_own edit" data-toggle="tooltip" data-placement="top" title="<?=TEXT_MODAL_UPDATE_ACTION?>">
                            <i class="fa fa-pencil-square-o"></i>
                        </button>
                        <a href="<?=$_SERVER['SCRIPT_NAME'] . '?action=' . $action . '&del=' . $id?>" class="del_link">
                            <button data-toggle="tooltip" data-placement="top" title="<?=TEXT_MODAL_DELETE_ACTION?>" class="btn_own del">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </a>
                    </td>
                </tr>
            <?php endfor; ?>
            </tbody>
        </table>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Форма "<?=HEADING_TITLE;?>"</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="<?=$_SERVER['SCRIPT_NAME'] . '?action=' . $action . '&new=row';?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id">
                            <?php foreach ($data['allowed_fields'] as $field_name => $value): ?>
                                <?php if (!empty($value['type']) && ($value['show'] === true || !isset($value['show']))): ?>
                                    <div class="form-group">
                                        <label for="<?=$field_name?>" class="col-sm-2 control-label"><?=$value['label']?>
                                            :</label>
                                        <div class="col-sm-10">
                                            <?php if ($value['type'] == 'select'): ?>
                                                <select required name="<?=$value['option']['fields'][0]?>" id="<?=$value['option']['fields'][1]?>" class="form-control">
                                                    <?php foreach ($data['option'][$value['option']['table']] as $key1 => $value1): ?>
                                                        <option value="<?=$value1[$value['option']['fields'][0]];?>"><?=$value1[$value['option']['fields'][1]];?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            <?php elseif ($value['type'] == 'file'): ?>
                                                <input type="<?=$value['type']?>" name="<?=$field_name?>" class="form-control file" id="<?=$field_name?>">
                                            <?php else: ?>
                                                <input type="<?=$value['type']?>" name="<?=$field_name?>" class="form-control" id="<?=$field_name?>" placeholder="<?=$value['label']?>">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <div class="form-group text-right">
                                <div class="col-sm-12">
                                    <input type="submit" value="OK" class="btn">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?=TEXT_MODAL_CANCEL_ACTION?></button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel"><?=TEXT_MODAL_CONFIRM_ACTION;?></h4>
                    </div>

                    <div class="modal-body">
                        <p><?=TEXT_MODAL_CONFIRMATION_ACTION;?></p>
                        <p class="debug-url"></p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?=TEXT_MODAL_CANCEL_ACTION?></button>
                        <a class="btn btn-danger btn-confirm"><?=TEXT_MODAL_DELETE_ACTION;?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <tbody>
    <?php for ($i = 0; $i < count($data['data']); $i++): ?>
        <tr data-id="<?=$id = $data['data'][$i]['id'];?>">
            <?php foreach ($data['allowed_fields'] as $key => $value): ?>
                <td data-name="<?=$key;?>" <?=isset($value['class']) ? "class='" . $value['class'] . "'" : '';?>>
                    <?php if ($key === 'status'): ?>
                        <div class="status">
                            <input id="cmn-toggle-<?=$id;?>" class="cmn-toggle cmn-toggle-round" <?=($data['data'][$i]['status']) ? 'checked' : ''?> type="checkbox">
                            <label for="cmn-toggle-<?=$id;?>"></label>
                        </div>
                    <?php elseif ($value['type'] == 'file') : ?>
                        <img src="/<?=(!is_dir(DIR_FS_CATALOG . DIR_WS_IMAGES . $data['data'][$i][$key])) ? DIR_WS_IMAGES . $data['data'][$i][$key] : ''?>" alt="<?=$data['data'][$i][$key];?>">
                    <?php else: ?>
                        <?=$data['data'][$i][$key];?>
                    <?php endif; ?>
                </td>
            <?php endforeach; ?>
            <td align="center">
                <button class="btn_own edit" data-toggle="tooltip" data-placement="top" title="<?=TEXT_MODAL_UPDATE_ACTION?>">
                    <i class="fa fa-pencil-square-o"></i>
                </button>
                <a href="<?=$_SERVER['SCRIPT_NAME'] . '?action=' . $action . '&del=' . $id?>" class="del_link">
                    <button data-toggle="tooltip" data-placement="top" title="<?=TEXT_MODAL_DELETE_ACTION?>" class="btn_own del">
                        <i class="fa fa-trash-o"></i>
                    </button>
                </a>
            </td>
        </tr>
    <?php endfor; ?>
    </tbody>
<?php endif; ?>

<div class="col-xs-4">
    Записи с <?=$data['paginate']['start'] + 1?> до <?=$data['paginate']['current_page'] * $data['paginate']['per_page']?> из <?=$data['paginate']['count']?> записей
</div>
<div class="col-xs-8">
    <nav aria-label="...">
        <ul id="own_pagination" class="pagination">
            <?php for ($i = 1; $i <= $data['paginate']['total']; $i++): ?>
                <li<?=($data['paginate']['current_page'] == $i) ? ' class="active"' : '';?>>
                    <a href="<?=$_SERVER['PHP_SELF'] . '?page=' . $i?>"><?=$i;?></a>
                </li>
            <?php endfor; ?>
        </ul>
    </nav>
</div>