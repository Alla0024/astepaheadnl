<style>
    @page {
        margin: 20px;
    }

    body {
        font-family: Arial;
    }

    table {
        width: 100%;
        margin: 20px auto !important;
        border-collapse: collapse;
        font-size: 12px;
    }

    .logo {
        width: 200px;
    }

    .products td {
        padding: 15px;
        text-align: center;
    }

    .products tr {
        border: 1px solid #f5f5f5;
    }
</style>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE_PRINT_ORDER . $oID; ?></title>
    <base href="<?php echo HTTP_SERVER . DIR_WS_CATALOG; ?>">
    <link rel="stylesheet" type="text/css" href="admin/includes/print.css">
</head>

<body marginwidth="10" marginheight="10" topmargin="10" bottommargin="10" leftmargin="10" rightmargin="10">

<table>
    <tbody>
    <tr>
        <td>
            <div class="logo"><img style="width:200px" src="<?= HTTP_SERVER . "/" . LOGO_IMAGE ?>"></div>
        </td>
        <td align="right">
            <div class="logo_text"
                 style="font-size: 30px;background-color: #1b302f;color: white;padding: 5px 10px;font-weight: 100;text-align: center;min-width: 250px; width: 250px; display: block"><?= DOCUMENT_TITLE; ?></div>
        </td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 115px;" valign="top"><?= ADDRESS_TITLE ?></td>
        <td style="width: 10px;" valign="top">:</td>
        <td><?= $order->customer["company"] ?><br>
        	<?= $order->customer["name"] ?><br>
            <?= $order->customer["street_address"] ?><br>
            <?= $order->customer["postcode"] . ' ' . $order->customer["city"] ?><br>
            <?= $order->customer["country"] ?></td>
    </tr>
    <tr>
        <td style="width: 115px;"><?= TEL_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $order->customer["telephone"]; ?></td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td><?= INVOICE_NUMBER_TITLE ?> :</td>
        <td><?= $customer_number["invoice_number"] ? 'W' . $customer_number["invoice_number"] : ''; ?></td>

        <td><?= INVOICE_DATE_TITLE ?> :</td>
        <td><?= date('d-m-y', strtotime($order->info['date_purchased'])) ?></td>

        <td><?= INVOICE_VAT_TITLE ?> :</td>
        <td><?= INVOICE_VAT ?></td>
    </tr>
    <tr>
        <td><?= INVOICE_DEBITEUR_TITLE ?> :</td>
        <td><?= $customer_number["customers_id"] ?></td>

        <td><?= INVOICE_EXPIRATION_DATE ?> :</td>
        <td><?= date('d-m-y', strtotime($order->info['date_purchased']) + 1209600) // + 14 days               ?> </td>

        <td><?= INVOICE_SHEET_TITLE ?> :</td>
        <td><?= INVOICE_SHEET_NUMBER ?></td>
    </tr>
    </tbody>
</table>

<table class="products">
    <thead>
    <tr style="background-color: #333;border: 1px solid #333;">
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo ENTRY_QTY; ?></td>
        <td width="75px" style="color: #fff;font-weight: normal;padding: 5px;text-align:left;"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;text-align:left;"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo TABLE_HEADING_INVOICE_PRODUCT_PRICE; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo TABLE_HEADING_TOTAL_EXCLUDING_TAX; ?></td>
    </tr>
    </thead>
    <tbody>
    <?php for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) { ?>
        <tr>
            <td><?php echo $order->products[$i]['qty'] ?></td>
            <td style="padding: 5px;text-align:left;"><?php echo $order->products[$i]['model'] ?></td>
            <td style="padding: 5px;text-align:left;"><?php echo $order->products[$i]['name'] ?></td>
            <td><?php echo $currencies->format($order->products[$i]['final_price'], true, $order->info['currency'], $order->info['currency_value']) ?></td>
            <td><?php echo $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) ?></td>
        </tr>
    <?php } ?>
    <?php
    $lng = tep_get_languages();
    foreach ($lng as $lngItem) {
        includeLanguages(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $lngItem['directory'] . '/modules/order_total/ot_subtotal.json');
        includeLanguages(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $lngItem['directory'] . '/modules/order_total/ot_total.json');
    }
    $excludeTotals = [
        MODULE_ORDER_TOTAL_SUBTOTAL_TITLE,
        MODULE_ORDER_TOTAL_TOTAL_TITLE,
        'B.T.W. 21.0%',
        'Total Excl. VAT:',
        'Total incl. VAT',

    ];
    for ($k = 0, $n = sizeof($order->totals); $k < $n; $k++) {
        $check = true;
        foreach ($excludeTotals as $exclude) {
            if (stripos($order->totals[$k]['title'], $exclude) !== false) {
                $check = false;
                break;
            }
        }
        switch ($order->totals[$k]['class']) {
            case 'ot_tax':
                $VatTax = str_replace('.', '', $order->totals[$k]['text']);
                $VatTax = (float)str_replace(['€', ','], ['', '.'], $VatTax);
                break;

            case 'ot_total':
                $totals = str_replace('.', '', $order->totals[$k]['text']);
                $totals = (float)str_replace(['<b>€', ','], ['', '.'], $totals);
                break;
        }
        if ($check) {
            $i++;
            ?>
            <tr>
                <td></td>
                <td></td>
                <td style="padding: 5px;text-align:left;"><?php echo $order->totals[$k]['title']; ?></td>
                <td><?php echo $order->totals[$k]['text']; ?></td>
                <td><?php echo $order->totals[$k]['text']; ?></td>
            </tr>
            <?php
        }
    }
    ?>
    </tbody>
</table>

<table class="products">
    <thead>
    <tr style="background-color: #333;border: 1px solid #333;">
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo CREDIT_LIMIT; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo OTHER; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo SUBTOTAL; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo VAT_SUBTOTAL; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo INVOICE_TOTAL; ?></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php echo $currencies->format(0, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo $currencies->format(0, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo $currencies->format($totals - $VatTax, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo $currencies->format($VatTax, true, $order->info['currency'], $order->info['currency_value']); ?></td>
        <td><?php echo '<b>' . $currencies->format($totals, true, $order->info['currency'], $order->info['currency_value']) . '</b>'; ?></td>
    </tr>
    </tbody>
</table>

<div class="test" style="display:block;position: absolute; bottom: 0; width: 95%">
    <table style="width: 100%">
        <tbody>
        <tr>
            <td><?= PAYMENT_CONDITION_TITLE ?> : <?= $order->info["payment_method"] ?></td>
        </tr>
 <!--       <tr>
            <td><?= PAYMENT_TERM_TITLE ?> : <?= PAYMENT_TERM ?></td>
        </tr>
        <tr>
            <td><?= CREDIT_LIMITATION_TITLE ?> : <?= CREDIT_LIMITATION ?></td>
        </tr> -->
        <tr>
            <td><?= COMPLAINTS ?></td>
        </tr>
        <tr>
            <td style="text-align: right">&nbsp;</td>
        </tr>
        </tbody>
    </table>

    <table  style="width: 100%" class="footer">
    <tr style="background-color: #e53f42;">
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"></td>
        </tr>
        <tr style="background-color: #0090b5;">
            <td style="padding: 10px;"></td>
            <td style="padding: 10px;"></td>
            <td style="padding: 10px;"></td>
            <td style="padding: 10px;"></td>
        </tr>
        <tbody>
        <tr>
            <td style="padding: 15px 15px 15px 0px; line-height: 20px;"><?= FOOTER_BLOCK1 ?></td>
            <td style="padding: 15px 15px 15px 0px; line-height: 20px;"><?= FOOTER_BLOCK2 ?></td>
            <td style="padding: 15px 15px 15px 0px; line-height: 20px;"><?= FOOTER_PHONE ?><br/>
                <?= FOOTER_EMAIL ?><br/>
                <?= FOOTER_SITE ?></td>
            <td style="padding: 15px 0px 15px 0px; line-height: 20px;"><?= FOOTER_CC ?><br/>
                <?= FOOTER_IBAN ?><br/>
                <?= FOOTER_BIC ?><br/>
                <?= FOOTER_VAT ?><br/></td>
        </tr>
        </tbody>
    </table>

    <br/>
    <br/>
    <br/>
</div>
</body>

</html>
