<?php
/*
  $Id: password_forgotten.php,v 1.2 2003/09/24 14:33:16 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PASSWORD_FORGOTTEN);

  if (isset($_GET['action']) && ($_GET['action'] == 'process')) {
    $email_address = tep_db_prepare_input($_POST['email_address']);

    $check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_password, customers_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
    if (tep_db_num_rows($check_customer_query)) {
      $check_customer = tep_db_fetch_array($check_customer_query);

      $new_password = tep_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);
      $crypted_password = tep_encrypt_password($new_password);

      tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_db_input($crypted_password) . "' where customers_id = '" . (int)$check_customer['customers_id'] . "'");

      if($content_email_array = get_email_contents('password_forgotten')){

        $store_categories = '';
        $sci = 0;
        foreach(array_keys($cat_tree) as $fcat) {
           if($sci<5) $store_categories .= '<a style="text-decoration:underline;color:inherit" href="'.tep_href_link(FILENAME_DEFAULT, 'cPath='.$fcat, 'NONSSL').'"><span>'.$cat_names[$fcat].'</span></a><span style="padding:0 5px">&bull;</span>';
           $sci++;
        }
        // array to replace variables from html template:
        $array_from_to = array (
            '{STORE_NAME}' =>         STORE_NAME,
            '{CUSTOMER_IP}' =>        $REMOTE_ADDR,
            '{CUSTOMER_PASSWORD}' =>  $new_password,
            '{STORE_LOGO}' =>         HTTP_SERVER . '/' . str_replace("images/", "images/150x150/", LOGO_IMAGE),
            '{STORE_URL}' =>          HTTP_SERVER,
            '{STORE_OWNER_EMAIL}' =>  STORE_OWNER_EMAIL_ADDRESS,
            '{STORE_ADDRESS}' =>      strip_tags(renderArticle('contacts_footer')),
            '{STORE_PHONE}' =>        strip_tags(renderArticle('phones')),
            '{STORE_CATEGORIES}' =>   $store_categories);
        $content_email_array['subject'] = strtr($content_email_array['subject'], $array_from_to);
        $email_text = strtr($content_email_array['content_html'], $array_from_to);
      }
      
      tep_mail($check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'], $email_address, $content_email_array['subject']?:EMAIL_PASSWORD_REMINDER_SUBJECT, $email_text?:sprintf(EMAIL_PASSWORD_REMINDER_BODY, $new_password), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

      $messageStack->add_session('login', SUCCESS_PASSWORD_SENT, 'success');

      tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
    } else {
      $messageStack->add('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
    }
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_LOGIN, '', 'SSL'),'rel="nofollow"');
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'),'rel="nofollow"');

  $content = CONTENT_PASSWORD_FORGOTTEN;

  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
