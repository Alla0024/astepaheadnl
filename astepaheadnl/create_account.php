<?php
/*
  $Id: create_account.tpl.php,v 1.3 2003/09/29 01:13:22 anotherlango Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
  require('includes/application_top.php');

  if (tep_session_is_registered('customer_id')) {
    // $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'));
  }


//
  require(DIR_WS_CLASSES.'geoplugin.class.php');

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CREATE_ACCOUNT);

 
  $_POST['state'] = $_POST['selectRegion'];
  $_POST['country'] = $_POST['selectCountry'] ;



  $geoplugin = new geoPlugin();
  $geoplugin->locate();


  $country_sql = "SELECT `countries_id` FROM `". TABLE_COUNTRIES . "` WHERE `countries_iso_code_2` = '". $geoplugin->countryCode ."'";
  $country_query = tep_db_query($country_sql);
  $country_id = tep_db_fetch_array($country_query);

//  if(tep_not_null($country_id))
//    $_POST['selectCountry'] = $country_id;

// guest account
  if (isset($_GET['guest_account'])) $guest_account = true;
// guest account end
// Guest account start
    if (isset($_POST['guest_account']) && ($_POST['guest_account'] == true)) {
      tep_session_register('guest_account');
      global $guest_account;
      $guest_account = true;
    }
// Guest account end


/*if (defined('DEFAULT_CAPTCHA_STATUS') && DEFAULT_CAPTCHA_STATUS !== 'false') {
  if (isset($_SESSION['captcha_keystring']) && $_POST['keystring'] !== $_SESSION['captcha_keystring']) {
    $error = true;
    $messageStack->add('create_account', 'Captcha error');
  } else {
    unset($_SESSION['captcha_keystring']);
  }
} else {
  unset($_SESSION['captcha_keystring']);
}*/

//Check captcha
/*if (defined('GOOGLE_RECAPTCHA_STATUS') && GOOGLE_RECAPTCHA_STATUS !== 'false' && file_exists(DIR_WS_EXT . 'recaptcha/recaptcha.php')) {
  if ($_SESSION['recaptcha'] !== true) {
    $error = true;
    $messageStack->add('create_account', 'reCaptcha error');
  }
}*/
//End Check captcha


  $process = false;
// +Country-State Selector
  if (isset($_POST['action']) && (($_POST['action'] == 'process') || ($_POST['action'] == 'refresh'))) {
    if ($_POST['action'] == 'process')  $process = true;  
  // -Country-State Selector



    $firstname = tep_db_prepare_input($_POST['firstname']);
    if (ACCOUNT_LAST_NAME == 'true') $lastname = tep_db_prepare_input($_POST['lastname']);
  //  $osebe = tep_db_prepare_input($_POST['osebe']);
  //  $firm = tep_db_prepare_input($_POST['firm']);
    if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($_POST['dob']);
    $email_address = tep_db_prepare_input($_POST['email_address']);
    if (ACCOUNT_COMPANY == 'true') $company = tep_db_prepare_input($_POST['company']);
    if (ACCOUNT_STREET_ADDRESS == 'true') $street_address = tep_db_prepare_input($_POST['street_address']);
    if (ACCOUNT_SUBURB == 'true') $suburb = tep_db_prepare_input($_POST['suburb']);
    if (ACCOUNT_POSTCODE == 'true') $postcode = tep_db_prepare_input($_POST['postcode']);
    if (ACCOUNT_CITY == 'true') $city = tep_db_prepare_input($_POST['city']);
    if (ACCOUNT_STATE == 'true') {
      $state = tep_db_prepare_input($_POST['state']);
      if (isset($_POST['zone_id'])) {
        $zone_id = tep_db_prepare_input($_POST['zone_id']);
      } else {
        $zone_id = false;
      }
    }
    if (ACCOUNT_COUNTRY == 'true' or ACCOUNT_STATE == 'true') { 
      $country = tep_db_prepare_input($_POST['country']);
    } else {
      $country = STORE_COUNTRY;
    }
    if (ACCOUNT_TELE == 'true') $telephone = tep_db_prepare_input($_POST['telephone']);
    if (ACCOUNT_FAX == 'true') $fax = tep_db_prepare_input($_POST['fax']);
    if (isset($_POST['newsletter'])) {
      $newsletter = tep_db_prepare_input($_POST['newsletter']);
    } else {
      $newsletter = false;
    }
    $password = tep_db_prepare_input($_POST['password']);
    $confirmation = tep_db_prepare_input($_POST['confirmation']);
   
   // +Country-State Selector
	if ($process) {
	// -Country-State Selector
    $error = false;
    
// Guest Account Start
     if ($guest_account) {
       $guest_pass = tep_create_random_value(ENTRY_PASSWORD_MIN_LENGTH, 'mixed');
       $password = tep_db_prepare_input($guest_pass);
     }
// Guest Account End

//Check captcha
/*      if (!(isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] === $_POST['keystring'])) {
        $error = true;
        $messageStack->add('create_account', ENTRY_TEXT_FOM_IMAGE);
      }*/
//End Check captcha
      
      
    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_FIRST_NAME_ERROR);
    }

    if (ACCOUNT_LAST_NAME == 'true') {
      if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
  
        $messageStack->add('create_account', ENTRY_LAST_NAME_ERROR);
      }
    }
    
    if (ACCOUNT_DOB == 'true') {
      if (checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4)) == false) {
        $error = true;

        $messageStack->add('create_account', ENTRY_DATE_OF_BIRTH_ERROR);
      }
    }

    if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR);
    } elseif (tep_validate_email($email_address) == false) {
      $error = true;

      $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    } else {
      // Guest Account added guest_flag
      $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "' and guest_flag != '1'");
      $check_email = tep_db_fetch_array($check_email_query);
      if ($check_email['total'] > 0) {
        $error = true;

        $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
      }
    }

 /*   if (ACCOUNT_STREET_ADDRESS == 'true') {
    if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_STREET_ADDRESS_ERROR);
    }
}  */

    if (ACCOUNT_POSTCODE == 'true') {
	    if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_POST_CODE_ERROR);
    }
}






    if (ACCOUNT_CITY == 'true') {
	    if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_CITY_ERROR);
    }
}

    if (ACCOUNT_COUNTRY == 'true') {
	    if (is_numeric($country) == false) {
      $error = true;

      $messageStack->add('create_account', ENTRY_COUNTRY_ERROR);
    }
}

    if (ACCOUNT_STATE == 'true') {
      $zone_id = 0;
      $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
      $check = tep_db_fetch_array($check_query);
      $entry_state_has_zones = ($check['total'] > 0);
      if ($entry_state_has_zones == true) {
       // $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and zone_name = '" . tep_db_input($state) . "'");
        $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and zone_id = '" . tep_db_input($state) . "'");
        if (tep_db_num_rows($zone_query) == 1) {
          $zone = tep_db_fetch_array($zone_query);
          $zone_id = $zone['zone_id'];
        } else {
          $error = true;

          $messageStack->add('create_account', ENTRY_STATE_ERROR_SELECT);
        }
      } else {
        if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
          $error = true;
          $messageStack->add('create_account', ENTRY_STATE_ERROR);
        }
      }
    }

    if (ACCOUNT_TELE == 'true') {
	    if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_TELEPHONE_NUMBER_ERROR);
    }
}

    // Guest Account Start
    if ($guest_account == false) {
      if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_PASSWORD_ERROR);
      } elseif ($password != $confirmation) {
        $error = true;

        $messageStack->add('create_account', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
      }
    } // guest account end

    if ($error == false) {
      $sql_data_array = array('customers_firstname' => $firstname,
                              'customers_lastname' => $lastname,
                              'customers_email_address' => $email_address,
                              'customers_telephone' => $telephone,
                              'customers_dob' => date('Y-m-d'),
                              'customers_fax' => $fax,
                              'customers_osebe' => $osebe,                        
                              
                              'customers_newsletter' => $newsletter,
                              'customers_password' => tep_encrypt_password($password));

      if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

// Guest Account Start
    if ($guest_account) $sql_data_array['guest_flag'] = '1';	
    tep_db_query("update " . TABLE_CUSTOMERS . " set customers_email_address = '@_" . $email_address . "' where customers_email_address = '" . $email_address . "' and guest_flag = '1'");
    tep_db_query("update " . TABLE_CUSTOMERS . " set customers_lastname = '@_" . $lastname . "' where customers_email_address = '@_" . $email_address . "'");
// Guest Account End

      tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);

      $customer_id = tep_db_insert_id();

      $sql_data_array = array('customers_id' => $customer_id,
                              'entry_firstname' => $firstname,
                              'entry_lastname' => $lastname,
                              'entry_street_address' => $street_address,
                              'entry_postcode' => $postcode,
                              'entry_city' => $city,
                              'entry_country_id' => $country);
      
      if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
      if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
      if (ACCOUNT_STATE == 'true') {
        if ($zone_id > 0) {
          $sql_data_array['entry_zone_id'] = $zone_id;
          $sql_data_array['entry_state'] = '';
        } else {
          $sql_data_array['entry_zone_id'] = '0';
          $sql_data_array['entry_state'] = $state;
        }
      }

      tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

      $address_id = tep_db_insert_id();

      tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int)$address_id . "' where customers_id = '" . (int)$customer_id . "'");

// Guest Account Start
      if (!$guest_account) {
        tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . tep_db_input($customer_id) . "', '0', now())");
      } else {
        tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . tep_db_input($customer_id) . "', '-1', now())");
      }
// Guest Account End

      if (SESSION_RECREATE == 'True') {
        tep_session_recreate();
      }

      $customer_first_name = $firstname;
      $customer_default_address_id = $address_id;
      $customer_country_id = $country;
      $customer_zone_id = $zone_id;
      tep_session_register('customer_id');
      tep_session_register('customer_first_name');
      tep_session_register('customer_default_address_id');
      tep_session_register('customer_country_id');
      tep_session_register('customer_zone_id');

// restore cart contents
      $cart->restore_contents();
      
// restore wishlist to sesssion
      $wishList->restore_wishlist();      

// build the message content
      $name = $firstname . ' ' . $lastname;

      $email_text = sprintf(EMAIL_GREET_NONE, $firstname);

// Guest Account Start
      if ($guest_account == true) {
        tep_redirect(tep_href_link(FILENAME_CHECKOUT, '', 'SSL'));
      } elseif($create_account_array = get_email_contents('create_account')){

        $store_categories = '';
        $sci = 0;
        foreach(array_keys($cat_tree) as $fcat) {
           if($sci<5) $store_categories .= '<a style="text-decoration:underline;color:inherit" href="'.tep_href_link(FILENAME_DEFAULT, 'cPath='.$fcat, 'NONSSL').'"><span>'.$cat_names[$fcat].'</span></a><span style="padding:0 5px">&bull;</span>';
           $sci++;
        }
        // array to replace variables from html template:
        $array_from_to = array (
            '{STORE_NAME}' =>         STORE_NAME,
            '{CUSTOMER_NAME}' =>      $firstname,
            '{CUSTOMER_LOGIN}' =>     $email_address,
            '{CUSTOMER_PASSWORD}' =>  $password,
            '{STORE_LOGO}' =>         HTTP_SERVER . '/' . str_replace("images/", "images/150x150/", LOGO_IMAGE),
            '{STORE_URL}' =>          HTTP_SERVER,
            '{STORE_OWNER_EMAIL}' =>  STORE_OWNER_EMAIL_ADDRESS,
            '{STORE_ADDRESS}' =>      strip_tags(renderArticle('contacts_footer')),
            '{STORE_PHONE}' =>        strip_tags(renderArticle('phones')),
            '{STORE_CATEGORIES}' =>   $store_categories);

        $create_account_array['subject'] = strtr($create_account_array['subject'], $array_from_to);
        $email_text = strtr($create_account_array['content_html'], $array_from_to);
      } else {
        $email_text .= EMAIL_WELCOME . EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
      }

      tep_mail($name, $email_address, $create_account_array['subject']?:EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
      tep_redirect(tep_href_link(FILENAME_CREATE_ACCOUNT_SUCCESS, '', 'SSL'));
      
// Guest Account End

// ICW - CREDIT CLASS CODE BLOCK ADDED  ******************************************************* BEGIN
/* 
  if (NEW_SIGNUP_GIFT_VOUCHER_AMOUNT > 0) {
    $coupon_code = create_coupon_code();
    $insert_query = tep_db_query("insert into " . TABLE_COUPONS . " (coupon_code, coupon_type, coupon_amount, date_created) values ('" . $coupon_code . "', 'G', '" . NEW_SIGNUP_GIFT_VOUCHER_AMOUNT . "', now())");
    $insert_id = tep_db_insert_id($insert_query);
    $insert_query = tep_db_query("insert into " . TABLE_COUPON_EMAIL_TRACK . " (coupon_id, customer_id_sent, sent_firstname, emailed_to, date_sent) values ('" . $insert_id ."', '0', 'Admin', '" . $email_address . "', now() )");

    $email_text .= sprintf(EMAIL_GV_INCENTIVE_HEADER, $currencies->format(NEW_SIGNUP_GIFT_VOUCHER_AMOUNT)) . "\n\n" .
                   sprintf(EMAIL_GV_REDEEM, $coupon_code) . "\n\n" .
                   EMAIL_GV_LINK . tep_href_link(FILENAME_GV_REDEEM, 'gv_no=' . $coupon_code,'NONSSL', false) .
                   "\n\n";
  }
  if (NEW_SIGNUP_DISCOUNT_COUPON != '') {
		$coupon_code = NEW_SIGNUP_DISCOUNT_COUPON;
    $coupon_query = tep_db_query("select * from " . TABLE_COUPONS . " where coupon_code = '" . $coupon_code . "'");
    $coupon = tep_db_fetch_array($coupon_query);
		$coupon_id = $coupon['coupon_id'];		
    $coupon_desc_query = tep_db_query("select * from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . $coupon_id . "' and language_id = '" . (int)$languages_id . "'");
    $coupon_desc = tep_db_fetch_array($coupon_desc_query);
    $insert_query = tep_db_query("insert into " . TABLE_COUPON_EMAIL_TRACK . " (coupon_id, customer_id_sent, sent_firstname, emailed_to, date_sent) values ('" . $coupon_id ."', '0', 'Admin', '" . $email_address . "', now() )");
    $email_text .= EMAIL_COUPON_INCENTIVE_HEADER .  "\n" .
                   sprintf("%s", $coupon_desc['coupon_description']) ."\n\n" .
                   sprintf(EMAIL_COUPON_REDEEM, $coupon['coupon_code']) . "\n\n" .
                   "\n\n";
  }     */
  
//    $email_text .= EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
// ICW - CREDIT CLASS CODE BLOCK ADDED  ******************************************************* END

    }
  }

 // +Country-State Selector 
 }
 if (!isset($country)){$country = DEFAULT_COUNTRY;}
 // -Country-State Selector

 $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CREATE_ACCOUNT,   '', 'SSL'));
 
  $content = CONTENT_CREATE_ACCOUNT;
  $javascript = 'form_check.js.php';

require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);


  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
