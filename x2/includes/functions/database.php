<?php
require_once __DIR__ . '/../classes/Chronicler/autoload.php';
use SoloMono\Chronicler\LoggerFactory;

require_once __DIR__ . '/../classes/Database/Mysqli/Connector.php';

function DB() {
    return Connector::getInstance()->getConnection();
}

function tep_db_connect() {
    $link = DB();
    $link->query("SET NAMES 'utf8mb4'");
    $link->query("SET SESSION sql_mode=''");
    //   $link->query("SET CHARACTER SET 'utf8'");
    return $link;
}

function tep_db_close($link='db_link') {
    return mysqli_close(DB());
}


/**
 * @param $query
 * @param $errno
 * @param $error
 * @return string
 */
function tep_db_error($query, $errno, $error) {
    global $path;
    // include(DIR_WS_LANGUAGES.'russian_db_error.php');
    $msg="\n" . 'MYSQL QUERY ERROR REPORT' . "\n" . " - " . date("d/m/Y H:m:s", time()) . "\n" . '---------------------------------------' . "\n";
    $msg.=$errno . ' - ' . $error . "\n\n" . $query . "\n";
    $msg.='---------------------------------------' . "\n";
    $msg.='Server Name   : ' . $_SERVER['SERVER_NAME'] . "\n";
    $msg.='Remote Address: ' . $_SERVER['REMOTE_ADDR'] . "\n";
    $msg.='Referer       : ' . $_SERVER["HTTP_REFERER"] . "\n";
    $msg.='Requested     : ' . $_SERVER["REQUEST_URI"] . "\n";
    if (defined ('SOLOMONO_ADMINS_EMAIL_ADDRESS')) {
        @mail('admin@solomono.net', 'solomono error report', $msg,'From: db_error@'.$_SERVER["SERVER_NAME"]);
    }

    if (class_exists('SoloMono\Chronicler\LoggerFactory')) {
        $logger = LoggerFactory::create()->createLogger("MYSQL/front", "index");
        $logger->error("Message = {message} ", ["message" => $msg]);
    } else {
        file_put_contents($path . '/logs/MYSQL/front/error_log_query.log',$msg . PHP_EOL,FILE_APPEND);
    }

    die($msg);
    //    return $msg;
}

function tep_db_query($query) {

/*  show all query turn in includes\application_bottom.php
    global $showAllQuery;
    $showAllQuery.=$query . "<br>";*/

    global $query_counts;
    global $query_total_time;
    $query_counts++;


    $time_start=explode(' ', microtime());
    // limex: mod query performance START
    list($usec, $sec)=explode(" ", microtime());
    $start=(float)$usec + (float)$sec;;

    $result=DB()->query($query);


    list($usec, $sec)=explode(" ", microtime());

    $end=(float)$usec + (float)$sec;
    $parsetime=$end - $start;
    $qlocation=$_SERVER["SCRIPT_FILENAME"];
    // limex: some queries come before having the config values. Default to 10 secs
    $mysql_perf_treshold=(defined('MYSQL_PERFORMANCE_TRESHOLD') and MYSQL_PERFORMANCE_TRESHOLD > 0) ? MYSQL_PERFORMANCE_TRESHOLD : 2;
    if ($parsetime > $mysql_perf_treshold) {
        $slow_when = date('F j, Y, g:i a', time());
        $slow_query = tep_db_input($query) . "\t" . $qlocation . "\t" . $parsetime . "\t" . $slow_when . "\r\n";
        if (class_exists('SoloMono\Chronicler\LoggerFactory')) {
            $logger = LoggerFactory::create()->createLogger("MYSQL/front/slow_query", "index");
            $logger->error("Message = {message} ", ["message" => $slow_query]);
        } else {
            $log_file = DIR_FS_CATALOG . 'logs/MYSQL/front/slow_query/slow_query.log';
            $dirname = dirname($log_file);
            if (!is_dir($dirname)) mkdir($dirname, 0775);
            $slow_log = fopen($log_file, 'a');
            fwrite($slow_log, $slow_query);
            fclose($slow_log);
        }
    }
    // limex: mod query performance END
    $time_end=explode(' ', microtime());
    $query_time=$time_end[1] + $time_end[0] - $time_start[1] - $time_start[0];
    $query_total_time+=$query_time;

    if (!$result) {
        tep_db_error($query, mysqli_errno(DB()), mysqli_error(DB()));
    }

    //    var_dump($result);
    return $result;
}

/**
 * @param $table
 * @param $data
 * @param string $action
 * @param string $parameters
 * @param string $link
 * @return mixed
 */
function tep_db_perform($table, $data, $action='insert', $parameters='', $link='db_link') {
    //    global DB();
    reset($data);

    if ($action=='insert') {
        $query='insert into ' . $table . ' (';
        foreach ($data as $columns => $value) {

//        while (list($columns,)=each($data)) {
            $query.=$columns . ', ';
        }
        $query=substr($query, 0, -2) . ') values (';
        reset($data);
        foreach ($data as $key => $value) {
//        while (list(, $value)=each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query.='now(), ';
                    break;
                case 'null':
                    $query.='null, ';
                    break;
                default:
                    $query.='\'' . tep_db_input($value) . '\', ';
                    break;
            }
        }
        $query=substr($query, 0, -2) . ')';
    }elseif ($action=='insertodku') {
        $query = [];
        foreach ($data as $key => $value) {
            $query[]= "`{$key}` = " . '\'' . tep_db_input($value) . '\'';
        }
        $query = implode(',',$query);
        $query = "INSERT INTO `{$table}` SET $query ON DUPLICATE KEY UPDATE {$query}";
    }elseif ($action=='update') {
        $query='update ' . $table . ' set ';
        foreach ($data as $columns => $value) {
//        while (list($columns, $value)=each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query.=$columns . ' = now(), ';
                    break;
                case 'null':
                    $query.=$columns.=' = null, ';
                    break;
                default:
                    $query.=$columns . ' = \'' . tep_db_input($value) . '\', ';
                    break;
            }
        }
        $query=substr($query, 0, -2) . ' where ' . $parameters;
    }

    return tep_db_query($query, DB());
}

function tep_db_fetch_array($db_query) {
    return $db_query ? mysqli_fetch_assoc($db_query) : null;
}

function tep_db_num_rows($db_query) {
    return mysqli_num_rows($db_query);
}

function tep_db_data_seek($db_query, $row_number) {
    return mysqli_data_seek($db_query, $row_number);
}

function tep_db_insert_id() {
    return DB()->insert_id;
}

function tep_db_free_result($db_query) {
    mysqli_free_result($db_query);
}

function tep_db_fetch_fields($db_query) {
    return mysqli_fetch_field($db_query);
}

function tep_db_output($string) {
    return htmlspecialchars($string);
}

function tep_db_input($string,$stripTags = true) {
    if (function_exists('mysqli_real_escape_string')) {
        $string = mysqli_real_escape_string(DB(), $string);
    }elseif (function_exists('mysqli_escape_string')) {
        $string = mysqli_escape_string(DB(), $string);
    }
    if ($stripTags){
        $string = tep_db_strip_tags($string);
    }
    return $string;
}

function tep_db_prepare_input($string) {
    if (is_string($string)) {
        return tep_db_input(tep_db_strip_tags(trim(tep_db_sanitize_string(stripslashes($string)))));
    }elseif (is_array($string)) {
        reset($string);
        foreach ($string as $key => $value) {
       //  while (list($key, $value)=each($string)) {
            $string[$key]=tep_db_prepare_input($value);
        }
        return $string;
    }else {
        return tep_db_input(tep_db_strip_tags($string));
    }
}

function tep_db_strip_tags($string){
    return strip_tags($string, '<p><div><span><br><b><a>');
}

function tep_db_sanitize_string($string) {
    $string=preg_replace('/ +/', ' ', trim($string));

    return preg_replace("/[<>]/", '_', $string);
}