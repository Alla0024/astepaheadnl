<?php

namespace SoloMono\Chronicler;

/**
 * Class FileLogger
 *
 * @package SoloMono\Chronicler
 */
class FileLogger implements LoggerInterface
{

    const FIFTY_MEGABYTETES = 50000; //5000 - 5Mb, 50000 - 50 Mb
    /**
     * Log data separator
     */
    const SEPARATOR = " | ";

    /**
     * @var integer maximum log file size
     */
    private $_maxFileSize = 1024; // in KB

    /**
     * @var integer number of log files used for rotation
     */
    private $_maxLogFiles = 5;

    /**
     * @var string
     */
    private $loggerDirectory;

    /**
     * @var string
     */
    private $loggerName;

    /**
     * @var boolean Whether to rotate primary log by copy and truncate
     * which is more compatible with log tailers. Defaults to false.
     */
    public $rotateByCopy = false;

    /**
     * FileLogger constructor.
     *
     * @param $loggerDirectory
     * @param $loggerName
     */
    public function __construct($loggerDirectory, $loggerName)
    {
        $this->loggerDirectory = $loggerDirectory;
        $this->loggerName = $loggerName;
    }

    /**
     * @inheritDoc
     */
    public function critical($message, $context = [])
    {
        $this->log(LogLevels::CRITICAL, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function log($lvl, $message, $context = [])
    {
        $dateTime = date('c');
        foreach ($context as $key => $value) {
            if (is_string($value) || $this->isConvertibleToSting($value)) {
                $value = (string)$value;
                $message = str_replace("{" . $key . "}", $value, $message);
            }
        }

        $message = implode(static::SEPARATOR,
            [
                $dateTime,
                $lvl,
                $message
            ]
        );

        $logFile = $this->loggerDirectory . DIRECTORY_SEPARATOR . $this->loggerName . ".log";
        $fp = @fopen($logFile, 'a');
        @flock($fp, LOCK_EX);
        if (@filesize($logFile) > $this->getMaxFileSize() * self::FIFTY_MEGABYTETES) {
            $this->rotateFiles($logFile);
            @flock($fp, LOCK_UN);
            @fclose($fp);
            file_put_contents(
                $logFile,
                $message . PHP_EOL,
                FILE_APPEND | LOCK_EX
            );
        } else {
            @fwrite($fp, $message . PHP_EOL);
            @flock($fp, LOCK_UN);
            @fclose($fp);
        }
    }

    /**
     * Rotates log files.
     */
    protected function rotateFiles($file)
    {
        $max = $this->getMaxLogFiles();
        for ($i = $max; $i > 0; --$i) {
            $rotateFile = $this->loggerDirectory . DIRECTORY_SEPARATOR . $this->loggerName . $i . '.log';
            if (is_file($rotateFile)) {
                // suppress errors because it's possible multiple processes enter into this section
                if ($i === $max) {
                    @unlink($rotateFile);
                } else {
                    $this->copyLog($rotateFile, $this->loggerDirectory . '/' . $this->loggerName . ($i + 1) . '.log');
                }
            }
        }

        if(is_file($file)) {
            $this->copyLog($file, $this->loggerDirectory . '/' . $this->loggerName . '1.log');
            if ($fp = @fopen($file, 'a')) {
                @ftruncate($fp, 0);
                @fclose($fp);
            }
        }
    }

    private function copyLog($file, $newPath)
    {
        copy($file, $newPath);
        unlink($file);
    }

    /**
     * @inheritDoc
     */
    public function emergency($message, $context = [])
    {
        $this->log(LogLevels::EMERGENCY, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function alert($message, $context = [])
    {
        $this->log(LogLevels::ALERT, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function error($message, $context = [])
    {
        $message .= PHP_EOL . 'POST = {post}' . PHP_EOL . 'GET = {get}' . PHP_EOL . 'SERVER = {server}';
        $server = $_SERVER;
        unset($server['DB_DATABASE'], $server['DB_USERNAME'], $server['DB_PASSWORD'], $server['HTTP_COOKIE'], $server['DB_HOST']);
        $context["post"] = print_r($_POST, 1);
        $context["get"] = print_r($_GET, 1);
        $context["server"] = print_r($server, 1);

        $this->log(LogLevels::ERROR, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function warning($message, $context = [])
    {
        $this->log(LogLevels::WARNING, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function debug($message, $context = [])
    {
        $this->log(LogLevels::DEBUG, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function info($message, $context = [])
    {
        $this->log(LogLevels::INFO, $message, $context);
    }

    /**
     * @param mixed $var
     * @return bool
     */
    private function isConvertibleToSting($var)
    {
        return $var === null
            || is_scalar($var)
            || (is_object($var) && method_exists($var, '__toString'));
    }

    /**
     * @return integer maximum log file size in kilo-bytes (KB). Defaults to 1024 (1MB).
     */
    public function getMaxFileSize()
    {
        return $this->_maxFileSize;
    }

    /**
     * @return integer number of files used for rotation. Defaults to 5.
     */
    public function getMaxLogFiles()
    {
        return $this->_maxLogFiles;
    }
}