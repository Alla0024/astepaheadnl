<?php
$redirectQuery = tep_db_query("SELECT 
r.redirect_from, r.redirect_to, r.action
FROM redirects r
WHERE r.status = 1");

while ($row = tep_db_fetch_array($redirectQuery)) {
    $url = substr($_SERVER['REQUEST_URI'], 1, strlen($_SERVER['REQUEST_URI']) - 1);
    switch($row['action']){
        //redirect
        case 1:
            $urlFrom = array();
            foreach (explode('/', $row['redirect_from']) as $urlPart) {
                $urlFrom[] = urlencode($urlPart);
            }
            $urlFrom = implode('/', $urlFrom);
            $urlTo = !empty($row['redirect_to']) ? $row['redirect_to'] : '../';

            if ($url == $row['redirect_from'] || $url == $urlFrom) {
                header("HTTP/1.0 301 Moved Permanently");
                header("Location: " . $urlTo);
                tep_exit();
            }
            break;
        //page not found
        case 2:
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
            exit;
            break;
        //noindex
        case 3:
            $robotsNoindex = true;
            break;
    }

}
