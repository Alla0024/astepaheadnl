<?php

chdir('../../');
$rootPath = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
require($rootPath.'/includes/application_top.php');

if($template->show("LIST_MODAL_ON")==true&&$_GET['product_href']||$_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest") {
    $url = array_reverse(explode("/",$_GET['product_href']))[0];
    $id = [];
    preg_match('!\d+!',$url,$id);
    $id = $id[0];
    $response = [];
    $query = tep_db_query("select p.lable_3, p.products_free_ship, p.lable_2, p.lable_1,
     p.products_id, pd.products_name, pd.products_viewed, pd.products_description,
      p.products_model, p.products_quantity, pd.products_info, p.products_image,
       pd.products_url, p." . $customer_price . " as products_old_price,
        p.products_tax_class_id, p.products_date_added, p.products_date_available, 
        p.manufacturers_id from " . TABLE_PRODUCTS . " p,
         " . TABLE_PRODUCTS_DESCRIPTION . " pd where
          p.products_status = '1' and p.products_id = '" . (int)$id . "'
           and pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "'");
    if(tep_db_num_rows($query)){
        $product = tep_db_fetch_array($query);
        $product_characteristics = tep_db_query("select pag_id, pag_name from products_attributes_groups where language_id = '" . (int)$languages_id . "'");
        $new_price = tep_get_products_special_price((int)$id);
        $product['product_new_price'] = $new_price;
        $imgs = explode(";",$product['products_image']);
        $product['products_image']=$imgs;
        /*for($i = tep_db_fetch_array($product_characteristics);$i!=null;$i=tep_db_fetch_array($product_characteristics)){
        }*/
        $options = [];
        while ($res = tep_db_fetch_array($product_characteristics)){
            $options[]=$res;
        }
        $products_options_name_query = tep_db_query("
        select 
        popt.products_options_id, 
        (CASE WHEN popt.products_options_name != '' 
              THEN popt.products_options_name
              ELSE (select po.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " po where popt.products_options_id = po.products_options_id AND po.language_id = " . (int)$lng->defaultLanguage['id'] . ") 
              END) as products_options_name, 
        popt.products_options_type, 
        popt.pag, 
        pov.products_options_values_id, 
        (CASE WHEN pov.products_options_values_name != ''
              THEN pov.products_options_values_name
              ELSE (select povv.products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " povv where pov.products_options_values_id = povv.products_options_values_id AND povv.language_id = " . (int)$lng->defaultLanguage['id'] . ") 
              END) as products_options_values_name, 
        pov.products_options_values_image, 
        pa.price_prefix, 
        pa.options_values_price, 
        pa.pa_qty 
        from 
        " . TABLE_PRODUCTS_OPTIONS . " popt, 
        " . TABLE_PRODUCTS_ATTRIBUTES . " pa, 
        " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov 
        where 
        pa.products_id=" . (int)$_GET['products_id'] . " and 
        pa.options_id = popt.products_options_id and 
        pa.options_values_id = pov.products_options_values_id and 
        popt.language_id = " . (int)$languages_id . " and 
        pov.language_id = " . (int)$languages_id . " 
        order by 
        popt.pag, 
        popt.products_options_sort_order, 
        pov.products_options_values_sort_order");
        $product_options = [];
        while ($res = tep_db_fetch_array($products_options_name_query)){
            $product_options[]=$res;
        }
        $options_container =[];
        foreach ($options as $option){
            $options_container[$option['pag_id']] = array_search($option,$options);
        }
        foreach ($product_options as $product_option){
            if(($index = array_search($product_option['pag'],($options_container)))===false){
                $options[$options_container[$product_option['pag']]]['values'][]=$product_option;
            }
            else{
                $options['empty']['values'][]=$product_option;
            }
        }
        $response = ["success"=>"true","status"=>"200","product"=>$product];
    }
    else{
        $response = ["success"=>"false","status"=>"404"];
    }
    die(json_encode($response));
}