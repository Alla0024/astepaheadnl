<?php
/*
  $Id: address_book_details.php,v 1.1.1.1 2003/09/18 19:04:52 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

?>

<h3><?php echo  NEW_ADDRESS_TITLE ?></h3>

<?php         

      $messageStack->render('addressbook');    
    
      if (!isset($process)) $process = false;

    	echo '<div class="form-group">'.ENTRY_FIRST_NAME.' '.tep_draw_input_field('firstname', $_POST['firstname']?:$entry['entry_firstname'], 'class="form-control"').'</div>';

    if (ACCOUNT_LAST_NAME == 'true') 
      echo '<div class="form-group">'.ENTRY_LAST_NAME.' '.tep_draw_input_field('lastname', $_POST['lastname']?:$entry['entry_lastname'], 'class="form-control"').'</div>';

    if (ACCOUNT_COMPANY == 'true') 
      echo '<div class="form-group">'.ENTRY_COMPANY.' '.tep_draw_input_field('company', $_POST['company']?:$entry['entry_company'], 'class="form-control"').'</div>';
    

    
  /*  
    if (ACCOUNT_STATE == 'true') {
      echo '<div class="form-group">'.ENTRY_STATE;
       
      // +Country-State Selector
      $zones_array = array();
      $zones_query = tep_db_query("select zone_name, zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)($_POST['country']?:$entry['entry_country_id']) . "' order by zone_name");
      while ($zones_values = tep_db_fetch_array($zones_query)) {
        $zones_array[] = array('id' => $zones_values['zone_id'], 'text' => $zones_values['zone_name']);
      }
      if (count($zones_array) > 0) {
        echo tep_draw_pull_down_menu('zone_id', $zones_array, $_POST['zone_id']?:$entry['entry_zone_id']);
        echo tep_draw_hidden_field('state', '');
      } else {
        echo tep_draw_input_field('state', $_POST['state']?:$entry['entry_state'], 'class="form-control"');
      }
      // -Country-State Selector
      echo '</div>';
    }  */
  
    if (ACCOUNT_STREET_ADDRESS == 'true') 
      echo '<div class="form-group">'.ENTRY_STREET_ADDRESS.' '.tep_draw_input_field('street_address', $_POST['street_address']?:$entry['entry_street_address'], 'class="form-control"').'</div>';
    
    if (ACCOUNT_CITY == 'true') 
  	  echo '<div class="form-group">'.ENTRY_CITY.' '.tep_draw_input_field('city', $_POST['city']?:$entry['entry_city'], 'class="form-control"').'</div>';

    if (ACCOUNT_COUNTRY == 'true' or ACCOUNT_STATE == 'true') {
        if (ACCOUNT_COUNTRY != 'true') $non_show = 'style="display:none;"';
        echo '<div class="form-group">'.ENTRY_STATE.'<span class="selectZone"></span></div>';
    }

    if(ACCOUNT_SUBURB == 'true') 
      echo '<div class="form-group">'.ENTRY_SUBURB.' '.tep_draw_input_field('suburb', $_POST['suburb']?:$entry['entry_suburb'], 'class="form-control"').'</div>';
    
    if(ACCOUNT_POSTCODE == 'true') 
      echo '<div class="form-group">'.ENTRY_POST_CODE.' '.tep_draw_input_field('postcode', $_POST['postcode']?:$entry['entry_postcode'], 'class="form-control"').'</div>';

    if ((isset($_GET['edit']) && ($customer_default_address_id != $_GET['edit'])) || (isset($_GET['edit']) == false) ) 
      echo '<div class="form-group">'. tep_draw_checkbox_field('primary', 'on', $_POST['primary'], 'id="primary" style="display:none;"') . ' <label for="primary">' . SET_AS_PRIMARY.'</label></div>';

    if (ACCOUNT_COUNTRY == 'true' or ACCOUNT_STATE == 'true') {
      if (ACCOUNT_COUNTRY != 'true') $non_show = 'style="display:none;"';
      echo '<div class="form-group edit_acc_country" '.$non_show.'>'.ENTRY_COUNTRY.' '.tep_get_country_list('country',  $_POST['country']?:$entry['entry_country_id'], 'data-zone="'.($_POST['zone_id']?:$entry['entry_zone_id']).'" ').'</div>';
    }
?>