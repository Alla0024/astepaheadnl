<?php

$rootDirectory = __DIR__ . "/../../../";
$pathToPaymentModule   = 'ext/acquiring/stripe/stripe.php';

if (CARDS_ENABLED == 'true' && file_exists($rootDirectory . $pathToPaymentModule)) {
    require_once $rootDirectory . $pathToPaymentModule;
} else {
    class stripe {
        var $code, $title, $description, $enabled;

        function __construct() {
        }

        function check() {
            return false;
        }

        function selection() {
            return false;
        }

        function before_process() {
            return false;
        }

        function pre_confirmation_check() {
            return false;
        }

        function confirmation() {
            return false;
        }

        function process_button() {
            return false;
        }

        function after_process() {
            return false;
        }
    }
}
