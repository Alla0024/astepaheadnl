<?php

$moduleFilePath = __DIR__ . "/../../../ext/multisafepay/multisafepay.php";

if (CARDS_ENABLED == 'true' && file_exists($moduleFilePath)) {
    require_once $moduleFilePath;
} else {
    class multisafepay {
        var $code, $title, $description, $enabled;

        function __construct() {
        }

        function check() {
            if (!isset($this->_check)) {
                $check_query  = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_LIQPAY_STATUS'");
                $this->_check = tep_db_num_rows($check_query);
            }
            return $this->_check;
        }
    }
}