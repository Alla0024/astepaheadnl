<?php

$rootDirectory = __DIR__ . "/../../../";
$pathToPaymentModule   = 'ext/acquiring/qiwi/qiwi.php';

if (CARDS_ENABLED == 'true' && file_exists($rootDirectory . $pathToPaymentModule)) {
    require_once $rootDirectory . $pathToPaymentModule;
} else {
    class qiwi {
        var $code, $title, $description, $enabled;

        function __construct() {
        }

        function check() {
            return false;
        }
    }
}
