<?php
define('YML_IMPORT_UPLOAD_IMAGES', 'Імпортувати зображення?');
define('YML_IMPORT_TRUNCATE_TABLE', 'Очистити поточну базу даних від товарів / категорій / виробників / атрибутів');
define('YML_IMPORT_UPLOAD_FILE', 'Виберіть .xml файл');
define('YML_IMPORT_UPLOAD_BUTTON', 'Завантажити');
define('YML_IMPORT_MAIN_CATEGORY', 'Імпортувати до категорії:');
define('YML_IMPORT_VENDOR_TEMPLATE', 'Шаблон iмпорту');
define('YML_IMPORT_REPLACE_TEXT', 'Перезаписувати у існуючих товарів поля:');
define('YML_IMPORT_REPLACE_NAME', 'Назва i опис');
define('YML_IMPORT_REPLACE_DESCRIPTION', 'Опис');
define('YML_IMPORT_REPLACE_PRICE', 'Ціна');
define('YML_IMPORT_REPLACE_QUANTITY', 'Кількість');
define('YML_IMPORT_REPLACE_IMAGE', 'Зображення');
define('YML_IMPORT_UPLOAD_PRESETS', 'Передустановки');
define('YML_IMPORT_UPLOAD_TEMPLATE', 'Шаблон');
define('YML_IMPORT_UPLOAD_LINK', 'Ссилка');
define('YML_IMPORT_UPLOAD_BUTTON_LINK', 'Завантажити ссилку');
define('YML_IMPORT_UPLOAD_UPLOADED', 'Завантажити');
