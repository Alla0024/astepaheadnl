<?php
/**
 * Created by PhpStorm.
 * User: ILIYA
 * Date: 14.06.2017
 * Time: 15:22
 */

namespace admin\includes\solomono\app\models\products_attributes;

use admin\includes\solomono\app\core\Model;

class products_attributes_groups extends Model {

    protected $allowed_fields=[
        'pag_name'=>[
            'label'=>TEXT_OPTION_GROUP_NAME,
            'type'=>'text',
            'sort'=>true,
            'filter'=>true
        ],
        'sort_order'=>[
            'label'=>TEXT_OPTION_SORT_ORDER,
            'general'=>'text',
            'sort'=>true,
            'filter'=>true
        ],

    ];
    
    protected $prefix_id='pag_id';

    public function select($id=false) {
        $sql="SELECT
                  `p`.`pag_id` as id,
                  `p`.`language_id`,
                  `p`.`pag_name`,
                  `p`.`sort_order`
                  
                FROM `products_attributes_groups` `p`
                  ";
        if ($id) {
            return $sql . " WHERE `p`.`pag_id` = {$id}";
        }
        $sql.=" WHERE `p`.`language_id`='{$this->language_id}'";
        return $sql;
    }

    public function selectOne($id) {
        $sql=$this->select($id);
        if ($id) {
            $this->data['data']=$this->getResultKey($sql, 'language_id');
        }
        $this->getLanguages();
    }

    public function update($data) {

        $id=$data['id'];
        unset($data['id']);

        //$query=$this->prepareGeneralField($data);
        if ($this->InsertUpdate($data, $id, __FUNCTION__, 'language_id', 'products_attributes_groups')) {
                return true;
        }
        return false;
    }

    public function insert($data) {

        $id=mysqli_fetch_assoc(tep_db_query("select max({$this->prefix_id})+1 as next_id from `{$this->table}`"))['next_id']?:1;
        
        //$query=$this->prepareGeneralField($data);
        if ($this->InsertUpdate($data, $id, __FUNCTION__, 'language_id', 'products_attributes_groups')) {
            return true;
        }
        return false;
    }
    

    private function InsertUpdate($data, $id, $action, $lang='language_id', $table=null) {

        $table=$table ? : $this->table;
        $this->getLanguages();
        $languages=$this->data['languages'];

        foreach ($languages as $k=>$v) {
            $query='';
            foreach ($data as $key=>$value) {
                if (!is_array($value)){
                    $tmp_val = [];
                    foreach ($languages as $lk => $lv){
                        $tmp_val[$lk] = $value;
                    }
                    $value = $tmp_val;
                }
                $value=tep_db_prepare_input($value[$k]);
                $query.="`{$key}` = " . '\'' . tep_db_input($value) . '\', ';
            }
            $query.="`{$this->prefix_id}`= {$id}, `{$lang}`={$k}";

            if ($action=='update') {
                $sql="INSERT INTO `{$table}` SET $query ON DUPLICATE KEY UPDATE {$query}";
            }elseif ($action=='insert') {
                $sql="INSERT INTO `{$table}` SET $query";
            }
            if (!tep_db_query($sql)) {
                return false;
            }
        }
        return true;
    }

}