<?php
define('IMPORTEXPORT_TAB_IMPORT', 'Importeren');
define('IMPORTEXPORT_TAB_EXPORT', 'Exporteren');
define('IMPORTEXPORT_ORDER_OWN_VARIANT', 'Bestel uw optie');
define('IMPORTEXPORT_UNDER_ORDER', 'Op bestelling');
define('IMPORTEXPORT_IF_INTERESTED', 'Als u geïnteresseerd bent in <span> export </span> naar een service die hierboven niet wordt vermeld');
define('IMPORTEXPORT_CONTACT_US', 'neem contact op');
define('COPY_BUTTON', 'Kopiëren');
define('COPY_TEXT', 'Gekopieerd');
define('OSC_IMPORT_BLOCKED_TEXT', 'Migratie is alleen beschikbaar op uw hosting');
