<?php
define('IMPORTEXPORT_TAB_IMPORT', 'Importer');
define('IMPORTEXPORT_TAB_EXPORT', 'Exportation');
define('IMPORTEXPORT_ORDER_OWN_VARIANT', 'Commandez votre option');
define('IMPORTEXPORT_UNDER_ORDER', 'Sur commande');
define('IMPORTEXPORT_IF_INTERESTED', 'Si vous souhaitez <span> exporter </span> vers un service non répertorié ci-dessus');
define('IMPORTEXPORT_CONTACT_US', 'nous contacter');
define('COPY_BUTTON', 'Copie');
define('COPY_TEXT', 'Copié');
define('OSC_IMPORT_BLOCKED_TEXT', 'La migration n\'est disponible que sur votre hébergement');
