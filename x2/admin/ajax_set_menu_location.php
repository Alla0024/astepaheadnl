<?php
require_once('includes/application_top.php');
if ($_GET['action']=='set_menu_location') {
    tep_db_query("update " . TABLE_CONFIGURATION . " 
        set configuration_value = '".(int)$_GET['value']."'
        where configuration_key  = 'MENU_LOCATION'
    ");
    exit;
}

if ($_GET['action']=='errorsAlert') {
    $sessionAlertErrors = [];
    if (!empty($_SESSION['alertErrors'])) {
        foreach ($_SESSION['alertErrors'] as $k => $v) {
            $sessionAlertErrors[$k]['text'] = constant($v['text']);
            $sessionAlertErrors[$k]['type'] = $v['type'];
        }
    }

    echo json_encode($sessionAlertErrors);
    exit;
}

