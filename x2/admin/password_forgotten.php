<?php
/*
  $Id: password_forgotten.php,v 1.2 2003/09/24 15:18:15 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGIN);

if (isset($_GET['action']) && ($_GET['action'] == 'process')) {
    $email_address = tep_db_prepare_input($_POST['email_address']);
    $firstname = tep_db_prepare_input($_POST['firstname']);
    $log_times = $_POST['log_times'] + 1;
    if ($log_times >= 4) {
        tep_session_register('password_forgotten');
    }

// Check if email exists
    $check_admin_query = tep_db_query("select admin_id as check_id, admin_firstname as check_firstname, admin_lastname as check_lastname, admin_email_address as check_email_address from " . TABLE_ADMIN . " where admin_email_address = '" . tep_db_input($email_address) . "'");
    if (!tep_db_num_rows($check_admin_query)) {
        $_GET['login'] = 'fail';
    } else {
        $check_admin = tep_db_fetch_array($check_admin_query);
        if ($check_admin['check_firstname'] != $firstname) {
            $_GET['login'] = 'fail';
        } else {
            $_GET['login'] = 'success';

            function randomize() {
                $salt = "ABCDEFGHIJKLMNOPQRSTUVWXWZabchefghjkmnpqrstuvwxyz0123456789";
                srand((double)microtime() * 1000000);
                $i = 0;

                while ($i <= 7) {
                    $num = rand() % 33;
                    $tmp = substr($salt, $num, 1);
                    $pass = $pass . $tmp;
                    $i++;
                }
                return $pass;
            }

            $makePassword = randomize();

            tep_mail($check_admin['check_firstname'] . ' ' . $check_admin['admin_lastname'], $check_admin['check_email_address'], ADMIN_EMAIL_SUBJECT, sprintf(ADMIN_EMAIL_TEXT, $check_admin['check_firstname'], HTTP_SERVER . DIR_WS_ADMIN, $check_admin['check_email_address'], $makePassword, STORE_OWNER), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            tep_db_query("update " . TABLE_ADMIN . " set admin_password = '" . tep_encrypt_password($makePassword) . "' where admin_id = '" . $check_admin['check_id'] . "'");
        }
    }
}

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE; ?></title>
    <!-- including css -->
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>material/libs/assets/animate.css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>material/libs/assets/font-awesome/css/font-awesome.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>material/libs/assets/simple-line-icons/css/simple-line-icons.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>material/libs/jquery/bootstrap/dist/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>material/css/font.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>material/css/app.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>solomono/css/solomono.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>javascript/OverlayScrollbars/OverlayScrollbars.min.css" type="text/css"/>
    <?php if($menu_location != '0'){ ?>
<!--        <link rel="stylesheet" href="--><?php //echo DIR_WS_INCLUDES; ?><!--solomono/css/menu_left.css" type="text/css"/>-->
        <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>solomono/css/menu_left1.css" type="text/css"/>
    <?php } else { ?>
        <link rel="stylesheet" href="<?php echo DIR_WS_INCLUDES; ?>solomono/css/menu_horizontal.css" type="text/css"/>
    <?php } ?>
    <meta name="theme-color" content="#efc600" />
</head>
<body class="admin_page <?=(!isMobile() && $menu_location==='1'?'open_sidebar':'');?>">
<?php
// push constants to JS:
if(!empty($array_for_js)) {
  foreach($array_for_js as $js_k => $js_val) $for_js .= 'const '.$js_k.' = "'.$js_val.'"; ';
  echo '<script>'.$for_js.'</script>';
}
?>
<div class="app app-header-fixed ">
    <?php echo tep_draw_form('login', FILENAME_PASSWORD_FORGOTTEN, 'action=process'); ?>
    <div class="modal-over bg-black">
        <div style="text-align: center;position: relative;top: 37%;"><?php echo HEADING_PASSWORD_FORGOTTEN;?></div>
        <div class="modal-center animated fadeInUp text-center loginModal">
            <div class="thumb-lg">
                <img src="includes/solomono/img/logo_crown_big.png" class="img-circle">
            </div>
            <?php if ($_GET['login'] == 'fail'): ?>
                <p class="login_heading m-t">
                    <?php echo TEXT_FORGOTTEN_ERROR;?>
                </p>
            <?php elseif ($_GET['login'] == 'success'): ?>
                <p class="login_heading m-t">
                    <?php echo TEXT_FORGOTTEN_SUCCESS;?>
                </p>
            <?php endif; ?>
            <?php if (tep_session_is_registered('password_forgotten')): ?>
                <p class="login_heading m-t">
                    <?php echo TEXT_FORGOTTEN_FAIL;?>
                </p>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-6 m-t">
                    <input class="form-control text-sm btn-rounded no-border email_address" placeholder="<?php echo ENTRY_EMAIL_ADDRESS;?>" name="email_address">
                    <span class="input-group-btn"></span>
                </div>
                <div class="col-md-6 m-t">
                    <div class="input-group">
                        <input class="form-control text-sm btn-rounded no-border" placeholder="<?php echo ENTRY_FIRSTNAME;?>" name="firstname" type="text">
                        <span class="input-group-btn">
                      <button style="padding: 9px 14px;" type="submit" class="btn btn-success btn-rounded submitButton"><i class="fa fa-arrow-right"></i></button>
                    </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right m-t">
                    <?php echo '<a class="sub" href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '">' . IMAGE_BACK . '</a>'; ?>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
</body>
</html>