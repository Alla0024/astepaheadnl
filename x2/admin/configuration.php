<?php
/*
  $Id: configuration.php,v 1.2 2003/09/24 13:57:05 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

header("Cache-control: private, no-cache");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); # Past date
header("Pragma: no-cache");

define('HIDE_CONFIGURATION_SORT_ORDER', 9999);
define('SET_REDIRECT_WWW_ID', 7271);

require('includes/application_top.php');

if (is_file($autoload = DIR_FS_CATALOG . 'includes/classes/Chronicler/autoload.php')) {
    require_once $autoload;
    unset($autoload);
}

use SoloMono\Chronicler\LoggerFactory;
$logger = null;
if (class_exists('SoloMono\Chronicler\LoggerFactory')) {
    $logger = LoggerFactory::create()->createLogger("adminloger/admin_action", "index");
}

$groupConfigurationKeys = getGroupConfigurationKeys();
$subConfigurationNames = getAllSubConfiguration($languages_id);

if($_GET['db'] == 1 && !empty($_POST['sortOrder']) && !empty($_POST['groupId'])) {
    $sortOrder = tep_db_prepare_input($_POST['sortOrder']);
    $groupId = tep_db_prepare_input($_POST['groupId']);
    $subGroupId = tep_db_prepare_input($_POST['subGroupId']);

    foreach ($sortOrder as $cId => $sortOrderValue) {
        $groupIdValue = $groupId[$cId];
        $subGroupIdValue = $subGroupId[$cId];
        tep_db_query("
            UPDATE " . TABLE_CONFIGURATION . "
            SET sort_order = '{$sortOrderValue}'
              , configuration_group_id = '{$groupIdValue}'
              , configuration_subgroup_id = '{$subGroupIdValue}'
            WHERE configuration_id = '{$cId}'
        ");
    }
}

$tooltipMap = [
    'STORE_OWNER_EMAIL_ADDRESS'       => TOOLTIP_CONFIGURATION_MAIN_EMAIL,
    'SEND_EXTRA_ORDER_EMAILS_TO'      => TOOLTIP_CONFIGURATION_ORDER_COPY_EMAIL,
    'CONTACT_US_LIST'                 => TOOLTIP_CONTACT_US_EMAIL,
    'STORE_COUNTRY'                   => TOOLTIP_STORE_COUNTRY,
    'STORE_ZONE'                      => TOOLTIP_STORE_REGION,
    'STORE_ADDRESS'                   => TOOLTIP_CONTACT_ADDRESS,
    'MIN_ORDER'                       => TOOLTIP_MINIMUM_ORDER,
    'MASTER_PASS'                     => TOOLTIP_MASTER_PASSWORD,
    'DISPLAY_PRICE_WITH_TAX'          => TOOLTIP_SHOW_PRICE_WITH_TAX,
    'DISPLAY_PRICE_WITH_TAX_CHECKOUT' => TOOLTIP_CALCULATE_TAX,
    'GUEST_DISCOUNT'                  => TOOLTIP_EXTRA_PRICE,
    'XPRICES_NUM'                     => TOOLTIP_PRICES_COUNT,
    'ALLOW_GUEST_TO_SEE_PRICES'       => TOOLTIP_SHOW_PRICE_TO_NOT_AUTHORIZED_CUSTOMER,
    'LOGO_IMAGE'                      => TOOLTIP_LOGO,
    'WATER_MARK'                      => TOOLTIP_WATERMARK,
    'FAVICON_IMAGE'                   => TOOLTIP_FAVICON,
    'STOCK_CHECK'                     => TOOLTIP_AUTO_STOCK,
    'STOCK_SHOW_BUY_BUTTON'           => TOOLTIP_DISABLED_BUY_BUTTON_FOR_ZERO_STOCK,
    'STOCK_LIMITED'                   => TOOLTIP_STOCK_AUTO_INCREMENT,
    'STOCK_ALLOW_CHECKOUT'            => TOOLTIP_ALLOW_ZERO_STOCK_ORDER,
    'STOCK_MARK_PRODUCT_OUT_OF_STOCK' => TOOLTIP_MARK_ZERO_STOCK_PRODUCT,
    'SMS_TEXT'                        => TOOLTIP_SMS_TEXT,
    'SMS_LOGIN'                       => TOOLTIP_SMS_LOGIN,
    'SMS_PASSWORD'                    => TOOLTIP_SMS_PASSWORD,
    'SMS_SIGN'                        => TOOLTIP_SMS_CODE_1,
    'SMS_ENC'                         => TOOLTIP_SMS_CODE_2,
];

// #CP - local dir to the template directory where you are uploading the company logo
$template_query = tep_db_query("
    SELECT `configuration_id`
         , `configuration_title`
         , sort_order
         , configuration_group_id
         , `configuration_value` 
         , `depends_on` 
         , `configuration_subgroup_id` 
    FROM " . TABLE_CONFIGURATION . " 
    WHERE `configuration_key` = 'DEFAULT_TEMPLATE'
");
$template = tep_db_fetch_array($template_query);
$CURR_TEMPLATE = $template['configuration_value'] . '/';

$upload_fs_dir = DIR_FS_TEMPLATES . $CURR_TEMPLATE . DIR_WS_IMAGES;
$upload_ws_dir = DIR_WS_TEMPLATES . $CURR_TEMPLATE . DIR_WS_IMAGES;
// #CP
$action = (isset($_GET['action']) ? $_GET['action'] : '');
$allowed_image_types = ['image/jpeg','image/gif','image/png','image/webp','image/vnd.microsoft.icon','image/x-icon'];
if (tep_not_null($action)) {
    $minify_query = tep_db_query("SELECT configuration_value as v FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MINIFY_CSSJS'");
    $minify_status_value = $minify_query->num_rows ? tep_db_fetch_array($minify_query)['v'] : false;
    switch ($action) {
        case 'setflag':
            $r_flag = '';
            $prefix = '';

            $newHref = $_POST['href'];

            if ($_GET['flag'] == '0') {
                $r_flag = 'false';
                $newHref = preg_replace('#flag=[1|0]#is', 'flag=1', $newHref);
            } elseif ($_GET['flag'] == '1') {
                $r_flag = 'true';
                $newHref = preg_replace('#flag=[1|0]#is', 'flag=0', $newHref);
            } else if (strpos($_GET['flag'], 'false') !== false) {
                $tmp = explode(':', $_GET['flag']);
                $r_flag = $tmp[1];
                $prefix = $tmp[0] . ':';

                $newHref = str_replace('flag=' . $prefix . $r_flag, 'flag=' . $prefix . 'true', $newHref);
            } elseif (strpos($_GET['flag'], 'true') !== false) {
                $tmp = explode(':', $_GET['flag']);
                $r_flag = $tmp[1];
                $prefix = $tmp[0] . ':';
                $newHref = str_replace('flag=' . $prefix . $r_flag, 'flag=' . $prefix . 'false', $newHref);
            } else $r_flag = $_GET['flag'];

        if($_GET['cID']==1908){#watermark id 1908
            rewriteHtaccess($r_flag,'#WATERMARK',3);
            if (file_exists($rootPath.'/images/cache')) rrmdir($rootPath.'/images/cache');
        }elseif ($_GET['cID']==2013){#set https id 2013
            rewriteHtaccess($r_flag,'#HTTPS',8);
            $reverseFlag = $r_flag === 'true' ? 'false' : 'true';
            rewriteHtaccess($reverseFlag,'#HTTP',8);
            $configuration_query = tep_db_query("
                SELECT `configuration_value` as cv
                FROM " . TABLE_CONFIGURATION . "
                WHERE `configuration_id` = 1919
            ");
            $robotsStatus = tep_db_fetch_array($configuration_query);
            rewriteRobots($robotsStatus['cv']);
        }elseif ($_GET['cID']==1919){#robots txt id 1919
            rewriteRobots($r_flag);
        }elseif ($_GET['cID']==2347){#seo filters id 2347
            rewriteHtaccess($r_flag,'#SEOFILTERS',1);
        }elseif ($_GET['cID']==1856&&$r_flag=="false"){#auth
            tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = 'false', last_modified = now() where configuration_key IN ('GOOGLE_OAUTH_STATUS','FACEBOOK_AUTH_STATUS')");
        }

        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . $prefix . $r_flag . "', last_modified = now() where configuration_id = '" . (int)$_GET['cID'] . "'");

        if ($logger) {
            $logger->debug("Message = {message} ", ["message" => date(PHP_DATE_TIME_FORMAT) . ' ' . $_GET['cID'] . ' ' . $r_flag]);
        }

        //set Minify Now for all actions with param need_minify = 1
        if (isset($_GET['need_minify']) && $_GET['need_minify'] == 1 && $minify_status_value == '2'){
            resetMinifiedFiles();
        }

            if (isset($tmp[0]) && count($tmp) == 2 && $tmp[0] == 'promurl'){
                //minify js again if changed urls
                resetMinifiedFiles();
                $flag = $r_flag == 'true' ? true : false;
                rewriteHtaccess(!$flag,'#SOLOMONO',11);
                rewriteHtaccess($flag,'#PROM',3);

            }
            if($_GET['cID'] == 2472){#on/off GOOGLE AUTH
                resetMinifiedFiles();
            }
            print json_encode(array(
                'result' => 'success',
                'check' => $r_flag,
                'newHref' => $newHref,
            ));
            exit;

            tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID));
            break;
        case 'save':

            $configuration_value = tep_db_prepare_input($_POST['configuration_value']);

            $newHref = $_POST['flagHref'];
            $r_flag = '';
            if (strpos($configuration_value, 'false') !== false) {
                $tmp = explode(':', $configuration_value);
                $r_flag = $tmp[1];
                $prefix = $tmp[0] . ':';

                $newHref = str_replace('flag=' . $prefix . $r_flag, 'flag=' . $prefix . 'true', $newHref);
            } else if (strpos($configuration_value, 'true') !== false) {
                $tmp = explode(':', $configuration_value);
                $r_flag = $tmp[1];
                $prefix = $tmp[0] . ':';
                $newHref = str_replace('flag=' . $prefix . $r_flag, 'flag=' . $prefix . 'false', $newHref);
            } else if ($configuration_value === 'true') {
                $newHref = str_replace('flag=1', 'flag=0', $newHref);
            } else if ($configuration_value === 'false') {
                $newHref = str_replace('flag=0', 'flag=1', $newHref);
            }

            /* One Page Checkout - BEGIN*/
            if ($_GET['gID'] == 7575) {
                tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . $_POST['configuration_value'] . "', last_modified = now() where configuration_id = '" . (int)$_GET['cID'] . "'");
                //tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $_GET['cID']));
            } else {

                $cID = tep_db_prepare_input($_GET['cID']);
                tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");

                // change robots.txt (08.12.16)
                //if ($cID == 1919) {
                if (isset($_GET['configuration_key']) && $_GET['configuration_key'] == 'ROBOTS_TXT') {
                    file_put_contents(DIR_FS_CATALOG . "robots.txt", $configuration_value);
                }
            }

            if ($logger) {
                $logger->debug("Message = {message} ", ["message" => date(PHP_DATE_TIME_FORMAT) . ' ' . $_GET['cID'] . ' ' . $_POST['configuration_value']]);
            }

            // Configuration Cache modification start
            //        require ('includes/configuration_cache.php');
            // Configuration Cache modification end
            print json_encode(array(
                'result' => 'success',
                'configuration_value' => (empty($r_flag) == true ? $configuration_value : $r_flag),
                'newHref' => $newHref
            ));

            exit;

            tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID));
            break;
        case 'tableSave':
            $cID = tep_db_prepare_input($_GET['cID']);
            $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
            $configuration_value = htmlentities($configuration_value);
            //if ($cID == 2012 || $cID == 2014 || $cID == 2029) {
            if (isset($_GET['use_function']) && $_GET['use_function'] == 'file_upload') {
                //$configuration_value = tep_db_prepare_input($_FILES['configuration_value']['name']);
                //$path = '/images/';
                if(!in_array($_FILES['configuration_value']['type'],$allowed_image_types)){
                    $_FILES['configuration_value']['error'] = 1;
                }
                $path = DIR_FS_CATALOG . 'images/';// директория для загрузки
                $ext_prep = explode('.', $_FILES['configuration_value']['name']); // расширение
                $ext = array_pop($ext_prep); // расширение
                $new_name = $_FILES['configuration_value']['name']; // новое имя с расширением
                if ($_GET['configuration_key'] == 'WATER_MARK') {
                    $new_name = 'watermark.png';
                }

                $full_path = $path . $new_name; // полный путь с новым именем и расширением
                $configuration_value = 'images/' . $new_name;
                if ($_FILES['configuration_value']['error'] == 0) {
                    if ($_GET['configuration_key'] == 'FAVICON_IMAGE' && $_FILES['configuration_value']['type'] != 'image/x-icon') {
                        $new_name = 'favicon.ico';
                        $configuration_value = 'images/' . $new_name;
                        $full_path = $path . $new_name; // полный путь с новым именем и расширением
                        $source = $_FILES['configuration_value']['tmp_name'];
                        $destination = $full_path;

                        require_once(DIR_WS_CLASSES . 'class-php-ico.php');

                        $ico_lib = new PHP_ICO( $source , [[16,16],[24,24],[32,32]]);
                        $ico_lib->save_ico( $destination );
                    }else {
                        if (!file_exists($path)) mkdir($path,'0644');
                        if (move_uploaded_file($_FILES['configuration_value']['tmp_name'], $full_path)) {
                            // Если файл успешно загружен, то вносим в БД (надеюсь, что вы знаете как)
                            // Можно сохранить $full_path (полный путь) или просто имя файла - $new_name

                        }
                    }
                }
            }
            if(is_array($configuration_value)){
                $configuration_value = serialize($configuration_value);
            }

            // if enable or disable google tag manager re-minify JavaScript
            if($_GET['configuration_key'] == "GOOGLE_ANALYTICS_AND_TAGS_MODULE_ENABLED") {
                resetMinifiedFiles();
            }

            if (SET_REDIRECT_WWW_ID === (int)$cID) {
                /**
                 * 0 - Отключить любые перенаправления с www
                 * 1 - Перенаправить из www на  no-www
                 * 2 - Перенаправить из no-www на www
                 */

                //Получаем текущий урл, для дальнейшего теста перенаправлений
                $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
                //Текущее состояние перенаправления www, в случае зацикленности - для возврата в рабочее состояние
                $currentValueQuery = tep_db_query("
                    SELECT `configuration_value` as val
                    FROM " . TABLE_CONFIGURATION . " 
                    WHERE `configuration_id` = '" . (int)$cID . "'
                ");
                $currentValue = tep_db_fetch_array($currentValueQuery);
                //Перезаписать htaccess
                redirectWWW($configuration_value);
                //Проверить на предел перенаправления
                if (isRedirectionLimitReached($url)) {
                    //возвращаем старое значение
                    redirectWWW($currentValue['val']);
                    $configuration_value = 3;
                } else {
                    tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");
                }

                print tep_cfg_read_redirect_www($configuration_value);
                exit;
            } else {
                tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");
            }

            if ($logger) {
                $logger->debug("Message = {message} ", ["message" => date(PHP_DATE_TIME_FORMAT) . ' ' . $cID . ' ' . $configuration_value]);
            }

            /* One Page Checkout - BEGIN*/
            if ($_GET['gID'] == 7575) {
                //tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $_GET['cID']));
            } elseif (isset($_GET['configuration_key']) && $_GET['configuration_key'] == 'ROBOTS_TXT') {
                // change robots.txt (08.12.16)
                file_put_contents(DIR_FS_CATALOG . "robots.txt", $configuration_value);
            }

            /*
             * tep_call_function missing fix
             */
            $configuration_query = tep_db_query("
                SELECT `configuration_id`
                     , `configuration_title`
                     , `set_function`
                     , sort_order
                     , configuration_group_id
                     , `configuration_key`
                     , `configuration_value`
                     , `use_function` 
                     , `depends_on` 
                     , `configuration_subgroup_id` 
                FROM " . TABLE_CONFIGURATION . " 
                WHERE `configuration_id` = '" . (int)$cID . "'
            ");
            while ($configuration = tep_db_fetch_array($configuration_query)) {
                if (tep_not_null($configuration['use_function'])) {
                    $use_function = $configuration['use_function'];
                    if (preg_match('/->/', $use_function)) {
                        $class_method = explode('->', $use_function);

                        if (!is_object(${$class_method[0]})) {
                            include(DIR_WS_CLASSES . $class_method[0] . '.php');
                            ${$class_method[0]} = new $class_method[0]();
                        }

                        $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
                    } else {
                        $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);

                    }
                } else {
                    $cfgValue = $configuration['configuration_value'];

                }
                //comment
                //if ($cID == 2012 || $cID == 2014 || $cID == 2029) {
                if (isset($_GET['use_function']) && $_GET['use_function'] == 'file_upload') {
                    $configuration_value = $cfgValue;
                }
            }


            print $configuration_value;

            exit;

            break;
        case 'tableEdit':

            $cID = tep_db_prepare_input($_GET['cID']);

            if (!empty($cID)) {

                $cfg_extra_query = tep_db_query("
                    select configuration_key
                         , configuration_value
                         , configuration_id
                         , configuration_description
                         , date_added
                         , last_modified
                         , use_function
                         , set_function 
                         , depends_on 
                         , configuration_subgroup_id 
                    from " . TABLE_CONFIGURATION . " 
                    where configuration_id = '" . (int)$cID . "'
                ");

                $cfg_extra = tep_db_fetch_array($cfg_extra_query);

                $cInfo_array = $cfg_extra;
                $cInfo = new objectInfo($cInfo_array);


                if ($cInfo->set_function) {
                    eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
                    $value_field .= tep_draw_hidden_field('configuration_key', $cInfo->configuration_key);
                } elseif ($cInfo->use_function == 'file_upload') {
                    $value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value, 'class="editable-input form-control input-sm"', false, 'file');
                    $value_field .= tep_draw_hidden_field('use_function', $cInfo->use_function);
                    $value_field .= tep_draw_hidden_field('configuration_key', $cInfo->configuration_key);
                } else {
                    $value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value, 'class="editable-input form-control input-sm"');
                }

                $href = preg_replace('#action=.*#', 'action=tableSave', $_SERVER['REQUEST_URI']);

                $value_field = '
                <form enctype="multipart/form-data" method="POST">
                <span class="editable-wrap editable-text">
                  <div class="editable-controls form-group">
                    ' . html_entity_decode($value_field) . '
                  </div>
                </span>
                <button type="submit" class="btn btn-sm btn-info tableEditSave" data-href="' . $href . '">
                ' . TEXT_SETTINGS_EDIT_FORM_SAVE . '
                </button>
                <button type="button" class="btn btn-sm btn-default tableEditCancel">
                ' . TEXT_SETTINGS_EDIT_FORM_CANCEL . '
                </button>
                </form>

              ';
                print html_entity_decode($value_field);
            }
            exit;
            break;
        case 'edit':
            $cID = tep_db_prepare_input($_GET['cID']);

            if (!empty($cID)) {

                $cfg_extra_query = tep_db_query("
                    SELECT `configuration_key`
                         , sort_order
                         , configuration_group_id
                         , `configuration_value`
                         , `configuration_id`
                         , `configuration_description`
                         , `date_added`
                         , `last_modified`
                         , `use_function`
                         , `set_function` 
                         , `depends_on` 
                         , `configuration_subgroup_id` 
                    FROM " . TABLE_CONFIGURATION . " 
                    WHERE `configuration_id` = '" . (int)$cID . "'
                ");

                $cfg_extra = tep_db_fetch_array($cfg_extra_query);

                $cInfo_array = $cfg_extra;
                $cInfo = new objectInfo($cInfo_array);

                $heading = array();
                $contents = array();

                if (defined(strtoupper($cInfo->configuration_key . '_DESC'))) $r_desc = constant(strtoupper($cInfo->configuration_key . '_DESC')); else $r_desc = '';


                if (isset($cInfo) && is_object($cInfo)) {
                    $heading[] = array('text' => '<b>' . constant(strtoupper($cInfo->configuration_key . '_TITLE')) . '</b>');
                }

                if ($cInfo->set_function) {
                    eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
                } else {
                    $value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value, 'class="form-control input-sm"');
                }

                /* One Page Checkout - BEGIN */
                if ($cInfo->set_function && $_GET['gID'] == 7575) {
                    eval('$value_field = ' . $cInfo->set_function . '"' . $cInfo->configuration_value . '");');
                }
                /* One Page Checkout - END */
                $contents = array('form' => tep_draw_form('configuration', FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=save'));

                $contents[] = array('text' => $cInfo->configuration_description . '<br>' . $r_desc . '<br>' . $value_field);

                /**
                 *  виводимо інфу про останнє редагування налаштувань
                 */

                if (isset($cInfo) && is_object($cInfo)) {

                    $time_info = '<span class="modalModifiedInfo">';
                    $time_info .= TEXT_INFO_DATE_ADDED . ' ' . tep_date_short($cInfo->date_added);
                    if (tep_not_null($cInfo->last_modified)) $time_info .= ' | ' . TEXT_INFO_LAST_MODIFIED . ' ' . tep_date_short($cInfo->last_modified);

                    $time_info .= '</span>';
                    $contents[] = array('text' => $time_info);

                }

                ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo TEXT_CLOSE_BUTTON; ?>">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="editModalLabel"><?php print $heading[0]['text']; ?></h4>
                    </div>
                    <?php
                    if (tep_not_null($contents)) {
                        $box2 = new box;
                        echo $box2->infoBoxModal($contents);
                    }
                    ?>
                </div>
                <?php
                exit;
            }
            break;
        // #CP - supporting functions to upload company logo to template images directory
        case 'processuploads':

            if (isset($GLOBALS['file_name']) && tep_not_null($GLOBALS['file_name'])) {

                $up_load = new upload('file_name', $upload_fs_dir);
                $file_name = $up_load->filename;

                if ($file_name != "logo.gif") {
                    unlink($upload_fs_dir . "logo.gif");
                    rename($upload_fs_dir . $file_name, $upload_fs_dir . "logo.gif");
                }
            }

            tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID']));
            break;
        case 'upload':
            $directory_writeable = true;
            if (!is_writeable($upload_fs_dir)) {
                $directory_writeable = false;
                $messageStack->add(sprintf(ERROR_DIRECTORY_NOT_WRITEABLE, $upload_fs_dir), 'error');
            }
            break;
    }
    // #CP
}

$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 1;

$cfg_group_query = tep_db_query("
    select configuration_group_key
         , configuration_group_title 
    from " . TABLE_CONFIGURATION_GROUP . " 
    where configuration_group_id = '" . (int)$gID . "'
");
$cfg_group = tep_db_fetch_array($cfg_group_query);


// check if the template image directory exists
if (is_dir($upload_fs_dir)) {
    if (!is_writeable($upload_fs_dir)) $messageStack->add(ERROR_TEMPLATE_IMAGE_DIRECTORY_NOT_WRITEABLE . $upload_fs_dir, 'error');
} else {
    $messageStack->add(ERROR_TEMPLATE_IMAGE_DIRECTORY_DOES_NOT_EXIST . $upload_fs_dir, 'error');
}

?>

<?php
if ((int)$gID == 500) {
    $configuration_query = tep_db_query("
        select configuration_id
             , configuration_title
             , set_function
             , configuration_key
             , sort_order
             , configuration_group_id
             , configuration_value
             , use_function 
             , depends_on 
             , configuration_subgroup_id 
        from " . TABLE_CONFIGURATION . " 
        where configuration_group_id = '" . (int)$gID . "' 
        order by configuration_subgroup_id
               , configuration_title
    ");
} else {
    $configuration_query = tep_db_query("
        select configuration_id
             , configuration_title
             , set_function
             , configuration_key
             , sort_order
             , configuration_group_id
             , configuration_value
             , use_function 
             , depends_on 
             , configuration_subgroup_id 
             , callback_func 
        from " . TABLE_CONFIGURATION . " 
        where configuration_group_id = '" . (int)$gID . "' 
        order by configuration_subgroup_id,
                 sort_order
    ");
}

?>

<?php
include_once('html-open.php');
include_once('header.php');
?>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
    <div class="modal-dialog modal-lg" role="document"></div>
</div><!-- content -->

        <div class="container app-content-body p-b-none">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <div class="wrapper_767">
                        <div class="bg-light lter ng-scope">
                            <h1 class="m-n font-thin h3">
                                <?php

                                if (in_array((int)$gID, ['277', '333'])) { //277 - solo modules, 333 - Google Analytics modules
                                    $cfg_group['configuration_group_key'] = 'TEXT_MENU_SITE_MODULES';

                                    $path_to_modules = DIR_FS_EXT; // Путь к папке с модулями
                                    $check_file_name = 'check.php'; // Дефолтное название файла проверки в каждом модуле

                                    $module_folders = scandir($path_to_modules);

                                    foreach ($module_folders as $module_folder) {
                                        if(stristr($module_folder, '.')) continue; // Пропускаем системные папки
                                        elseif (!is_dir($path_to_modules . $module_folder)) continue; // пропускаем файлы
                                        $module_check_file = $path_to_modules . $module_folder . '/' . $check_file_name;
                                        if(file_exists($module_check_file)) {
                                            require $module_check_file;
                                        }

                                    }

                                    if($module_installed) echo "<script>location.reload();</script>"; // JS page reload

                                }

                                if (isset($groupConfigurationKeys[$gID])) {
                                    echo constant(strtoupper($groupConfigurationKeys[$gID]));
                                } else {
                                    echo "&nbsp;";
                                }


                                $configurations = [];
                                while ($configuration = tep_db_fetch_array($configuration_query)) {
                                    $configurations[$configuration['configuration_subgroup_id']][] = $configuration;
                                }

                                ?>
                            </h1>
                        </div>
                    </div>

                    <div>

                        <?php if ($login_email_address == "admin@solomono.net" && isset($_GET['db']) && $_GET['db'] == 1): ?>
                        <form action="<?= tep_href_link(FILENAME_CONFIGURATION, tep_get_all_get_params('')) ?>" method="POST">
                            <input type="submit" style="display:none;">
                            <?php endif; ?>

                        <?php foreach ($configurations as $subConfigurationId => $subConfigurations): ?>

                            <?php if(!empty($subConfigurationNames[$subConfigurationId])): ?>
                                <h4>
                                    <?=$subConfigurationNames[$subConfigurationId]?>
                                </h4>
                            <?php endif; ?>

                        <div class="panel panel-default">
<!--                            <div class="table-responsive">-->
                                <table class="table table-bordered table-hover table-condensed bg-white-only b-t b-light configuration-table">
                                    <thead>
                                    <tr>
                                        <?php if ($login_email_address == "admin@solomono.net" && isset($_GET['db']) && $_GET['db'] == 1): ?>
                                            <th style="width: 10px;">Config group</th>
                                            <th style="width: 10px;">Config sub-group</th>
                                            <th style="width: 10px;">Sort order</th>
                                        <?php endif; ?>
                                        <th><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></th>
                                        <th><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($subConfigurations as $configuration) {

                                        if (($configuration['sort_order'] == HIDE_CONFIGURATION_SORT_ORDER && $login_email_address != "admin@solomono.net") ||
                                            ($configuration['depends_on'] !== '' && !isExtensionExist($configuration['depends_on']))) {
                                            continue;
                                        }

                                        if (tep_not_null($configuration['use_function'])) {
                                            $use_function = $configuration['use_function'];
                                            if (preg_match('/->/', $use_function)) {
                                                $class_method = explode('->', $use_function);

                                                if (!is_object(${$class_method[0]})) {
                                                    include(DIR_WS_CLASSES . $class_method[0] . '.php');
                                                    ${$class_method[0]} = new $class_method[0]();
                                                }
                                                $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
                                            } else {
                                                $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
                                            }
                                        } else {
                                            $cfgValue = $configuration['configuration_value'];

                                        }

                                        if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                                            $cfg_extra_query = tep_db_query("
                                                select configuration_key
                                                     , sort_order
                                                     , configuration_group_id
                                                     , configuration_description
                                                     , date_added
                                                     , last_modified
                                                     , use_function
                                                     , set_function 
                                                     , depends_on 
                                                     , configuration_subgroup_id 
                                                from " . TABLE_CONFIGURATION . " 
                                                where configuration_id = '" . (int)$configuration['configuration_id'] . "'
                                            ");
                                            $cfg_extra       = tep_db_fetch_array($cfg_extra_query);

                                            $cInfo_array = array_merge($configuration, $cfg_extra);
                                            $cInfo       = new objectInfo($cInfo_array);
                                        }

                                        if ((isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id)) {

                                            if ($cInfo->set_function == 'file_upload') {
                                                echo '      <tr class="dataTableRowSelected" data-href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=upload') . '\'>' . "\n";
                                            } else {
                                                echo '      <tr class="dataTableRowSelected" data-href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=tableEdit') . '\'>' . "\n";
                                            }
                                        } else {
                                            $style = $configuration['configuration_value'] == 'false' && $configuration['configuration_id'] == 1919 ? ' style="background: #ff1f1f47;"' : '';
                                            echo '
                                  <tr' . $style . ' class="dataTableRowSelected" data-href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id']) . '&action=tableEdit\'>';
                                        }
                                        ?>

                                        <?php if ($login_email_address == "admin@solomono.net" && isset($_GET['db']) && $_GET['db'] == 1): ?>
                                            <td><input style="width: 80px;" type="number"
                                                       name="groupId[<?= $configuration['configuration_id'] ?>]"
                                                       value="<?= $configuration['configuration_group_id'] ?>"></td>
                                            <td><input style="width: 80px;" type="number"
                                                       name="subGroupId[<?= $configuration['configuration_id'] ?>]"
                                                       value="<?= $configuration['configuration_subgroup_id'] ?>"></td>
                                            <td><input style="width: 80px;" type="number"
                                                       name="sortOrder[<?= $configuration['configuration_id'] ?>]"
                                                       value="<?= $configuration['sort_order'] ?>"></td>
                                        <?php endif; ?>
                                        <?php
                                            $criticalCssBool = (($criticalCssValue && $configuration['callback_func'] == 'setAndShowCriticalWindow') || $configuration['configuration_key'] == 'USE_CRITICAL_CSS');
                                            $confKeyConst = strtoupper($configuration['configuration_key']);
                                            $confKeyConstTitle = strtoupper($configuration['configuration_key'] . '_TITLE');
                                        ?>
                                        <td data-label="<?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?>"
                                            class="ajaxSpin v-middle">
                                            <?php
                                            echo defined($confKeyConstTitle) ? constant($confKeyConstTitle) : $confKeyConst;
                                            echo (!empty($tooltipMap[$confKeyConst]) ? renderTooltip($tooltipMap[$confKeyConst]) : '');
                                            if($criticalCssBool){ ?>
                                                <a class="critical-info" href="javascript:void(0);">
                                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= INFO_ICON_NEED_GENERATE_CRITICAL?>"></i>
                                                </a>
                                            <?php }
                                            if($configuration['callback_func'] == 'NEED_MINIFY'){ ?>
                                                <a class="critical-info" href="javascript:void(0);">
                                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= INFO_ICON_NEED_MINIFY?>"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                        <?php
                                        $critBoolRes = $criticalCssBool ? 'setAndShowCriticalWindow' : '' ;
                                        $redirectWWWRes = SET_REDIRECT_WWW_ID === (int)$configuration['configuration_id'] ? $configuration['configuration_key'] : '';
                                        $minifyRes = ($configuration['callback_func'] == 'NEED_MINIFY') ? 'NEED_MINIFY' : '';
                                        ?>
                                        <td data-label="<?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?>"
                                            class="setValue v-middle <?= $critBoolRes, ' ', $redirectWWWRes, ' ',$minifyRes, ' ', $configuration['configuration_key']?>"
                                            data-conf-key="<?=$configuration['configuration_key']?>"
                                        >
                                            <?php
                                            if ($confKeyConst == 'SET_HTTPS') { // if https:

                                                // CHECK SSL:
                                                $ssl_check = @fsockopen('ssl://' . $_SERVER['HTTP_HOST'], 443, $errno, $errstr, 30);
                                                $result    = !!$ssl_check;
                                                if ($ssl_check) fclose($ssl_check);

                                                if ($result) $https_enabled = '';
                                                else $https_enabled = 'style="pointer-events: none;"';
                                            } else $https_enabled = '';

                                            $needMinifyParam = $configuration['callback_func'] == 'NEED_MINIFY' ? '&need_minify=1' : '';
                                            $check_for_folders = explode(':', $cfgValue);
                                            if ($check_for_folders[1] == 'true') {
                                                echo '<label class="i-switch bg-info m-t-xs m-r" ' . $https_enabled . '>
                                                        <input type="checkbox" class="switchValue" checked="" data-href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id'] . $needMinifyParam. '&action=setflag&flag=' . $check_for_folders[0] . ':false') . '">
                                                        <i></i>
                                                      </label>';
                                            } else if ($check_for_folders[1] == 'false') {
                                                echo '<label class="i-switch bg-info m-t-xs m-r" ' . $https_enabled . '>
                                                        <input type="checkbox" class="switchValue" data-href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id'] . $needMinifyParam . '&action=setflag&flag=' . $check_for_folders[0] . ':true') . '">
                                                        <i></i>
                                                      </label>';
                                            } else if (htmlspecialchars($cfgValue) == 'true') {
                                                echo '<label class="i-switch bg-info m-t-xs m-r" ' . $https_enabled . '>
                                                        <input type="checkbox" class="switchValue" checked="" data-href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id'] . $needMinifyParam. '&action=setflag&flag=0') . '">
                                                        <i></i>
                                                      </label>';
                                            } else if (htmlspecialchars($cfgValue) == 'false') {
                                                echo '<label class="i-switch bg-info m-t-xs m-r" ' . $https_enabled . '>
                                                        <input type="checkbox" class="switchValue seo" data-href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id'] . $needMinifyParam. '&action=setflag&flag=1') . '">
                                                        <i></i>
                                                      </label>';
                                            } elseif (strpos($cfgValue, 'buyme') !== false) {
                                                echo $cfgValue;
                                            } elseif ($configuration['use_function'] == 'file_upload') {
                                                echo $cfgValue;
                                            } else if ($configuration['configuration_key'] == 'ROBOTS_TXT') {
                                                echo '<span class="editable" data-toggle="tooltip" data-placement="right" title="" data-original-title="' . TEXT_SETTINGS_EDIT_FORM_TOOLTIP . '">' . (htmlspecialchars(substr($cfgValue, 0, 20)) == '' ? TEXT_SETTINGS_EDIT_FORM_TOOLTIP : htmlspecialchars(substr($cfgValue, 0, 20))) . '</span>';
                                            } else {
                                                echo '<span class="editable" data-toggle="tooltip" data-placement="right" title="" data-original-title="' . TEXT_SETTINGS_EDIT_FORM_TOOLTIP . '">' . (htmlspecialchars($cfgValue) == '' ? TEXT_SETTINGS_EDIT_FORM_TOOLTIP : htmlspecialchars($cfgValue)) . '</span>';
                                            }

                                            ?>
                                        </td></tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
<!--                            </div>-->
                        </div>

                        <?php endforeach; ?>

                            <?php if ($login_email_address == "admin@solomono.net" && isset($_GET['db']) && $_GET['db'] == 1): ?>
                        </form>
                    <?php endif; ?>
                    </div>
                </div>
                <!-- / main -->
            </div>
        </div>

<?php

include_once('footer.php');
include_once('html-close.php');
?>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
