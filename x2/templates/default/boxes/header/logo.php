<!-- LOGO -->

<?php
       if (LOGO_IMAGE) $logo_str = '<img class="img-responsive lazyload custom-logo" alt="' . LOGO_IMAGE_TITLE . '" src="'.DIR_WS_IMAGES_CDN.'pixel_trans.png" data-src="' . HTTP_SERVER . '/' . str_replace("images/", "images/45x45/", LOGO_IMAGE) . '&r=x" width="35" height="35"/>';
       else $logo_str = '<p style="font-size: 18px;">' . STORE_NAME . '</p>';
       echo '<div class="logo"><a href="' . tep_href_link('/') . '">'.$logo_str.'</a></div>';
?>

<!-- END LOGO -->