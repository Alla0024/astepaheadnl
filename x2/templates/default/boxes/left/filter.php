<?php

if (($_GET['cPath'] != '' or isset($_GET['manufacturers_id']) or isset($_GET['keywords'])) and ATTRIBUTES_PRODUCTS_MODULE_ENABLED == 'true') {

    if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {
        require(DIR_WS_TEMPLATES . 'default/boxes/left/filter_seo.php');
    } else { ?>
        <div id="filters_box" class="box filter_box">
            <div class="filter_box_in">
                <?php

                $r_content = '';

                // show max price (for Price Range Filter):     //  " .$ifs. " deleted from query
                if ($all_pids_string != '') {

                    if (DISPLAY_PRICE_WITH_TAX == 'true') {
                        $listing_sql_max_query = tep_db_query("select MAX(`p`.`products_price` * ((100 + if(`p`.`products_tax_class_id` != 0,`tr`.`tax_rate`,0)) / 100)) AS `max_price`, MIN(`p`.`products_price` * ((100 + if(`p`.`products_tax_class_id` != 0,`tr`.`tax_rate`,0)) / 100)) AS `min_price` from " . TABLE_PRODUCTS . " p LEFT JOIN " . TABLE_TAX_RATES . " tr on p.products_tax_class_id = tr.tax_class_id where " . $all_pids_string . " ");
                    } else {
                        $listing_sql_max_query = tep_db_query("select MAX(p.products_price) as max_price, MIN(p.products_price) as min_price from " . TABLE_PRODUCTS . " p where " . $all_pids_string . " ");
                    }

                    $listing_sql_max = tep_db_fetch_array($listing_sql_max_query);
                    $listing_sql_max['max_price'] = ceil($ccr * $listing_sql_max['max_price']);  // with current_currency_rate
                    $listing_sql_max['min_price'] = floor($ccr * $listing_sql_max['min_price']);  // with current_currency_rate
                }

                // Price Range Filter:
                $r_content .= '<div class="dipcen">
                         <div><span class="filter_heading">' . COMP_PROD_PRICE . '</span></div>
                         <div id="slider-range"></div>
                         <span class="left slider-from">
                           <input type="number" min="0" class="input-type-number-custom" name="rmin" id="range1" value="' . (($_GET['rmin'] != '') ? $_GET['rmin'] : $listing_sql_max['min_price'] + 0) . '"  />
                         </span>
                         <span class="left slider-to">                                                                                                             
                           <input type="number" min="0" class="input-type-number-custom" name="rmax" id="range2" value="' . (($_GET['rmax'] != '') ? $_GET['rmax'] : $listing_sql_max['max_price'] + 0) . '" />
                         </span>
                         &nbsp;&nbsp;<span class="price_fltr">' . ($currencies->currencies[$currency]['symbol_left'] ? $currencies->currencies[$currency]['symbol_left'] : $currencies->currencies[$currency]['symbol_right']) . '</span>
                       </div>';

                // show max price value for js:
                $r_content .= '<input type="hidden" name="slider_max" value="' . ($listing_sql_max['max_price'] + 0) . '" />';
                $r_content .= '<input type="hidden" name="slider_min" value="' . ($listing_sql_max['min_price'] + 0) . '" />';
                $r_content .= '<div class="clear"></div>';

                // if checkbox "all" is checked
                if ($_GET['filter_id'] == '') $allchecked = 'checked';
                else $allchecked = '';

                // Show manufacturers of current category
                if (is_array($all_pids)) {
                    if (is_string($all_pids_string) && !empty(trim($all_pids_string))) {
                        $all_pids_string = " and " . $all_pids_string;
                    }
                }
                else $all_pids_string = '';

                $manuf_sql=tep_db_query("select distinct mi.manufacturers_id, mi.manufacturers_name 
    		                             from " . TABLE_PRODUCTS . " p left join " . TABLE_MANUFACTURERS_INFO . " mi on p.manufacturers_id = mi.manufacturers_id 
                            where mi.manufacturers_name !='' " . $all_pids_string . " and mi.languages_id = $languages_id order by mi.manufacturers_name");
                if (tep_db_num_rows($manuf_sql)) {
                    $r_content .= '<div class="attrib_divs ajax">
                          <div id="ajax_search_brands" class="block">
                            <div class="filter_heading">' . FILTER_BRAND . '</div>
                              <div class="inner-scroll">
                                <div class="item">
                                  <input class="filter_all" type="checkbox" id="brand_all" ' . $allchecked . ' name="filter_id[]" value="not" />
                                  <label for="brand_all">' . FILTER_ALL . '</label>
                                </div>';

                    while ($manufacturers_values = tep_db_fetch_array($manuf_sql)) {

                        // check if current manufacturer value is checked
                        $checked_manuf = '';
                        foreach (explode('-', $_GET['filter_id']) as $val) {
                            if ($manufacturers_values['manufacturers_id'] == $val) {
                                $checked_manuf = 'checked';
                            }
                        }


                        $r_content .= '<div class="item">';
                        $r_content .= '<input type="checkbox" id="brand_' . $manufacturers_values['manufacturers_id'] . '" name="filter_id[]" ' . $checked_manuf . ' value="' . $manufacturers_values['manufacturers_id'] . '" />
                                   <label for="brand_' . $manufacturers_values['manufacturers_id'] . '">' . $manufacturers_values['manufacturers_name'] . '</label>';
                        $r_content .= '</div>';

                    }

                    $r_content .= '</div></div></div>';
                }

                // Attribute Filter:
                $r_content .= '<div class="filter_cont" id="attribs" ><noindex>';

                // sort attributes values as natural
                if (is_array($attr_vals_array)) {
                    foreach ($attr_vals_array as $at_id => $at_vals) {
                        foreach ($at_vals as $at_val_id => $at_val_sort) $at_vals[$at_val_id] = $attr_vals_names_array[$at_val_id];
                        natsort($at_vals);
                        $attr_vals_array_tmp[$at_id] = $at_vals;
                    }
                    $attr_vals_array = $attr_vals_array_tmp;
                }

                if (is_array($attrs_array)) {
                    foreach ($attrs_array as $at_id) {
                        if (is_array($show_in_filter) and in_array($at_id, $show_in_filter)) {
                            //   if(!empty($counts_array_true[$at_id])) {
                            $r_content .= '<span class="filter_heading">' . $attr_names_array[$at_id] . '</span>';
                            $r_content .= '<div class="attrib_divs">';

                            // if checkbox "all" is checked
                            if ($_GET[$at_id] == '')
                                $allchecked = 'checked'; else $allchecked = '';

                            // output checkbox "all"
                            $r_content .= '<div class="item"><input class="filter_all" id="filter_all_' . $at_id . '" type="checkbox" ' . $allchecked . ' name="' . $at_id . '" value="not" /><label for="filter_all_' . $at_id . '">' . FILTER_ALL . '</label></div>';

                            // get all values for current option (attribute)
                            if (is_array($attr_vals_array[$at_id])) {
                                foreach ($attr_vals_array[$at_id] as $at_val_id => $at_val_name) {
                                    // check if current attribute value is checked
                                    $checked_attr = '';
                                    foreach (explode('-', $_GET[$at_id]) as $val) {
                                        if ($at_val_id == $val) {
                                            $checked_attr = 'checked';
                                        }
                                    }

                                    // output values of current attribute (option)
                                    //$counts_number = ' ('.$counts_array_true[$at_id][$at_val_id].')';
                                    //if($counts_array_true[$at_id][$at_val_id]!=0) {
                                    $r_content .= '<div class="item">
                                     <input id="' . $at_val_id . '2" type="checkbox" name="' . $at_id . '" ' . $checked_attr . ' value="' . $at_val_id . '" />
                                     <label for="' . $at_val_id . '2">' . $at_val_name . $counts_number . '</label>
                                   </div>';
                                    //}
                                }
                            }

                            $r_content .= '</div>';
                            //  }
                        }
                    }
                }

                $r_content .= ' 
            </noindex>';
                $reset = "<a rel='nofollow' href='" . getFilterUrl(
                        $_GET['cPath']
                    ) . "'>reset</a>";
          $r_content.='</div>';

                echo $r_content;
                ?>
            </div>
        </div>
    <?php }
}
?>