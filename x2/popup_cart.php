<?php
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') require('includes/application_top.php');
    if(!function_exists('includeLanguages')){
        header("HTTP/1.0 301 Moved Permanently");
        header("Location: 404.php");
        die;
    }
    includeLanguages(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOPPING_CART);

    $any_out_of_stock = 0;
    $disabled = '';
    $stock_text = '';
    $products = $cart->get_products();
    $html = [];
    $skipTemplates = [
        'gadgetorio',
        'solo_cellphones',
        'solo_health',
    ];

    foreach ($products as $product) {
        // Push all attributes information in an array
        if (isset($product['attributes']) && is_array($product['attributes'])) {
            foreach($product['attributes'] as $option => $value) {
                $attributes = tep_db_query("SELECT  popt.products_options_name, 
                                                    poval.products_options_values_name, 
                                                    pa.options_values_price, 
                                                    pa.price_prefix
                                               FROM " . TABLE_PRODUCTS_OPTIONS . " popt, " .
                    TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " .
                    TABLE_PRODUCTS_ATTRIBUTES . " pa
                                                  WHERE pa.products_id = '" . $product['id'] . "'
                                                    and pa.options_id = '" . $option . "'
                                                    and pa.options_id = popt.products_options_id
                                                    and pa.options_values_id = '" . (int)$value . "'
                                                    and pa.options_values_id = poval.products_options_values_id
                                                    and popt.language_id = '" . $languages_id . "'
                                                    and poval.language_id = '" . $languages_id . "'");

                $attributes_values = tep_db_fetch_array($attributes);
                if ($value == PRODUCTS_OPTIONS_VALUE_TEXT_ID) {
                    $attr_value = $product['attributes_values'][$option]
                        .tep_draw_hidden_field('id[' . $product['id'] . '][' . TEXT_PREFIX . $option . ']',  $product['attributes_values'][$option]);;
                } else {
                    $attr_value = $attributes_values['products_options_values_name']
                        .tep_draw_hidden_field('id[' . $product['id'] . '][' . $option . ']', $value);
                }

                $product[$option]['products_options_name'] = $attributes_values['products_options_name'];
                $product[$option]['options_values_id'] = $value;
                $product[$option]['products_options_values_name'] = $attr_value;
                $product[$option]['options_values_price'] = $attributes_values['options_values_price'];
                $product[$option]['price_prefix'] = $attributes_values['price_prefix'];
            }
        }

        $products_name = '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product['id']) . '">' . $product['name'] . '</a>';
        $checkout_products_name = '<a class="cart_prod_name product-id-' . $product['id'] . '" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product['id']) . '">' . $product['name'] . '</a>';

        if (STOCK_CHECK == 'true') {
            $stock_check = tep_check_stock($product['id'], $product['quantity']);
            if (tep_not_null($stock_check)) {
                $any_out_of_stock = 1;
                $products_name .= $stock_check;
                $checkout_products_name .= $stock_check;
                $checkout_products_name = '<a class="cart_prod_name product-id-' . $product['id'] . '" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product['id']) . '">' . $product['name'] . $stock_check . '</a>';
            }
        }

        $attrChain = '';
        if (isset($product['attributes']) && is_array($product['attributes'])) {
            reset($product['attributes']);
            $products_name .= '<ul class="attributes_list">';
            foreach ($product['attributes'] as $option => $value) {
                $attrChain .= '__' . $option . '_' . $value;
                $products_name .= '<li><span>' . $product[$option]['products_options_name'] . '</span> : ' . $product[$option]['products_options_values_name'] . '</li>';
            }
            $products_name .= '</ul>';
        }
        $products_image = explode(';', $product['image']);
        if (isset($_GET['action']) && $_GET['action'] == 'update_product' && !in_array(TEMPLATE_NAME, $skipTemplates)) {
            $html[] = [
                'id' => $product['id'],
                'name' => $products_name,
                'checkout_name' => $checkout_products_name,
                'qty' => $product['quantity'],
                'string_id' =>$product['string_id'],
                'attr_chain' => $attrChain,
                'price_full' => $currencies->display_price($product['final_price'], tep_get_tax_rate($product['tax_class_id']), $product['quantity'])
            ];
        } else {
            $html[] = [
                'id' => $product['id'],
                'image' => $products_image[0],
                'string_id' =>$product['string_id'],
                'attr_chain' => $attrChain,
                'name' => $products_name,
                'price' => $currencies->display_price($product['final_price'], tep_get_tax_rate($product['tax_class_id'])),
                'qty' => $product['quantity'],
                'price_full' => $currencies->display_price($product['final_price'], tep_get_tax_rate($product['tax_class_id']), $product['quantity'])
            ];
        }
    }

    // Products, signed as *** are not enought...
    if ($any_out_of_stock == 1){
        if (STOCK_ALLOW_CHECKOUT == 'true') {
            $stock_text = '<div class="warning_mess">'.sprintf(OUT_OF_STOCK_CAN_CHECKOUT, STOCK_MARK_PRODUCT_OUT_OF_STOCK, STOCK_MARK_PRODUCT_OUT_OF_STOCK).'</div>';
        } else {
            $stock_text = '<div class="warning_mess">'.sprintf(OUT_OF_STOCK_CANT_CHECKOUT, STOCK_MARK_PRODUCT_OUT_OF_STOCK, STOCK_MARK_PRODUCT_OUT_OF_STOCK).'</div>';
            $disabled = ' style="pointer-events: none; opacity: 0.5;"';
        }
    }

    if (returnJson($html)) {
        $rez['prod'] = $html;
        $rez['total'] = TOTAL_CART . ': <b>' . $currencies->display_price($cart->show_total(), 0) . '</b>';
        $rez['stock_text'] = $stock_text;
        $rez['count'] = $cart->count_contents();
        echo json_encode($rez);
        exit();
    }
    $content = CONTENT_POPUP_CART;
    if (file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/' . $content . '.tpl.php')) {
        require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/' . $content . '.tpl.php');
    } else {
        require(DIR_WS_CONTENT . $content . '.tpl.php');
    }

    function returnJson($html)
    {
        global $skipTemplates;

        return isset($_GET['action']) && $_GET['action'] == 'update_product' &&
                count($html) > 0 && !in_array(TEMPLATE_NAME, $skipTemplates);
    }
?>
