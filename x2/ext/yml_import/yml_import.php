<?php
ini_set('max_execution_time', '3600');
ini_set('memory_limit', '2024M');
ini_set('display_errors', '1');
/*
if(!is_null($argv)){ // CRON
    //chdir('../../');
    $rootPath = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
    chdir($rootPath);
    //var_dump(getcwd());
    //var_dump($rootPath . '/includes/application_top.php');
	//die;
	require_once 'includes/application_top.php';
	//chdir($configParse['ADMIN_FOLDER']);
    //require_once $rootPath . '/includes/application_top.php';
    //require_once $rootPath . '/includes/languages/' . $language . '/new_importexport.php';
    $_GET['import_type'] = 'yml';
    $_GET['vendor'] = 'ukrservice';
    $_GET['upload_images'] = 'true';
    $_GET['truncate_table'] = 'false';
    $_GET['replace_name'] = 'true';
    $_GET['replace_price'] = 'true';
    $_GET['replace_quantity'] = 'true';
    $_GET['replace_images'] = 'true';
    $_GET['replace_attributes'] = 'true';
    $_GET['category_id'] = 0;
    $_GET['missing_offers'] = 'nothing';
    $_GET['markup_type_price0'] = 0;
    $_GET['markup_type_price2'] = 0;
    $_GET['markup_type_price3'] = 0;
    $_GET['markup_type_price4'] = 0;
    $_GET['markup_type_price5'] = 0;
    $_GET['link'] = 'https://shopua.salesdrive.me/export/yml/export.yml?publicKey=iAKT0e8CWoBSmwgAWx14Yks1THiKspjqdXmmNCMUyaw8oZZr9nXT33cCZDbdCOiNRqhS';
}
*/

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    chdir('../../');
    $configParse = parse_ini_file(".env");
    $rootPath = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
    chdir($configParse['ADMIN_FOLDER']);
    require_once $rootPath . '/' . $configParse['ADMIN_FOLDER'] . '/includes/application_top.php';
    require_once $rootPath . '/' . $configParse['ADMIN_FOLDER'] . '/includes/languages/' . $language . '/new_importexport.php';

    // if we are checking progress
    if ($_GET['checkImportProgress'] == true) {
        echo file_get_contents($rootPath . '/ext/yml_import/progress.txt');
        exit;
    }

    // if we went to stop process
    if ($_GET['stopImportProcess'] == true) {
        file_put_contents($rootPath . '/ext/yml_import/stop.txt', '1');
        exit;
    }

    if ($_GET['endImportProcess'] == true) {
        file_put_contents($rootPath . '/ext/yml_import/progress.txt', '{"productsProcessed":0,"imagesUploaded":0}');
        exit;
    }
} else {
    file_put_contents(__DIR__. '/stop.txt', '0');
//  file_put_contents($rootPath.'/ext/yml_import/progress.txt', '{"productsProcessed":0,"imagesUploaded":0}');
}


$checkProgress = json_decode(file_get_contents($rootPath . '/ext/yml_import/progress.txt'));
if ($checkProgress->productsProcessed == 100 and $checkProgress->imagesUploaded == 100) {
    file_put_contents($rootPath . '/ext/yml_import/progress.txt', '{"productsProcessed":0,"imagesUploaded":0}');
} elseif ($checkProgress->productsProcessed != 0 or $checkProgress->imagesUploaded != 0) {
    $continueProgress = true;
} else {
    $continueProgress = false;
}


require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$exportLog = '';
$allowed_image_types = ['image/jpeg', 'image/gif', 'image/png', 'image/webp'];
$allowed_table_types = ['text/xml', 'application/xml'];

if (!empty($_GET['vendor'])) {
    $vendor_template = $_GET['vendor'] ?? 'ukrservice';
    $modelCodeTag = $_GET['modelCodeTag'] ?: 'vendorCode';
    //import actions
    $upload_images = isTrueString($_GET['upload_images']);
    $truncate_table = isTrueString($_GET['truncate_table']);
    //options to replace
    $replace_name = isTrueString($_GET['replace_name']);
    $replace_price = isTrueString($_GET['replace_price']);
    $replace_quantity = isTrueString($_GET['replace_quantity']);
    $replace_image = isTrueString($_GET['replace_images']);
    $replace_attributes = isTrueString($_GET['replace_attributes']);
    //target category for import
    $main_category = !empty($_GET['category_id']) ? $_GET['category_id'] : 0;
    //action to missing offers
    $missing_offers = isset($_GET['missing_offers']) ? $_GET['missing_offers'] : 'nothing';
    //markup(in yml.php)
    //import target language
    $import_lang_id = $_GET['import_language'] ?: tep_get_languages_id(DEFAULT_LANGUAGE);
    //excel data
    $link = str_replace('*', '&', $_GET['link']);
    $urlData = download_price($link, $vendor_template);
    $file_path = $urlData['filepath'] ?? false;

    if (empty($urlData['success'])) {
        echo json_encode($urlData);
        exit;
    }

    require_once('../ext/yml_import/yml.php');
} else {
    if (empty($_REQUEST)) {
        //There is no data for import
        echo json_encode([
            'success' => false,
            'message' => 'Error. There is no data. Probably you need to increase value of post_max_size.'
        ]);
        exit;
    }
    if (!empty($_POST['excel_file_url']) || (tep_not_null($_FILES) && in_array($_FILES['excelfile']['type'], $allowed_table_types))) {
        $vendor_template = $_POST['vendor_template'] ?? 'ukrservice';
        $modelCodeTag = $_GET['modelCodeTag'] ?: 'vendorCode';
        //import actions
        $upload_images = $_POST['uploadImages'] ?? false;
        $truncate_table = $_POST['truncateTable'] ?? false;
        //options to replace
        $replace_name = $_POST['replaceName'] ?? false;
        $replace_price = $_POST['replacePrice'] ?? false;
        $replace_quantity = $_POST['replaceQuantity'] ?? false;
        $replace_image = $upload_images ? true : ($_POST['replaceImage'] ?? false);
        $replace_attributes = $_POST['replaceAttributes'] ?? false;
        //target category for import
        $main_category = !empty($_POST['main_category']) ? $_POST['main_category'] : 0;
        //action to missing offers
        $missing_offers = isset($_POST['missing_offers']) ? $_POST['missing_offers'] : 'nothing';
        //markup
        //import target language
        $import_lang_id = $_POST['import_language'] ?: tep_get_languages_id(DEFAULT_LANGUAGE);
        //excel data
        if (!empty($_POST['excel_file_url'])) {
            //data from link
            $link = str_replace('*', '&', $_POST['excel_file_url']);
            $urlData = download_price($link, $vendor_template);
            $file_path = $urlData['filepath'] ?? false;
        } else {
            //data from file
            $urlData['success'] = true;
            $file_path = $_FILES['excelfile']['tmp_name'];
        }

        if (empty($urlData['success'])) {
            echo json_encode($urlData);
            exit;
        }

        require_once('../ext/yml_import/yml.php');
        exit;
    }
}

function isTrueString($string)
{
    return (bool)($string === "true");
}

function download_price($url, $vendor_template)
{
    $file_path = DIR_FS_DOCUMENT_ROOT . 'temp/' . $vendor_template . '.xml';

    $result = [
        'filepath' => $file_path,
    ];
    if ($url) {
        $path = $file_path;
        //$url = $file_path;
/*        $path = $file_path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($ch);
        curl_close($ch);
*/
        $content = @file_get_contents($url);
        if ($content !== false) {
            //remove angle branches from an attribute of tag
            preg_match_all('~<([^/][^>]*?)>~', $content, $matches);
            foreach ($matches as $tags) {
                foreach ($tags as $tag) {
                    if (!is_array($tag)) {
                        $tag = trim($tag, '<>');
                        if (strpos($tag, '<') !== false || strpos($tag, '>') !== false) {
                            $newTag = str_replace(['<', '>'], ['&lt;', '&gt;'], $tag);
                            $content = str_replace($tag, $newTag, $content);
                        }
                    }
                }
            }

            file_put_contents($path, $content);
            if (file_exists($file_path) && filesize($file_path) > 0) {
                $result = array(
                    'success' => true,
                    'filepath' => $file_path,
                    'msg' => 'Downloaded successfully'
                );
            } else {
                $result = array(
                    'success' => false,
                    'filepath' => $file_path,
                    'msg' => 'File not in place'
                );
            }
        } else {
            $result = array(
                'success' => false,
                'filepath' => $file_path,
                'msg' => 'Wrong link'
            );
        }
        //echo json_encode($result);
    }
    return $result;
}

if ($current_page == FILENAME_NEW_IMPORT_EXPORT) { ?>
    <?php if (tep_not_null($exportLog)) { ?>
        <div id="progress"></div>
        <?= $exportLog ?>
    <?php } else { ?>
        <form id="upload_form" enctype="multipart/form-data" method="post">
            <?php if ($continueProgress) { ?>
                <input type="hidden" name="continueProgress" id="continueProgress" value="true"
                       data-products="<?php echo $checkProgress->productsProcessed; ?>"
                       data-images="<?php echo $checkProgress->imagesUploaded; ?>">
            <?php } ?>
            <div class="settings_block yml_import">
                <!--pre import actions-->
                <div class="import_settings_block">
                    <label>
                        <input type="checkbox" name="uploadImages" id="uploadImages">
                        <span><?php echo YML_IMPORT_UPLOAD_IMAGES; ?></span>
                    </label>
                    <label>
                        <input type="checkbox" name="truncateTable" id="truncateTable">
                        <span><?php echo YML_IMPORT_TRUNCATE_TABLE; ?></span>
                    </label>
                </div>
                <!--options to replace-->
                <div class="import_settings_block">
                    <span style="width:100%"><?php echo YML_IMPORT_REPLACE_TEXT; ?></span>
                    <label>
                        <input type="checkbox" checked name="replaceName" id="replaceName">
                        <span><?php echo YML_IMPORT_REPLACE_NAME; ?></span>
                    </label>
                    <label>
                        <input type="checkbox" checked name="replacePrice" id="replacePrice">
                        <span><?php echo YML_IMPORT_REPLACE_PRICE; ?></span>
                    </label>
                    <label>
                        <input type="checkbox" checked name="replaceQuantity" id="replaceQuantity">
                        <span><?php echo YML_IMPORT_REPLACE_QUANTITY; ?></span>
                    </label>
                    <label>
                        <input type="checkbox" checked name="replaceImage" id="replaceImage">
                        <span><?php echo YML_IMPORT_REPLACE_IMAGE; ?></span>
                    </label>
                    <label>
                        <input type="checkbox" checked name="replaceAttributes" id="replaceAttributes">
                        <span><?php echo YML_IMPORT_REPLACE_ATTRIBUTES; ?></span>
                    </label>
                </div>
                <!--target category for import-->
                <div class="import_settings_block">
                    <div style="width: 100%;">
                        <label><span><?php echo YML_IMPORT_MAIN_CATEGORY; ?></span></label>
                        <?php echo tep_draw_pull_down_categories('main_category', $tep_get_category_tree) ?>
                    </div>
                </div>
                <!--action to missing offers-->
                <div class="import_settings_block">
                    <div style="width: 100%;">
                        <label><span><?php echo YML_IMPORT_MISSING_OFFERS; ?></span></label>
                        <?php
                        $missing_offers_options = [
                            ['id' => 'nothing', 'text' => YML_IMPORT_MISSING_OFFERS_TEXT_NOTHING],
                            ['id' => 'turnoff', 'text' => YML_IMPORT_MISSING_OFFERS_TEXT_TURN_OFF],
                            ['id' => 'delete', 'text' => YML_IMPORT_MISSING_OFFERS_TEXT_DELETE],
                        ];
                        ?>
                        <?php echo tep_draw_pull_down_menu('missing_offers', $missing_offers_options) ?>
                    </div>
                </div>
                <!--markup-->
                <div class="import_settings_block">
                    <div style="width: 100%;">
                        <label><span><?php echo YML_IMPORT_MARKUP; ?></span></label>
                        <div class="price_markup">
                            <font><?php echo YML_IMPORT_REPLACE_PRICE; ?></font>
                            <?php echo tep_draw_pull_down_menu('markup_type_price0', array(['id' => '0', 'text' => YML_IMPORT_NUMBER], ['id' => '1', 'text' => '%'])) ?>
                            <?php echo tep_draw_input_field('markup_value_price0', '', 'class="form-control"'); ?>
                        </div>
                        <?php
                        $prices_num = tep_xppp_getpricesnum();
                        if ($prices_num > 1) {
                            for ($i = 2; $i <= $prices_num; $i++) { ?>
                                <div class="price_markup">
                                    <font><?php echo YML_IMPORT_REPLACE_PRICE . ' ' . $i; ?></font>
                                    <?php echo tep_draw_pull_down_menu('markup_type_price' . $i, array(['id' => '0', 'text' => YML_IMPORT_NUMBER], ['id' => '1', 'text' => '%'])) ?>
                                    <?php echo tep_draw_input_field('markup_value_price' . $i, '', 'class="form-control"'); ?>
                                </div>
                            <?php }
                        }
                        ?>
                    </div>
                </div>
                <!--import target language-->
                <div class="import_settings_block">
                    <?php if (count(tep_get_languages()) > 1) { ?>
                        <div style="width: 100%;">
                            <label><span><?php echo YML_IMPORT_LANGUAGE; ?></span></label>
                            <?php
                            $import_languages = [];
                            foreach (tep_get_languages() as $lang_data) {
                                $import_languages[] = [
                                    'id' => $lang_data['id'],
                                    'text' => $lang_data['name'],
                                ];
                            }
                            echo tep_draw_pull_down_menu('import_language', $import_languages) ?>
                        </div>
                    <?php } ?>
                </div>
                <!--excel file url-->
                <div class="import_settings_block">
                    <label><span><?php echo YML_IMPORT_UPLOAD_FILE_LINK; ?></span></label>
                    <?php echo tep_draw_input_field('excel_file_url', '', 'class="form-control"'); ?>
                </div>
                <!--automatic import link-->
                <div class="import_settings_block">
                    <div style="display: flex;align-items: center;">
                        <span><?php echo YML_IMPORT_AUTOMATIC_UPLOAD_LINK; ?></span>
                        <span id="automatic_import_url" data-origin-url="<?php echo tep_href_link(FILENAME_NEW_IMPORT_EXPORT); ?>" data-params="" style="overflow-wrap: anywhere;display: none;"></span>
                        <!--link icon-->
                        <svg id="copy_automatic_import_url" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"/>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="actions">
                <label>
                    <input type="file" class="load_file" name="excelfile" id="excelfile" accept=".xml">
                    <span id="replacement_block">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                <g fill="none" fill-rule="evenodd">
                                    <g fill="#1283E4" fill-rule="nonzero">
                                        <g>
                                            <g>
                                                <g>
                                                    <path fill-opacity=".4" d="M0 15.5v-1c0-.276.224-.5.5-.5h15c.276 0 .5.224.5.5v1c0 .276-.224.5-.5.5H.5c-.276 0-.5-.224-.5-.5z"
                                                          transform="translate(-1136 -110) translate(430 100) translate(650) translate(56 10)"/>
                                                    <path d="M9.147.75v7.251h3.137c.637 0 .956.672.505 1.066l-4.297 3.757c-.27.235-.707.235-.977 0L3.211 9.067c-.45-.394-.132-1.066.505-1.066h3.14V.751c0-.2.09-.391.252-.532.16-.14.38-.22.608-.219h.572c.228 0 .447.078.608.22.16.14.251.331.25.53z"
                                                          transform="translate(-1136 -110) translate(430 100) translate(650) translate(56 10)"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <?php echo YML_IMPORT_UPLOAD_FILE; ?>
                        </span>
                </label>
                <button type="submit" id="upload_button" class="btn btn-info green_btn"><?php echo YML_IMPORT_UPLOAD_BUTTON; ?></button>
            </div>
            <div class="progresses">
                <div class="error_block clearfix"></div>
                <div class="progress_upload_block clearfix">
                    <div class="progress_left">File upload progress</div>
                    <div id="progress_upload">
                        <div class="bar"></div>
                    </div>
                </div>
                <div class="progress_products_block clearfix">
                    <div class="progress_left">Products processing progress</div>
                    <div id="progress_products">
                        <div class="checkImportProgress"></div>
                    </div>
                </div>
                <div class="progress_images_block clearfix">
                    <div class="progress_left">Images Uploading progress</div>
                    <div id="progress_images">
                        <div class="checkImportProgress"></div>
                    </div>
                </div>

            </div>
        </form>
    <?php } ?>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("#continueProgress").val()) {
            $('.progresses').fadeIn();
            $('.progress_products_block').fadeIn();
            $('#progress_upload .bar').css('width', '100%');
            $('#progress_products .checkImportProgress').css('width', ($("#continueProgress").attr('data-products')) + '%');
            $('#progress_images .checkImportProgress').css('width', ($("#continueProgress").attr('data-images')) + '%');
            timer = window.setInterval(refreshProgress, 1000);
            $('#replacement_block').fadeOut(100);
            $('#upload_button').fadeOut(0).after('<div class="btn btn-info green_btn abortProcess">ABORT</div>');
        }
    });

    $("#upload_button").click(function () {
        event.preventDefault();

        $('.checkImportProgress').css('width', '0%');
        $('#progress_upload .bar').css('width', '0%');
        $('.progresses').fadeIn();

        var form = document.querySelector("#upload_form");
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "../ext/yml_import/yml_import.php",
            data: formData,
            dataType: "json",
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.addEventListener('progress', function (ev) {
                    $('#progress_upload .bar').css('width', (ev.loaded / (ev.total / 100)) + '%');
                }, false);
                xhr.upload.addEventListener('load', function (ev) {
                    $('.progress_products_block').fadeIn();
                    timer = window.setInterval(refreshProgress, 1000);
                    $('#replacement_block').fadeOut(100);
                    $('#upload_button').fadeOut(0).after('<div class="btn btn-info green_btn abortProcess">ABORT</div>');
                }, false);

                return xhr;
            },
            beforeStart: function () {
                $('#progress_upload .bar').css('width', '0%');
            },
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data) {
                if (typeof data == 'object' && !data.success) {
                    $('.progresses .error_block').prepend(data.message);
                }
                // $("#progress_products .checkImportProgress").html(data+' products updated');
            }
        });
    });

    var timer;

    function refreshProgress() {
        $.ajax({
            url: "../ext/yml_import/yml_import.php?checkImportProgress=true",
            dataType: 'json',
            success: function (data) {
                $('#progress_products .checkImportProgress').css('width', (data.productsProcessed) + '%');

                // If the process is completed, we should stop the checking process.
                if (data.productsProcessed == 100) {

                    if ($(".progress_images_block").is(":hidden")) $('.progress_images_block').fadeIn();
                    $('#progress_images .checkImportProgress').css('width', (data.imagesUploaded) + '%');
                    if (data.imagesUploaded == 100) {
                        window.clearInterval(timer);
                        timer = window.setInterval(completed, 500);
                    }
                }
            }
        });
    }

    function completed() {
        window.clearInterval(timer);
        $('.abortProcess').fadeOut(0);
        $('#upload_button').fadeIn(0);
        $('#replacement_block').fadeIn(100);

        // change counters to 0:
        $.ajax({
            url: "../ext/yml_import/yml_import.php?endImportProcess=true",
        });
    }

    jQuery('body').on('click', '.abortProcess', function (event) {
        $.ajax({
            url: "../ext/yml_import/yml_import.php?stopImportProcess=true",
            success: function (data) {
                $('.progresses').fadeOut(0);
                $('.checkImportProgress').css('width', '0%');
                $('#progress_upload .bar').css('width', '0%');
                completed();
            }
        });
    });

    $(document).ready(function () {
        //get data
        var dataList = $('#automatic_import_url').data('params');
        var data = dataList !== '' ? JSON.parse(dataList) : {};
        //collect each formatted data
        $('.yml_import input, .yml_import select').each(function () {
            data = formatData($(this), data);
        });
        //save data
        $('#automatic_import_url').data('params', JSON.stringify(data));
        //format and draw automatic url
        drawAutomaticUrl(data);
        //enable/disable automatic import link
        changeAutoImportLinkAction();
    });

    $(document).on('change', '.yml_import input, .yml_import select', function () {
        //get data
        var dataList = $('#automatic_import_url').data('params');
        var data = dataList !== '' ? JSON.parse(dataList) : {};
        //collect formatted data
        data = formatData($(this), data);
        //save data
        $('#automatic_import_url').data('params', JSON.stringify(data));
        //format and draw automatic url
        drawAutomaticUrl(data);
        //enable/disable automatic import link
        changeAutoImportLinkAction();
    });

    $(document).on('click', '#copy_automatic_import_url', function () {
        if ($('[name="excel_file_url"]').val().length > 0) {
            copyToClipboard($("#automatic_import_url"));
            show_tooltip("<?=COPY_TEXT?>", 1000);
        } else {
            $('[name="excel_file_url"]').addClass('error_import_option_field');
        }
    });

    function changeAutoImportLinkAction() {
        if ($('[name="excel_file_url"]').val().length > 0) {
            $('#copy_automatic_import_url').addClass('disabled_copy');
            $('[name="excel_file_url"]').removeClass('error_import_option_field');
        } else {
            $('#copy_automatic_import_url').removeClass('disabled_copy');
        }
    }

    function formatData($this, data) {
        if ($this.val() && $this.val().length > 0) {
            data['import_type'] = 'yml';
            data['vendor'] = 'ukrservice';
            switch ($this.attr('name')) {
                //import actions
                case 'uploadImages':
                    data['upload_images'] = $this.is(':checked');
                    break;
                case 'truncateTable':
                    data['truncate_table'] = $this.is(':checked');
                    break;
                //options to replace
                case 'replaceName':
                    data['replace_name'] = $this.is(':checked');
                    break;
                case 'replacePrice':
                    data['replace_price'] = $this.is(':checked');
                    break;
                case 'replaceQuantity':
                    data['replace_quantity'] = $this.is(':checked');
                    break;
                case 'replaceImage':
                    data['replace_images'] = $this.is(':checked');
                    break;
                case 'replaceAttributes':
                    data['replace_attributes'] = $this.is(':checked');
                    break;
                //target category for import
                case 'main_category':
                    data['category_id'] = $this.val();
                    break;
                //action to missing offers
                case 'missing_offers':
                    data['missing_offers'] = $this.val();
                    break;
                //import target language
                case 'import_language':
                    data['import_language'] = $this.val();
                    break;
                //excel file url
                case 'excel_file_url':
                    data['link'] = $this.val();
                    break;
                default:
                    //markup
                    if ($this.attr('name').includes('markup_type_price') || $this.attr('name').includes('markup_value_price')) {
                        data[$this.attr('name')] = $this.val();
                    }
            }
        }
        return data;
    }

    function drawAutomaticUrl(data) {
        //format url
        var automatic_url = $('#automatic_import_url').data('origin-url') + '?';
        var parts = [];
        $.each(data, function (param, value) {
            parts.push(param + '=' + value);
        });
        automatic_url += parts.join('&');

        //draw url
        $('#automatic_import_url').text(automatic_url);
    }

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>
<style>
    .progresses {
        display: none;
        padding: 20px;
    }

    .error_block {
        background-color: red;
        color: white;
        padding: 2px;
    }

    .checkImportProgress {
        background: #FFD700;
        height: 20px;
        color: #fff;
        padding: 2px;
        transition: 1s linear;
        text-align: left;
    }

    #progress_images .checkImportProgress {
        background: #eee;
    }

    .progress_upload_block {
        color: #fff;
    }

    #progress_upload .bar {
        background: #0057B7;
        width: 0%;
        transition: 1s linear;
        height: 20px;
    }

    .progress_products_block, .progress_images_block {
        display: none;
    }

    .progress_left {
        float: left;
        width: 200px;
        padding: 3px;
    }

    .abortProcess {
        background-color: red !important;
    }

    .price_markup {
        display: inline-block;
        margin-bottom: 10px;
        height: 35px;
    }

    .price_markup font {
        width: 39px;
        display: inline-block;
    }

    .price_markup select {
        width: 75px;
        display: inline;
        margin-right: -4px;
        padding: 5px;
    }

    .price_markup input[type=text] {
        width: 50px;
        display: inline;
        padding: 5px;
    }

    #copy_automatic_import_url {
        cursor: pointer;
    }

    /*reset general import styles*/
    .importexport .settings_block.yml_import span {
        margin: 5px 0 5px 0;
        color: #313132;
    }

    .importexport .settings_block.yml_import svg {
        height: 18px;
    }
    /*reset general import styles END*/
</style>

